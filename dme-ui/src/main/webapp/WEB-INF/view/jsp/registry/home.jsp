<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<tiles:importAttribute name="fluidLayout" />

<div class="container<c:if test="${fluidLayout==true}">-fluid</c:if>">
	<div class="row">
	    <div class="col-12 col-xl-6">
	    	<h1><s:message code="~de.unibamberg.minf.dme.model.schema.title" /></h1>
		    <div id="model-table-container" class="col-12">
			    <div class="row">
			    	<div class="col-sm-12 col-md-8">
			    		<div class="data-table-count float-left">
							<label><s:message code="~de.unibamberg.minf.common.labels.show" />:
								<select class="custom-select custom-select-sm form-control form-control-sm" aria-controls="model-table">
								  <option>10</option>
								  <option>25</option>
								  <option>50</option>
								  <option>100</option>
								  <option><s:message code="~de.unibamberg.minf.common.link.all" /></option>
								</select>
							</label>
						</div>	
			    		<div class="data-table-filter float-left">
			    			<label><s:message code="~de.unibamberg.minf.common.link.filter" />: 
								<input type="text" class="form-control form-control-sm" aria-controls="model-table">
							</label>
						</div>
	   				</div>
	   				<div class="col-sm-12 col-md-4">
	   					<div style="text-align: right;">
		   					<button id="btn-add-model" class="btn btn-primary">
								<i class="fas fa-plus-circle"></i> <s:message code="~de.unibamberg.minf.dme.button.add_schema" />
							</button>
	   					</div>
	   				</div>
	 			</div>
	 			<div class="row">
	 				<div class="col-sm-12">
					    <table id="model-table" class="table table-striped table-bordered table-hovering" style="width:100%" role="grid">
					    	<thead>
								<tr>
									<th></th> <!-- Status -->
									<th><s:message code="~de.unibamberg.minf.dme.model.schema.label" /></th>
								</tr>
							</thead>
							<tbody>
							<tr>
								<td colspan="2" align="center"><s:message code="~de.unibamberg.minf.common.view.no_data_fetched_yet" /></td>
							</tr>
							</tbody>
					    </table>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="col-12 col-xl-6">
	    	<h1><s:message code="~de.unibamberg.minf.dme.model.mapping.title" /></h1>
		    <div id="mapping-table-container" class="col-12">
			    <div class="row">
			    	<div class="col-sm-12 col-md-8">
			    		<div class="data-table-count float-left">
							<label><s:message code="~de.unibamberg.minf.common.labels.show" />: 
								<select class="custom-select custom-select-sm form-control form-control-sm" aria-controls="mapping-table">
								  <option>10</option>
								  <option>25</option>
								  <option>50</option>
								  <option>100</option>
								  <option><s:message code="~de.unibamberg.minf.common.link.all" /></option>
								</select>
							</label>
						</div>	
			    		<div class="data-table-filter float-left">
			    			<label><s:message code="~de.unibamberg.minf.common.link.filter" />: 
								<input type="text" class="form-control form-control-sm" aria-controls="mapping-table">
							</label>
						</div>
	   				</div>
	   				<div class="col-sm-12 col-md-4">
	   					<div style="text-align: right;">
		   					<button id="btn-add-mapping" class="btn btn-primary">
								<i class="fas fa-plus-circle"></i> <s:message code="~de.unibamberg.minf.dme.button.add_mapping" />
							</button>
	   					</div>
	   				</div>
	 			</div>
	 			<div class="row">
	 				<div class="col-sm-12">
					    <table id="mapping-table" class="table table-striped table-bordered table-hovering" style="width:100%" role="grid">
					    	<thead>
								<tr>
									<th></th> <!-- Status -->
									<th><s:message code="~de.unibamberg.minf.dme.model.mapping.source" /></th>
									<th><s:message code="~de.unibamberg.minf.dme.model.mapping.target" /></th>
								</tr>
							</thead>
							<tbody>
							<tr>
								<td colspan="3" align="center"><s:message code="~de.unibamberg.minf.common.view.no_data_fetched_yet" /></td>
							</tr>
							</tbody>
					    </table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>