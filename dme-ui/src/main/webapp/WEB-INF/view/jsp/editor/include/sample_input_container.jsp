<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<c:set var="currentSampleCount" value="${sampleOutput==null ? 0 : fn:length(sampleOutput)}"/>
<input type="hidden" id="currentSampleCount" value="${currentSampleCount}">
<input type="hidden" id="currentTransformedCount" value="${session.sampleMappedCount}">
<input type="hidden" id="currentSampleIndex" value="${session.selectedOutputIndex==null ? 0 : session.selectedOutputIndex}">

<div class="d-flex flex-column h-100">
	
	<c:set var="sampleInputSet" value="${session.sampleFile!=null}" />
	<c:set var="sampleInputDisplayed" value="${inputSet && sampleInputOversize==false}" />
	<div class="sample-loading-indicator hide flex-fill h-100 text-center mt-3"><i class="fas fa-cog fa-spin fa-2x" style="color: var(--primary-var-2);"></i></div>
	<form id="sample-input-textarea-container" class="flex-fill d-flex flex-column h-100">
		<input type="hidden" id="sample-set" value="${sampleInputSet}">
		
		<div class="editor-content-container sample-input-optionbar d-flex">
			<div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="inputType" id="inputTypeXml" value="XML" ${session.sampleFile==null || session.sampleFile.type=='XML' ? 'checked' : ''}>
					<label class="form-check-label" for="inputTypeXml">XML</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="inputType" id="inputTypeJson" value="JSON" ${session.sampleFile!=null && session.sampleFile.type=='JSON' ? 'checked' : ''}>
					<label class="form-check-label" for="inputTypeJson">JSON</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="inputType" id="inputTypeJson" value="YAML" ${session.sampleFile!=null && session.sampleFile.type=='YAML' ? 'checked' : ''}>
					<label class="form-check-label" for="inputTypeJson">YAML</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="inputType" id="inputTypeCsv" value="CSV" ${session.sampleFile!=null && session.sampleFile.type=='CSV' ? 'checked' : ''}>
					<label class="form-check-label" for="inputTypeCsv">CSV</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="inputType" id="inputTypeTsv" value="TSV" ${session.sampleFile!=null && session.sampleFile.type=='TSV' ? 'checked' : ''}>
					<label class="form-check-label" for="inputTypeTsv">TSV</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="inputType" id="inputTypeText" value="TEXT" ${session.sampleFile!=null && session.sampleFile.type=='TEXT' ? 'checked' : ''}>
					<label class="form-check-label" for="inputTypeText">Text</label>
				</div>
			</div>
			<div class="flex-grow-1">
				<c:set var="currentContext" value="input" />
				<%@ include file="sample_buttons.jsp" %>
			</div>
		</div>
		
		<div class="editor-content-container flex-fill h-100">			
			<div id="sample-input-textarea-placeholder" onclick="editor.sampleHandler.handleEnterTextarea(); return false;" class="sample-codearea h-100<c:if test="${sampleInputDisplayed}"> hide</c:if>">
				<p class="placeholder-no-sample form-control-plaintext<c:if test="${sampleInputSet}"> hide</c:if>"><s:message code="~de.unibamberg.minf.dme.editor.sample.placeholder" /></p>
				
				<c:set var="showlink"><button class="btn btn-inline" onclick="editor.sampleHandler.loadSampleInput(); return false;"><i class="fa fa-eye" aria-hidden="true"></i> <s:message code="~de.unibamberg.minf.dme.editor.sample.placeholder_set.show" /></button></c:set>
				<c:set var="downlink"><button class="btn btn-inline" onclick="editor.sampleHandler.downloadSampleInput(); return false;"><i class="fa fa-download" aria-hidden="true"></i> <s:message code="~de.unibamberg.minf.dme.editor.sample.placeholder_set.download" /></button></c:set>
				<p class="placeholder-sample form-control-plaintext<c:if test="${!sampleInputSet}"> hide</c:if>"><s:message code="~de.unibamberg.minf.dme.editor.sample.placeholder_set" arguments="${showlink}, ${downlink}" /></p>										
			</div>
			<c:choose>
				<c:when test="${sampleInputDisplayed}">
					<textarea id="sample-input-textarea" class="sample-textarea form-control h-100" rows="3">${session.sampleFile}</textarea>
				</c:when>
				<c:otherwise>
					<textarea id="sample-input-textarea" class="sample-textarea form-control h-100 hide" rows="3"></textarea>
				</c:otherwise>
			</c:choose>
		</div>		
	</form>
</div>	