<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div id='schema-properties-container' class='editor-properties editor-content-container' >
	<div id='editor-entity-properties'>
		<h3><s:message code="~de.unibamberg.minf.dme.model.datamodel" /></h3>
		<div class="editor-property">
			<label><s:message code="~de.unibamberg.minf.common.model.label" />:</label>
			<span>${datamodel.pojo.name}</span>
		</div>
		<div class="editor-property">
			<label><s:message code="~de.unibamberg.minf.common.model.id" />:</label>
			<span>${datamodel.pojo.id}</span>
		</div>
		<div class="editor-property">
			<label><s:message code="~de.unibamberg.minf.common.model.readonly" />:</label>
			<span>
				<c:choose>
					<c:when test="${datamodel.readOnly}">
						<i class="fas fa-check"></i>
				    </c:when>
					<c:otherwise>-</c:otherwise>
				</c:choose>	
			</span>
		</div>
		<div class="editor-property">
			<label><s:message code="~de.unibamberg.minf.dme.model.datamodel.natures" />:</label>
			<span>
				<c:choose>
					<c:when test="${fn:length(datamodel.pojo.natures)>0}">
					  <c:forEach items="${datamodel.pojo.natures}" var="nature" varStatus="status">
					  	<s:message code="~${nature['class'].name}.display_label" />${status.index<(fn:length(datamodel.pojo.natures)-1) ? ', ' : ''}
					  </c:forEach>
				    </c:when>
					<c:otherwise>-</c:otherwise>
				</c:choose>	
			</span>
		</div>
		<div class="editor-property">
			<label><s:message code="~de.unibamberg.minf.dme.model.schema.description" />:</label>
			<span><c:out value="${fn:length(datamodel.pojo.description)>0 ? datamodel.pojo.description : '-'}" /></span>
		</div>
	</div>
	<div id='editor-element-properties'></div>
</div>