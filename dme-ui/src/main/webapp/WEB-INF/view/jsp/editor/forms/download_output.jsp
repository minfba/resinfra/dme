<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div id="download-output-modal">
	<div class="form-header">
		<h2 id="form-header-title"><s:message code="~de.unibamberg.minf.dme.editor.sample.download.title" /></h2>
	</div>
	<div class="form-content">
		<div class="form-group row">
			<label class="col-4 col-form-label" for="data_model"><s:message code="~de.unibamberg.minf.dme.model.datamodel" /></label>
			<div class="col-8">
				<c:choose>
					<c:when test="${targetModel!=null}">
						<div class="radio">
							<label>
						    	<input type="radio" name="download-model-radios" id="download-data-radio1" value="source" checked>
						    	<s:message code="~de.unibamberg.minf.dme.model.datamodel.source_model" />: ${sourceModel}
						  	</label>
						</div>
						<div class="radio">
							<label>
						    	<input type="radio" name="download-model-radios" id="download-data-radio2" value="target">
						    	<s:message code="~de.unibamberg.minf.dme.model.datamodel.target_model" />: ${targetModel}
							</label>
						</div>
					</c:when>
					<c:otherwise>
						<label class="col-form-label">
						    ${sourceModel}
						</label>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		
		<c:choose>
			<c:when test="${datasetCount>0}">
				<div class="form-group row">
					<label class="col-4 col-form-label" for="data_model"><s:message code="~de.unibamberg.minf.dme.editor.sample.download.set" />:</label>
					<div class="col-8">
						<div class="form-check">
							<input class="form-check-input" type="radio" name="download-data-radios" id="download-data-radio1" value="single" checked>
							<label class="form-check-label">
						    	<s:message code="~de.unibamberg.minf.dme.editor.sample.download.set.single" />: 
						    	<button class="btn btn-inline btn-sample-prev-resource <c:if test="${datasetCurrent==0}"> disabled</c:if>"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>	
						    	<span class="sample-output-counter">${datasetCurrent+1} / ${datasetCount}</span>
						    	<button class="btn btn-inline btn-sample-next-resource <c:if test="${datasetCurrent==datasetCount-1}"> disabled</c:if>"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>	
						  	</label>
						</div>
						<c:if test="${datasetCount>0}">
							<div class="form-check">
								<input class="form-check-input" type="radio" name="download-data-radios" id="download-data-radio2" value="all">
							    <label class="form-check-label"><s:message code="~de.unibamberg.minf.dme.editor.sample.download.set.all" arguments="${datasetCount}" /></label>
							</div>
						</c:if>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-4 col-form-label" for="data_model"><s:message code="~de.unibamberg.minf.dme.editor.sample.download.format" />:</label>
					<div class="col-8">
						<div class="form-check">
							<input class="form-check-input" type="radio" name="download-format-radios" id="download-format-radio1" value="xml" checked>
							<label class="form-check-label">XML</label>
						</div>
						<div class="form-check">
						    <input class="form-check-input" type="radio" name="download-format-radios" id="download-format-radio2" value="json">
							<label class="form-check-label">JSON</label>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="offset-4 col-8">
						<button onclick="editor.sampleHandler.createDownload();" class="btn btn-primary"><s:message code="~de.unibamberg.minf.common.link.create_download_link" /></button>
					</div>
					<div id="download-link-container" class="offset-4 col-8 pt-3"></div>
				</div>
			</c:when>
			<c:otherwise>
				<div class="alert alert-warning"><strong><s:message code="~de.unibamberg.minf.dme.editor.sample.download.no_data.head" /></strong> <s:message code="~de.unibamberg.minf.dme.editor.sample.download.no_data.body" /></div>
			</c:otherwise>
		</c:choose>
	
		
	</div>
	<div class="form-footer control-group">
		<div class="controls">
			<button class="btn btn-default cancel form-btn-cancel" type="reset"><s:message code="~de.unibamberg.minf.common.link.close" /></button>
		</div>
	</div>
</div>
