<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div id="grammar-validation-container">
	<div class="form-header">
		<h1 class="form-header-title"><s:message code="~de.unibamberg.minf.dme.form.grammar.process.title" /></h1>	
	</div>
	<div class="form-content">
		<div id="grammar-uploading" class="grammar-processing">	
			<h2 data-toggle="collapse" data-target="#grammar-uploading-collapse" aria-expanded="false" aria-controls="grammar-uploading-collapse"> 
				<i class="fas fa-cog fa-spin grammar-loading hide"></i>
				<i class="far fa-clock grammar-waiting"></i>
				<i class="fas fa-check grammar-ok hide"></i>
				<i class="fas fa-exclamation-triangle grammar-error hide"></i>
				<s:message code="~de.unibamberg.minf.dme.form.grammar.process.uploading" />
			</h2>
			<div class="collapse" id="grammar-uploading-collapse">
				<code class="card card-body">
				</code>
			</div>
		</div>
		<div id="grammar-parsing" class="grammar-processing">	
			<h2 data-toggle="collapse" data-target="#grammar-parsing-collapse" aria-expanded="false" aria-controls="grammar-parsing-collapse"> 
				<i class="fas fa-cog fa-spin grammar-loading hide"></i>
				<i class="far fa-clock grammar-waiting"></i>
				<i class="fas fa-check grammar-ok hide"></i>
				<i class="fas fa-exclamation-triangle grammar-error hide"></i>
				<s:message code="~de.unibamberg.minf.dme.form.grammar.process.java_creation" />
			</h2>
			<div class="collapse" id="grammar-parsing-collapse">
				<code class="card card-body">
				</code>
			</div>
		</div>
		<div id="grammar-compiling" class="grammar-processing">	
			<h2 data-toggle="collapse" data-target="#grammar-compiling-collapse" aria-expanded="false" aria-controls="grammar-compiling-collapse"> 
				<i class="fas fa-cog fa-spin grammar-loading hide"></i>
				<i class="far fa-clock grammar-waiting"></i>
				<i class="fas fa-check grammar-ok hide"></i>
				<i class="fas fa-exclamation-triangle grammar-error hide"></i>
				<s:message code="~de.unibamberg.minf.dme.form.grammar.process.compiling" />
			</h2>
			<div class="collapse" id="grammar-compiling-collapse">
				<code class="card card-body">
				</code>
			</div>
		</div>
		<div id="grammar-sandboxing" class="grammar-processing">	
			<h2 data-toggle="collapse" data-target="#grammar-sandboxing-collapse" aria-expanded="false" aria-controls="grammar-sandboxing-collapse">
				<i class="fas fa-cog fa-spin grammar-loading hide"></i>
				<i class="far fa-clock grammar-waiting"></i>
				<i class="fas fa-check grammar-ok hide"></i>
				<i class="fas fa-exclamation-triangle grammar-error hide"></i>
				<s:message code="~de.unibamberg.minf.dme.form.grammar.process.sandboxing" />
			</h2>
			<div class="collapse" id="grammar-sandboxing-collapse">
				<code class="card card-body">
				</code>
			</div>
		</div>
	</div>
	<div class="form-footer">
		<div class="controls">
			<button class="btn btn-default btn-sm cancel form-btn-cancel" type="reset"><s:message code="~de.unibamberg.minf.common.link.close" /></button>
		</div>
	</div>
</div>
