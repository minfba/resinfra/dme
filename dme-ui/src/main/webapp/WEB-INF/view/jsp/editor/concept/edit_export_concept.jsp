<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>


<s:url value="${actionPath}" var="saveUrl" />
<sf:form method="POST" action="${saveUrl}" modelAttribute="concept" >
	<div class="form-header">
		<h2 class="form-header-title"><s:message code="~de.unibamberg.minf.dme.form.concept.edit" /></h2>
		<input type="hidden" id="export-availableNatures" value="${natures}" />
	</div>
	<div class="form-content">
		<h3><strong>1)</strong> <s:message code="~de.unibamberg.minf.dme.model.export_concept.properties" /></h3>
		<div class="form-group row">
			<label class="col-form-label col-3" for="mapping_source"><s:message code="~de.unibamberg.minf.dme.model.export_concept.format" /></label>
			<div class="col-9">
				<sf:select path="format" cssClass="form-control" items="${formats}" />
				<sf:errors path="format" cssClass="error" />
			</div>
		</div>
		<div class="form-group row">
			<label class="col-form-label col-3" for="mapping_source"><s:message code="~de.unibamberg.minf.common.view.options" /></label>
			<div class="col-9">
				<div class="form-check">
				  <sf:checkbox path="includeTree" />&nbsp;
				  <label class="form-check-label" for="includeTree"><s:message code="~de.unibamberg.minf.dme.model.export_concept.option.include_subtree" /></label>
				</div>

				<div class="form-check">
				  <sf:checkbox path="escape" />&nbsp;
				  <label class="form-check-label" for="escape"><s:message code="~de.unibamberg.minf.dme.model.export_concept.option.escape" /></label>
				</div>
				
				<div class="form-check">
				 <sf:checkbox path="includeSelf" />&nbsp;
				  <label class="form-check-label" for="includeSelf"><s:message code="~de.unibamberg.minf.dme.model.export_concept.option.include_self" /></label>
				</div>
				
				<div class="form-check">
				  <sf:checkbox path="useTerminalsIfAvailable" />&nbsp;
				  <label class="form-check-label" for="useTerminalsIfAvailable1"><s:message code="~de.unibamberg.minf.dme.model.export_concept.option.use_terminals" /></label>
				</div>
				
			</div>
		</div>
		
		<div class="form-group mt-4">
			<h3><strong>2)</strong> <s:message code="~de.unibamberg.minf.dme.editor.export.execute" /></h3>
	
			<div class="sample-input-container">
				<input type="hidden" name="export-sample-available" value='<c:out value="${sampleAvailable}" />'/>
				<input type="hidden" name="export-sample-oversize" value="${sampleOversize}" />
				<pre id="export-sample" class="hide">${sample}</pre>
				<div id="export-sample-rendered" class="sample-output sample-codearea hide" style="max-height: 150px;"></div>
			</div>
			<div class="clearfix">
				<button class="btn-parse-sample btn btn-warning float-right hide" onclick="editor.conceptEditor.performExport(); return false;"><span class="glyphicon glyphicon-play" aria-hidden="true"></span> <s:message code="~de.unibamberg.minf.dme.editor.sample.process_input" /></button>
			</div>
		</div>

		<div id="export-result-outer-container" class="hide">
			<h3><strong>3)</strong> <s:message code="~de.unibamberg.minf.dme.editor.export.result" /></h3>
			<div class="transformation-result-container">
				<div class="transformation-alerts">
					<c:choose>
						<c:when test="${grammar.error}">
							<div class="alert alert-sm alert-warning"><s:message code="~de.unibamberg.minf.dme.editor.sample.error.error_in_grammar" /></div>
						</c:when>
						<c:otherwise>
							<div class="alert alert-sm alert-info"><s:message code="~de.unibamberg.minf.dme.editor.sample.notice.hint_sample" /></div>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="no-results-alert alert alert-sm alert-warning hide"><s:message code="~de.unibamberg.minf.dme.notification.transformation.no_results" /></div>
				<pre class="transformation-result codearea hide">
				</pre>
			</div>
		</div>
		
		<div class="form-footer control-group">
			<div class="controls">
				<button class="btn btn-default cancel form-btn-cancel" type="reset"><s:message code="~de.unibamberg.minf.common.link.cancel" /></button>
				<button class="btn btn-primary start form-btn-submit" type="submit"><s:message code="~de.unibamberg.minf.common.link.save" /></button>
			</div>
		</div>
	</div>
</sf:form>