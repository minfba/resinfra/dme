<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div id="model-natures-controls">
	
	<div class="btn-toolbar" role="toolbar">
	  	<c:if test="${datamodel.own || datamodel.write}">
		  	<div class="btn-group" role="group">
		  		<button id="edit-model-nature" type="button" class="btn btn-sm btn-link"><i class="fas fa-pencil-alt"></i></button>
				<button id="remove-model-nature" type="button" class="btn btn-sm btn-link fa-color-danger"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>
				<button id="add-model-nature" type="button" class="btn btn-sm btn-link"><i class="fa fa-plus" aria-hidden="true"></i></button>
		  	</div>
	  	</c:if>
	  	<div class="input-group">    	
	    	<select id="select-model-natures" class="form-control form-control-sm">
			  <option selected="selected" value="logical_model"><s:message code="~de.unibamberg.minf.dme.model.datamodel.natures.LogicalModel.display_label" /></option>
			  <c:if test="${fn:length(datamodel.pojo.natures)>0}">
				  <option disabled="disabled">──────────</option>
				  <c:forEach items="${datamodel.pojo.natures}" var="nature">
				  	<option value="${nature['class'].name}"><s:message code="~${nature['class'].name}.display_label" /></option>
				  </c:forEach>
			  </c:if>
			</select>
		</div>
	</div>
</div>
<div id="schema-editor-container" class="editor-container flex-fill">
	<div class="model-loading-indicator flex-fill h-100 text-center mt-3"><i class="fas fa-cog fa-spin fa-2x" style="color: var(--primary-var-2);"></i></div>
	<canvas id="schema-editor-canvas"></canvas>
</div>