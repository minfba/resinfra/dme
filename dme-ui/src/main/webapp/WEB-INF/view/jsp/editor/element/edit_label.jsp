<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<s:url value="${actionPath}" var="saveUrl" />

<sf:form method="POST" action="${saveUrl}" modelAttribute="element">
	<div class="form-header">
		<h2 id="form-header-title"><s:message code="~de.unibamberg.minf.dme.form.label.edit" /></h2>	
		<sf:hidden path="id" />
		<sf:hidden path="entityId" />
	</div>
	<div class="form-content">
		<fieldset<c:if test="${readonly}"> disabled</c:if>>
			<div class="form-group row">
				<label class="col-3 col-form-label" for="element_name"><s:message code="~de.unibamberg.minf.dme.model.element.name" />:</label>
				<div class="col-9">
					<sf:input path="name" class="form-control" id="labelImpl_name" />
					<sf:errors path="name" cssClass="error" />
				</div>
			</div>		
			<div class="form-group row">
				<label class="col-form-label col-3" for="readOnly"><s:message code="~de.unibamberg.minf.common.view.options" />:</label>
				<div class="col-9">
					<div class="form-check">
						<sf:checkbox cssClass="form-check-input" path="transient" />
						<label class="form-check-label">
							<s:message code="~de.unibamberg.minf.dme.model.element.transient" />
						</label>
					</div>

					<div class="form-check">
						<sf:checkbox cssClass="form-check-input" path="identifierElement" />
						<label class="form-check-label">
							<s:message code="~de.unibamberg.minf.dme.model.element.identifierElement" />
						</label>
					</div>
					
					<div class="form-check">
						<c:set var="sessionVariableSet" value="${element.sessionVariable!=null && fn:length(element.sessionVariable)>0}" />
						<input class="form-check-input" type="checkbox" ${sessionVariableSet ? "checked='checked'" : ""} onchange="$('#labelImpl_sessionVariable').prop('disabled', !$(this).is(':checked')); if (!$(this).is(':checked')) $('#labelImpl_sessionVariable').val('');">
						<label class="form-check-label"><s:message code="~de.unibamberg.minf.dme.model.element.useSessionVariable" /></label>
						<sf:input path="sessionVariable" class="form-control" id="labelImpl_sessionVariable" disabled="${!sessionVariableSet}" />
						<sf:errors path="sessionVariable" cssClass="error" />
					</div>		

					<div class="form-check">
						<sf:checkbox id="chk-processing-root" cssClass="form-check-input" path="processingRoot" />
						<label class="form-check-label">
							<s:message code="~de.unibamberg.minf.dme.model.element.processing_root" />
						</label>
					</div>
					
				</div>
				<div class="processing-root-option col-8 offset-4 ${element.processingRoot ? '' : 'hide'}">					
					<div class="form-check form-check-inline">
						<sf:radiobutton cssClass="form-check-input" path="includeHeader" value="false" />
						<label class="form-check-label">
							<s:message code="~de.unibamberg.minf.dme.model.element.include_header.discard" />
						</label>
					</div>
					<div class="form-check form-check-inline">
						<sf:radiobutton cssClass="form-check-input" path="includeHeader" value="true" />
						<label class="form-check-label">
							<s:message code="~de.unibamberg.minf.dme.model.element.include_header.replicate" />
						</label>
					</div>
					<small id="passwordHelpBlock" class="form-text text-muted">
					  <s:message code="~de.unibamberg.minf.dme.model.element.include_header.hint" />
					</small>
				</div>
				<div class="processing-root-option col-8 offset-4 ${element.processingRoot ? '' : 'hide'}">
					<div class="form-check">
						<sf:checkbox cssClass="form-check-input" path="hierarchicalRoot" />
						<label class="form-check-label">
							<s:message code="~de.unibamberg.minf.dme.model.element.hierarchical_root" />
						</label>
					</div>
					<small id="hierarchicalRootHelpBlock" class="form-text text-muted">
					  <s:message code="~de.unibamberg.minf.dme.model.element.hierarchical_root.hint" />
					</small>
				</div>
			</div>	
		</fieldset>
	</div>
	<div class="form-footer control-group">
		<div class="controls">
			<c:choose>
				<c:when test="${readonly}">
					<button class="btn btn-primary cancel form-btn-cancel" type="reset"><s:message code="~de.unibamberg.minf.common.link.close" /></button>
				</c:when>
				<c:otherwise>
					<button class="btn btn-default cancel form-btn-cancel" type="reset"><s:message code="~de.unibamberg.minf.common.link.cancel" /></button>
					<button class="btn btn-primary start form-btn-submit" type="submit"><s:message code="~de.unibamberg.minf.common.link.save" /></button>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</sf:form>