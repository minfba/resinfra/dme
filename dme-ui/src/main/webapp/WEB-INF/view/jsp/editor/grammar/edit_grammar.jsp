<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<s:url value="${actionPath}" var="saveUrl" />

<sf:form method="POST" action="${saveUrl}" modelAttribute="grammar">
	<div class="form-header">
		<h2 class="form-header-title"><s:message code="~de.unibamberg.minf.dme.form.grammar.edit" /> <small><i class="fas fa-info-circle" onclick="grammarEditor.showHelp(); return false;"></i></small></h2>	
		<sf:hidden path="id" />
		<sf:hidden path="passthrough" />
		<sf:hidden path="error" />
		<sf:hidden path="entityId" />
	</div>
	<div class="form-content row" style="padding-bottom: 0px;">
		<div class="col-md-7" style="border-right: 1px solid #E5E5E5;">
			<h3><strong>1)</strong> <s:message code="~de.unibamberg.minf.dme.form.grammar.legend.edit_function" /></h3>
			<div class="form-group row">
				<div class="col-6">
					<fieldset<c:if test="${readonly}"> disabled</c:if>>
						<label class="col-form-label" for="name"><s:message code="~de.unibamberg.minf.dme.model.grammar.name" />:</label>
						<div>
							<sf:input path="name" id="grammarImpl_name" class="form-control" />
							<sf:errors path="name" cssClass="error" />
						</div>
					</fieldset>
				</div>
				<div class="col-6">
					<label class="col-form-label" for="base_method"><s:message code="~de.unibamberg.minf.dme.model.grammar.base_rule" />:</label>
					<div>
						<sf:input path="baseMethod" class="form-control" />
						<sf:errors path="baseMethod" cssClass="error" />
					</div>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-12">
					<label class="col-form-label" for="grammarContainer_lexerGrammar"><s:message code="~de.unibamberg.minf.dme.model.grammar.grammar_layout" />:</label>
					
					<input type="radio" name="lexer-parser-option" id="lexer-parser-option-passthrough" class="lexer-parser-option lexer-parser-option-passthrough" value="passthrough">
					<label class="form-check-label" for="lexer-parser-option-passthrough" data-toggle="tooltip" data-placement="top" title="<s:message code="~de.unibamberg.minf.dme.form.grammar.hint.passthrough" />">
						<s:message code="~de.unibamberg.minf.dme.model.grammar.passthrough" />
					</label>					
				
					<input type="radio" name="lexer-parser-option" id="lexer-parser-option-combined" class="lexer-parser-option lexer-parser-option-combined" value="combined" checked>
					<label class="form-check-label" for="lexer-parser-option-combined" data-toggle="tooltip" data-placement="top" title="<s:message code="~de.unibamberg.minf.dme.form.grammar.hint.combined_layout" />">
						<s:message code="~de.unibamberg.minf.dme.model.grammar.combined" />
					</label>
				
					<input type="radio" name="lexer-parser-option" id="lexer-parser-option-separate" class="lexer-parser-option lexer-parser-option-separate" value="separate">
					<label class="form-check-label" for="lexer-parser-option-separate" data-toggle="tooltip" data-placement="top" title="<s:message code="~de.unibamberg.minf.dme.form.grammar.hint.separate_layout" />">
						<s:message code="~de.unibamberg.minf.dme.model.grammar.separate" />
					</label>
					
					<button class="btn btn-info float-right non-passthrough-only" onclick="grammarEditor.validateGrammar(); return false;"><i class="fas fa-cog"></i> <s:message code="~de.unibamberg.minf.common.link.validate" /></button>
				</div>				
				<div class="col-12">
					<span class="grammar_state"><i class="fas fa-check"></i> <s:message code="~de.unibamberg.minf.common.link.ok" /></span>
				</div>		
			</div>
			<div class="form-group row">	
				<div class="col-12">
					
					<button class="btn btn-info btn-sm float-right non-passthrough-only" onclick="grammarEditor.addFile(); return false;"><i class="fas fa-plus"></i></button>
								
					<!-- Nav tabs -->
					<ul id="grammar-tabs" class="nav nav-tabs non-passthrough-only" role="tablist">
					  <li class="nav-item" role="presentation">
					    <a class="nav-link active" id="parser-grammar-tab" data-toggle="tab" href="#parser-grammar-content" role="tab" aria-controls="parser-grammar-content" aria-selected="false"><s:message code="~de.unibamberg.minf.dme.model.grammar.parser_grammar" /></a>
					  </li>
					  <li class="nav-item form-group-lexer-grammar" role="presentation">
					    <a class="nav-link" id="lexer-grammar-tab" data-toggle="tab" href="#lexer-grammar-content" role="tab" aria-controls="lexer-grammar-content" aria-selected="false"><s:message code="~de.unibamberg.minf.dme.model.grammar.lexer_grammar" /></a>
					  </li>
					  
					  <c:if test="${grammar.grammarContainer.auxiliaryFiles!=null}">
						  <c:forEach items="${grammar.grammarContainer.auxiliaryFiles}" var="auxFile" varStatus="status">
						  	<c:set var="currFile" value="${auxFile}" scope="request" />
						  	<c:set var="currIndex" value="${status.index}" scope="request" />
						  	<jsp:include page="incl/grammar_tab.jsp" />
						  </c:forEach>
					  </c:if>
												
					</ul>
					
					<!-- Tab panes -->
					<div id="grammar-panes" class="tab-content non-passthrough-only">
					  <div class="tab-pane active" id="parser-grammar-content" role="tabpanel" data-fileindex="-1" aria-labelledby="parser-grammar-tab">
						<sf:textarea path="grammarContainer.parserGrammar" rows="14" class="form-control codearea grammarContainer_parserGrammar" />
						<sf:errors pathu="grammarContainer.parserGrammar" cssClass="error" />
					  </div>
					  <div class="tab-pane form-group-lexer-grammar" id="lexer-grammar-content" data-fileindex="-1" role="tabpanel" aria-labelledby="lexer-grammar-tab">
						<sf:textarea path="grammarContainer.lexerGrammar" rows="14" class="form-control codearea grammarContainer_lexerGrammar" />
						<sf:errors path="grammarContainer.lexerGrammar" cssClass="error" />
					  </div>
					  
					  <c:if test="${grammar.grammarContainer.auxiliaryFiles!=null}">
						  <c:forEach items="${grammar.grammarContainer.auxiliaryFiles}" var="auxFile" varStatus="status">
						  	<c:set var="currFile" value="${auxFile}" scope="request" />
						  	<c:set var="currIndex" value="${status.index}" scope="request" />
						  	<jsp:include page="incl/grammar_pane.jsp" />
						  </c:forEach>
					  </c:if>
					  
					</div>
				</div>
			</div>
			<div class="form-footer control-group">
				<div class="controls">
					<c:choose>
						<c:when test="${readonly}">
							<button class="btn btn-primary cancel form-btn-cancel" type="reset"><s:message code="~de.unibamberg.minf.common.link.close" /></button>
						</c:when>
						<c:otherwise>
							<button class="btn btn-default cancel form-btn-cancel" type="reset"><s:message code="~de.unibamberg.minf.common.link.cancel" /></button>
							<button class="btn btn-primary start form-btn-submit" type="submit"><s:message code="~de.unibamberg.minf.common.link.save" /></button>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
		<div class="col-md-5" style="border-left: 1px solid #E5E5E5; margin-left: -1px;">
			<div class="form-group row">
				<div class="col-12">
					<h3><strong>2)</strong> <s:message code="~de.unibamberg.minf.dme.editor.sample.execute" /></h3>
					<div class="non-passthrough-only">
						<div class="form-group">
							<label class="control-label" for="grammar-sample-input"><s:message code="~de.unibamberg.minf.dme.editor.sample.input" />:</label>
							<div>
								<textarea rows="4" class="grammar-sample-input form-control codearea">${elementSample}</textarea>
							</div>
						</div>
						<div class="clearfix">
							<button class="btn-parse-sample btn btn-warning float-right disabled" onclick="grammarEditor.parseSample(); return false;"><span class="glyphicon glyphicon-play" aria-hidden="true"></span> <s:message code="~de.unibamberg.minf.dme.editor.sample.process_input" /></button>
						</div>
					</div>
					<div class="passthrough-only">
					</div>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-12">
				<h3><strong>3)</strong> <s:message code="~de.unibamberg.minf.dme.editor.sample.transformation_result" /></h3>
					<div class="non-passthrough-only">
						<div class="grammar-parse-alerts">
							<div class="alert alert-sm alert-info"><s:message code="~de.unibamberg.minf.dme.editor.sample.notice.hint_sample" /></div>
						</div>
						<div class="grammar-sample-svg-embedded outer-svg-container hide">
							<div class="inner-svg-container"></div>
							<div class="svg-button-container">
								<button class="btn btn-sm btn-link btn-svg-zoomin"><i class="fas fa-plus"></i></button>
								<button class="btn btn-sm btn-link btn-svg-zoomout"><i class="fas fa-minus"></i></button>
								<button class="btn btn-sm btn-link btn-svg-reset"><i class="fas fa-redo"></i></button>
								<button class="btn btn-sm btn-link btn-svg-newwindow"><i class="fas fa-external-link-alt"></i></button>
							</div>
						</div>
					</div>
					<div class="passthrough-only">
						<s:message code="~de.unibamberg.minf.dme.form.grammar.hint.passthrough" />
					</div>
				</div>
			</div>	
		</div>
	</div>
</sf:form>