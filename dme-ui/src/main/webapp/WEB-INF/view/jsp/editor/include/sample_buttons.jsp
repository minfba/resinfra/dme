<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="btn-group float-right" role="group">
	<button type="button" onclick="editor.sampleHandler.applyAndExecuteSample(); return false;" data-toggle="tooltip" data-placement="top" title="<s:message code="~de.unibamberg.minf.dme.editor.actions.execute"/>" class="btn btn-inline"><i class="fas fa-play"></i></button>
	<button type="button" onclick="editor.sampleHandler.uploadAndExecuteSample(); return false;" class="btn btn-inline"><i class="fas fa-upload"></i></button>
	<button type="button" onclick="editor.sampleHandler.editSessionData(); return false;" class="btn btn-inline"><i class="fas fa-database"></i></button>

	<div class="btn-group" role="group">
		<button id="btnGroupDrop-${currentContext}" type="button" class="btn btn-inline" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fas fa-bars"></i>
		</button>
		<div class="dropdown-menu" aria-labelledby="btnGroupDrop${currentContext}">
			<a href="#" class="dropdown-item" onclick="editor.sampleHandler.downloadSampleInput(); return false;"><i class="fa fa-download" aria-hidden="true"></i> <s:message code="~de.unibamberg.minf.dme.editor.actions.download_sample_file" /></a>
	    	<a href="#" class="dropdown-item" onclick="editor.sampleHandler.downloadSampleOutput(); return false;"><i class="fa fa-download" aria-hidden="true"></i> <s:message code="~de.unibamberg.minf.dme.editor.actions.download_sample_output" /></a>
	    	<div class="dropdown-divider"></div>
	    	<a href="#" class="dropdown-item" onclick="editor.sampleHandler.newSampleSession(); return false;"><i class="fa fa-plus" aria-hidden="true"></i> <s:message code="~de.unibamberg.minf.dme.button.new_session" /></a>
	    	<a href="#" class="dropdown-item" onclick="sessions.saveSession(editor.getEntityId());"><i class="fas fa-save" aria-hidden="true"></i> <s:message code="~de.unibamberg.minf.dme.button.save_session" /></a>
			<a href="#" class="dropdown-item" onclick="sessions.loadSession(editor.getEntityId());"><i class="fa fa-folder-open" aria-hidden="true"></i> <s:message code="~de.unibamberg.minf.dme.button.load_session" /></a>
			<a href="#" class="dropdown-item" onclick="editor.sampleHandler.deleteSampleSession(); return false;"><i class="fa fa-trash" aria-hidden="true"></i> <s:message code="~de.unibamberg.minf.dme.button.delete_session" /></a>
		</div>
	</div>
</div>