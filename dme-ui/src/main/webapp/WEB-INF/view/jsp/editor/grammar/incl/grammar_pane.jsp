<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<s:bind path="grammarContainer.auxiliaryFiles[${currIndex}].*">
<div class="tab-pane auxiliary-file-pane" data-fileindex="${currIndex}" id="add-file-content-${currIndex}" role="tabpanel" aria-labelledby="add-file-tab-${currIndex}">
	<div class="form-group row">
		<label for="grammarContainer.auxiliaryFiles${currIndex}.fileType" class="col-sm-2 col-form-label"><s:message code="~de.unibamberg.minf.dme.editor.grammar.aux_file.type" /></label>
		<div class="col-sm-6">
			<select id="grammarContainer.auxiliaryFiles${currIndex}.fileType" name="grammarContainer.auxiliaryFiles[${currIndex}].fileType" class="form-control auxiliary-file-type">
				<c:forEach items="${auxiliaryFileTypes}" var="auxiliaryFileType">
					<option value="${auxiliaryFileType}" ${currFile.fileType==auxiliaryFileType ? "selected='selected'" : ""}>${auxiliaryFileType}</option>
				</c:forEach>
			</select>
			<sf:errors path="grammarContainer.auxiliaryFiles[${currIndex}].fileType" element="div" cssClass="validation-error alert alert-danger" />
		</div>
		<div class="col-sm-4">
			<button class="btn btn-info float-right" onclick="grammarEditor.removeFile(${currIndex}); return false;"><i class="fas fa-trash"></i> <s:message code="~de.unibamberg.minf.common.link.delete" /></button>
		</div>	
	</div>
	<div class="form-group row">
		<div class="col-sm-12">
			<textarea name="grammarContainer.auxiliaryFiles[${currIndex}].content" id="grammarContainer.auxiliaryFiles${currIndex}.content" class="form-control codearea auxiliary-file-content" rows="7">${currFile.content}</textarea>
			<sf:errors path="grammarContainer.auxiliaryFiles[${currIndex}].content" element="div" cssClass="validation-error alert alert-danger" />
		</div>
	</div>
</div>
</s:bind>