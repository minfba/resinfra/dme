<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="d-flex flex-column h-100">
	<div class="sample-loading-indicator hide flex-fill h-100 text-center mt-3"><i class="fas fa-cog fa-spin fa-2x" style="color: var(--primary-var-2);"></i></div>
	<form id="sample-${resultType}-container" class="flex-fill d-flex flex-column h-100">

		<div class="editor-content-container sample-input-optionbar d-flex">
			<div class="sample-navigation">				
				<button type="button" onclick="editor.sampleHandler.getPrevSampleResource(); return false;" class="btn-sample-prev-resource btn btn-inline disabled"><i class="fas fa-chevron-left"></i></button>
				<span class="sample-${resultType}-counter">- / -</span>
				<button type="button" onclick="editor.sampleHandler.getNextSampleResource(); return false;" class="btn-sample-next-resource btn btn-inline disabled"><i class="fas fa-chevron-right"></i></button>
			</div>
			
			<div class="flex-grow-1">
				<c:set var="currentContext" value="output" />
				<%@ include file="sample_buttons.jsp" %>
			</div>
		</div>
		
		<div id="sample-${resultType}-container" class="editor-content-container flex-fill h-100">
			<div class="sample-output-placeholder sample-codearea h-100"><s:message code="~de.unibamberg.minf.dme.editor.sample.notice.no_output" /></div>	
			<div class="sample-output sample-codearea h-100 hide"></div>
		</div>
	</form>
</div>	 