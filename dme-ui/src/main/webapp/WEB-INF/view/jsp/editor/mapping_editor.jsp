<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<tiles:importAttribute name="fluidLayout" />

<div class="flex-fill d-flex flex-column container<c:if test="${fluidLayout==true}">-fluid</c:if>">
	<div class="flex-fill d-flex flex-column">
		<div class="d-flex">
	    	<h1><small><s:message code="~de.unibamberg.minf.dme.model.mapping.source" />:</small>&nbsp;${source.pojo.name} 
	    		<small><s:message code="~de.unibamberg.minf.dme.model.mapping.target" />:</small>&nbsp;${target.pojo.name}</h1>
			<div class="ml-2">
				<c:if test="${mapping.draft}"><span class="badge badge-warning"><s:message code="~de.unibamberg.minf.common.model.draft" /></span></c:if>
				<c:if test="${mapping.readOnly}"><span class="badge badge-primary"><s:message code="~de.unibamberg.minf.common.model.readonly" /></span></c:if>
			</div>
			<div id="editor-messages-container" class="ml-2 mr-2 flex-grow-1"></div>
			<div>
				<c:choose>
					<c:when test="${mapping.own || mapping.write}">
						<button type="button" onclick="editor.triggerEdit(); return false;" class="btn btn-primary"><i class="far fa-edit"></i> <s:message code="~de.unibamberg.minf.common.link.edit" /></button>
						<button type="button" onclick="editor.triggerDelete(); return false;" class="btn btn-danger"><i class="far fa-trash-alt"></i> <s:message code="~de.unibamberg.minf.common.link.delete" /></button>
						<c:if test="${mapping.draft}">
							<button type="button" onclick="editor.triggerPublish(); return false;" class="btn btn-primary"><i class="fas fa-upload"></i> <s:message code="~de.unibamberg.minf.common.link.publish" /></button>
						</c:if>
					</c:when>
					<c:otherwise>
						<i class="fas fa-lock"></i>
					</c:otherwise>
				</c:choose>
				<button id="btn-layout-toggle" type="button" onclick="editor.toggleLayout(); return false;" class="btn btn-sm btn-inline ml-3"><i class="far fa-window-maximize"></i></button>
			</div>
	    </div>
	
	    <div class="flex-grow-1 d-flex flex-column">
	    	<input type="hidden" id="mapping-id" value="${mapping.id}" />
			<input type="hidden" id="source-id" value="${source.id}" />
			<input type="hidden" id="target-id" value="${target.id}" />
			<input type="hidden" id="mapping-write" value="${mapping.write}" />
			<input type="hidden" id="mapping-own" value="${mapping.own}" />

	    	<div class="editor-layout-container flex-fill"></div>	 
	    </div>
	</div>
</div>

