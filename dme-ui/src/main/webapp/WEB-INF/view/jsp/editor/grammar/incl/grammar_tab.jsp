<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<li class="nav-item" data-fileindex="${currIndex}" role='presentation'>
	<a class="nav-link" id="add-file-tab-${currIndex}" data-toggle="tab" href="#add-file-content-${currIndex}" role="tab" aria-controls="add-file-content-${currIndex}" aria-selected="false">
		<c:out value="${currFile.fileName}" />
	</a>
</li>