<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class='editor-properties editor-content-container' >
	<div id='editor-entity-properties'>
		<h3><s:message code="~de.unibamberg.minf.dme.model.mapping.mapping" /></h3>
		<div class="editor-property">
			<label><s:message code="~de.unibamberg.minf.common.model.id" />:</label>
			<span>${mapping.pojo.id}</span>
		</div>
		<div class="editor-property">
			<label><s:message code="~de.unibamberg.minf.common.model.readonly" />:</label>
			<span>
				<c:choose>
					<c:when test="${mapping.readOnly}">
						<i class="fas fa-check"></i>
				    </c:when>
					<c:otherwise>-</c:otherwise>
				</c:choose>	
			</span>
		</div>
		<div class="editor-property">
			<label><s:message code="~de.unibamberg.minf.dme.model.schema.description" />:</label>
			<span><c:out value="${fn:length(mapping.pojo.description)>0 ? mapping.pojo.description : '-'}" /></span>
		</div>
		
		<h3><s:message code="~de.unibamberg.minf.dme.model.datamodel.source_model" /></h3>
		<div class="editor-property">
			<label><s:message code="~de.unibamberg.minf.common.model.label" />:</label>
			<span>${source.pojo.name}</span>
		</div>
		<div class="editor-property">
			<label><s:message code="~de.unibamberg.minf.common.model.id" />:</label>
			<span>${source.pojo.id}</span>
		</div>
		
		<h3><s:message code="~de.unibamberg.minf.dme.model.datamodel.target_model" /></h3>
		<div class="editor-property">
			<label><s:message code="~de.unibamberg.minf.common.model.label" />:</label>
			<span>${target.pojo.name}</span>
		</div>
		<div class="editor-property">
			<label><s:message code="~de.unibamberg.minf.common.model.id" />:</label>
			<span>${target.pojo.id}</span>
		</div>
	</div>
	<div id='editor-element-properties'></div>
</div>