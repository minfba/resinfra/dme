<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<s:url value="${actionPath}" var="saveUrl" />
<form method="POST" action="${saveUrl}">
	<div class="form-header">
		<h2 id="form-header-title"><s:message code="~de.unibamberg.minf.dme.editor.sample.session_data" /></h2>
		<input type="hidden" name="schemaId" value="${schema.id}">
	</div>
	<div class="form-content">
		<div class="form-group row">
			<label class="col-12 col-form-label" for="sessionData"><s:message code="~de.unibamberg.minf.dme.editor.sample.session_data.content" /></label>
			<div class="col-12">
				<textarea rows="14" name="sessionData" id="sessionData" class="form-control">${sessionData}</textarea>
			</div>
		</div>
	</div>
	<div class="form-footer control-group">
		<div class="controls">
			<button class="btn btn-default cancel form-btn-cancel" type="reset"><s:message code="~de.unibamberg.minf.common.link.cancel" /></button>
			<button id="btn-submit-schema-elements" class="btn btn-primary start form-btn-submit" type="submit"><s:message code="~de.unibamberg.minf.common.link.save" /></button>
		</div>
	</div>
</form>
