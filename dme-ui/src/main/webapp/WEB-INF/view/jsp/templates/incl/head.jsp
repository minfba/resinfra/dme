<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<head>
	<meta charset="utf-8">
    <title><s:message code="~de.unibamberg.minf.dme.title" /></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="Tobias Gradl, University of Bamberg">
    <meta name="description" content="<s:message code="~de.unibamberg.minf.dme.title" />">
    <tiles:importAttribute name="genericStyles" />  	
  	<c:forEach items="${genericStyles}" var="css">
  		<link rel="stylesheet" href="<s:url value="/resources/css/${css}" />" type="text/css" media="screen, projection" />
  	</c:forEach>
  	<tiles:importAttribute name="templateStyles" />
  	<c:forEach items="${templateStyles}" var="css">
  		<link rel="stylesheet" href="<s:url value="/themes/${css}" />" type="text/css" media="screen, projection" />
  	</c:forEach>
 		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
 	<link rel="shortcut icon" type="image/x-icon" href="<s:url value="/theme/img/favicon.ico" />">
	<link rel="icon" type="image/png" href="<s:url value="/theme/img/favicon.png" />">
</head>