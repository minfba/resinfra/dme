<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
	<%@ include file="incl/head.jsp" %>
	<body class="drawer drawer--right drawer--sidebar">
		<tiles:importAttribute name="sideNav" />
		<tiles:importAttribute name="collapsePanel" ignore="true" />
		<tiles:importAttribute name="sideOpts" ignore="true" />
		<tiles:importAttribute name="navbarInverse" />
		<tiles:importAttribute name="fluidLayout" />
		<tiles:importAttribute name="smallLogo" />
		        

		        
        <wrapper class="d-flex flex-column">
        
        		<!-- i.e. version panel -->
        <c:if test="${collapsePanel!=null}">
        	<tiles:insertAttribute name="collapsePanel"/>
        </c:if>
               
        	<!-- Top Navigation -->
      		<%@ include file="incl/topNav.jsp" %>
        	
        	<main class="flex-fill">        	
				<div class="container<c:if test="${fluidLayout==true}">-fluid</c:if>">
					<div id="primary-content-area" class="row">
						<!-- Notifications -->
						<div id="notifications-area"></div>
						<div class="grid-hidden grid-lg-visible col-lg-4 col-xl-4" >
							<div class="sidebar-container"> 
								<div id="editor-sidebar" class="sidebar">
									<c:if test="${sideNav==true}">
										<%@ include file="incl/sideNav.jsp" %>
									</c:if>
									<c:if test="${sideOpts!=null}">
										<tiles:insertAttribute name="sideOpts"/>
									</c:if>
				              	</div>
			              	</div>
			      		</div>
			      		
			      		<div class="col-12 col-lg-8 col-xl-7">
			      			<main class="main">
								<!-- Content -->
								<tiles:insertAttribute name="content"/>
							</main>						
						</div>
					</div>
				</div>
			</main>
			<noscript>
		        <div><s:message code="~de.unibamberg.minf.common.view.noscript" /></div>
		    </noscript>			
		    
		    <!-- Footer -->
			<tiles:importAttribute name="theme" />
			<jsp:include page="../../../../themes/${theme}/jsp/footer.jsp" />
	  	</wrapper>
	  	
	  	

	  	<!-- JavaScript files at the end for faster loading of documents -->
	  	<tiles:importAttribute name="genericScripts" />  	
	  	<c:forEach items="${genericScripts}" var="s">
	  		<script type="text/javascript" src="<s:url value="/resources/js/${s}" />"></script>
	  	</c:forEach>
	  	<tiles:importAttribute name="templateScripts" />  	
	  	<c:forEach items="${templateScripts}" var="s">
	  		<script type="text/javascript" src="<s:url value="/themes/${s}" />"></script>
	  	</c:forEach>
	</body>
</html>