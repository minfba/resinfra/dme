<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
	<%@ include file="incl/head.jsp" %>
	<body class="site">
		<wrapper class="d-flex flex-column">
	        <tiles:importAttribute name="navbarInverse" />
	        <tiles:importAttribute name="fluidLayout" />
	        <tiles:importAttribute name="smallLogo" />
		        
	        <!-- Top Navigation -->
	        <%@ include file="incl/topNav.jsp" %>
			
			<main class="flex-fill d-flex">
				<!-- Content -->
				<tiles:insertAttribute name="content"/>
			</main>

			<!-- Footer -->
			<tiles:importAttribute name="theme" />
			<jsp:include page="../../../../themes/${theme}/jsp/footer.jsp" />
			
		</wrapper>
		
		<noscript>
	        <div><s:message code="~de.unibamberg.minf.common.view.noscript" /></div>
	    </noscript>
	  	<!-- JavaScript files at the end for faster loading of documents -->
	  	<tiles:importAttribute name="genericScripts" />  	
	  	<c:forEach items="${genericScripts}" var="s">
	  		<script type="text/javascript" src="<s:url value="/resources/js/${s}" />"></script>
	  	</c:forEach>
	  	<tiles:importAttribute name="templateScripts" />  	
	  	<c:forEach items="${templateScripts}" var="s">
	  		<script type="text/javascript" src="<s:url value="/themes/${s}" />"></script>
	  	</c:forEach>
	</body>
</html>