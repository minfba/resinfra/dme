<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tpl" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:importAttribute name="collapsePanel" ignore="true" />

<header>
	<nav class="navbar navbar-expand-xl bg-${_nuance} navbar-dark">
	
		<div class="container-fluid" style="max-width: 1200px">
	
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
	
				<span class="ti-menu"></span>
				<s:message code="~de.unibamberg.minf.common.labels.menu" />
			</button>
		
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="<s:url value='/' />"><span class="ti-home"></span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<s:url value='/registry/' />"><s:message code="~de.unibamberg.minf.dme.registry.title" /></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<s:url value='/vocabulary/' />"><s:message code="~de.unibamberg.minf.dme.vocabulary.title" /></a>
					</li>
				</ul>
			</div>
			
			<div class="navbar-expand" id="navbarSupportedContent2" style="margin-right: 40px;">
			
				<!-- Language, login, search -->
				<ul class="navbar-nav">
					<li class="nav-item dropdown navbar-separator">
						<a class="nav-link" href="#" id="languageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="ti-world"></span> 
							<c:forEach items="${_LANGUAGES}" var="_LANGUAGE">
								<c:if test="${_LANGUAGE.key==pageContext.response.locale}">
									${_LANGUAGE.value}
								</c:if>	
							</c:forEach>
						</a>
						<div class="dropdown-menu" aria-labelledby="languageDropdown">
							<c:forEach items="${_LANGUAGES}" var="_LANGUAGE">
								<a class="dropdown-item" href="?lang=${_LANGUAGE.key}">${_LANGUAGE.value}</a>
							</c:forEach>
						</div>
					</li>
					<li class="nav-item">
						<c:set var="currentUrl" value="${requestScope['javax.servlet.forward.request_uri']}" />
						<c:choose>
							<c:when test="${_auth==null || _auth.auth!=true}">							
								<a class="nav-link account_toggle" href="<s:url value='/startLogin?url=${currentUrl}' />"><span class="ti-login"></span></a>
							</c:when>
							<c:otherwise>
								<a class="nav-link account_toggle" href="<s:url value='/centralLogout' />"><span class="ti-logout"></span></a>
							</c:otherwise>
						</c:choose>
					</li>
					<c:if test="${_auth!=null && _auth.auth==true}">
						<li class="nav-item">
							<a class="nav-link" href="<s:url value='/user' />" title="${_auth.displayName}"><span class="ti-user"></span></a>
						</li>
					</c:if>
	
					<c:catch>
						<tiles:importAttribute name="theme" />
						<jsp:include page="../../../../themes/${theme}/jsp/add_navigation.jsp" />
					</c:catch>
				</ul>
			</div>
		</div>
	</nav>
	<h1 class="logobar">
		<a class="logobar-link<c:if test="${smallLogo==true}"> logobar-link-sm</c:if>" href="" title="Startseite">
			<img class="logobar-logo" src='<s:url value="/theme/img/theme-logo-de.svg" />' alt='<s:message code="~eu.dariah.de.minfba.theme.name" />'>
			<span class="logobar-title">
				<s:message code="${smallLogo==true ? '~eu.dariah.de.minfba.theme.application_titles.dme' : '~eu.dariah.de.minfba.theme.application_titles.br.dme'}" />
			</span>
		</a>
	</h1>
</header>
<input id="currentUrl" type="hidden" value="${requestScope['javax.servlet.forward.request_uri']}" />
<input id="baseUrl" type="hidden" value="<s:url value="/" />" />
<input id="baseUrl2" type="hidden" value="<s:url value="/{}" />" />
