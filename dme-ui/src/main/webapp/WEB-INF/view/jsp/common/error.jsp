<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<tiles:importAttribute name="fluidLayout" />


<div class="container<c:if test="${fluidLayout==true}">-fluid</c:if>">
	<div class="row">
	    <div class="col-12">
	    
	    	<h1>${error} ${reason}</h1>
			
			<c:if test="${hideHelpText==null||hideHelpText==false}">
				<p>Please try to reproduce the steps that led to this error and notify the DARIAH-DE helpdesk if you suspect an issue with this service.</p>
			</c:if>
			
			<ul>
				<li><a href="<s:url value='/registry/' />"><s:message code="~de.unibamberg.minf.dme.registry.title" /></a></li>
				<li><a href="<s:url value='/vocabulary/' />"><s:message code="~de.unibamberg.minf.dme.vocabulary.title" /></a></li>
			</ul>
		</div>
	</div>
</div>



