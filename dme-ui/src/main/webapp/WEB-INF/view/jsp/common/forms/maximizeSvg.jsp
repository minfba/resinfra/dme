<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="form-horizontal" >
	<div class="form-header">
		<h2 class="form-header-title"><s:message code="~de.unibamberg.minf.dme.editor.sample.show_processed_svg" /></h2>	
	</div>
	<div class="form-content">
		<div class="maximized-svg-container">
			<div class="inner-svg-container"></div>
			<div class="svg-button-container">
				<button class="btn btn-link btn-sm btn-svg-zoomin"><i class="fas fa-plus"></i></button>
				<button class="btn btn-link btn-sm btn-svg-zoomout"><i class="fas fa-minus"></i></button>
				<button class="btn btn-link btn-sm btn-svg-reset"><i class="fas fa-redo"></i></button>
			</div>
		</div>
	</div>
	<div class="form-footer">
		<div class="controls">
			<button class="btn btn-default cancel form-btn-cancel" type="reset"><s:message code="~de.unibamberg.minf.common.link.close" /></button>
		</div>
	</div>
</div>