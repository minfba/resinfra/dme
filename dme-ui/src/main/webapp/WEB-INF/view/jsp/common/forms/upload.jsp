<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div>
	<div class="fileupload-buttonbar">
		<span class="btn btn-primary fileinput-button"> <i class="fas fa-file-upload"></i> 
			<s:message code="~de.unibamberg.minf.common.link.upload" />
		<input type="file" name="file" /></span>
	</div>
	<div class="fileupload-progress hide">
		<div class="progress">
			<div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><span class="progress-extended">&nbsp;</span></div>
		</div>		
	</div>
	<div class="fileupload-files" role="presentation" data-toggle="modal-gallery" data-target="#modal-gallery"></div>
</div>