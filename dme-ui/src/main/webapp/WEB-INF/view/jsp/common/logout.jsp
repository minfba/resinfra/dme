<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<tiles:importAttribute name="fluidLayout" />

<div class="container<c:if test="${fluidLayout==true}">-fluid</c:if>">
	<div class="row">
	    <div id="notifications-area" class="col-10 offset-1"></div>		
	    <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
	    	<h1><s:message code="~de.unibamberg.minf.common.logout.head" /></h1>
			<p><s:message code="~de.unibamberg.minf.common.logout.body" /></p>
			<p><a href="<s:url value="/" />"><s:message code="~de.unibamberg.minf.common.labels.back_to_start" /></a></p>
	    </div>
	</div>
</div>