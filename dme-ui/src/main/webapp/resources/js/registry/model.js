var modelTable;
$(document).ready(function() {
	modelTable = new ModelTable();
	
	$("#btn-add-model").click(function() { 
		modelTable.triggerAdd(); 
	});
});

var ModelTable = function() {
	this.prepareTranslations([	"~de.unibamberg.minf.common.model.draft",
								"~de.unibamberg.minf.common.model.stub",
								"~de.unibamberg.minf.common.model.public",
								"~de.unibamberg.minf.common.model.readonly",
								"~de.unibamberg.minf.common.view.forms.servererror.head",
								"~de.unibamberg.minf.common.view.forms.servererror.body"
	                          ]);
	this.createTable();
};

ModelTable.prototype = new BaseTable(__util.getBaseUrl() + "model/async/getData", "#model-table-container");

ModelTable.prototype.createTable = function() {
	var _this = this;
	this._base.table = $('#model-table').DataTable($.extend(true, {
		"order": [[1, "asc"]],
		"columnDefs": [
	       {
	           "targets": [0],
	           "class" : "td-no-wrap",
	           "data": function (row, type, val, meta) { return _this.renderBadgeColumn(row, type, val, meta); }
	       }, {
	    	   "targets": [1],
	    	   "data" : function (row, type, val, meta) { return _this.getRowLink(row, row.entity.pojo.name); },
	    	   "width" : "100%"
	       }
	   ]
	}, this.baseSettings));
};

/* Overrides the base abstract method */
ModelTable.prototype.handleSelection = function(id) { };

ModelTable.prototype.renderBadgeColumn = function(row, type, val, meta) {
	var result = "";	
	if (type==="display") {
		if (row.entity.pojo.type=="BaseSchema") {
			result += '<span class="badge badge-warning">' + __translator.translate("~de.unibamberg.minf.common.model.stub") + '</span> ';
		}		
		if (row.entity.draft) {
			result += '<span class="badge badge-warning">' + __translator.translate("~de.unibamberg.minf.common.model.draft") + '</span> ';
		} else {
			result += '<span class="badge badge-primary">' + __translator.translate("~de.unibamberg.minf.common.model.public") + '</span> ';
		}
		
		if (row.entity.readOnly) {
			result += '<span class="badge badge-primary">' + __translator.translate("~de.unibamberg.minf.common.model.readonly") + '</span> ';
		} 
	} else if (type==="filter" || type==="sort") {
		if (row.entity.draft) {
			result += __translator.translate("~de.unibamberg.minf.common.model.draft") + " ";
		} else {
			result += __translator.translate("~de.unibamberg.minf.common.model.public" + " ");
		}
		if (row.entity.readOnly) {
			result += __translator.translate("~de.unibamberg.minf.common.model.draft" + " ");
		}
	}
	return result;
};

ModelTable.prototype.triggerAdd = function () {
	this.triggerEdit();
};

ModelTable.prototype.triggerEdit = function(schemaId) {
	if (!__util.isLoggedIn()) {
		__util.showLoginNote();
		return;
	}
	
	var _this = this;
	var form_identifier = "edit-model-" + schemaId;
	var url = __util.getBaseUrl() + "model/" + (schemaId!=undefined ? ("forms/edit/" + schemaId) : "forms/add");
	
	modalFormHandler = new ModalFormHandler({
		formFullUrl: url,
		identifier: form_identifier,
		additionalModalClasses: "modal-lg",
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],
		completeCallback: function() {_this.refresh();}
	});
		
	modalFormHandler.show(form_identifier);
};

ModelTable.prototype.getRowLink = function(row, label) {
	var link = "<a href=\"" + __util.composeUrl("model/editor/" + row.entity.id + "/") + "\">";
	if (label!==undefined) {
		link += label + "</a>";
	}
	return link;
};