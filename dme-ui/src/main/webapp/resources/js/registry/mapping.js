var mappingTable;
$(document).ready(function() {
	mappingTable = new MappingTable();
	
	$("#btn-add-mapping").click(function() { 
		mappingTable.triggerAdd(); 
	});
});

var MappingTable = function() {
	this.prepareTranslations([	"~de.unibamberg.minf.common.model.draft",
								"~de.unibamberg.minf.common.model.public",
								"~de.unibamberg.minf.common.model.readonly",
								"~de.unibamberg.minf.common.view.forms.servererror.head",
								"~de.unibamberg.minf.common.view.forms.servererror.body"
	                          ]);
	this.createTable();
};

MappingTable.prototype = new BaseTable(__util.getBaseUrl() + "mapping/async/getData", "#mapping-table-container");

MappingTable.prototype.createTable = function() {
	var _this = this;
	
	this._base.table = $('#mapping-table').DataTable($.extend(true, {
		"order": [[1, "asc"]],
		"columnDefs": [
	       {
	           "targets": [0],
	           "class" : "td-no-wrap",
	           "data": function (row, type, val, meta) { return _this.renderBadgeColumn(row, type, val, meta); }
	       }, {	
	    	   "targets": [1],
	    	   "data": function (row, type, val, meta) { return _this.getRowLink(row, row.entity.pojo.sourceLabel); },
	    	   "width" : "50%"
	       }, {	
	    	   "targets": [2],
	    	   "data": function (row, type, val, meta) { return _this.getRowLink(row, row.entity.pojo.targetLabel); },
	    	   "width" : "50%"
	       }, {	
	    	   "targets": [3],
	    	   "data": "entity.pojo.sourceId",
	    	   "visible" : false
	       }, {	
	    	   "targets": [4],
	    	   "data": "entity.pojo.targetId",
	    	   "visible" : false
	       }
	   ]
	}, this.baseSettings));
};

/* Overrides the base abstract method */
MappingTable.prototype.handleSelection = function(id) { };

MappingTable.prototype.renderBadgeColumn = function(row, type, val, meta) {
	var result = "";	
	if (type=="display") {
		if (row.entity.draft) {
			result += '<span class="badge badge-warning">' + __translator.translate("~de.unibamberg.minf.common.model.draft") + '</span> ';
		} else {
			result += '<span class="badge badge-primary">' + __translator.translate("~de.unibamberg.minf.common.model.public") + '</span> ';
		}
		
		if (row.entity.readOnly) {
			result += '<span class="badge badge-primary">' + __translator.translate("~de.unibamberg.minf.common.model.readonly") + '</span> ';
		} 
	} else {
		if (row.entity.draft) {
			result += __translator.translate("~de.unibamberg.minf.common.model.draft") + " ";
		} else {
			result += __translator.translate("~de.unibamberg.minf.common.model.public" + " ");
		}
		if (row.entity.readOnly) {
			result += __translator.translate("~de.unibamberg.minf.common.model.draft" + " ");
		}
	}
	return result;
};

MappingTable.prototype.triggerAdd = function () {
	this.triggerEdit();
};

MappingTable.prototype.triggerEdit = function(mappingId) {
	if (!__util.isLoggedIn()) {
		__util.showLoginNote();
		return;
	}
	
	var _this = this;
	var form_identifier = "edit-mapping-" + mappingId;
	var url = __util.getBaseUrl() + "mapping/" + (mappingId!=undefined ? ("forms/edit/" + mappingId) : "forms/add");
	
	modalFormHandler = new ModalFormHandler({
		formFullUrl: url,
		identifier: form_identifier,
		setupCallback: function(form) {
			$(form).find("select.form-control").on("change", function(e) {
				if ($("#sourceId").val()==$("#targetId").val()) {
					var changeId = e.target.id=="sourceId" ? "#targetId" : "#sourceId";
					$(changeId).find("option").removeAttr("selected");
					$(changeId).find("option").each(function() {
						if ($(this).val()!=$(changeId).val()) {
							$(this).attr("selected", true);
						}
					});
				}
			})
		},
		additionalModalClasses: "modal-lg",
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],
		completeCallback: function() {_this.refresh();}
	});
		
	modalFormHandler.show(form_identifier);
};


MappingTable.prototype.getRowLink = function(row, label) {
	var link = "<a href=\"" + __util.composeUrl("mapping/editor/" + row.entity.id + "/") + "\">";
	if (label!==undefined) {
		link += label + "</a>";
	}
	return link;
};