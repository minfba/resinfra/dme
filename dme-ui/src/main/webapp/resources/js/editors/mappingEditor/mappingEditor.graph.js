MappingEditor.prototype.initGraphs = function() {
	var _this = this;
	this.graph = new Model(this.context.canvas, {
		readOnly: !(_this.mapping.owned || _this.mapping.write),
		elementTemplateOptions: [
		    { 
		    	key: "Nonterminal", 
		    	primaryColor: "#e6f1ff", 
		    	secondaryColor: "#0049a6",
				getContextMenuItems: _this.getElementContextMenu
			}, {
				key: "Label",
				primaryColor: "#f3e6ff", 
				secondaryColor: "#5700a6",
				getContextMenuItems: _this.getElementContextMenu
			}],
		mappingTemplateOption : {
			relativeControlPointX : 4, 
			connectionHoverTolerance : 5,
			getContextMenuItems: _this.getConnectionContextMenu,
			functionTemplateOptions : {
				primaryColor: "#FFE173", secondaryColor: "#6d5603", radius: 3
			}
		}
		
	});
	this.source = this.graph.addArea({ getContextMenuItems: _this.getAreaContextMenu });
	this.target = this.graph.addArea({ getContextMenuItems: _this.getAreaContextMenu });
	
	this.graph.init();
	this.createActionButtons(this.mappingContextButtons, this.source.getContextMenuItems());
	this.loadActivitiesForEntity(this.mappingId, this.mappingActivitiesContainer);
	
	this.contextMenuClickEventHandler = this.handleContextMenuClicked.bind(this);
	
	this.resize();
		
 	this.getElementHierarchy(this.sourcePath, this.source);
 	this.getElementHierarchy(this.targetPath, this.target);
};

MappingEditor.prototype.registerEvents = function() {
	document.addEventListener("selectionEvent", this.selectionHandler, false);
	document.addEventListener("deselectionEvent", this.deselectionHandler, false);
	document.addEventListener("newConceptMappingEvent", this.saveConceptMappingHandler, false);
	document.addEventListener("changeConceptMappingEvent", this.saveConceptMappingHandler, false);
	document.addEventListener("contextMenuClickEvent", this.contextMenuClickEventHandler, false);
};

MappingEditor.prototype.deregisterEvents = function() {
	document.removeEventListener("selectionEvent", this.selectionHandler);
	document.removeEventListener("deselectionEvent", this.deselectionHandler);
	document.removeEventListener("newConceptMappingEvent", this.saveConceptMappingHandler);
	document.removeEventListener("changeConceptMappingEvent", this.saveConceptMappingHandler);
	document.removeEventListener("contextMenuClickEvent", this.contextMenuClickEventHandler);
};

MappingEditor.prototype.getAreaContextMenu = function(area) {
	return [
	    area.model.createContextMenuHeading("~de.unibamberg.minf.dme.model.schema.schema"),
	    area.model.createContextMenuItem("expandAll", "~de.unibamberg.minf.dme.button.expand_all", "fas fa-expand", area.index, "area"),
	    area.model.createContextMenuItem("collapseAll", "~de.unibamberg.minf.dme.button.collapse_all", "fas fa-compress", area.index, "area"),
	    area.model.createContextMenuHeading("~de.unibamberg.minf.dme.editor.element_model"),
	    area.model.createContextMenuItem("reset", "~de.unibamberg.minf.common.link.reset_view", "fas fa-redo"),
	    area.model.createContextMenuItem("reload", "~de.unibamberg.minf.common.link.reload_data", "fas fa-sync"),
	    
	    area.model.createContextMenuHeading("~de.unibamberg.minf.dme.editor.mapping"),
	    area.model.createContextMenuItem("exportMapping", "~de.unibamberg.minf.dme.button.export", "fas fa-file-export"),
	    area.model.createContextMenuItem("importMapping", "~de.unibamberg.minf.dme.button.import", "fas fa-file-import"),
	];
};

MappingEditor.prototype.getElementContextMenu = function(element) { 
	var _this = editor;
	return [
	        _this.graph.createContextMenuHeading("~de.unibamberg.minf.dme.model.element.element"),
	        _this.graph.createContextMenuItem("expandFromHere", "~de.unibamberg.minf.dme.button.expand_from_here", "fas fa-expand", element.id, element.template.options.key),
			_this.graph.createContextMenuItem("collapseFromHere", "~de.unibamberg.minf.dme.button.collapse_from_here", "fas fa-compress", element.id, element.template.options.key)	
	];
};

MappingEditor.prototype.getConnectionContextMenu = function(connection) {
	const _this = editor;
	let items = [
	        _this.graph.createContextMenuHeading("~de.unibamberg.minf.dme.model.mapping.mapping"),
		    _this.graph.createContextMenuItem("ensureConnectedVisible", "~de.unibamberg.minf.dme.editor.actions.ensure_connected_visible", "fas fa-expand", connection.id),
		    _this.graph.createContextMenuItem("resetPosition", "~de.unibamberg.minf.dme.editor.actions.reset_position", "fas fa-redo", connection.id),
	];
	if (_this.mapping.owned || _this.mapping.write) {
		if (connection.type=="FUNCTION" || connection.type=="VALUE") {
			items.push(_this.graph.createContextMenuSeparator(),
			    _this.graph.createContextMenuItem("editFunctionConnection", "~de.unibamberg.minf.dme.editor.actions.edit_connection", "far fa-edit", connection.id, connection.type),
			    _this.graph.createContextMenuItem("switchToExportedConnection", "~de.unibamberg.minf.dme.editor.actions.switch_connection_to_exported", "fas fa-random", connection.id, "EXPORT"))
			
		} else if (connection.type=="EXPORT") {
			items.push(_this.graph.createContextMenuSeparator(),
			    _this.graph.createContextMenuItem("editExportConnection", "~de.unibamberg.minf.dme.editor.actions.edit_connection", "far fa-edit", connection.id, connection.type),
			    _this.graph.createContextMenuItem("switchToFunctionConnection", "~de.unibamberg.minf.dme.editor.actions.switch_connection_to_function", "fas fa-random", connection.id, "FUNCTION"))
		}
		items.push(_this.graph.createContextMenuItem("removeMapping", "~de.unibamberg.minf.common.link.delete", "far fa-trash-alt", connection.id, undefined, "danger"))
	} else {
		items.push(_this.graph.createContextMenuSeparator(),
				_this.graph.createContextMenuItem("editFunctionConnection", "~de.unibamberg.minf.dme.editor.actions.show_connection", "far fa-edit", connection.id, connection.type));
	}
	return items;
};

MappingEditor.prototype.handleContextMenuClicked = function(e) {
	this.performTreeAction(e.key, e.id, e.nodeType);
};


MappingEditor.prototype.getAreaForElementId = function(elementId) {
	for (let area of this.graph.areas) {
		if (area.findElementById(area.root, elementId) != null) {
			return area;
		}
	}
};

MappingEditor.prototype.performTreeAction = function(action, elementId, elementKey) {
	switch(action) {
		case "expandFromHere" : 
			var area = this.getAreaForElementId(elementId);
			return area.expandFromElement(elementId, true);
		case "collapseFromHere" : 
			var area = this.getAreaForElementId(elementId);
			return area.expandFromElement(elementId, false);
	
	    case "expandAll" :  return this.graph.areas[elementId].expandAll(true);
	    case "collapseAll" : return this.graph.areas[elementId].expandAll(false);
	    case "reload" : return this.reloadAll();
	    case "reset" : return this.graph.resetView();
	 
	    case "ensureConnectedVisible" : return this.ensureConnectedVisible(elementId);
	    case "resetPosition" : return this.resetMappingPosition(elementId);
	    case "editFunctionConnection" : return this.editFunctionConnection(elementId, elementKey);
	    case "editExportConnection" : return this.editExportConnection(elementId, elementKey);
	    case "removeMapping" : return this.removeConceptMapping(elementId);
	    
	    case "importMapping" : return this.importMapping();
	    case "exportMapping" : return this.exportMapping();
	    
	    case "switchToExportedConnection" : return this.switchConnectionType(elementId, elementKey);
	    case "switchToFunctionConnection" : return this.switchConnectionType(elementId, elementKey);
	    
	    /*default:
	        throw new Error("Unknown tree action requested: " + action);*/
	}  
};

MappingEditor.prototype.resetMappingPosition = function(connectionId) {
	var mapping = this.graph.getMappingById(connectionId);
	mapping.clearMovedForkPoint();
	this.graph.paint();
};

MappingEditor.prototype.ensureConnectedVisible = function(connectionId) {
	var mapping = this.graph.getMappingById(connectionId);
	for (var i=0; i<mapping.from.length; i++) {
		mapping.from[i].element.ensureVisible();
	}
	for (var i=0; i<mapping.to.length; i++) {
		mapping.to[i].element.ensureVisible();
	}
	this.graph.update();
};

MappingEditor.prototype.reloadAll = function() {
	this.deregisterEvents();
	
	var selectedItemIds = [];
	for (var i=0; i<this.graph.selectedItems.length; i++) {
		selectedItemIds.push(this.graph.selectedItems[i].getId());
	}

	this.reloadElementHierarchy(this.sourcePath, this.source, selectedItemIds);
 	this.reloadElementHierarchy(this.targetPath, this.target, selectedItemIds);
};

MappingEditor.prototype.reloadElementHierarchy = function(path, area, selectedItemIds) {
	if (area.root==null) {
		this.getElementHierarchy(path, source);
		return;
	}
	
	var rootX = area.root.rectangle.x;
	var rootY = area.root.rectangle.y;
	var expandedItemIds = area.getExpandedElementIds(area.root);

	var _this = this;
	$.ajax({
		url: path + "async/getHierarchy",
		data: { staticElementsOnly: true },
	    type: "GET",
	    success: function(data) {
	    	if (data===null || data===undefined || data.length==0) {
	    		return;
	    	}
	    	area.clear();
	    	
	    	_this.processElementHierarchy(area, data);	    	
	    	
	    	area.root.rectangle = new Rectangle(rootX, rootY, area.root.rectangle.width, area.root.rectangle.height);
	    			
	    	area.selectElementsByIds(area.root, selectedItemIds);    		
	    	area.expandElementsByIds(area.root, expandedItemIds);
	    	
	    	area.invalidate();
	    	
	    	if (_this.oneDone) {
	    		_this.getMappings(selectedItemIds);
	    		_this.oneDone = false;
	    	} else {
	    		_this.oneDone = true;
	    	}
	    }/*,
	    error: function(jqXHR, textStatus, errorThrown) {
	    	__util.processServerError(jqXHR, textStatus, errorThrown);
	    	_this.initGraph();
	    }*/
	});	
};

MappingEditor.prototype.getElementHierarchy = function(path, area) {
	var _this = this;
	$.ajax({
		url: path + "async/getHierarchy",
		data: { staticElementsOnly: true },
	    type: "GET",
	    success: function(data) {
	    	if (data===null || data===undefined || data.length==0) {
	    		return;
	    	}
	    	_this.processElementHierarchy(area, data);
	    	//_this.graph.paint();
	    	
	    	if (_this.oneDone) {
	    		_this.getMappings();
	    		_this.oneDone = false;
	    	} else {
	    		_this.oneDone = true;
	    	}
	    }/*,
	    error: __util.processServerError*/
	});
};

MappingEditor.prototype.updateMapping = function(connectionId) {
	const _this = this;
	$.ajax({
	    url: __util.composeRelativeUrl("async/getConcept?id=" + connectionId),
	    type: "GET",
	    success: function(data) {
			if (data!=undefined && data!=null && data.type!=undefined && data.type!=null) {
				for (let mapping of _this.graph.mappings) {
					if(mapping.id==data.id) {
						mapping.type=data.type;
					}
				}
				_this.graph.update();
			}	
		}
	});
		
}

MappingEditor.prototype.getMappings = function(selectedItemIds) {
	var _this = this;
	$.ajax({
	    url: __util.composeRelativeUrl("async/getConcepts"),
	    type: "GET",
	    success: function(data) {
	    	_this.graph.clearMappings();
	    	
	    	if (data!==undefined && data!=null && data.length>0) {
		    	for (var i=0; i<data.length; i++) {
		    		var lhs = [];
		    		var rhs = [];
		    		
		    		for (var j=0; j<data[i].sourceElementIds.length; j++) {
		    			 try {
		    				 lhs.push(_this.source.getElementById(data[i].sourceElementIds[j]).getConnector("mappings")); 
	                     }
	                     catch (e) {
	                             __notifications.showMessage(NOTIFICATION_TYPES.ERROR, "Unknown source element", "" + data[i].sourceElementIds[j] + " mapping cell: " + data[i].id, "err-" + data[i].sourceElementIds[j], false);
	                             console.log("Unknown source element", "" + data[i].sourceElementIds[j] + " mapping cell: " + data[i].id);
	                     }
		    		}
	                for (var j=0; j<data[i].targetElementIds.length; j++) {
	                        try {
	                                rhs.push(_this.target.getElementById(data[i].targetElementIds[j]).getConnector("mappings")); 
	                        }
	                        catch (e) {
	                                __notifications.showMessage(NOTIFICATION_TYPES.ERROR, "Unknown target element", "" + data[i].targetElementIds[j] + " mapping cell: " + data[i].id, "err-" + data[i].sourceElementIds[j], false);
	                                console.log("Unknown target element", "" + data[i].sourceElementIds[j] + " mapping cell: " + data[i].id);
	                        }
	                }
	                if (lhs.length>0 && rhs.length>0) {
	                        _this.graph.addMappingConnection(lhs, rhs, data[i].id, false, data[i].type);
	                }
		    	}
	    	}
	    	if (selectedItemIds!==undefined && selectedItemIds.length>0) {
	    		_this.graph.selectMappingsByIds(selectedItemIds);   
	    	}
	    	_this.graph.update();
	    	
	    	_this.registerEvents();
	    }/*,
	    error: __util.processServerError*/
	});
};

MappingEditor.prototype.processElementHierarchy = function(area, data) {
	this.generateTree(area, data.pojo, null, true, data.pojo.pRoot, data.pojo.disabled, !data.pojo.pRoot && data.statusInfo.includeHeaders===true);
	area.elements[0].setExpanded(true);
};

MappingEditor.prototype.generateTree = function(area, node, parentNode, isSource, processed, disabled, header) {
	var icon = null;
	var terminalMissing = false;
	if (node.state==="ERROR") {
		icon = this.options.icons.error;
	} else if (node.state==="WARNING") {
		icon = this.options.icons.warning;
	} else if (node.state==="REUSING") {
		icon = this.options.icons.reusing;
	} else if (node.state==="REUSED") {
		icon = this.options.icons.reused;
	}
	if (node.state!=="REUSING") {
		if (node.type==="Nonterminal" && this.availableNatures!==undefined && this.availableNatures!==null && this.availableNatures.length > 0) {
			if (node.info===undefined || node.info===null || node.info["mappedNatureClasses"]===undefined || node.info["mappedNatureClasses"].length<this.availableNatures.length) {
				icon = this.options.icons.warning;
				terminalMissing = true;
			}
		}
	}
	if (node.sessionVariable) {
		icon = this.options.icons.database;
	}
	
	var e = area.addElement(node.type, parentNode, node.id, this.formatLabel(node.label), icon, (processed || node.pRoot), node.disabled, !(processed || node.pRoot) && header);
	e.reusing = node.state==="REUSING";
	e.reused = node.state==="REUSED";
	e.terminalMissing = terminalMissing;
		
	if (node.childElements!=null && node.childElements instanceof Array) {
		for (var i=0; i<node.childElements.length; i++) {
			this.generateTree(area, node.childElements[i], e, isSource, processed || node.pRoot, node.disabled, !(processed || node.pRoot) && header);
		}
	}
}

MappingEditor.prototype.formatLabel = function(label) {
	if (label.length > 25) {
		return label.substring(0,25) + "...";
	} else {
		return label;
	}	
};