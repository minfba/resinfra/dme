MappingEditor.prototype.importMapping = function() {
	var _this = this;
	var form_identifier = "upload-file-" + this.mapping.id;
	
	var modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl("forms/import/"),
		identifier: form_identifier,
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],
		additionalModalClasses: "modal-lg",
		displayCallback: function(container, form) {
			$(container).find("form").submit(function() {
				$(this).find(".btn")
					.attr("disabled", "disabled")
					.addClass("disabled")
					.removeAttr("type");
				
				$(this).find(".form-btn-submit")
					.prepend("<i class=\"fas fa-spinner fa-lg fa-spin\"></i> ");
			});
		},
		completeCallback: function() { 
			_this.reloadPage();
		}
	});
	
	modalFormHandler.fileUploadElements.push({
		selector: "#mapping_source",			// selector for identifying where to put widget
		formSource: "forms/fileupload",			// where is the form
		uploadTarget: "async/upload", 			// where to we upload the file(s) to
		multiFiles: false, 						// one or multiple files
		elementChangeCallback: _this.setupImportOptions
	});
		
	modalFormHandler.show(form_identifier);
};


MappingEditor.prototype.exportMapping = function() {
	var _this = this;

	$.ajax({
	    url: __util.composeRelativeUrl("async/export"),
	    type: "GET",
	    dataType: "json",
	    success: function(data) {
	    	blob = new Blob([JSON.stringify(data.pojo)], {type: "application/json; charset=utf-8"});
	    	saveAs(blob, "mapping_" + _this.mapping.id + ".json");
	    },
	    error: __util.processServerError
	});
};

MappingEditor.prototype.setupImportOptions = function(data) {
	$("#importer-type").text(data.pojo.importerMainType);
	$("#importer-subtype").text(data.pojo.importerSubtype);
	
	if (data.pojo.keepIdsAllowed===true) {
		$("#importer-keep-ids").removeClass("hide");
	} else {
		$("#importer-keep-ids").addClass("hide");
	}
	
	$("#importer-options").removeClass("hide");	
};