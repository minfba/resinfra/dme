var editor;

$(document).ready(function() {
	editor = new MappingEditor({
		layout: {
			component : {
				name : "editor-layout",
				closable : false,
				editor: {
					title: "~de.unibamberg.minf.dme.editor.mapping_model"
				},
				sample: {
					outputTitle: "~de.unibamberg.minf.dme.editor.sample.source"
				}
			}, 
			thresholds : {
				twoRowHeight : 350,
				twoColWidth : 850,
				threeColWidth : 1100
			}
		},
		footerOffset: 70,
		icons: {
			warning: __util.getBaseUrl() + "resources/img/warning.png",
			error: __util.getBaseUrl() + "resources/img/error.png",
			reusing: __util.getBaseUrl() + "resources/img/reuse.png",
			reused: __util.getBaseUrl() + "resources/img/reuse.png",
			identifier: __util.getBaseUrl() + "resources/img/identifier.png",
			database: __util.getBaseUrl() + "resources/img/database.png"
		}
	});
	
	for (var icon in editor.options.icons) {
	    if (editor.options.icons.hasOwnProperty(icon)) {
	    	var img = new Image();
	    	img.src = editor.options.icons[icon];
	    	editor.options.icons[icon] = img;
	    }
	}	
	
	$('[data-toggle="tooltip"]').tooltip( { container: 'body' });
});
$(window).resize(function() {
	editor.resize();
});


var MappingEditor = function(options) {
	this.options = options;
	this.mapping = { 
			id: $("#mapping-id").val(),
			owned: $("#mapping-own").val()==="true",
			write: $("#mapping-write").val()==="true",
			sourceId : $("#source-id").val(),
			targetId : $("#target-id").val()
	}
	this.entityId = this.mapping.id;
	
	this.pathname = __util.composeUrl("mapping/editor/" + this.mapping.id + "/");
	this.sourcePath = __util.composeUrl("model/editor/" + this.mapping.sourceId + "/");
	this.targetPath = __util.composeUrl("model/editor/" + this.mapping.targetId + "/");
	
	this.oneDone = false; // -> initGraph()
	
	this.context = null; 

	this.layout = null;
	this.layoutContainer = $(".editor-layout-container");
	this.container = null;
	
	this.containers = {};
	
	this.layoutMaximized = false;

	this.sampleHandler = new SampleHandler({
		mappingId: this.mapping.id,
		requiredAreas: ["input", "output", "mapped"]
	});
	this.handleTranslations();
	
	this.initContainers();
	this.initLayout();
	
	this.resize();
};

MappingEditor.prototype = new BaseEditor();

MappingEditor.prototype.initContainers = function() {
	var _this = this;
	this.initBaseContainers();
	this.containers.sampleContainers.push(this.initComponent({
		componentId: "mapped",
		titleCode: "~de.unibamberg.minf.dme.editor.sample.target",
		contentUrl: this.pathname + "incl/mapped",
		initCallback: function(element, container) {
			_this.sampleHandler.setContainer("mapped", element, container);
		},
		state: {
			resultCounter: true,
			startCount: 0
		}
	}));
}

MappingEditor.prototype.initEditor = function(element, container) {
	var _this = this;
	
	this.context = $(element).find("#mapping-editor-canvas")[0].getContext("2d");
	this.container = $(element).find(".editor-container");
	
	container.on('resize', function() {
		_this.context.canvas.width = container.width;
		_this.context.canvas.height = container.height;		
		if (_this.graph !== null) {
			_this.graph.update();
		}
	});
	
	this.initGraphs();
	this.context.canvas.width = container.width;
	this.context.canvas.height = container.height;		
	if (this.graph !== null) {
		this.graph.update();
	}
};




MappingEditor.prototype.deselectionHandler = function() {
	var _this = editor;
	
	_this.elementContextContainer.text("");

	_this.elementContextContainer.addClass("hide");
	_this.entityContextContainer.removeClass("hide");
};


MappingEditor.prototype.selectionHandler = function(e) {
	var _this = editor;

	_this.elementContextContainer.text("");

	if (e.element instanceof Connection || e.element instanceof Function) {
		_this.getElementDetails(_this.pathname, "mappedconcept", e.element.getId(), _this.elementContextContainer);		
	} else {
		var path;
		if (e.element.template.area===_this.source) {
			path = _this.sourcePath;
		} else {
			path = _this.targetPath;
		}
		_this.getElementDetails(path, e.element.getType(), e.element.getId(), _this.elementContextContainer);
	}
	
	_this.elementContextContainer.removeClass("hide");
	_this.entityContextContainer.addClass("hide");
};

MappingEditor.prototype.editFunctionConnection = function(connectionId, connectionType) {
	var form_identifier = "edit-connection-" + connectionId;
	var _this = this;
	
	modalFormHandler = new ModalFormHandler({
		formUrl: "mappedconcept/" + connectionId + "/form/editFunction",
		identifier: form_identifier,
		additionalModalClasses: "modal-max",
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],     
		displayCallback: function(container, modal) { 
			if (_this.conceptEditor!==undefined && _this.conceptEditor!==null) {
				_this.conceptEditor.dispose();
				_this.conceptEditor = null;
			}
			_this.conceptEditor = new MappedConceptEditor(_this, container, modal, { conceptId: connectionId, conceptType: connectionType }); 
		},
		cancelCallback: function() {
			_this.updateMapping(connectionId);
		}
	});
		
	modalFormHandler.show(form_identifier);
};

MappingEditor.prototype.editExportConnection = function(connectionId, connectionType) {
	var form_identifier = "edit-connection-" + connectionId;
	var _this = this;
	
	modalFormHandler = new ModalFormHandler({
		formUrl: "mappedconcept/" + connectionId + "/form/editExport",
		identifier: form_identifier,
		additionalModalClasses: "modal-lg",
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],     
		displayCallback: function(container, modal) { 
			_this.conceptEditor = new ExportedConceptEditor(_this, container, modal, { conceptId: connectionId, conceptType: connectionType }); 
		},
		cancelCallback: function() {
			_this.updateMapping(connectionId);
		}
	});
		
	modalFormHandler.show(form_identifier);
};

MappingEditor.prototype.switchConnectionType = function(connectionId, connectionType) {
	var _this = this;
	$.ajax({
		url: __util.composeRelativeUrl("mappedconcept/" + connectionId + '/async/switchConceptType'),
	    data: { type : connectionType },
	    type: "POST",
	    success: function(data) {
	    	_this.graph.replaceMappingConnection(connectionId, data.pojo.id, data.pojo.type);
	    	_this.graph.update();
	    	_this.showMessage("success", null, "~de.unibamberg.minf.dme.notification.editor.mapping.saved");
	    }
	});
}


MappingEditor.prototype.updateMapping = function(connectionId) {
	var _this = this;
	$.ajax({
	    url: _this.pathname + "async/getConcept",
	    data: { id : connectionId },
	    type: "GET",
	    success: function(data) {
	    	_this.graph.updateMappingById(connectionId, data.type);
	    	_this.graph.update();
	    }
	});
};

MappingEditor.prototype.changeConceptMappingHandler = function(e) {
	if (e.connection.id===undefined) {
		throw Error("update failed, connection not saved yet");
	}
	var _this = editor;
	
	_this.newConceptMappingHandler(e);
}

MappingEditor.prototype.removeConceptMapping = function(conceptMappingId) {
	if (!__util.isLoggedIn()) {
		__util.showLoginNote();
		return;
	}
	
	var _this = this;
	bootbox.confirm(String.format(__translator.translate("~de.unibamberg.minf.dme.dialog.confirm_delete"), conceptMappingId), function(result) {
		if(result) {
			$.ajax({
			    url: __util.composeRelativeUrl("mappedconcept/" + conceptMappingId + "/async/remove"),
			    type: "POST",
			    dataType: "json",
			    success: function(data) {
			    	_this.graph.deselectAll();
			    	_this.reloadAll();
			    	_this.showMessage("success", null, "~de.unibamberg.minf.dme.notification.editor.mapping.deleted");
			    },
			    error: __util.processServerError
			});
		}
	});
};

MappingEditor.prototype.saveConceptMappingHandler = function(e) {
	var targetIds = [];
	for (var i=0; i<e.connection.to.length; i++) {
		targetIds.push(e.connection.to[i].element.id);
	}
	
	var sourceIds = [];
	for (var i=0; i<e.connection.from.length; i++) {
		sourceIds.push(e.connection.from[i].element.id);
	}
	
	
	var _this = editor;
	$.ajax({
		url: __util.composeRelativeUrl("mappedconcept/" + e.connection.id + '/async/save'),
        type: "POST",
        data: { sourceElementId: sourceIds, targetElementId: targetIds},
        dataType: "json",
        success: function(data) { 
        	e.connection.id = data.pojo.id;
        	
        	_this.showMessage("success", null, "~de.unibamberg.minf.dme.notification.editor.mapping.saved");
        },
        error: function(textStatus) { _this.showMessage("danger", "~de.unibamberg.minf.dme.notification.editor.update_failed.head", "~de.unibamberg.minf.dme.notification.editor.update_failed.body"); }
 	});
	
};

MappingEditor.prototype.triggerEdit = function() {
	if (!__util.isLoggedIn()) {
		__util.showLoginNote();
		return;
	}
	
	var _this = this;
	var form_identifier = "edit-mapping-" + _this.mapping.id;
	var url = __util.getBaseUrl() + "mapping/forms/edit/" + _this.mapping.id;
	
	modalFormHandler = new ModalFormHandler({
		formFullUrl: url,
		identifier: form_identifier,
		additionalModalClasses: "modal-lg",
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],  
        completeCallback: function(d) {
        	if (d.success) {
        		console.log(d);
    			if (d.statusInfo!==undefined && d.statusInfo!==null && d.statusInfo.length==24 && d.statusInfo!==_this.mapping.id) {
    				window.location.replace(__util.composeUrl("mapping/editor/" + d.statusInfo + "/"));
    			} else {
    				window.location.reload();
    			}
        	} else {
        		__notifications.showMessage(NOTIFICATION_TYPES.ERROR, 
	        			__translator.translate(data.message.messageHead), 
	        			__translator.translate(data.message.messageBody));
        	}
		}
	});
		
	modalFormHandler.show(form_identifier);
};

MappingEditor.prototype.triggerPublish = function() {
	if (!__util.isLoggedIn()) {
		__util.showLoginNote();
		return;
	}
	var _this = this;
	bootbox.confirm(String.format(__translator.translate("~de.unibamberg.minf.dme.dialog.confirm_publish"), _this.mapping.id), function(result) {
		if(result) {
			$.ajax({
		        url: __util.composeUrl("mapping/async/publish/" + _this.mapping.id),
		        type: "GET",
		        dataType: "json",
		        success: function(data) { 
		        	if (data.success) {
		        		window.location.reload();
		        	} else {
		        		__notifications.showMessage(NOTIFICATION_TYPES.ERROR, 
			        			__translator.translate(data.message.messageHead), 
			        			__translator.translate(data.message.messageBody));
		        	}
		        },
		        error: function(textStatus) {
		        	__notifications.showMessage(NOTIFICATION_TYPES.ERROR, 
		        			__translator.translate("~de.unibamberg.minf.common.view.forms.servererror.head"), 
		        			__translator.translate("~de.unibamberg.minf.common.view.forms.servererror.body"));
		        }
			});
		}
	});
};

MappingEditor.prototype.triggerDelete = function() {
	if (!__util.isLoggedIn()) {
		__util.showLoginNote();
		return;
	}
	var _this = this;
	bootbox.confirm(String.format(__translator.translate("~de.unibamberg.minf.dme.dialog.confirm_delete"), _this.mapping.id), function(result) {
		if(result) {
			$.ajax({
		        url: __util.composeUrl("mapping/async/delete/" + _this.mapping.id),
		        type: "GET",
		        dataType: "json",
		        success: function(data) { 
		        	if (data.success) {
		        		window.location.reload();
		        	} else {
		        		__notifications.showMessage(NOTIFICATION_TYPES.ERROR, 
			        			__translator.translate(data.message.messageHead), 
			        			__translator.translate(data.message.messageBody));
		        	}
		        },
		        error: function(textStatus) {
		        	__notifications.showMessage(NOTIFICATION_TYPES.ERROR, 
		        			__translator.translate("~de.unibamberg.minf.common.view.forms.servererror.head"), 
		        			__translator.translate("~de.unibamberg.minf.common.view.forms.servererror.body"));
		        }
			});
		}
	});
};


MappingEditor.prototype.handleTranslations = function() {
	__translator.addTranslations(["~de.unibamberg.minf.dme.model.schema.schema",
        "~de.unibamberg.minf.dme.editor.element_model",
        "~de.unibamberg.minf.dme.model.mapping.mapping",
        "~de.unibamberg.minf.dme.model.element.element",
        "~de.unibamberg.minf.dme.dialog.confirm_delete",
        
        "~de.unibamberg.minf.dme.button.expand_all",
        "~de.unibamberg.minf.dme.button.collapse_all",
        "~de.unibamberg.minf.dme.button.expand_from_here",
        "~de.unibamberg.minf.dme.button.collapse_from_here",
        "~de.unibamberg.minf.common.link.reload_data",
        "~de.unibamberg.minf.common.link.reset_view",
        "~de.unibamberg.minf.common.link.delete",
            
        "~de.unibamberg.minf.dme.editor.actions.ensure_connected_visible",
        "~de.unibamberg.minf.dme.editor.actions.reset_position",
        "~de.unibamberg.minf.dme.editor.actions.edit_connection",
        "~de.unibamberg.minf.dme.editor.actions.show_connection",
        "~de.unibamberg.minf.dme.editor.actions.switch_connection_to_function",
        "~de.unibamberg.minf.dme.editor.actions.switch_connection_to_exported",
        
        "~de.unibamberg.minf.dme.editor.sample.source",
        "~de.unibamberg.minf.dme.editor.sample.target",
        "~de.unibamberg.minf.dme.editor.mapping_model",
        
        "~de.unibamberg.minf.dme.button.export",
        "~de.unibamberg.minf.dme.button.import",
        "~de.unibamberg.minf.dme.editor.mapping",
        
        "~de.unibamberg.minf.dme.dialog.confirm_publish",
        "~de.unibamberg.minf.dme.model.mapping.validation.no_pub_schema_drafts",
        
        "~de.unibamberg.minf.dme.notification.editor.update_failed.head",
        "~de.unibamberg.minf.dme.notification.editor.update_failed.body",
        "~de.unibamberg.minf.dme.notification.editor.mapping.deleted",
        "~de.unibamberg.minf.dme.notification.editor.mapping.saved"]);
	__translator.getTranslations();
};