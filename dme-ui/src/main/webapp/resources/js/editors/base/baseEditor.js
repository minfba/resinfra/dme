function BaseEditor() {
	__translator.addTranslations([
	    "~de.unibamberg.minf.common.labels.no_match_found",

	    "~de.unibamberg.minf.common.model.id",
	    "~de.unibamberg.minf.common.model.label",
	  
	    "~de.unibamberg.minf.common.view.forms.servererror.head",
	    "~de.unibamberg.minf.common.view.forms.servererror.body",
	    
	    "~de.unibamberg.minf.dme.model.mapped_concept",
	    "~de.unibamberg.minf.dme.model.mapped_concept.source",
	    "~de.unibamberg.minf.dme.model.mapped_concept.targets",
	    "~de.unibamberg.minf.dme.model.element.element",
	    "~de.unibamberg.minf.dme.model.element.name",
	    "~de.unibamberg.minf.dme.model.element.transient",
	    "~de.unibamberg.minf.dme.model.element.namespace",
	    "~de.unibamberg.minf.dme.model.element.attribute",
	    "~de.unibamberg.minf.dme.model.element.terminal",
	    "~de.unibamberg.minf.dme.model.function.state",
	    "~de.unibamberg.minf.dme.model.function.function",
	    "~de.unibamberg.minf.dme.model.grammar.base_rule",
	    "~de.unibamberg.minf.dme.model.grammar.grammar_layout",
	    "~de.unibamberg.minf.dme.model.grammar.separate",
	    "~de.unibamberg.minf.dme.model.grammar.combined",
	    "~de.unibamberg.minf.dme.model.grammar.state",
	    "~de.unibamberg.minf.dme.model.grammar.grammar",
	    
	    "~de.unibamberg.minf.dme.notification.sample_download_not_available"
	]);
	this.addLayoutTranslations();
	this.addMessagesTranslations();
	
	this.initMessages();
	
	this.vocabularySources = new Array();
	
	this.elementContextContainer = null;
	this.entityContextContainer = null;
}

BaseEditor.prototype.createActionButtons = function(container, contextMenuItems) {
	if (contextMenuItems==undefined) {
		return;
	}
	for (var i=0; i<contextMenuItems.length; i++) {
		if (contextMenuItems[i].key==="-" || contextMenuItems[i].key==undefined) {
			continue;
		}
		button = "<button " +
					"class='btn btn-default btn-sm' " +
					"onclick='editor.performTreeAction(\"" + contextMenuItems[i].key + "\", \"" + contextMenuItems[i].id + "\", \"" + contextMenuItems[i].type + "\"); return false;' type='button'>" +
						"<i class='fas fa-" + contextMenuItems[i].glyphicon + "'></i> " + contextMenuItems[i].label + 
				 "</button> ";
		//container.append(button);
	}
};

BaseEditor.prototype.addVocabularySource = function(name, urlSuffix, params) {
	this.vocabularySources[name] = new Bloodhound({
		  datumTokenizer: Bloodhound.tokenizers.whitespace,
		  queryTokenizer: Bloodhound.tokenizers.whitespace,
		  remote: {
			  url: urlSuffix + "%QUERY" + (params!==undefined ? "?" + params : ""),
			  wildcard: '%QUERY'
		  }
	});
};

BaseEditor.prototype.registerTypeahead = function(element, name, datasource, displayAttr, limit, suggestionCallback, selectionCallback, changeCallback) {
	
	var _this = this;
	element.typeahead(null, {
		name: name,
		hint: false,
		display: displayAttr,
		source: datasource,
		limit: limit,
		templates: {
			empty: ['<div class="tt-empty-message">',
			        	__translator.translate("~de.unibamberg.minf.common.labels.no_match_found"),
			        '</div>'].join('\n'),
			suggestion: function(data) { return suggestionCallback(data); }
		}
	});

	// Executed when a suggestion has been accepted by the user
	if (selectionCallback!==undefined && selectionCallback!==null && typeof selectionCallback==='function') {
		element.bind('typeahead:select typeahead:autocomplete', function(ev, suggestion) {
			selectionCallback(this, suggestion);
		});
	}
	
	// Executed on custom input -> typically needs some validation
	if (changeCallback!==undefined && changeCallback!==null && typeof changeCallback==='function') {
		element.bind('change', function() {
			changeCallback(this, $(this).val());
		});
	}
};

BaseEditor.prototype.validateInput = function(element, urlPrefix, value) {
	var _this = this;
	$.ajax({
        url: urlPrefix + value,
        type: "GET",
        dataType: "json",
        success: function(data) {
        	$(element).closest(".form-group").removeClass("has-error");
        },
        error: function(textStatus) { 
        	$(element).closest(".form-group").addClass("has-error");
        }
	});
};

BaseEditor.prototype.deregisterTypeahead = function(element) {
	typeahead.typeahead('destroy');
};

BaseEditor.prototype.getElementDetails = function(pathPrefix, type, id, container, callback) {
	var _this = this;
	var elementType = this.getElementType(type);
	$.ajax({
		url: pathPrefix + elementType + "/" + id + "/async/get",
        type: "GET",
        dataType: "json",
        data: elementType.startsWith("terminal") ? { n: this.currentNature } : undefined,
        success: function(data) {
        	switch (_this.getElementType(type)) {
				case "element": return _this.processElementDetails(data, callback, container, pathPrefix);
				case "grammar": return _this.processGrammarDetails(data, callback, container, pathPrefix);
				case "function": return _this.processFunctionDetails(data, callback, container, pathPrefix);
				case "mappedconcept": return _this.processMappedConceptDetails(data, callback, container, pathPrefix);
				case "terminal": return _this.processTerminalElement(data, callback, container, pathPrefix);
				case "terminal/missing": return _this.processTerminalElement(data, callback, container, pathPrefix);
				default: throw Error("Unknown element type: " + type);
			}
        },
        error: __util.processServerError
 	});	
};

BaseEditor.prototype.getElementType = function(originalType) {
	var type = originalType;
	if (type==="Nonterminal" || type==="Label") {
		type="element";
	}
	return type.toLowerCase();
};

BaseEditor.prototype.processMappedConceptDetails = function(data, callback, container, pathPrefix) {
	container.append("<h3>" + __translator.translate("~de.unibamberg.minf.dme.model.mapped_concept") + "</h3>");
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.common.model.id"), data.id));
	
	var _this = this;
	var inputIds = [];
	
	for (var elementId in data.elementGrammarIdsMap) {
		if (data.elementGrammarIdsMap.hasOwnProperty(elementId)) {
			inputIds.push(elementId);
		}
	}
	
	container.append("<h3>" + __translator.translate("~de.unibamberg.minf.dme.model.mapped_concept.source") + "</h3>");
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.common.model.id"), inputIds));
	
	container.append("<h3>" + __translator.translate("~de.unibamberg.minf.dme.model.mapped_concept.targets") + "</h3>");
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.common.model.id"), data.targetElementIds));
	
	if (callback!==undefined) {
		callback(data, container, pathPrefix);
	}
};

BaseEditor.prototype.processGrammarDetails = function(data, callback, container, pathPrefix) { 
	container.append("<h3>" + __translator.translate("~de.unibamberg.minf.dme.model.grammar.grammar") + "</h3>");
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.dme.model.grammar.state"), 
			(data.locked!=true && data.error!=true ? "<i class='fas fa-check'></i>&nbsp;" : "") +
			(data.locked==true ? "<i class='fas fa-wrench'></i>&nbsp;" : "") +
			(data.error==true ? "<i class='fas fa-exclamation'></i>&nbsp;" : "") +
			(data.passthrough==true ? "<i class='fas fa-forward'></i>&nbsp;" : ""), 
			false, (data.error==true ? "color-danger" : "") 
		));
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.common.model.id"), data.id));
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.common.model.label"), data.name));
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.dme.model.grammar.base_rule"), data.baseMethod));
	
	if (data.passthrough!=true && data.grammarContainer!=null) {
		if (data.grammarContainer.lexerGrammar!==null && data.grammarContainer.lexerGrammar !=="") {
			container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.dme.model.grammar.grammar_layout"), __translator.translate("~de.unibamberg.minf.dme.model.grammar.separate")));
		} else {
			container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.dme.model.grammar.grammar_layout"), __translator.translate("~de.unibamberg.minf.dme.model.grammar.combined")));
		}
	}
	
	if (callback!==undefined) {
		callback(data, container, pathPrefix);
	}
};

BaseEditor.prototype.processFunctionDetails = function(data, callback, container, pathPrefix) { 
	container.append("<h3>" + __translator.translate("~de.unibamberg.minf.dme.model.function.function") + "</h3>");
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.dme.model.function.state"), 
			(data.locked!=true && data.error!=true ? "<i class='fas fa-check'></i>&nbsp;" : "") +
			(data.locked==true ? "<i class='fas fa-wrench'></i>&nbsp;" : "") +
			(data.error==true ? "<i class='fas fa-exclamation'></i>&nbsp;" : ""), 
			false, (data.error==true ? "color-danger" : "") 
	));
	
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.common.model.id"), data.id));
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.common.model.label"), data.name));
	
	if (callback!==undefined) {
		callback(data, container, pathPrefix);
	}
};

BaseEditor.prototype.processElementDetails = function(data, callback, container, pathPrefix) { 
	container.append("<h3>" + __translator.translate("~de.unibamberg.minf.dme.model.element.element") + "</h3>");
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.common.model.id"), data.id));
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.dme.model.element.name"), data.label));
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.dme.model.element.transient"), data.disabled));
	
	if (callback!==undefined) {
		callback(data, container, pathPrefix);
	}
};

BaseEditor.prototype.processTerminalElement = function(data, callback, container, pathPrefix) {
	container.append("<h3>" + __translator.translate("~de.unibamberg.minf.dme.model.element.terminal") + "</h3>");
	
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.common.model.id"), data.id));
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.dme.model.element.name"), data.name));
	if (data.namespace!==undefined) {
		container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.dme.model.element.namespace"), data.namespace));
	}
	if (data.attribute!==undefined) {
		container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.dme.model.element.attribute"), data.attribute));
	}
	
	if (callback!==undefined) {
		callback(data, container, pathPrefix);
	}
};

BaseEditor.prototype.renderContextTabDetail = function(label, data, pre, classes) {
	var detail = $("<div class=\"editor-property " + (classes===undefined ? "" : classes) + "\">");	
	if (label!=null && label!="") {
		detail.append("<label>" + label + ":</label> ");
	}
	if (pre) {
		detail.append("<pre>" + this.renderData(data) + "</pre>");
	} else {
		detail.append("<span>" + this.renderData(data) + "</span>");
	}
	return detail;
};

BaseEditor.prototype.renderData = function(data) {
	if (data===undefined || data===null || data.length==0 || data===false) {
		return "-";
	} else if (data===true) {
		return "<i class='fas fa-check'></i>";
	} else if (data instanceof Array) {
		var strArray = "";
		for (var i=0; i<data.length; i++) {
			strArray += data[i];
			if (i<data.length-1) {
				strArray += ", ";
			}
		}
		return strArray;
	} else {
		return data;
	}
}

BaseEditor.prototype.loadActivitiesForEntity = function(entityId, container) {
	var _this = this;
	$.ajax({
        url: __util.composeRelativeUrl("async/getChangesForEntity/" + entityId),
        type: "GET",
        dataType: "json",
        success: function(data) { __util.renderActivities(container, null, data); },
        error: function(textStatus) {
        	__notifications.showMessage(NOTIFICATION_TYPES.ERROR, 
        			__translator.translate("~de.unibamberg.minf.common.view.forms.servererror.head"), 
        			__translator.translate("~de.unibamberg.minf.common.view.forms.servererror.body"));
        }
	});
};

BaseEditor.prototype.loadActivitiesForElement = function(elementId, container) {
	var _this = this;
	$.ajax({
		url: __util.composeRelativeUrl("async/getChangesForElement/" + elementId),
        type: "GET",
        dataType: "json",
        success: function(data) { __util.renderActivities(container, elementId, data); },
        error: function(textStatus) {
        	__notifications.showMessage(NOTIFICATION_TYPES.ERROR, 
        			__translator.translate("~de.unibamberg.minf.common.view.forms.servererror.head"), 
        			__translator.translate("~de.unibamberg.minf.common.view.forms.servererror.body"));
        }
	});
};

BaseEditor.prototype.checkEntityState = function() {
	var _this = this;
	var result = false;
	
	$.ajax({
	    url: __util.composeRelativeUrl("state"),
	    type: "GET",
	    async: false,
	    success: function(data) {
	    	if (data===null || data===undefined || data.length==0) {
	    		return;
	    	}
	    	if (data.pojo.processing) {
	    		_this.stateNotificationId = __notifications.showTranslatedMessage(NOTIFICATION_TYPES.INFO, 
	    				"~de.unibamberg.minf.dme.notification.import_processing.head", 
	    				"~de.unibamberg.minf.dme.notification.import_processing.body", 
	    				_this.stateNotificationId, false);
	    		$("#schema-editor-canvas").addClass("hide");
	    		$("#schema-editor-container .model-loading-indicator").removeClass("hide");
	    	} else {
	    		if (data.pojo.error) {
		    		_this.stateNotificationId = __notifications.showTranslatedMessage(NOTIFICATION_TYPES.ERROR, 
		    				"~de.unibamberg.minf.dme.notification.import_error.head", 
		    				"~de.unibamberg.minf.dme.notification.import_error.body", 
		    				_this.stateNotificationId, false);
		    	} else if (data.pojo.ready) {
		    		__notifications.quitMessage(_this.stateNotificationId);
		    		_this.stateNotificationId = undefined;
		    	}
	    		result = true;
	    		$("#schema-editor-canvas").removeClass("hide");
	    		$("#schema-editor-container .model-loading-indicator").addClass("hide");
	    	}	
	    },
	    error: function(jqXHR, textStatus, errorThrown) {
	    	__util.processServerError(jqXHR, textStatus, errorThrown);
	    	result = true;
	    }
	});
	return result;
};

BaseEditor.prototype.reloadPage = function() {
	var _this = this;
	if (this.checkEntityState()===true) {
		window.location.reload();
	} else {
		setTimeout(function() { _this.reloadPage() }, 2000);
	}
};