var SampleHandler = function(config) {
	this.options = $.extend(true, {
		requiredAreas: undefined,
		modelId: undefined,
		mappingId: undefined,
		/* selectors are available as e.g. this.elements.inputTextarea if area 
		 *  is initialized with setContainer("input", container) */
		loadingIndicators: ".sample-loading-indicator",
		
		selectors: {
			input: {
				inputContainer: "#sample-input-textarea-container",
				inputTextarea: "#sample-input-textarea",
				inputTextareaPlaceholder: "#sample-input-textarea-placeholder",
				placeholderSample: ".placeholder-sample",
				placeholderNoSample: ".placeholder-no-sample",
				currentSampleCount: "#currentSampleCount",
				currentTransformedCount: "#currentTransformedCount",
				currentSampleIndex: "#currentSampleIndex",
				sampleSetInput: "#sample-set"
			},
			output: {
				outputResultContainer: "#sample-output-container",
				outputResultArea: "#sample-output-container .sample-output",
				outputResultPlaceholder: "#sample-output-container .sample-output-placeholder",
				outputCounter: ".sample-output-counter",
				outputNextResourceButton: ".btn-sample-next-resource",
				outputPrevResourceButton: ".btn-sample-prev-resource"
			},
			mapped: {
				mappedResultContainer: "#sample-mapped-container",
				mappedResultArea: "#sample-mapped-container .sample-output",
				mappedResultPlaceholder: "#sample-mapped-container .sample-output-placeholder",
				mappedCounter: ".sample-mapped-counter",
				mappedNextResourceButton: ".btn-sample-next-resource",
				mappedPrevResourceButton: ".btn-sample-prev-resource"
			}
		}
	}, config);
	
	this.addTranslations();
	
	this.initialized = false;
	
	// e.g. this.containers.input
	this.containers = {};
	
	// e.g. this.elements.inputTextarea
	this.elements = {};
	
	this.layoutAreas = {};
	this.layoutInitialized = false;
	
	this.logArea = undefined;
	
	this.sampleModified = false;
		
	this.sampleSet = false;
	
	this.getSelectedInputType = function() { return this.containers.input.find("input[name=inputType]:checked").val(); };
	
	this.getEntityId = function() { return this.options.mappingId!==undefined ? this.options.mappingId : this.options.modelId; };
	
	this.getCurrentSampleIndex = null;
	this.setCurrentSampleIndex = null;
	
	this.getSampleResourceCount = null;
	this.setSampleResourceCount = null;
}

SampleHandler.prototype.setContainer = function(area, container, layoutArea) { 
	this.containers[area] = container;
	this.layoutAreas[area] = layoutArea;
	if (!_.isUndefined(this.options.selectors[area])) {
		for (var selector in this.options.selectors[area]) {
			if (this.options.selectors[area].hasOwnProperty(selector)) {
				this.elements[selector] = $(container).find(this.options.selectors[area][selector]);
		    }
		}
		this.options.requiredAreas = _.without(this.options.requiredAreas, area);
	}
	if (this.options.requiredAreas.length==0) {
		this.init();
	}
};

SampleHandler.prototype.setLayoutInitialized = function(initialized) {
	this.layoutInitialized = initialized;
	this.init();
};

SampleHandler.prototype.init = function() {
	if (this.options.requiredAreas.length > 0 || !this.layoutInitialized || this.initialized) {
		return;
	}
	
	var _this = this;
	
	this.initialized = true;
	
	this.elements.inputTextarea.focusout(function() { _this.handleLeaveTextarea(); });
	this.elements.inputTextarea.on("change keyup paste", function() { _this.sampleModified = true; });
	
	this.sampleSet = this.elements.sampleSetInput.val()=="true";
	
	this.getCurrentSampleIndex = function() { return parseInt(Number(this.elements.currentSampleIndex.val())); };
	this.setCurrentSampleIndex = function(index) { this.elements.currentSampleIndex.val(index); };
	
	this.getSampleResourceCount = function() { return parseInt(Number(this.elements.currentSampleCount.val())); };
	this.getSampleTransformedCount = function() { return parseInt(Number(this.elements.currentTransformedCount.val())); };
	
	this.setSampleResourceCount = function(index) { this.elements.currentSampleCount.val(index); };
	this.setSampleTransformedCount = function(index) { this.elements.currentSampleCount.val(index); };
	
	if (this.sampleSet==true) {
		this.processSampleExecutionResult({ pojo: this.getSampleResourceCount()});
		if (this.options.mappingId!=undefined) {
			this.processSampleTransformationResult({ pojo: this.getSampleTransformedCount() });
		}
	}

};

SampleHandler.prototype.handleEnterTextarea = function() {
	this.elements.inputTextarea.removeClass("hide");
	this.elements.inputTextareaPlaceholder.addClass("hide");
	this.elements.inputTextarea.focus();
};

SampleHandler.prototype.handleLeaveTextarea = function() {
	if (this.elements.inputTextarea.val().length==0) {
		this.elements.inputTextarea.addClass("hide");
		this.elements.inputTextareaPlaceholder.removeClass("hide");
	}
};

SampleHandler.prototype.getSupportsMappings = function() {
	for (var area in this.containers) {
		if (this.containers.hasOwnProperty(area) && area==="mapped") {
			return true;
		}
	}
	return false;
};

SampleHandler.prototype.applyAndExecuteSample = function() {
	var _this = this;

	if (this.sampleModified) {
		this.applySample(function() {
			_this.executeSample();
		})
	} else {
		this.executeSample();
	}
};

SampleHandler.prototype.applySample = function(callback) {
	var _this = this;
	$.ajax({
	    url: __util.composeRelativeUrl("async/applySample"),
	    type: "POST",
	    data: { 
	    	sample : _this.elements.inputTextarea.val(),
	    	inputType : this.getSelectedInputType()
	    },
	    dataType: "json",
	    success: function(data) {
	    	if (data.success) { 
	    		_this.logArea.refresh();
	    		_this.sampleModified = false;
	    		callback();
	    	}
	    }, 
	    error: function(jqXHR, textStatus, errorThrown ) {
	    	__util.processServerError(jqXHR, textStatus, errorThrown);
	    }
	});
};

SampleHandler.prototype.setSampleLoading = function() {
	$(this.options.loadingIndicators).removeClass("hide");
	
	this.elements.inputContainer.addClass("hide");
	this.elements.outputResultContainer.addClass("hide");
	if (this.options.mappingId!=undefined) {
		this.elements.mappedResultContainer.addClass("hide");
	}
};

SampleHandler.prototype.setSampleLoaded = function() {
	$(this.options.loadingIndicators).addClass("hide");
	
	this.elements.inputContainer.removeClass("hide");
	this.elements.outputResultContainer.removeClass("hide");
	if (this.options.mappingId!=undefined) {
		this.elements.mappedResultContainer.removeClass("hide");
	}
};

SampleHandler.prototype.executeSample = function() {
	var _this = this;

	this.elements.outputResultArea.text("");
	this.setSampleLoading();
	
	$.ajax({
	    url: __util.composeRelativeUrl("async/executeSample"),
	    type: "GET",
	    data: { inputType : _this.getSelectedInputType() },
	    dataType: "json",
	    //async: false,
	    success: function(data) { 
	    	_this.processSampleExecutionResult(data) 
	    	_this.setSampleLoaded();
	    }, 
	    error: function(jqXHR, textStatus, errorThrown ) {
	    	__util.processServerError(jqXHR, textStatus, errorThrown);
	    	_this.processSampleExecutionResult({ pojo: 0 });
	    	_this.setSampleLoaded();
	    }
	});
};

SampleHandler.prototype.processSampleExecutionResult = function(data) {
	var count = 0;
	if (data!=undefined && data!=null && _.isNumber(data.pojo)) {
		count = data.pojo;
   	}
	if (this.getCurrentSampleIndex()>count) {
		this.setCurrentSampleIndex(count);
	}
	this.setSampleResourceCount(count)
	this.setTabResultCounter("output", count);	
	
	if (this.getSupportsMappings()) {
		this.executeSampleMapping();
	}
	this.getSampleResource();
	if (this.logArea!==undefined) {
		this.logArea.refresh();
	}
};


SampleHandler.prototype.executeSampleMapping = function() {
	var _this = this;
	$.ajax({
	    url: __util.composeRelativeUrl("async/executeSampleMapping"),
	    type: "GET",
	    //data: { sample : _this.sampleTextbox.val() },
	    dataType: "json",
	    async: false,
	    success: function(data) { _this.processSampleTransformationResult(data) },
	    error: function(jqXHR, textStatus, errorThrown ) {
	    	__util.processServerError(jqXHR, textStatus, errorThrown);
	    	_this.processSampleTransformationResult({ pojo: 0 });
	    }
	});
};

SampleHandler.prototype.setTabResultCounter = function(area, count) {
	
	var tabElement = this.layoutAreas[area].tab.element;
	
	var tabCounter = this.layoutAreas[area].tab.element.find(".resultCounter");
	tabCounter.text(count);
	tabCounter.removeClass("hide");
	
	if (count==0) {
		tabCounter.addClass("badge-danger");
		tabCounter.removeClass("badge-success");
	} else {
		tabCounter.removeClass("badge-danger");
		tabCounter.addClass("badge-success");	
	}
	
	
};

SampleHandler.prototype.processSampleTransformationResult = function(data) {
	var count = 0;
	if (data!=undefined && data!=null && _.isNumber(data.pojo)) {
		count = data.pojo;
   	}
	this.setSampleTransformedCount(count)
	this.setTabResultCounter("mapped", count);	
	this.getTransformedResource();
	if (this.logArea!==undefined) {
		this.logArea.refresh();
	}
};


SampleHandler.prototype.loadSampleInput = function() {
	var _this = this;

	$.ajax({
	    url: __util.composeRelativeUrl("async/load_sample"),
	    type: "GET",
	    dataType: "json",
	    data: { t: "input" },
	    success: function(data) {
	    	
	    	_this.elements.inputTextarea.val(data.pojo)
    		
	    	//$('#sample-input-textarea').val(data.pojo) , '#load-sample-input-buttonbar'
	    	//$('#load-sample-input-buttonbar').addClass("hide");

    		
	    },
	    error: __util.processServerError
	});
};

SampleHandler.prototype.downloadSampleInput = function() {
	var _this = this;
	$.ajax({
	    url: __util.composeRelativeUrl("async/download_sample_input"),
	    type: "GET",
	    dataType: "json",
	    success: function(data) {
	    	if (data==null || data.content===undefined || data.content===null) {
	    		bootbox.alert(__translator.translate("~de.unibamberg.minf.dme.editor.sample.notice.empty_sample"));
	    	} else {
		    	if (data.mime.includes("json")) {
		    		data.content = JSON.stringify(data.content);
		    	}
		    	blob = new Blob([data.content], {type: data.mime});
		    	saveAs(blob, data.name);
	    	}
	    },
	    error: __util.processServerError
	});
};

SampleHandler.prototype.downloadSampleOutput = function() {
	var _this = this;
	var form_identifier = "download-sample";
	var modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl("forms/download_output/"),
		additionalModalClasses: "modal-lg",
		identifier: form_identifier,
		setupCallback: function(container, modal) {
			
			var callback = function() {
				$(container).find(".sample-output-counter").text(_this.elements.outputCounter.text());
				if (_this.elements["outputNextResourceButton"].hasClass("disabled")) {
					$(container).find(".btn-sample-next-resource").addClass("disabled");
				} else {
					$(container).find(".btn-sample-next-resource").removeClass("disabled");
				}
				if (_this.elements["outputPrevResourceButton"].hasClass("disabled")) {
					$(container).find(".btn-sample-prev-resource").addClass("disabled");
				} else {
					$(container).find(".btn-sample-prev-resource").removeClass("disabled");
				}	
			}
			
			$(container).find(".btn-sample-prev-resource").bind("click", function() {
				_this.getPrevSampleResource(callback);			
			});
			
			$(container).find(".btn-sample-next-resource").bind("click", function() {
				_this.getNextSampleResource(callback);
			});
			
			
		}
	});
	modalFormHandler.show(form_identifier);
};

SampleHandler.prototype.createDownload = function() {
	var _this = this;
	$.ajax({
	    url: __util.composeRelativeUrl("async/download_link"),
	    type: "GET",
	    dataType: "json",
	    data: { 
	    	data: $('input[name=\"download-data-radios\"]:checked').val(), 
	    	model: $('input[name=\"download-model-radios\"]:checked').val(),
	    	format: $('input[name=\"download-format-radios\"]:checked').val()
	    },
	    success: function(data) {
			if(data.count==0) {
				$("#download-output-modal #download-link-container").html(
					"<span>" +
						__translator.translate("~de.unibamberg.minf.dme.notification.sample_download_not_available") +
					"</span>");
			} else {
		    	$("#download-output-modal #download-link-container").html(
		    			"<span>" +
			    			(data.count==1 ? 
			    					__translator.translate("~de.unibamberg.minf.dme.editor.sample.download.file_count") :
			    					String.format(__translator.translate("~de.unibamberg.minf.dme.editor.sample.download.files_count"), data.count)
			    			) + ": " +
		    				"<a target=\"_blank\" href=\"" + __util.composeRelativeUrl("async/download_output/") + "\">" +
		    					"<i class=\"fa fa-download\" aria-hidden=\"true\"></i> " + 
		    					__translator.translate("~de.unibamberg.minf.common.link.download") +
		    				"</a>  " +
		    			"</span>");
	    	}
	    	
	    },
	    error: __util.processServerError
	});
};



SampleHandler.prototype.uploadAndExecuteSample = function() {
	var _this = this;
	var form_identifier = "edit-root";

	modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl("forms/uploadSample"),
		data: { inputType: this.getSelectedInputType() },
		additionalModalClasses: "modal-lg",
		identifier: form_identifier,
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"},
		                {placeholder: "~*file.uploadcomplete.head", key: "~de.unibamberg.minf.dme.editor.forms.sample_uploaded.head"},
		                {placeholder: "~*file.uploadcomplete.body", key: "~de.unibamberg.minf.dme.editor.forms.sample_uploaded.body"}	
		                ],
		completeCallback: function() { 
			_this.logArea.refresh();
    		_this.sampleModified = false;

    		_this.elements.placeholderSample.removeClass("hide");
    		_this.elements.placeholderNoSample.addClass("hide");
			_this.executeSample(); 
		}
	});
	
	modalFormHandler.fileUploadElements.push({
		selector: "#upload_source",				// selector for identifying where to put widget
		formSource: "forms/fileupload",			// where is the form
		uploadTarget: "async/uploadSample", 	// where to we upload the file(s) to
		multiFiles: false, 						// one or multiple files
		maxFileUploadSize: 52428800,			// 50MB maximum size
		elementChangeCallback: _this.handleValidatedOrFailed
	});	
		
	modalFormHandler.show(form_identifier);
};

SampleHandler.prototype.editSessionData = function() {
var _this = this;
	var form_identifier = "edit-root";

	modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl("forms/sampleData"),
		data: { inputType: this.getSelectedInputType() },
		additionalModalClasses: "modal-lg",
		identifier: form_identifier,
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"},
		                {placeholder: "~*file.uploadcomplete.head", key: "~de.unibamberg.minf.dme.editor.forms.sample_uploaded.head"},
		                {placeholder: "~*file.uploadcomplete.body", key: "~de.unibamberg.minf.dme.editor.forms.sample_uploaded.body"}	
		                ],
		completeCallback: function() { 
			/*_this.logArea.refresh();
    		_this.sampleModified = false;

    		_this.elements.placeholderSample.removeClass("hide");
    		_this.elements.placeholderNoSample.addClass("hide");*/
			//_this.executeSample(); 
		}
	});

	modalFormHandler.show(form_identifier);
};

SampleHandler.prototype.getSampleResource = function(force, callback) {
	var _this = this;
	
	this.setSampleLoading();
	
	$.ajax({
	    url: __util.composeRelativeUrl("async/getSampleResource"),
	    type: "GET",
	    data: { 
	    	index: _this.getCurrentSampleIndex(),
	    	force: (force===true)
	    },
	    dataType: "json",
	    success: function(data) {
	    	if (data!=null && data.success===true) {
	    		_this.elements.outputResultArea.html(_this.buildSampleResourceObject(data, "output"));
	    		_this.elements.outputResultArea.removeClass("hide");
		    	_this.elements.outputResultPlaceholder.addClass("hide");
		    	_this.setSampleLoaded();
	    	} else {
	    		_this.elements.outputResultArea.text("");
		    	_this.elements.outputResultArea.addClass("hide");
		    	_this.elements.outputResultPlaceholder.removeClass("hide");
		    	_this.setSampleLoaded();
	    	}
	    	_this.setSampleNavigationBar();
	    	
	    	if (callback != undefined && typeof callback == 'function') {
	    		callback();
	    	}
	    },
	    error: function() {
	    	_this.elements.outputResultArea.text("");
	    	_this.elements.outputResultArea.addClass("hide");
	    	_this.elements.outputResultPlaceholder.removeClass("hide");
	    	_this.setSampleLoaded();
	    	_this.setSampleNavigationBar();
	    	
	    	if (callback != undefined && typeof callback == 'function') {
	    		callback();
	    	}
	    }
	});
};


SampleHandler.prototype.getTransformedResource = function(force) {
	var _this = this;
	$.ajax({
	    url: __util.composeRelativeUrl("async/getTransformedResource"),
	    type: "GET",
	    data: { 
	    	index: _this.getCurrentSampleIndex(),
	    	force: (force===true)
	    },
	    dataType: "json",
	    success: function(data) {
	    	if (data!=null && data.success===true) {
	    		if (data!=null && ((data!=undefined && data.pojo!=null) || data.statusInfo.oversize)) {
		    		_this.elements.mappedResultArea.html(_this.buildSampleResourceObject(data, "mapped"));
		    	} else {
		    		_this.elements.mappedResultArea.text(__translator.translate("~de.unibamberg.minf.dme.editor.sample.notice.no_transformed_resource"));
		    	}
		    	_this.elements.mappedResultArea.removeClass("hide");
		    	_this.elements.mappedResultPlaceholder.addClass("hide");
	    	} else {
	    		_this.elements.mappedResultArea.text("");
		    	_this.elements.mappedResultArea.addClass("hide");
		    	_this.elements.mappedResultPlaceholder.removeClass("hide");
	    	}
	    	_this.setSampleNavigationBar();
	    },
	    error: function() {
	    	_this.elements.mappedResultArea.text("");
	    	_this.elements.mappedResultArea.addClass("hide");
	    	_this.elements.mappedResultPlaceholder.removeClass("hide");
	    }
	});
};


SampleHandler.prototype.buildSampleResourceObject = function(data, type) {
	if (data.statusInfo.available===true) {
		if (data.statusInfo.oversize===false) {
			return $("<ul>").append(this.buildSampleResource(data.pojo));
		} else {
			
			let getResource = type=="mapped" ? "editor.sampleHandler.getTransformedResource(true)" : "editor.sampleHandler.getSampleResource(true)";
			return "<p>" +
						String.format(__translator.translate("~de.unibamberg.minf.dme.editor.sample.notice.oversize"),
								"<a href=\"#\" onclick=\"" + getResource + "; return false;\">" +
									"<i class=\"fas fa-eye\"></i> " + __translator.translate("~de.unibamberg.minf.dme.editor.sample.notice.oversize.show") + 
								"</a>",
								"<a href=\"#\" onclick=\"editor.sampleHandler.downloadSampleOutput(); return false;\">" + 
									"<i class=\"fas fa-file-download\"></i> " + __translator.translate("~de.unibamberg.minf.dme.editor.sample.notice.oversize.download") + 
								"</a>") +
					"</p>";
		}
	}
};

SampleHandler.prototype.setSampleNavigationBar = function() {
	var counterText = "- / -";
	
	if (this.getSampleResourceCount()>0) {
		counterText = (this.getCurrentSampleIndex()+1) + " / " + this.getSampleResourceCount();
	} 
	
	this.elements.outputCounter.text(counterText);
	this.setSampleNavigationButtons("outputPrevResourceButton", "outputNextResourceButton");
	if (this.getSupportsMappings()) {
		this.elements.mappedCounter.text(counterText);
		this.setSampleNavigationButtons("mappedPrevResourceButton", "mappedNextResourceButton");
	}
};

SampleHandler.prototype.setSampleNavigationButtons = function(prevResourceButton, nextResourceButton) {
	if (this.getCurrentSampleIndex() > 0) {
		this.elements[prevResourceButton].removeClass("disabled");
	} else {
		this.elements[prevResourceButton].addClass("disabled");
	}
	if (this.getCurrentSampleIndex() < this.getSampleResourceCount()-1) {
		this.elements[nextResourceButton].removeClass("disabled");
	} else {
		this.elements[nextResourceButton].addClass("disabled");
	}	
}


SampleHandler.prototype.getPrevSampleResource = function(sampleResourceLoadedCallback) {
	var index = this.getCurrentSampleIndex();
	if (index > 0) {
		this.setCurrentSampleIndex(index-1);
		this.getSampleResource(false, sampleResourceLoadedCallback);
		if (this.options.mappingId!==undefined) {
			this.getTransformedResource();
		}
	}
};

SampleHandler.prototype.getNextSampleResource = function(sampleResourceLoadedCallback) {
	var index = this.getCurrentSampleIndex();
	if (index < this.getSampleResourceCount()-1) {
		this.setCurrentSampleIndex(index+1);
		this.getSampleResource(false, sampleResourceLoadedCallback);
		if (this.options.mappingId!==undefined) {
			this.getTransformedResource();
		}
	}
};

SampleHandler.prototype.buildSampleResource = function(resource, parentItem) {
	var items = [];

	for (var i=0; i<Object.getOwnPropertyNames(resource).length; i++) {
		var key = Object.getOwnPropertyNames(resource)[i];
		if (key==="#") {
			continue;
		}
		
		if (key==="~") {
			parentItem.append("<button onclick=\"editor.sampleHandler.toggleSampleOutputValue(this); return false;\" class=\"btn btn-inline btn-xs sample-output-value-expanded\">" +
								"<i class=\"fa fa-minus-square\" aria-hidden=\"true\"></i>" +
								"<i class=\"fa fa-plus-square\" aria-hidden=\"true\"></i>" +
							  "</button>");
			parentItem.append("<span class=\"sample-output-value\">" + resource[key] + "</span>");
			parentItem.append("<span class=\"sample-output-value-placeholder\" style=\"display: none;\">...</span>");
			continue;
		}
		
		var value = resource[key];
		if (Array.isArray(value)) {
			for (var j=0; j<value.length; j++) {
				items.push(this.buildSampleResourceItem(key, value[j]));
			}
		} else {
			items.push(this.buildSampleResourceItem(key, value));
		}
	
	}
	return items;
};

SampleHandler.prototype.buildSampleResourceItem = function(key, resource) {
	
	var item = $("<li>");
	var displayKey;
	if (key.indexOf("|")>0) {
		displayKey = key.substring(0, key.indexOf("|"));
	} else {
		displayKey = key;
	}
	
	item.append("<span class=\"sample-output-key\">" + displayKey + "</span>");
	
	var subItems = this.buildSampleResource(resource, item);
	if (subItems.length > 0) {
		item.append($("<ul>").append(subItems));
	}
	
	return item;
};


SampleHandler.prototype.buildSampleResourceValue = function(resource, parentItem, subItems) {
	var key = Object.getOwnPropertyNames(resource)[0];
	if (key==="") {
		parentItem.append(": <span class=\"sample-output-value\">" + this.escapeValue(resource[key]) + "</span>");
		return 0;
	} else {
		subItems.append(this.buildSampleResource(resource));
		return 1;
	}
};

SampleHandler.prototype.escapeValue = function(value) {
	return value.replace("\n", "\\n");
}

SampleHandler.prototype.toggleSampleOutputValue = function(button) {
	if ($(button).hasClass("sample-output-value-expanded")) {
		$(button).addClass("sample-output-value-collapsed");
		$(button).removeClass("sample-output-value-expanded");
		
		$(button).siblings(".sample-output-value").hide();
		$(button).siblings(".sample-output-value-placeholder").show();
	} else {
		$(button).addClass("sample-output-value-expanded");
		$(button).removeClass("sample-output-value-collapsed");
		
		$(button).siblings(".sample-output-value").show();
		$(button).siblings(".sample-output-value-placeholder").hide();
	}
};

SampleHandler.prototype.newSampleSession = function() {
	var _this = this;
	sessions.newSession(_this.getEntityId(), function() {
		window.location.reload();
	});
};

SampleHandler.prototype.deleteSampleSession = function() {
	var _this = this;
	sessions.deleteSession(_this.getEntityId(), function() {
		window.location.reload();
	});
};


SampleHandler.prototype.addTranslations = function() {
	__translator.addTranslations([
	    "~de.unibamberg.minf.common.link.download",
	    
	    "~de.unibamberg.minf.common.view.forms.servererror.head",
	    "~de.unibamberg.minf.common.view.forms.servererror.body",

	    "~de.unibamberg.minf.dme.editor.sample.notice.empty_sample",
	    "~de.unibamberg.minf.dme.editor.sample.notice.oversize",
	    "~de.unibamberg.minf.dme.editor.sample.notice.oversize.download",
	    "~de.unibamberg.minf.dme.editor.sample.notice.oversize.show",
	    "~de.unibamberg.minf.dme.editor.sample.notice.oversize.not_loadable",
	    "~de.unibamberg.minf.dme.editor.sample.notice.no_transformed_resource",
	    "~de.unibamberg.minf.dme.editor.sample.download.file_count",
	    "~de.unibamberg.minf.dme.editor.sample.download.files_count",
	    "~de.unibamberg.minf.dme.editor.forms.sample_uploaded.head",
	    "~de.unibamberg.minf.dme.editor.forms.sample_uploaded.body"
	]);
	// called by owning editor__translator.getTranslations();
};