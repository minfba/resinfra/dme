BaseEditor.prototype.addMessagesTranslations = function() {};

BaseEditor.prototype.initMessages = function() {
	this.messageOptions = {
		containerSelector: "#editor-messages-container",
		defaultTimeout: 3000,
		fadeSpeed: 300
	}
	this.messageContainer = $(this.messageOptions.containerSelector);
};

// types: success, info, warning, danger
BaseEditor.prototype.showMessage = function(type, head, body, timeout) {
	var messageTimeout = (timeout==undefined||timeout==null) ? this.messageOptions.defaultTimeout : timeout;
	
	var message = this.renderMessage(type, head, body);
	this.messageContainer.html(message);
	
	// Quit messages
	if (type=="success") {
		setTimeout( function() {
			message.fadeOut(400, function() { message.remove() }); 
		}, messageTimeout);
	}
};

BaseEditor.prototype.renderMessage = function(type, head, body) {
	var message = $("<span>");
	message.addClass("editor-message-" + type);
	if (head!=undefined && head!=null && head.length>0) {
		message.html("<strong>" + this.getMessageIcon(type) + __translator.translate(head) + "</strong>&nbsp;");
	} else {
		message.html(this.getMessageIcon(type));
	}
	if (body!=undefined && body!=null && body.length>0) {
		message.append(__translator.translate(body) + "&nbsp;");
	}
	message.append(new Date().toLocaleTimeString());
	return message;
};

BaseEditor.prototype.getMessageIcon = function(type) {
	switch(type) {
	    case "success":
	        return "<i class='fas fa-check'></i>&nbsp;";
	    case "info":
	        return "<i class='fas fa-info'></i>&nbsp;";
	    case "warning":
	        return "<i class='fas fa-exclamation'></i>&nbsp;";
	    case "danger":
	    	return "<i class='fas fa-exclamation'></i>&nbsp;";
	} 
	return "";
};