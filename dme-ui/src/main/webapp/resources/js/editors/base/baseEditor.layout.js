BaseEditor.prototype.addLayoutTranslations = function() {
	__translator.addTranslations([
	    "~de.unibamberg.minf.common.link.close",
	    "~de.unibamberg.minf.common.link.maximize",
	    "~de.unibamberg.minf.common.link.minimize",
	    "~de.unibamberg.minf.common.link.open_new_window",

	    "~de.unibamberg.minf.dme.editor.sample.input",
	    "~de.unibamberg.minf.dme.editor.sample.output",
	    "~de.unibamberg.minf.dme.editor.element_model",
	    "~de.unibamberg.minf.dme.editor.sample.log",
	    "~de.unibamberg.minf.dme.editor.history",
	    "~de.unibamberg.minf.dme.editor.sample.title",
	    "~de.unibamberg.minf.dme.editor.properties"
	]);
};

BaseEditor.prototype.initBaseContainers = function() {
	var _this = this;
		
	this.containers.sampleContainers = [];
	this.containers.sampleContainers.push(this.initComponent({
		componentId: "input",
		titleCode: "~de.unibamberg.minf.dme.editor.sample.input",
		contentUrl: __util.composeRelativeUrl("incl/sample"),
		initCallback: function(element, container) {
			_this.sampleHandler.setContainer("input", element, container);
		}
	}));
	this.containers.sampleContainers.push(this.initComponent({
		componentId: "output",
		titleCode: this.options.layout.component.sample.outputTitle,
		contentUrl: __util.composeRelativeUrl("incl/output"),
		initCallback: function(element, container) {
			_this.sampleHandler.setContainer("output", element, container);
		},
		state: {
			resultCounter: true,
			startCount: 0
		}
	}));
	
	this.containers.editorContainer = this.initComponent({
		componentId: "editor", 
		titleCode: this.options.layout.component.editor.title,
		contentUrl: __util.composeRelativeUrl("incl/editor"),
		initCallback: function(element, container) {
			_this.initEditor(element, container);
		},
		state : {
			addClass : "content-pane-no-overflow"
		}
	});

	this.containers.logContainer = this.initComponent({
		componentId: "log", 
		titleCode: "~de.unibamberg.minf.dme.editor.sample.log",
		contentHtml: '<ul id="schema-editor-log" class="log"></ul>',
		initCallback: function(element, container) {
			_this.logArea = new LogArea({
				pathPrefix :  __util.getBaseUrl() + "sessions/",
				entityId : _this.entityId,
				logList: $(element).find("ul#schema-editor-log"),
			});
			_this.sampleHandler.logArea = _this.logArea;
		}
	});
	

	this.containers.activityContainer = this.initComponent({
		componentId: "history", 
		titleCode: "~de.unibamberg.minf.dme.editor.history",
		contentHtml: "<div id='schema-context-container' class='editor-content-container'><div id='activity-history'></div></div>",
		initCallback: function(element, container) {
			_this.loadActivitiesForEntity(_this.entityId, $(element).find("#activity-history"))
		}
	});
	
	this.containers.propertiesContainer = this.initComponent({
		componentId: "properties", 
		titleCode: "~de.unibamberg.minf.dme.editor.properties",
		contentUrl: __util.composeRelativeUrl("incl/properties"),
		initCallback: function(element, container) {
			_this.elementContextContainer = $("#editor-element-properties");
			_this.entityContextContainer = $("#editor-entity-properties");
		}
	});
};

BaseEditor.prototype.initComponent = function(c) {
	var state = $.extend(true, {
		html: c.contentHtml,
    	contentUrl: c.contentUrl,
    	initCallback: c.initCallback
    }, c.state);
	
	return {
        type: "component",
        id: c.componentId,
        componentName: this.options.layout.component.name,
        title: __translator.translate(c.titleCode),
        isClosable: this.options.layout.component.closable,
        componentState: state
    };
};

BaseEditor.prototype.initLayout = function() {
	var _this = this;
	
	var layoutRows = (this.layoutContainer.innerHeight() < this.options.layout.thresholds.twoRowHeight ? 1 : 2);
	var layoutColumns = (this.layoutContainer.innerWidth() < this.options.layout.thresholds.threeColHeight ? (this.layoutContainer.innerWidth() < this.options.layout.thresholds.twoColHeight ? 1 : 2) : 3);
	
	var mainRow = { type: 'row', content: [] };
	var centerStack = { type: "stack", content: [ this.containers.editorContainer ] };
	var centerColumn = { type:'column', content: [ centerStack ] };
		
	var sampleStack = { 
		type: "stack",
		title: __translator.translate("~de.unibamberg.minf.dme.editor.sample.title"),
		isClosable: this.options.layout.component.closable,
		content: this.containers.sampleContainers
	};
		
	if (layoutRows==2) {
		centerStack.height = 70;
		this.containers.logContainer.height = 30;
		centerColumn.content.push(this.containers.logContainer);
	} else {
		centerStack.content.push(this.containers.logContainer);
	}
	
	if (layoutColumns==1) {
		centerStack.content.push(sampleStack);
		centerStack.content.push(this.containers.activityContainer);
		centerStack.content.push(this.containers.propertiesContainer);
		mainRow.content.push(centerColumn);
	} else {
		mainRow.content.push(sampleStack);
		mainRow.content.push(centerColumn);
		if (layoutColumns==2) {
			centerStack.content.push(this.containers.activityContainer);
			centerStack.content.push(this.containers.propertiesContainer);
			sampleStack.width = 34;
			centerColumn.width = 64;
		} else {
			if (layoutRows==2) {
				this.containers.propertiesContainer.height = 50;
				mainRow.content.push({ type: "column", content: [ this.containers.propertiesContainer, this.containers.activityContainer ] });
			} else {
				mainRow.content.push({ type: "stack", content: [ this.containers.propertiesContainer, this.containers.activityContainer ] });
			}
			sampleStack.width = 25;
			centerColumn.width = 55;
			this.containers.activityContainer.width = 20;
		}
	}

	var config = {
		settings:{
	        showPopoutIcon: false,
	        showMaximiseIcon: false,
	        showCloseIcon: false
	    }, labels: {
	        close: __translator.translate("~de.unibamberg.minf.common.link.close"),
	        maximise: __translator.translate("~de.unibamberg.minf.common.link.maximize"),
	        minimise: __translator.translate("~de.unibamberg.minf.common.link.minimize"),
	        popout: __translator.translate("~de.unibamberg.minf.common.link.open_new_window")
	    }, dimensions: {
	        headerHeight: 28,
	    }, content: [ mainRow ]
	};

	this.layout = new window.GoldenLayout( config, $('.editor-layout-container') );
	this.layout.on('initialised', function() { _this.handleLayoutInitialized() });
	
	this.layout.registerComponent(this.options.layout.component.name, function(container, state) {_this.registerComponent(container, state);});
	this.layout.init();
};

BaseEditor.prototype.registerComponent = function(container, state) {
	container.getElement().addClass("content-pane");
	
	if (state.resultCounter) {
		var counter = $('<div class="resultCounter badge badge-success">' + state.startCount + '</div>');
		if(state.startCount==0) {
			counter.addClass("hide");
		}
		container.on('tab', function(tab){
            tab.element.append(counter);
        });
	}
     
	if (!_.isUndefined(state.addClass)) {
		container.getElement().addClass(state.addClass);
	}
	if (state.html!==null && state.html!==undefined) {
		container.getElement().html(state.html);
	}
	
	// This is not nice, but ensures editor is there and context menu works
	var _this = this;
	setTimeout(function(){ 
		if (state.contentUrl!==null && state.contentUrl!==undefined) {
			_this.loadInclude(state.contentUrl, container.getElement(), container, state.initCallback);
		} else if (!_.isUndefined(state.initCallback) && _.isFunction(state.initCallback)) {
	    	state.initCallback(container.getElement(), container);
	    	_this.resize();
	    } 
	}, 250);
};

BaseEditor.prototype.handleLayoutInitialized = function() {
	this.sampleHandler.setLayoutInitialized(true);
};

BaseEditor.prototype.loadInclude = function(url, element, container, callback) {
	var _this = this;
	$.ajax({
	    url: url,
	    type: "GET",
	    dataType: "html",
	    success: function(html) {
	    	element.html(html);
	    	if (!_.isUndefined(callback) && _.isFunction(callback)) {
		    	callback(element, container);
		    }
	    	_this.resize();
	    },
	    error: __util.processServerError
	});
};

BaseEditor.prototype.resize = function() {
	var height = $(window).height() - $(".editor-layout-container").position().top - 20;
	if ($("footer").css("display")!=="none") {
		height -= $("footer").outerHeight(); 
	}
	this.layout.updateSize(undefined, height);
};

BaseEditor.prototype.toggleLayout = function() {
	if (!this.layoutMaximized) {
		$("#btn-layout-toggle").html('<i class="far fa-window-restore"></i>');
		$("header").hide();
		$("footer").hide();
	} else {
		$("#btn-layout-toggle").html('<i class="far fa-window-maximize"></i>');
		$("header").show();
		$("footer").show();
	}
	this.layoutMaximized = !this.layoutMaximized;
	this.resize();
};