var grammarEditor;

var GrammarEditor = function(modal, options) {
	this.options = $.extend({ 	
		//pathPrefix: "",
		entityId : "",
		grammarId : ""
	}, options)
	
	this.modal = modal;
	this.combinedGrammar = true;
	this.originalMode = "";
	this.originalModeModified = false;
	this.grammarModified = false;
	this.validated = false;
	this.error = $(this.modal).find("#error").val()=="true";
	this.schemaId = this.options.entityId;
	this.grammarId = this.options.grammarId;
	this.pathname = __util.composeRelativeUrl("grammar/" + this.grammarId);
	
	this.processGrammarModal = null;
	
	this.svg = null;
	
	__translator.addTranslations(["~de.unibamberg.minf.common.link.ok",
	                              "~de.unibamberg.minf.common.link.error",
	                              "~de.unibamberg.minf.common.link.validated",
	                              "~de.unibamberg.minf.common.link.modified",
	                              "~de.unibamberg.minf.common.link.validation_required",
	                              
	                              "~de.unibamberg.minf.dme.dialog.confirm_remove_file",
	                              
	                              "~de.unibamberg.minf.dme.form.grammar.generated_files",
	                              	                              
	                              "~de.unibamberg.minf.dme.model.grammar.passthrough"]);
	__translator.getTranslations();
	
	
	this.init();
}

GrammarEditor.prototype.registerTabs = function() {
	this.deregisterTabs();
	this.modal.find(".nav-tabs a").on('click', function (event) {
			event.preventDefault();
			$(this).tab('show');
		});
}

GrammarEditor.prototype.deregisterTabs = function() {
	this.modal.find(".nav-tabs a").off('click');
};

GrammarEditor.prototype.addFile = function() {
	var fileindex = -1;
	$("#grammar-panes .tab-pane").each(function() {
		let compindex=Number($(this).data("fileindex")); 
		if (compindex>fileindex) {
			fileindex=compindex; 
		}
	});
	
	fileindex++;

	var _this = this;
	$.ajax({
        url: _this.pathname + "/includes/inclAuxFilePane",
        type: "GET",
        data: {index : fileindex},
        dataType: "html",
        success: function(data) {
        	$("#grammar-panes").append($(data));    	
        	
    		$.ajax({
		        url: _this.pathname + "/includes/inclAuxFileTab",
		        type: "GET",
		        data: {index : fileindex},
		        dataType: "html",
		        success: function(data) {
					let tab = $(data);
		        	$("#grammar-tabs").append(tab);    	
					_this.registerTabs();
					tab.find("a").tab('show');
		        },
		        error: function(textStatus) { }
			});
        	
        },
        error: function(textStatus) { }
	});
};

GrammarEditor.prototype.removeFile = function(index) {
	var _this = this;

	bootbox.confirm(__translator.translate("~de.unibamberg.minf.dme.dialog.confirm_remove_file"), function(result) {
		if(result) {
			_this.deregisterTabs();
			_this.modal.find('#grammar-panes .tab-pane[data-fileindex="' + index + '"]').remove();
			_this.modal.find('#grammar-tabs .nav-item[data-fileindex="' + index + '"]').remove();
			_this.modal.find('#grammar-tabs li:first-child a').tab('show') // Select first tab
			_this.registerTabs();
		}
	});
};
	
GrammarEditor.prototype.updateGrammarState = function() {
	var state = "";
	if ($(this.modal).find("#passthrough").val()=="true") {
		state = "<i class='fas fa-forward'></i> " + __translator.translate("~de.unibamberg.minf.dme.model.grammar.passthrough");
		this.setSampleParseFunctionality(false);
	} else if (this.originalModeModified || this.grammarModified) {
		state = "<i class='fas fa-info fa-color-info'></i> " + __translator.translate("~de.unibamberg.minf.common.link.modified") + "; " + __translator.translate("~de.unibamberg.minf.common.link.validation_required");
		this.setSampleParseFunctionality(false);
	} else if (this.error) {
		state = "<i class='fas fa-exclamation fa-color-danger'></i> " + __translator.translate("~de.unibamberg.minf.common.link.error");
		this.setSampleParseFunctionality(false);
	} else if (this.validated) {
		state = "<i class='fas fa-check fa-color-success'></i> " + __translator.translate("~de.unibamberg.minf.common.link.validated");
		this.setSampleParseFunctionality(true);
	} else {
		state = "<i class='fas fa-check'></i> " + __translator.translate("~de.unibamberg.minf.common.link.ok");
		this.setSampleParseFunctionality(true);
	}
	$(this.modal).find(".grammar_state").html(state);
};

GrammarEditor.prototype.setSampleParseFunctionality = function(enabled) {
	if (enabled) {
		$(this.modal).find(".btn-parse-sample").removeClass("disabled");
		$(this.modal).find(".btn-parse-sample").removeClass("btn-warning");
		$(this.modal).find(".btn-parse-sample").addClass("btn-info");
	} else {
		$(this.modal).find(".btn-parse-sample").addClass("disabled");
		$(this.modal).find(".btn-parse-sample").addClass("btn-warning");
		$(this.modal).find(".btn-parse-sample").removeClass("btn-info");
	}
};

GrammarEditor.prototype.init = function() {
	var _this = this;
	
	if ($(this.modal).find("#passthrough").val()=="true") {
		this.originalMode = "passthrough";
		this.setLexerParserPassthrough();
		$(this.modal).find(".lexer-parser-option-passthrough").prop("checked", "checked");
	} else if ($(this.modal).find(".grammarContainer_lexerGrammar").val()!==null && $(this.modal).find(".grammarContainer_lexerGrammar").val()!=="") {
		this.originalMode = "separate";
		this.setLexerParserSeparate();
		$(this.modal).find(".lexer-parser-option-separate").prop("checked", "checked");
	} else {
		this.originalMode = "combined";
		this.setLexerParserCombined();
		$(this.modal).find(".lexer-parser-option-combined").prop("checked", "checked");
	}
	
	this.updateGrammarState();
	
	$(this.modal).find(".grammarContainer_lexerGrammar").on('change keyup paste', function() {
		_this.grammarModified = true;
		_this.updateGrammarState();
	});
	$(this.modal).find(".grammarContainer_parserGrammar").on('change keyup paste', function() {
		_this.grammarModified = true;
		_this.updateGrammarState();
	});
	
	$(this.modal).find(".lexer-parser-option").change(function() {
		  if($(this).val()=="combined") {
			  _this.setLexerParserCombined();
		  } else if($(this).val()=="passthrough") {
			  _this.setLexerParserPassthrough();
		  } else{
			  _this.setLexerParserSeparate();
		  }
		  $(_this.modal).modal("layout");
		  _this.updateGrammarState();
	});
};

GrammarEditor.prototype.showHelp = function() {
	var _this = this;
	
	var form_identifier = "edit-grammar-help";
	modalFormHandler = new ModalFormHandler({
		formUrl: "grammar/" + this.grammarId + "/async/help/editGrammar",
		identifier: form_identifier,
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],
		additionalModalClasses: "modal-lg"
	});
	modalFormHandler.show(form_identifier);
};

GrammarEditor.prototype.setLexerParserCombined = function() {	
	$(this.modal).find(".non-passthrough-only").removeClass("hide");
	$(this.modal).find(".passthrough-only").addClass("hide");
	$(this.modal).find("#passthrough").val("false");
	
	$(this.modal).find(".form-group-lexer-grammar").addClass("hide");	
		
	this.combinedGrammar = true;
	
	if (this.originalMode!=="combined") {
		this.originalModeModified = true;
	} else {
		this.originalModeModified = false;
	}
}

GrammarEditor.prototype.setLexerParserSeparate = function() {	
	$(this.modal).find(".non-passthrough-only").removeClass("hide");
	$(this.modal).find(".passthrough-only").addClass("hide");	
	$(this.modal).find("#passthrough").val("false");
	
	$(this.modal).find(".form-group-lexer-grammar").removeClass("hide");
	
	this.combinedGrammar = false;
	
	if (this.originalMode!=="separate") {
		this.originalModeModified = true;
	} else {
		this.originalModeModified = false;
	}
}

GrammarEditor.prototype.setLexerParserPassthrough = function() {
	$(this.modal).find(".non-passthrough-only").addClass("hide");
	$(this.modal).find(".passthrough-only").removeClass("hide");
	$(this.modal).find("#passthrough").val("true");
	
	this.combinedGrammar = false;
	
	if (this.originalMode!=="passthrough") {
		this.originalModeModified = true;
	} else {
		this.originalModeModified = false;
	}
}

GrammarEditor.prototype.validateGrammar = function() {
	var _this = this;	
	var form_identifier = "process-grammar";
	this.processGrammarModal = new ModalFormHandler({
		formUrl: "grammar/" + this.grammarId + "/async/processGrammarDialog",
		identifier: form_identifier,
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],
		displayCallback: function() { 
			_this.validated = false;
			_this.grammarModified = true;
			_this.updateGrammarState();
			_this.uploadGrammar(); 
		},
		additionalModalClasses: "modal-lg",
		completeCallback: function() {_this.reload();}
	});
	this.processGrammarModal.show(form_identifier);
};

GrammarEditor.prototype.uploadGrammar = function() {
	var _this = this;
	
	this.setGrammarProcessingPanelStatus("grammar-uploading", "loading");
	
	var auxFiles = [];
	
	$(".auxiliary-file-pane").each(function() {
		let auxFile = {};
		auxFile.fileType = $(this).find(".auxiliary-file-type").val();
		auxFile.content = $(this).find(".auxiliary-file-content").val();
		
		auxFiles.push(auxFile);
	});
	
	var gc = {};
	gc.lexerGrammar = _this.combinedGrammar ? null : $(_this.modal).find(".grammarContainer_lexerGrammar").val();
	gc.parserGrammar = $(_this.modal).find(".grammarContainer_parserGrammar").val();
	gc.auxiliaryFiles = auxFiles;
    
	
	$.ajax({
	    url: _this.pathname + "/async/upload?combined=" + _this.combinedGrammar,
	    type: "POST",
	    data: JSON.stringify(gc),
	    dataType: "json",
	    contentType: "application/json; charset=utf-8",
	    success: function(data) {
	    	if (data.success) {
	    		_this.setGrammarProcessingPanelStatus("grammar-uploading", "success");
	    		_this.setGrammarProcessingPanelSuccessFiles("grammar-uploading", data.pojo);
	    	   	_this.parseGrammar();
	    	} else {
	    		_this.setGrammarProcessingPanelStatus("grammar-uploading", "error");
	    		_this.setGrammarProcessingPanelErrors("grammar-uploading", data.objectErrors, data.fieldErrors)
	    	}
	    	$(window).trigger('resize');
	    }, error: function(jqXHR, textStatus, errorThrown ) {
	    	__util.processServerError(jqXHR, textStatus, errorThrown);
	    	_this.setGrammarProcessingPanelStatus("grammar-uploading", "error");
	    }
	});
};

GrammarEditor.prototype.parseGrammar = function() {
	var _this = this;
	
	this.setGrammarProcessingPanelStatus("grammar-parsing", "loading");
		
	$.ajax({
	    url: _this.pathname + "/async/parse",
	    type: "GET",
	    success: function(data) {
	    	if (data.success) {
	    		_this.setGrammarProcessingPanelStatus("grammar-parsing", "success");
	    		if ( (data.objectErrors!=null && data.objectErrors.length>0) || 
	    				(data.fieldErrors!=null && data.fieldErrors.length>0)) {
	    			_this.setGrammarProcessingPanelErrors("grammar-parsing", data.objectErrors, data.fieldErrors)
	    		}
	    		_this.setGrammarProcessingPanelSuccessFiles("grammar-parsing", data.pojo);
	    		_this.compileGrammar();
	    	} else {
	    		_this.setGrammarProcessingPanelStatus("grammar-parsing", "error");
	    		_this.setGrammarProcessingPanelErrors("grammar-parsing", data.objectErrors, data.fieldErrors)
	    	}
	    	$(window).trigger('resize');
	    }, error: function(jqXHR, textStatus, errorThrown ) {
	    	__util.processServerError(jqXHR, textStatus, errorThrown);
	    	_this.setGrammarProcessingPanelStatus("grammar-parsing", "error");
	    }
	});
};

GrammarEditor.prototype.compileGrammar = function() {
	var _this = this;
	
	this.setGrammarProcessingPanelStatus("grammar-compiling", "loading");
	
	$.ajax({
	    url: _this.pathname + "/async/compile",
	    type: "GET",
	    success: function(data) {
	    	if (data.success) {
	    		_this.setGrammarProcessingPanelStatus("grammar-compiling", "success");
	    		if ( (data.objectErrors!=null && data.objectErrors.length>0) || 
	    				(data.fieldErrors!=null && data.fieldErrors.length>0)) {
	    			_this.setGrammarProcessingPanelErrors("grammar-compiling", data.objectErrors, data.fieldErrors)
	    		} 
	    		_this.setGrammarProcessingPanelSuccessFiles("grammar-compiling", data.pojo);
	    		_this.sandboxGrammar();
	    	} else {
	    		_this.setGrammarProcessingPanelStatus("grammar-compiling", "error");
	    		_this.setGrammarProcessingPanelErrors("grammar-compiling", data.objectErrors, data.fieldErrors)
	    	}	    
	    	$(window).trigger('resize');
	    }, error: function(jqXHR, textStatus, errorThrown ) {
	    	__util.processServerError(jqXHR, textStatus, errorThrown);
	    	_this.setGrammarProcessingPanelStatus("grammar-compiling", "error");
	    }
	});
};

GrammarEditor.prototype.sandboxGrammar = function() {
	var _this = this;
	
	this.setGrammarProcessingPanelStatus("grammar-sandboxing", "loading");
	
	$.ajax({
	    url: _this.pathname + "/async/sandbox",
	    type: "GET",
	    data: { baseMethod: $(_this.modal).find("#baseMethod").val() },
	    success: function(data) {
	    	if (data.success) {
	    		_this.setGrammarProcessingPanelStatus("grammar-sandboxing", "success");
	    		
	    		_this.originalModeModified = false;
		    	_this.grammarModified = false;
		    	_this.validated = true;
		    	_this.error = false;
		    	_this.updateGrammarState();
	    	} else {
	    		_this.setGrammarProcessingPanelStatus("grammar-sandboxing", "error");
	    		_this.setGrammarProcessingPanelErrors("grammar-sandboxing", data.objectErrors, data.fieldErrors)
	    	}
	    	$(window).trigger('resize');
	    }, error: function(jqXHR, textStatus, errorThrown ) {
	    	__util.processServerError(jqXHR, textStatus, errorThrown);
	    	_this.setGrammarProcessingPanelStatus("grammar-sandboxing", "error");
	    }
	});
};

GrammarEditor.prototype.setGrammarProcessingPanelSuccessFiles = function(id, files) {
	var panel = $(this.processGrammarModal.container).find("#" + id);
	
	panel.find(".card-body").text("");
	
	//$(this.modal).find(".accordion-validate-grammar .panel-collapse").removeClass("in");
	panel.find(".collapse").addClass("show");
	
	var fileList = $("<ul>");
	for (var i=0; i<files.length; i++) {
		fileList.append("<li>" + files[i] + "</li>");
	}
	panel.find(".card-body").append(fileList);
};

GrammarEditor.prototype.setGrammarProcessingPanelErrors = function(id, objectErrors, fieldErrors) {
	var panel = $(this.processGrammarModal.container).find("#" + id);
	
	//panel.find(".card-body").html("");
	
	$(this.processGrammarModal.container).find(".collapse").removeClass("show");
	panel.find(".collapse").addClass("show");
	
	if (objectErrors!=null && objectErrors.length > 0) {
		var errorList = $("<ul>");
		for (var i=0; i<objectErrors.length; i++) {
			errorList.append("<li>" + objectErrors[i] + "</li>");
		}
		panel.find(".card-body").append(errorList);
	}
	
	if (fieldErrors!=null && fieldErrors.length > 0) {
		for (var i=0; i<fieldErrors.length; i++) {
			var errorList = $("<ul>");
			for (var j=0; j<fieldErrors[i].errors.length; j++) {
				errorList.append("<li>" + fieldErrors[i].errors[j] + "</li>");
			}
			panel.find(".card-body").append("<h4>" + fieldErrors[i].field + "</h4>");
			panel.find(".card-body").append(errorList);
		}
	}
}

GrammarEditor.prototype.setGrammarProcessingPanelStatus = function(id, state) {
	var panel = $(this.processGrammarModal.container).find("#" + id);
	
	if (state==="loading") {
		panel.find(".grammar-waiting").addClass("hide");
		panel.find(".grammar-loading").removeClass("hide");
	} else if (state==="success") {
		panel.addClass("grammar-success");
		panel.find(".grammar-loading").addClass("hide");
		panel.find(".grammar-ok").removeClass("hide");
	} else if (state==="error") {
		panel.addClass("grammar-error");
		panel.find(".grammar-loading").addClass("hide");
		panel.find(".grammar-error").removeClass("hide");
		this.error = true;
	} 
};

GrammarEditor.prototype.setGrammarProcessingPanelSuccessFiles = function(id, files) {
	var panel = $(this.processGrammarModal.container).find("#" + id);
	
	//panel.find(".card-body").text("");
	
	//$(this.modal).find(".accordion-validate-grammar .panel-collapse").removeClass("in");
	//panel.find(".collapse").addClass("show");
	
	panel.find(".card-body").append("<h4>" + __translator.translate("~de.unibamberg.minf.dme.form.grammar.generated_files") + "</h4>");
	
	var fileList = $("<ul>");
	for (var i=0; i<files.length; i++) {
		fileList.append("<li>" + files[i] + "</li>");
	}
	panel.find(".card-body").append(fileList);
};

GrammarEditor.prototype.parseSample = function() {
	var _this = this;
	
	$(this.modal).find(".grammar-parse-alerts").html("");
	if (this.svg!=null) {
		this.svg.destroy();
	}

	$.ajax({
	    url: _this.pathname + "/async/parseSample",
	    type: "POST",
	    data: { 
	    	sample : $(this.modal).find(".grammar-sample-input").val(),
	    	initRule : $(this.modal).find("#baseMethod").val(),
	    	temporary : _this.originalModeModified || _this.grammarModified || _this.validated
	    },
	    dataType: "json",
	    success: function(data) {
	    	if (data.success===true) {
	    		_this.showParseSampleResult(data.pojo.svg, null, data.pojo.errors);
	    	} else {
	    		_this.showParseSampleResult(null, data.objectErrors, data.objectWarnings);
	    	}
	    }, error: function(jqXHR, textStatus, errorThrown ) {
	    	__util.processServerError(jqXHR, textStatus, errorThrown);
	    }
	});
};

GrammarEditor.prototype.showParseSampleResult = function(svg, errors, warnings) {
	if (errors!=null && Array.isArray(errors) && errors.length > 0) {
		var list = $("<ul>");
		for (var i=0; i<errors.length; i++) {
			$(list).append("<li>" + errors[i] + "</li>");
		}

		var alerts = $("<div class=\"alert alert-sm alert-danger\"> " +
					"<i class='fas fa-exclamation'></i>" +
				"~Errors" +
			"</div>");
		
		$(alerts).append(list)
		$(this.modal).find(".grammar-parse-alerts").append(alerts);
	}
	if (warnings!=null && Array.isArray(warnings) && warnings.length > 0) {
		var list = $("<ul>");
		for (var i=0; i<warnings.length; i++) {
			$(list).append("<li>" + warnings[i] + "</li>");
		}

		var alerts = $("<div class=\"alert alert-sm alert-warning\"> " +
				"<i class='fas fa-exclamation'></i>" +
				"~Warnings" +
			"</div>");
		
		$(alerts).append(list)
		$(this.modal).find(".grammar-parse-alerts").append(alerts);
	}
	if (svg != null) {
		var svgContainer = ".grammar-sample-svg-embedded";	
		$(this.modal).find(".grammar-sample-svg-embedded").removeClass("hide");
		this.svg = new SvgViewer(svgContainer, svg)
	}
};