var ExportedConceptEditor = function(owner, container, modal, options) {
	this.options = {
			conceptId: "",
			conceptType: "EXPORT",
			path: "mappedconcept/{0}",
			readOnly: false
	};
	$.extend(true, this.options, options);
	this.options.path = __util.composeRelativeUrl(String.format(this.options.path, this.options.conceptId));
	
	this.modal = modal;
	this.container = container;
		
	this.owningEditor = owner;
	
	let strNatures = $(this.container).find("#export-availableNatures").val().toUpperCase(); 
	
	if (strNatures=="[]") {
		this.natures = [];
	} else {
		this.natures = strNatures.substring(1, strNatures.length-1).split(",");
	}
	
	const _this = this;
	$( "select[name='format']" ).change(function() { _this.handleFormatChange(); });
	this.handleFormatChange();
	
	
	this.exportSample = {};
	this.exportSample.available = $(_this.container).find("input[name='export-sample-available']").val()=="true";
	this.exportSample.oversize = $(_this.container).find("input[name='export-sample-oversize']").val()=="true";
	this.exportSample.sample = $(_this.container).find("pre[id='export-sample']").text();
	
	this.showSampleResourceObject(this.exportSample);
	
	__translator.addTranslations([
		"~de.unibamberg.minf.common.link.edit",
		"~de.unibamberg.minf.common.link.delete",
		"~de.unibamberg.minf.common.link.view"]);
	__translator.getTranslations();
};

ExportedConceptEditor.prototype.handleFormatChange = function() {
	let currentFormat = $(this.container).find("select[name='format'] option:selected").val();
	let terminalOptionAvailable = false; 
	for (let nature of this.natures) {
		if (nature.startsWith(currentFormat)) {
			terminalOptionAvailable = true;
			break;
		}
	}		
	let select = $(this.container).find("input[name='useTerminalsIfAvailable']");
	if (terminalOptionAvailable) {
		$(select).prop("disabled", false);
	} else {
		$(select).prop("checked", false);
		$(select).prop("disabled", true);
	}
}

ExportedConceptEditor.prototype.showSampleResourceObject = function(data) {
	let html = "";
	if (data.available===true) {
		if (data.oversize===false) {
			html = $("<ul>").append(this.owningEditor.sampleHandler.buildSampleResource(JSON.parse(data.sample)));
		} else {
			html = "<p>" + __translator.translate("~de.unibamberg.minf.dme.editor.sample.notice.oversize.not_loadable") + "</p>";
		}
		
		$(this.container).find(".btn-parse-sample").removeClass("hide");
		$(this.container).find("#export-result-outer-container").removeClass("hide");
	} else {
		html = "<p>" + __translator.translate("~de.unibamberg.minf.dme.editor.sample.notice.empty_sample") + "</p>";
	}
	
	$(this.container).find("#export-sample-rendered")
		.html(html)
		.removeClass("hide");
		
};

ExportedConceptEditor.prototype.performExport = function() {
	const _this = this;		    	
   	let samples = [];
	$(_this.container).find(".sample-input").each(function() {
		samples.push({
			elementId : $(this).find("input[name='elementId']").val(),
			text: $(this).find(".form-control").val()
		});
	});
	
	let includeTree = $(_this.container).find("input[name='includeTree']:checked").val();
	let includeSelf = $(_this.container).find("input[name='includeSelf']:checked").val();
	let escape = $(_this.container).find("input[name='escape']:checked").val();
	let format = $(_this.container).find("select[name='format'] option:selected").val();
	let useTerminalsIfAvailable = $(_this.container).find("input[name='useTerminalsIfAvailable']:checked").val();
	
	
	
	$.ajax({
	    url: _this.options.path + "/async/exportSample",
	    type: "POST",
	    dataType: "json",
	    contentType: 'application/json',
	    data: JSON.stringify ({ 
			format: format,
			samples: samples,
			includeTree: includeTree,
			includeSelf: includeSelf,
			useTerminalsIfAvailable : useTerminalsIfAvailable,
			escape : escape
		}),
		success: function(data) {
			if (data.success) {
				//$(_this.container).find("#transformation-result-container").text(JSON.stringify(data.pojo));
				_this.showExportResults(data);
			} else {
				alert("error1");
			}
		}, error: function(jqXHR, textStatus, errorThrown) {
			__util.processServerError(jqXHR, textStatus, errorThrown);
		}
	});
};

ExportedConceptEditor.prototype.showExportResults = function(data) {
	const _this = this;
	if (data.pojo==null || !Array.isArray(data.pojo)) {
		$(_this.container).find(".transformation-result").addClass("hide");
		$(_this.container).find(".transformation-result").text("");
		$(_this.container).find(".no-results-alert").removeClass("hide");
		return;
	}
	$(_this.container).find(".no-results-alert").addClass("hide");
	
	var list = $("<ul>");
	_this.appendTransformationResults(data.pojo, list);
	$(_this.container).find(".transformation-result").removeClass("hide");
	$(_this.container).find(".transformation-result").html(list);
	$(_this.container).find(".transformation-alerts").html("");
	if (data.objectWarnings!=null && Array.isArray(data.objectWarnings)) {
		for (var i=0; i<data.objectWarnings.length; i++) {
			$(_this.container).find(".transformation-alerts").append(
					"<div class=\"alert alert-sm alert-warning\">" +
						"<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> " 
						+ data.objectWarnings[i] + 
					"</div>");			
		}
	}
	
};

ExportedConceptEditor.prototype.appendTransformationResults = function(elements, container) {
	for (var i=0; i<elements.length; i++) {
		var elem = $("<li>");
		elem.append("<span class=\"transformation-result-label\">" + elements[i].label + "</span>");
		if (elements[i].value!=undefined && elements[i].value!=null) {
			elem.append(": ");
			elem.append("<span class=\"transformation-result-value\">" + elements[i].value + "</span>");
		}
		if (elements[i].children!=null && Array.isArray(elements[i].children) && elements[i].children.length > 0) {
			var subelem = $("<ul>");
			this.appendTransformationResults(elements[i].children, subelem);
			elem.append(subelem);
		}
		container.append(elem);
	}
};
