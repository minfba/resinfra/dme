var editor;

$(document).ready(function() {
	editor = new SchemaEditor({
		layout: {
			component : {
				name : "editor-layout",
				closable : false,
				editor: {
					title: "~de.unibamberg.minf.dme.editor.element_model"
				},
				sample: {
					outputTitle: "~de.unibamberg.minf.dme.editor.sample.output"
				}
			}, 
			thresholds : {
				twoRowHeight : 350,
				twoColWidth : 850,
				threeColWidth : 1100
			}
		},
		footerOffset: 70,
		icons: {
			warning: __util.getBaseUrl() + "resources/img/warning.png",
			error: __util.getBaseUrl() + "resources/img/error.png",
			reusing: __util.getBaseUrl() + "resources/img/reuse.png",
			reused: __util.getBaseUrl() + "resources/img/reuse.png",
			identifier: __util.getBaseUrl() + "resources/img/identifier.png",
			database: __util.getBaseUrl() + "resources/img/database.png"
		}
	});
	
	for (var icon in editor.options.icons) {
	    if (editor.options.icons.hasOwnProperty(icon)) {
	    	var img = new Image();
	    	img.src = editor.options.icons[icon];
	    	editor.options.icons[icon] = img;
	    }
	}	
	
	$('[data-toggle="tooltip"]').tooltip( { container: 'body' });
});

$(window).resize(function() {
	editor.resize();
});

var SchemaEditor = function(options) {	
	this.options = options;
	this.schema = { 
			id: $("#schema-id").val(),
			owned: $("#schema-own").val()==="true",
			write: $("#schema-write").val()==="true"
	}
	this.entityId = this.schema.id;
	
	this.pathname = __util.composeUrl("model/editor/" + this.schema.id + "/");
	this.context = null; 

	this.layout = null;
	this.layoutContainer = $(".editor-layout-container");
	this.container = null;
	
	this.containers = {};
	
	this.layoutMaximized = false;

	this.sampleHandler = new SampleHandler({
		modelId: this.schema.id,
		requiredAreas: ["input", "output"]
	});
	
	document.addEventListener("selectionEvent", this.selectionHandler, false);
	document.addEventListener("deselectionEvent", this.deselectionHandler, false);
	
	this.handleTranslations();
	
	this.initContainers();
	this.initLayout();
	
	this.addVocabularySource("elements", "query/");
	
	this.resize();
}

SchemaEditor.prototype = new BaseEditor();

SchemaEditor.prototype.initContainers = function() {
	this.initBaseContainers();
}

SchemaEditor.prototype.initEditor = function(element, container) {
	var _this = this;
	
	this.context = $(element).find("#schema-editor-canvas")[0].getContext("2d");
	this.container = $(element).find(".editor-container");
	
	container.on('resize', function() {
		_this.context.canvas.width = container.width;
		_this.context.canvas.height = container.height;		
		if (_this.graph !== null) {
			_this.graph.update();
		}
	});
	
	this.initGraph();
	this.initNatures();
	this.loadElementHierarchy();
	this.resize();
		
	this.context.canvas.width = container.width;
	this.context.canvas.height = container.height;		
	if (this.graph !== null) {
		this.graph.update();
	}
};


SchemaEditor.prototype.deselectionHandler = function() {
	var _this = editor;
	
	_this.elementContextContainer.text("");
	/*_this.elementContextButtons.text("");
	*/
	_this.elementContextContainer.addClass("hide");
	_this.entityContextContainer.removeClass("hide");
	//_this.loadActivitiesForEntity(_this.schema.id, _this.schemaActivitiesContainer);*/
};

SchemaEditor.prototype.selectionHandler = function(e) {
	var _this = editor;

	_this.elementContextContainer.text("");
	/*_this.elementContextButtons.text("");
	*/
	//_this.createActionButtons(_this.elementContextButtons, e.element.getContextMenuItems(), "editor");
	_this.getElementDetails(_this.pathname, e.element.getType(), e.element.id, _this.elementContextContainer);
	/*_this.loadActivitiesForElement(e.element.id, _this.elementActivitiesContainer);
	*/
	_this.elementContextContainer.removeClass("hide");
	_this.entityContextContainer.addClass("hide");
};



SchemaEditor.prototype.addDescription = function(id) {
	this.addNode("element", "grammar", id);
};

SchemaEditor.prototype.addTransformation = function(id) {
	this.addNode("grammar", "function", id);
};

SchemaEditor.prototype.addNonterminal = function(id) {
	this.addNode("element", "nonterminal", id);
};

SchemaEditor.prototype.addLabel = function(id) {
	this.addNode("element", "label", id);
};


SchemaEditor.prototype.addNode = function(type, childType, id) {
	var _this = this;
	var form_identifier = "edit-element-" + id;
	
	modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl(type + "/" + id + "/form/new_" + childType),
		identifier: form_identifier,
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],               
        completeCallback: function() {
			_this.reloadElementHierarchy(function() {
				_this.area.expandFromElement(id, true);
			});
		}
	});
		
	modalFormHandler.show(form_identifier);
};

SchemaEditor.prototype.createRoot = function() {
	var _this = this;
	var form_identifier = "edit-root";
	
	modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl("form/createRoot"),
		identifier: form_identifier,
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],               
		completeCallback: function() {
	    	_this.reloadPage();
		}
	});
		
	modalFormHandler.show(form_identifier)
};

SchemaEditor.prototype.editElement = function(id) {
	var _this = this;
	var form_identifier = "edit-element-" + id;
	
	modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl("element/" + id + "/form/element"),
		identifier: form_identifier,
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],
		additionalModalClasses: "modal-lg",                
        displayCallback: function(modal) {
        	$(modal).find("#chk-processing-root").change(function() {
        		if (this.checked) {
        			$(modal).find(".processing-root-option").removeClass("hide");
        		} else {
        			$(modal).find(".processing-root-option").addClass("hide");
        		}
        	});
		},
		completeCallback: function() { _this.reloadElementHierarchy(); }
	});
		
	modalFormHandler.show(form_identifier);
};

SchemaEditor.prototype.moveGrammar = function(delta) {
	var _this = this;
	
	$.ajax({
	    url: __util.composeRelativeUrl("grammar/" + this.graph.selectedItems[0].id + "/move?delta=" + delta),
	    type: "GET",
	    dataType: "json",
	    success: function(data) {
	    	_this.reloadElementHierarchy();
	    },
	    error: __util.processServerError
	});
};

SchemaEditor.prototype.moveElement = function(delta) {
	var _this = this;
	
	$.ajax({
	    url: __util.composeRelativeUrl("element/" + this.graph.selectedItems[0].id + "/move?delta=" + delta),
	    type: "GET",
	    dataType: "json",
	    success: function(data) {
	    	_this.reloadElementHierarchy();
	    },
	    error: __util.processServerError
	});
};

SchemaEditor.prototype.moveFunction = function(delta) {
	var _this = this;
	
	$.ajax({
	    url: __util.composeRelativeUrl("function/" + this.graph.selectedItems[0].id + "/move?delta=" + delta),
	    type: "GET",
	    dataType: "json",
	    success: function(data) {
	    	_this.reloadElementHierarchy();
	    },
	    error: __util.processServerError
	});
};

SchemaEditor.prototype.editGrammar = function() {
	var _this = this;
	var form_identifier = "edit-grammar-" + this.graph.selectedItems[0].id;
	
	modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl("grammar/" + this.graph.selectedItems[0].id + "/form/edit"),
		identifier: form_identifier,
		additionalModalClasses: "modal-max",
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],
		setupCallback: function(modal) { 
			grammarEditor = new GrammarEditor(modal, {
				pathPrefix: __util.getBaseUrl() + "model/editor/" + _this.schema.id,
				entityId : _this.schema.id,
				grammarId : _this.graph.selectedItems[0].id
			});
			grammarEditor.registerTabs();
		},       
		completeCallback: function() { _this.reloadElementHierarchy(); }
	});
		
	modalFormHandler.show(form_identifier);
};


SchemaEditor.prototype.editFunction = function() {
	var _this = this;
	var form_identifier = "edit-function-" + this.graph.selectedItems[0].id;
	
	modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl("function/" + this.graph.selectedItems[0].id + "/form/edit"),
		identifier: form_identifier,
		additionalModalClasses: "modal-max",
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],
        setupCallback: function(modal) { 
        	functionEditor = new FunctionEditor(modal, {
				pathPrefix: __util.getBaseUrl() + "model/editor/" + _this.schema.id,
				entityId : _this.schema.id,
				functionId : _this.graph.selectedItems[0].id
			}); 
        },       
		completeCallback: function() { _this.reloadElementHierarchy(); }
	});
		
	modalFormHandler.show(form_identifier);
};

SchemaEditor.prototype.linkElement = function(elementId, elementType) {
	var _this = this;
	var form_identifier = "link-element-" + elementId;//this.graph.selectedItems[0].id;
	
	modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl("element/" + elementId + "/form/linkElement"),
		identifier: form_identifier,
		additionalModalClasses: "modal-lg",
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],      
		displayCallback: function(modal) { 
			_this.registerLinkTypeahead($(modal).find("#link-element"), elementType, elementId);
		},
		completeCallback: function(data, modal) { 
			_this.reloadElementHierarchy(); 
		}
	});
		
	modalFormHandler.show(form_identifier);
};

SchemaEditor.prototype.assignChild = function(elementId) {
	var _this = this;
	var form_identifier = "edit-function-" + elementId;//this.graph.selectedItems[0].id;
	
	modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl("element/" + elementId + "/form/assignChild"),
		identifier: form_identifier,
		additionalModalClasses: "modal-lg",
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],      
		displayCallback: function(modal) { 
			_this.registerElementTypeahead($(modal).find("#child-element"));
		},
		completeCallback: function(data, modal) { 
			_this.reloadElementHierarchy(); 
		}
	});
		
	modalFormHandler.show(form_identifier);
};

SchemaEditor.prototype.registerLinkTypeahead = function(typeahead, elementType, excludeId) {
	var _this = this;
	
	var source = new Bloodhound({
		  datumTokenizer: Bloodhound.tokenizers.whitespace,
		  queryTokenizer: Bloodhound.tokenizers.whitespace,
		  remote: {
			  url: "query/%QUERY?types=" + elementType + "&excludeIds=" + excludeId,
			  wildcard: '%QUERY'
		  }
	});
	
	this.registerTypeahead(typeahead, "linkable_elements", source, "code", 8, 
			function(data) { return _this.showTypeaheadFoundResult(data); },
			function(t, suggestion) { 
				$(t).closest(".form-group").removeClass("has-error"); 
				$(t).closest(".form-content").find("#element-id").val(suggestion.id);
				$(t).closest(".form-content").find("#element-id-display").val(suggestion.id);
				$(t).closest(".form-content").find("#element-name").val(suggestion.name!==undefined ? suggestion.name : suggestion.grammarName);
				$(t).closest("form").find(".form-btn-submit").removeAttr("disabled");
			},
			null
	);
};

SchemaEditor.prototype.registerElementTypeahead = function(typeahead) {
	var _this = this;
	this.registerTypeahead(typeahead, "elements", _this.vocabularySources["elements"], "code", 8, 
			function(data) { return _this.showTypeaheadFoundResult(data); },
			function(t, suggestion) { 
				$(t).closest(".form-group").removeClass("has-error"); 
				$(t).closest(".form-content").find("#element-id").val(suggestion.id);
				$(t).closest(".form-content").find("#element-id-display").val(suggestion.id);
				$(t).closest(".form-content").find("#element-name").val(suggestion.name!==undefined ? suggestion.name : suggestion.grammarName);
			},
			null
	);
};

SchemaEditor.prototype.showTypeaheadFoundResult = function(data) {
	var result = '<p>' +
					'<strong>' + (data.name!==undefined ? data.name : data.grammarName) + '</strong>' +
					'<br/ >';
	
	if (data.simpleType==='Nonterminal') {
		result += 	 	'Nonterminal: ';
	} else if (data.simpleType==='Label') {
		result += 	 	'Label: ';
	} else if (data.simpleType==='Grammar') {
		result += 	 	'Grammar: ';
	} else if (data.simpleType==='Function') {
		result += 	 	'Function: ';
	}
	result += 	 data.id + '<p>';
	return result;
};

SchemaEditor.prototype.removeElement = function(type, id) { 
	var _this = this;
	
	bootbox.confirm(String.format(__translator.translate("~de.unibamberg.minf.dme.dialog.confirm_delete_element"), id), function(result) {
		if(result) {
			$.ajax({
			    url: __util.composeRelativeUrl(_this.getElementType(type) + "/" + id + "/async/remove"),
			    type: "GET",
			    dataType: "json",
			    success: function() {
			    	_this.graph.deselectAll();
			    	_this.reloadElementHierarchy();
			    },
			    error: __util.processServerError
			});
		}
	});
};

SchemaEditor.prototype.toggleElementDisabled = function(type, id, disable) { 
	var _this = this;
	
	bootbox.confirm(String.format((disable ? __translator.translate("~de.unibamberg.minf.dme.dialog.confirm_disable") : __translator.translate("~de.unibamberg.minf.dme.dialog.confirm_enable")), id), function(result) {
		if(result) {
			$.ajax({
			    url: __util.composeRelativeUrl(_this.getElementType(type) + "/" + id + "/async/disable"),
			    data: { disabled: disable },
			    type: "GET",
			    dataType: "json",
			    success: function() {
			    	_this.reloadElementHierarchy();
			    },
			    error: __util.processServerError
			});
		}
	});
};

SchemaEditor.prototype.editTerminal = function(elementType, id) {
	var _this = this;
	var form_identifier = "edit-terminal" + id;
	
	modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl(elementType.toLowerCase() + "/" + id + "/form/edit"),
		data: { n: this.currentNature },
		identifier: form_identifier,
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],       
		additionalModalClasses: "modal-lg",
		completeCallback: function() { 
			_this.reloadElementHierarchy();
		}
	});
		
	modalFormHandler.show(form_identifier);
};

SchemaEditor.prototype.removeTerminal = function() {
	var terminalId = $("#terminalId").val();
	
	var _this = this;
	bootbox.confirm(String.format(__translator.translate("~de.unibamberg.minf.dme.dialog.confirm_delete"), terminalId), function(result) {
		if(result) {
			$.ajax({
			    url: __util.composeRelativeUrl("terminal/" + terminalId + "/async/remove"),
			    data: { n: this.currentNature },
			    type: "GET",
			    dataType: "json",
			    success: function(data) {
			    	_this.graph.update();
			    	_this.updateTerminalList();
			    }
			});
		}
	});
};

SchemaEditor.prototype.triggerEditSchema = function() {
	if (!__util.isLoggedIn()) {
		__util.showLoginNote();
		return;
	}
	
	var _this = this;
	var form_identifier = "edit-schema-" + _this.schema.id;
	
	modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl("forms/edit/"),
		identifier: form_identifier,
		additionalModalClasses: "modal-lg",
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],
		completeCallback: function(d) {
			if (d.statusInfo!==undefined && d.statusInfo!==null && d.statusInfo.length==24 && d.statusInfo!==_this.schema.id) {
				window.location.replace(__util.composeUrl("model/editor/" + d.statusInfo + "/"));
			} else {
				window.location.reload();
			}
		}
	});
		
	modalFormHandler.show(form_identifier);
};

SchemaEditor.prototype.triggerDeleteSchema = function() {
	if (!__util.isLoggedIn()) {
		__util.showLoginNote();
		return;
	}
	var _this = this;
	bootbox.confirm(String.format(__translator.translate("~de.unibamberg.minf.dme.dialog.confirm_delete"), _this.schema.id), function(result) {
		if(result) {
			$.ajax({
		        url: __util.composeRelativeUrl("async/delete/"),
		        type: "GET",
		        dataType: "json",
		        success: function(data) { 
		        	window.location.reload();
		        },
		        error: function(textStatus) {
		        	__notifications.showMessage(NOTIFICATION_TYPES.ERROR, 
		        			__translator.translate("~de.unibamberg.minf.common.view.forms.servererror.head"), 
		        			__translator.translate("~de.unibamberg.minf.common.view.forms.servererror.body"));
		        }
			});
		}
	});
};

SchemaEditor.prototype.triggerPublish = function() {
	if (!__util.isLoggedIn()) {
		__util.showLoginNote();
		return;
	}
	var _this = this;
	bootbox.confirm(String.format(__translator.translate("~de.unibamberg.minf.dme.dialog.confirm_publish"), _this.schema.id), function(result) {
		if(result) {
			$.ajax({
		        url: __util.composeUrl("model/async/publish/" + _this.schema.id),
		        type: "GET",
		        dataType: "json",
		        success: function(data) { 
		        	window.location.reload();
		        },
		        error: function(textStatus) {
		        	__notifications.showMessage(NOTIFICATION_TYPES.ERROR, 
		        			__translator.translate("~de.unibamberg.minf.common.view.forms.servererror.head"), 
		        			__translator.translate("~de.unibamberg.minf.common.view.forms.servererror.body"));
		        }
			});
		}
	});
};

SchemaEditor.prototype.handleTranslations = function() {
	__translator.addTranslations([
		"~de.unibamberg.minf.common.link.cancel",
		"~de.unibamberg.minf.common.view.forms.servererror.head",
	    "~de.unibamberg.minf.common.view.forms.servererror.body",
	    
	    "~de.unibamberg.minf.dme.button.headers_include",
	    "~de.unibamberg.minf.dme.button.headers_ignore",
	    
	    "~de.unibamberg.minf.dme.dialog.confirm_delete_element",
	    "~de.unibamberg.minf.dme.dialog.confirm_disable",
	    "~de.unibamberg.minf.dme.dialog.confirm_enable",
	    "~de.unibamberg.minf.dme.dialog.confirm_delete",
	    "~de.unibamberg.minf.dme.dialog.confirm_publish"
	]);
	this.addImportExportTranslations();
	this.addGraphTranslations();
	this.addNaturesTranslations();
	
	__translator.getTranslations();
};