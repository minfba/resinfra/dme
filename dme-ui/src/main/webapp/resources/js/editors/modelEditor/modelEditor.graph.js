SchemaEditor.prototype.addGraphTranslations = function() {
	__translator.addTranslations([
	    "~de.unibamberg.minf.common.link.edit",
	    "~de.unibamberg.minf.common.link.enable",
	    "~de.unibamberg.minf.common.link.disable",
	    "~de.unibamberg.minf.common.link.delete",
	    "~de.unibamberg.minf.common.link.view",
	    "~de.unibamberg.minf.common.link.move_top",
	    "~de.unibamberg.minf.common.link.move_up",
	    "~de.unibamberg.minf.common.link.move_down",
	    "~de.unibamberg.minf.common.link.move_bottom",
	    "~de.unibamberg.minf.common.link.reset_view",
	    "~de.unibamberg.minf.common.link.reload_data",
	    "~de.unibamberg.minf.common.model.id",

	    "~de.unibamberg.minf.dme.button.expand_all",
	    "~de.unibamberg.minf.dme.button.collapse_all",
	    "~de.unibamberg.minf.dme.button.expand_from_here",
	    "~de.unibamberg.minf.dme.button.collapse_from_here",

	    "~de.unibamberg.minf.dme.button.show_reused",
	    "~de.unibamberg.minf.dme.button.model_individually",
	    "~de.unibamberg.minf.dme.button.create_root",
	    "~de.unibamberg.minf.dme.button.linkElement",
	    "~de.unibamberg.minf.dme.button.assign_child",
	    
	    "~de.unibamberg.minf.dme.button.add_element",
	    "~de.unibamberg.minf.dme.button.add_label",
	    "~de.unibamberg.minf.dme.button.add_nonterminal",
	    "~de.unibamberg.minf.dme.button.add_desc_function",
	    "~de.unibamberg.minf.dme.button.add_trans_function",
	    
	    "~de.unibamberg.minf.dme.button.export",
	    "~de.unibamberg.minf.dme.button.import",
	    "~de.unibamberg.minf.dme.button.export_from_here",
	    "~de.unibamberg.minf.dme.button.import_here",

	    "~de.unibamberg.minf.dme.dialog.confirm_clone_tree.head",
	    "~de.unibamberg.minf.dme.dialog.confirm_clone_tree.body",

	    "~de.unibamberg.minf.dme.form.nature.terminals.missing",

	    "~de.unibamberg.minf.dme.model.element.element",
	    "~de.unibamberg.minf.dme.model.element.name",
	    "~de.unibamberg.minf.dme.model.element.transient",

	    "~de.unibamberg.minf.dme.notification.import_processing.head", 
	    "~de.unibamberg.minf.dme.notification.import_processing.body", 
	    "~de.unibamberg.minf.dme.notification.import_error.head", 
	    "~de.unibamberg.minf.dme.notification.import_error.body"
	]);
};

/**   - getContextMenuItems() must return an array of item objects
*   	- {key: ..., label: ..., glyphicon: ..., e: ..., callback: function(itemKey, e)} for items or
*   	- {key: "-"} for a separator
*/
SchemaEditor.prototype.initGraph = function() {
	var _this = this;
	this.graph = new Model(this.context.canvas, {
		readOnly: !(_this.schema.owned || _this.schema.write),
		elementTemplateOptions : [{
			key: "Nonterminal",
			primaryColor: "#e6f1ff", secondaryColor: "#0049a6",
			getContextMenuItems: function(element) { 
				var items = [];
				items.push(_this.graph.createContextMenuItem("expandFromHere", "~de.unibamberg.minf.dme.button.expand_from_here", "fas fa-expand", element.id, element.template.options.key));
				items.push(_this.graph.createContextMenuItem("collapseFromHere", "~de.unibamberg.minf.dme.button.collapse_from_here", "fas fa-compress", element.id, element.template.options.key));
				items.push(_this.graph.createContextMenuSeparator());

				if (_this.schema.owned || _this.schema.write) {
					items.push(_this.graph.createContextMenuItem("editElement", "~de.unibamberg.minf.common.link.edit", "far fa-edit", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("addNonterminal", "~de.unibamberg.minf.dme.button.add_element", "fas fa-plus", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("addDescription", "~de.unibamberg.minf.dme.button.add_desc_function", "fas fa-plus", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuSeparator());
					if (element.reusing || element.reused) {
						items.push(_this.graph.createContextMenuItem("showReused", "~de.unibamberg.minf.dme.button.show_reused", "fab fa-pagelines", element.id, element.template.options.key));
						if (element.reusing || element.reused) {
							items.push(_this.graph.createContextMenuItem("modelIndividually", "~de.unibamberg.minf.dme.button.model_individually", "fas fa-seedling", element, element.template.options.key));
						}
					} else {
						items.push(_this.graph.createContextMenuItem("linkElement", "~de.unibamberg.minf.dme.button.linkElement", "fas fa-recycle", element.id, element.template.options.key));	
					}
					items.push(_this.graph.createContextMenuItem("assignChild", "~de.unibamberg.minf.dme.button.assign_child", "fas fa-link", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuSeparator());
					items.push(_this.graph.createContextMenuItem("moveTopElement", "~de.unibamberg.minf.common.link.move_top", "fas fa-angle-double-up", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("moveUpElement", "~de.unibamberg.minf.common.link.move_up", "fas fa-angle-up", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("moveDownElement", "~de.unibamberg.minf.common.link.move_down", "fas fa-angle-down", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("moveBottomElement", "~de.unibamberg.minf.common.link.move_bottom", "fas fa-angle-double-down", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuSeparator());
					items.push(_this.graph.createContextMenuItem("exportSubtree", "~de.unibamberg.minf.dme.button.export_from_here", "fas fa-file-export", element.id));
					items.push(_this.graph.createContextMenuItem("importSubtree", "~de.unibamberg.minf.dme.button.import_here", "fas fa-file-import", element.id));
					items.push(_this.graph.createContextMenuSeparator());
					if (element.disabled) {
						items.push(_this.graph.createContextMenuItem("enableElement", "~de.unibamberg.minf.common.link.enable", "fas fa-plus-circle", element.id, element.template.options.key));
					} else {
						items.push(_this.graph.createContextMenuItem("disableElement", "~de.unibamberg.minf.common.link.disable", "fas fa-minus-circle", element.id, element.template.options.key));
					}
					
					items.push(_this.graph.createContextMenuItem("removeElement", "~de.unibamberg.minf.common.link.delete", "far fa-trash-alt", element.id, element.template.options.key));
				} else {
					items.push(_this.graph.createContextMenuItem("editElement", "~de.unibamberg.minf.common.link.view", "far fa-edit", element.id, element.template.options.key));
				}
				return items; 
			}
		}, {
			key: "Label",
			primaryColor: "#f3e6ff", secondaryColor: "#5700a6",
			getContextMenuItems: function(element) { 
				var items = [
				    _this.graph.createContextMenuItem("expandFromHere", "~de.unibamberg.minf.dme.button.expand_from_here", "fas fa-expand", element.id, element.template.options.key),
				    _this.graph.createContextMenuItem("collapseFromHere", "~de.unibamberg.minf.dme.button.collapse_from_here", "fas fa-compress", element.id, element.template.options.key)
				];				
				items.push(_this.graph.createContextMenuSeparator());
				
				if (_this.schema.owned || _this.schema.write) {
					items.push(_this.graph.createContextMenuItem("editElement", "~de.unibamberg.minf.common.link.edit", "far fa-edit", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("addLabel", "~de.unibamberg.minf.dme.button.add_element", "fas fa-plus", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("addDescription", "~de.unibamberg.minf.dme.button.add_desc_function", "fas fa-plus", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuSeparator());
					if (element.reusing || element.reused) {
						items.push(_this.graph.createContextMenuItem("showReused", "~de.unibamberg.minf.dme.button.show_reused", "fab fa-pagelines", element.id, element.template.options.key));
						if (element.reusing || element.reused) {
							items.push(_this.graph.createContextMenuItem("modelIndividually", "~de.unibamberg.minf.dme.button.model_individually", "fas fa-seedling", element, element.template.options.key));
						}
					} else {
						items.push(_this.graph.createContextMenuItem("linkElement", "~de.unibamberg.minf.dme.button.linkElement", "fas fa-recycle", element.id, element.template.options.key));	
					}
					items.push(_this.graph.createContextMenuItem("assignChild", "~de.unibamberg.minf.dme.button.assign_child", "fas fa-link", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuSeparator());
					items.push(_this.graph.createContextMenuItem("moveTopElement", "~de.unibamberg.minf.common.link.move_top", "fas fa-angle-double-up", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("moveUpElement", "~de.unibamberg.minf.common.link.move_up", "fas fa-angle-up", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("moveDownElement", "~de.unibamberg.minf.common.link.move_down", "fas fa-angle-down", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("moveBottomElement", "~de.unibamberg.minf.common.link.move_bottom", "fas fa-angle-double-down", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuSeparator());
					items.push(_this.graph.createContextMenuItem("exportSubtree", "~de.unibamberg.minf.dme.button.export_from_here", "fas fa-file-export", element.id));
					items.push(_this.graph.createContextMenuItem("importSubtree", "~de.unibamberg.minf.dme.button.import_here", "fas fa-file-import", element.id));
					items.push(_this.graph.createContextMenuSeparator());
					if (element.disabled) {
						items.push(_this.graph.createContextMenuItem("enableElement", "~de.unibamberg.minf.common.link.enable", "fas fa-plus-circle", element.id, element.template.options.key));
					} else {
						items.push(_this.graph.createContextMenuItem("disableElement", "~de.unibamberg.minf.common.link.disable", "fas fa-minus-circle", element.id, element.template.options.key));
					}
					items.push(_this.graph.createContextMenuItem("removeElement", "~de.unibamberg.minf.common.link.delete", "far fa-trash-alt", element.id, element.template.options.key));
				} else {
					items.push(_this.graph.createContextMenuItem("editElement", "~de.unibamberg.minf.common.link.view", "far fa-edit", element.id, element.template.options.key));
				}
				return items; 
			}
		}, {
			key: "Function",
			primaryColor: "#FFE173", secondaryColor: "#6d5603", radius: 5,
			getContextMenuItems: function(element) { 
				var items = [
				    _this.graph.createContextMenuItem("expandFromHere", "~de.unibamberg.minf.dme.button.expand_from_here", "fas fa-expand", element.id, element.template.options.key),
				    _this.graph.createContextMenuItem("collapseFromHere", "~de.unibamberg.minf.dme.button.collapse_from_here", "fas fa-compress", element.id, element.template.options.key),
				    _this.graph.createContextMenuSeparator()
				];
				
				if (_this.schema.owned || _this.schema.write) {
					items.push(_this.graph.createContextMenuItem("addLabel", "~de.unibamberg.minf.dme.button.add_label", "fas fa-plus", element.id, element.template.options.key, undefined, "5700A6"));
					items.push(_this.graph.createContextMenuItem("addNonterminal", "~de.unibamberg.minf.dme.button.add_nonterminal", "fas fa-plus", element.id, element.template.options.key, undefined, "0049A6"));
					items.push(_this.graph.createContextMenuItem("editFunction", "~de.unibamberg.minf.common.link.edit", "far fa-edit", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuSeparator());
					items.push(_this.graph.createContextMenuItem("moveTopFunction", "~de.unibamberg.minf.common.link.move_top", "fas fa-angle-double-up", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("moveUpFunction", "~de.unibamberg.minf.common.link.move_up", "fas fa-angle-up", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("moveDownFunction", "~de.unibamberg.minf.common.link.move_down", "fas fa-angle-down", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("moveBottomFunction", "~de.unibamberg.minf.common.link.move_bottom", "fas fa-angle-double-down", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuSeparator());
					items.push(_this.graph.createContextMenuItem("exportSubtree", "~de.unibamberg.minf.dme.button.export_from_here", "fas fa-file-export", element.id));
					items.push(_this.graph.createContextMenuItem("importSubtree", "~de.unibamberg.minf.dme.button.import_here", "fas fa-file-import", element.id));
					items.push(_this.graph.createContextMenuSeparator());
					if (element.disabled) {
						items.push(_this.graph.createContextMenuItem("enableElement", "~de.unibamberg.minf.common.link.enable", "fas fa-plus-circle", element.id, element.template.options.key));
					} else {
						items.push(_this.graph.createContextMenuItem("disableElement", "~de.unibamberg.minf.common.link.disable", "fas fa-minus-circle", element.id, element.template.options.key));
					}
					items.push(_this.graph.createContextMenuItem("removeElement", "~de.unibamberg.minf.common.link.delete", "far fa-trash-alt", element.id, element.template.options.key));
				} else {
					items.push(_this.graph.createContextMenuItem("editFunction", "~de.unibamberg.minf.common.link.view", "far fa-edit", element.id, element.template.options.key));
				}
				return items; 
			}
		}, {
			key: "Grammar",
			primaryColor: "#FFE173", secondaryColor: "#6d5603", radius: 5,
			getContextMenuItems: function(element) { 
				var items = [
				    _this.graph.createContextMenuItem("expandFromHere", "~de.unibamberg.minf.dme.button.expand_from_here", "fas fa-expand", element.id, element.template.options.key),
				    _this.graph.createContextMenuItem("collapseFromHere", "~de.unibamberg.minf.dme.button.collapse_from_here", "fas fa-compress", element.id, element.template.options.key),
				    _this.graph.createContextMenuSeparator()
				];
				if (_this.schema.owned || _this.schema.write) {
					items.push(_this.graph.createContextMenuItem("addFunction", "~de.unibamberg.minf.dme.button.add_trans_function", "fas fa-plus", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("editGrammar", "~de.unibamberg.minf.common.link.edit", "far fa-edit", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuSeparator());
					items.push(_this.graph.createContextMenuItem("moveTopGrammar", "~de.unibamberg.minf.common.link.move_top", "fas fa-angle-double-up", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("moveUpGrammar", "~de.unibamberg.minf.common.link.move_up", "fas fa-angle-up", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("moveDownGrammar", "~de.unibamberg.minf.common.link.move_down", "fas fa-angle-down", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuItem("moveBottomGrammar", "~de.unibamberg.minf.common.link.move_bottom", "fas fa-angle-double-down", element.id, element.template.options.key));
					items.push(_this.graph.createContextMenuSeparator());
					items.push(_this.graph.createContextMenuItem("exportSubtree", "~de.unibamberg.minf.dme.button.export_from_here", "fas fa-file-export", element.id));
					items.push(_this.graph.createContextMenuItem("importSubtree", "~de.unibamberg.minf.dme.button.import_here", "fas fa-file-import", element.id));
					items.push(_this.graph.createContextMenuSeparator());
					if (element.disabled) {
						items.push(_this.graph.createContextMenuItem("enableElement", "~de.unibamberg.minf.common.link.enable", "fas fa-plus-circle", element.id, element.template.options.key));
					} else {
						items.push(_this.graph.createContextMenuItem("disableElement", "~de.unibamberg.minf.common.link.disable", "fas fa-minus-circle", element.id, element.template.options.key));
					}
					items.push(_this.graph.createContextMenuItem("removeElement", "~de.unibamberg.minf.common.link.delete", "far fa-trash-alt", element.id, element.template.options.key));
				} else {
					items.push(_this.graph.createContextMenuItem("editGrammar", "~de.unibamberg.minf.common.link.view", "far fa-edit", element.id, element.template.options.key));
				}
				return items; 
			}
		}, {
			key: "Terminal",
			primaryColor: "#E4FFEF", secondaryColor: "#00772C",
			getContextMenuItems: function(element) { 
				var items = [];
				items.push(_this.graph.createContextMenuItem("expandFromHere", "~de.unibamberg.minf.dme.button.expand_from_here", "fas fa-expand", element.id, element.template.options.key));
				items.push(_this.graph.createContextMenuItem("collapseFromHere", "~de.unibamberg.minf.dme.button.collapse_from_here", "fas fa-compress", element.id, element.template.options.key));
				
				if (element.reusing || element.reused) {
					items.push(_this.graph.createContextMenuItem("showReused", "~de.unibamberg.minf.dme.button.show_reused", "fas fa-expand", element.id, element.template.options.key));
				}
				
				items.push(_this.graph.createContextMenuSeparator());

				if (_this.schema.owned || _this.schema.write) {
					items.push(_this.graph.createContextMenuItem("editTerminal", "~de.unibamberg.minf.common.link.edit", "far fa-edit", element.id, element.template.options.key));
				} else {
					items.push(_this.graph.createContextMenuItem("editTerminal", "~de.unibamberg.minf.common.link.view", "far fa-edit", element.id, element.template.options.key));
				}
				return items; 
			}, 
		}, {
			key: "Terminal/Missing",
			primaryColor: "#E4FFEF", secondaryColor: "#A40000",
			getContextMenuItems: function(element) { 
				var items = [];
				items.push(_this.graph.createContextMenuItem("expandFromHere", "~de.unibamberg.minf.dme.button.expand_from_here", "fas fa-expand", element.id, element.template.options.key));
				items.push(_this.graph.createContextMenuItem("collapseFromHere", "~de.unibamberg.minf.dme.button.collapse_from_here", "fas fa-compress", element.id, element.template.options.key));
				
				if (element.reusing || element.reused) {
					items.push(_this.graph.createContextMenuItem("showReused", "~de.unibamberg.minf.dme.button.show_reused", "fas fa-expand", element.id, element.template.options.key));
				}
				
				items.push(_this.graph.createContextMenuSeparator());

				if (_this.schema.owned || _this.schema.write) {
					items.push(_this.graph.createContextMenuItem("editTerminal", "~de.unibamberg.minf.common.link.edit", "far fa-edit", element.id, element.template.options.key));
				} else {
					items.push(_this.graph.createContextMenuItem("editTerminal", "~de.unibamberg.minf.common.link.view", "far fa-edit", element.id, element.template.options.key));
				}
				return items; 
			}, 
		}
		]
	});
	
	this.area = this.graph.addArea({
		getContextMenuItems: function(area) { 
			var items = [
				_this.graph.createContextMenuItem("expandAll", "~de.unibamberg.minf.dme.button.expand_all", "fas fa-expand", area, "schema"),
				_this.graph.createContextMenuItem("collapseAll", "~de.unibamberg.minf.dme.button.collapse_all", "fas fa-compress", area, "schema"),
				_this.graph.createContextMenuItem("reset", "~de.unibamberg.minf.common.link.reset_view", "fas fa-redo", area, "schema"),
				_this.graph.createContextMenuSeparator(),
				_this.graph.createContextMenuItem("reload", "~de.unibamberg.minf.common.link.reload_data", "fas fa-sync", area, "schema"),
				_this.graph.createContextMenuItem("exportSchema", "~de.unibamberg.minf.dme.button.export", "fas fa-file-export", area, "schema"),
			]; 
			if (_this.schema.owned || _this.schema.write) {
				items.push(_this.graph.createContextMenuSeparator(),
						_this.graph.createContextMenuItem("importSchema", "~de.unibamberg.minf.dme.button.import", "fas fa-file-import", area, "schema"),
						_this.graph.createContextMenuItem("createRoot", "~de.unibamberg.minf.dme.button.create_root", "fas fa-plus-circle", area, "schema")); 
				
			}
			return items;
		}
	});
	this.graph.init();
	this.createActionButtons(this.schemaContextButtons, this.area.getContextMenuItems());
	
	this.contextMenuClickEventHandler = this.handleContextMenuClicked.bind(this);
	document.addEventListener("contextMenuClickEvent", this.contextMenuClickEventHandler, false);
};

SchemaEditor.prototype.handleContextMenuClicked = function(e) {
	this.performTreeAction(e.key, e.id, e.nodeType);
};

SchemaEditor.prototype.performTreeAction = function(action, elementId, elementType) {	
	switch(action) {
		case "expandFromHere" : return this.area.expandFromElement(elementId, true);
		case "collapseFromHere" : return this.area.expandFromElement(elementId, false);
	
		case "addNonterminal": return this.addNonterminal(elementId);
	    case "addDescription": return this.addDescription(elementId);
	    case "addFunction": return this.addTransformation(elementId);
	    case "addLabel": return this.addLabel(elementId);
	    
	    case "editElement" : return this.editElement(elementId);
	    case "editGrammar" : return this.editGrammar(elementId);
	    case "editFunction" : return this.editFunction(elementId);
	    case "editTerminal": return this.editTerminal(elementType, elementId);
	    
	    case "linkElement" : return this.linkElement(elementId, elementType);
	    case "assignChild" : return this.assignChild(elementId);

	    case "moveTopGrammar" : return this.moveGrammar(2);
	    case "moveUpGrammar" : return this.moveGrammar(1);
	    case "moveDownGrammar" : return this.moveGrammar(-1);
	    case "moveBottomGrammar" : return this.moveGrammar(-2);
	    
	    case "moveTopFunction" : return this.moveFunction(2);
	    case "moveUpFunction" : return this.moveFunction(1);
	    case "moveDownFunction" : return this.moveFunction(-1);
	    case "moveBottomFunction" : return this.moveFunction(-2);
	    
	    case "moveTopElement" : return this.moveElement(2);
	    case "moveUpElement" : return this.moveElement(1);
	    case "moveDownElement" : return this.moveElement(-1);    
	    case "moveBottomElement" : return this.moveElement(-2);
	    
	    case "exportSubtree" :  return this.exportSubtree(elementId);
	    case "importSubtree" :  return this.importSchema(elementId);
	    
	    case "removeElement" :  return this.removeElement(elementType, elementId);
	    case "disableElement" :  return this.toggleElementDisabled(elementType, elementId, true);
	    case "enableElement" :  return this.toggleElementDisabled(elementType, elementId, false);
	    
	    case "expandAll" :  return this.area.expandAll(true);
	    case "collapseAll" : return this.area.expandAll(false);
	    case "reload" : return this.reloadElementHierarchy();
	    case "reset" : return this.area.resetView();
	    
	    case "showReused" : return this.showReuseOccurrences(elementId);
	    case "modelIndividually" : return this.modelIndividually(elementId);
	    
	    case "exportSchema" : return this.exportSchema();
	    case "importSchema" : return this.importSchema();
	    case "createRoot" : return this.createRoot();
	    
	    default:
	        throw new Error("Unknown tree action requested: " + action);
	}  
};

SchemaEditor.prototype.showReuseOccurrences = function(elementId) {
	this.area.ensureExpandedTo(elementId);
	//this.area.selectElementsByIds(this.area.root, [elementId]);
};

SchemaEditor.prototype.modelIndividually = function(elementId) {
	var _this = this;
	bootbox.confirm({
		title: __translator.translate("~de.unibamberg.minf.dme.dialog.confirm_clone_tree.head"),
		message: __translator.translate("~de.unibamberg.minf.dme.dialog.confirm_clone_tree.body"),
		callback: function(result) {
			if(result) {
				var path = _this.area.getElementPath(_this.graph.selectedItems[0]);
				$.ajax({
				    url: __util.composeRelativeUrl("element/" + _this.graph.selectedItems[0].id + "/async/clone"),
				    type: "POST",
				    data: {path: path.reverse()},
				    dataType: "json",
				    //contentType: "application/json; charset=UTF-8",
				    success: function(data) {
				    	_this.reloadElementHierarchy();
				    },
				    error: __util.processServerError
				});
			}
		}
	});
};

SchemaEditor.prototype.loadElementHierarchy = function() {
	var _this = this;
	
	if (this.checkEntityState()===true) {
		
		$.ajax({
		    url: __util.composeRelativeUrl("async/getHierarchy"),
		    //data: { model: $("#current-model-nature").val(), collectNatureClasses: true },
		    data: { model: "logical_model", collectNatureClasses: true },
		    type: "GET",
		    success: function(data) {
		    	if (data===null || data===undefined || data.length==0) {
		    		return;
		    	}
		    	_this.processElementHierarchy(data);
		    	_this.updateGraph();
		    },
		    error: __util.processServerError
		});
	} else {
		setTimeout(function() { _this.loadElementHierarchy() }, 2000);
	}
};

SchemaEditor.prototype.reloadElementHierarchy = function(callback) {
	if (editor.area.root==null) {
		this.loadElementHierarchy();
		return;
	}
	
	var _this = this;
	var rootX = this.area.root.rectangle.x;
	var rootY = this.area.root.rectangle.y;
	var expandedItemIds = this.area.getExpandedElementIds(this.area.root);

	
	var selectedItemIds = [];
	for (var i=0; i<this.graph.selectedItems.length; i++) {
		selectedItemIds.push(this.graph.selectedItems[i].id);
	}
	
	if (this.checkEntityState()===true) {
		var _this = this;
		$.ajax({
		    url: __util.composeRelativeUrl("async/getHierarchy"),
		    type: "GET",
		    data: { model: $("#current-model-nature").val(), collectNatureClasses: true },
		    dataType: "json",
		    success: function(data) {
		    	if (data===null || data===undefined || data.length==0) {
		    		return;
		    	}
		    	_this.area.clear();
		    	_this.processElementHierarchy(data);	    	
		    	_this.area.root.rectangle = new Rectangle(rootX, rootY, _this.area.root.rectangle.width, _this.area.root.rectangle.height);
		    			
		    	_this.area.selectElementsByIds(_this.area.root, selectedItemIds);    		
		    	_this.area.expandElementsByIds(_this.area.root, expandedItemIds);
		    	if (callback!==undefined) {
		    		callback();
		    	}
		    	_this.area.invalidate();
		    	_this.graph.paint();
		    },
		    error: function(jqXHR, textStatus, errorThrown) {
		    	__util.processServerError(jqXHR, textStatus, errorThrown);
		    	_this.initGraph();
		    }
		});	
	} else {
		setTimeout(function() { _this.reloadElementHierarchy(callback) }, 2000);
	}
};

SchemaEditor.prototype.processElementHierarchy = function(data) {
	this.generateTree(this.area, data.pojo, null, true, data.pojo.pRoot, data.pojo.disabled, !data.pojo.pRoot && data.statusInfo.includeHeaders===true);
	this.area.elements[0].setExpanded(true);
	this.graph.update();
};

SchemaEditor.prototype.generateTree = function(area, node, parentNode, isSource, processed, disabled, header) {
	var icons = [];
	var terminalMissing = false;
	if (node.state==="ERROR") {
		icons.push(this.options.icons.error);
	} else if (node.state==="WARNING") {
		icons.push(this.options.icons.warning);
	} else if (node.state==="REUSING") {
		icons.push(this.options.icons.reusing);
	} else if (node.state==="REUSED") {
		icons.push(this.options.icons.reused);
	}
	if (node.state!=="REUSING") {
		if (node.type==="Nonterminal" && this.availableNatures!==undefined && this.availableNatures!==null && this.availableNatures.length > 0) {
			if (node.info===undefined || node.info===null || node.info["mappedNatureClasses"]===undefined || node.info["mappedNatureClasses"].length<this.availableNatures.length) {
				icons.push(this.options.icons.warning);
				terminalMissing = true;
			}
		}
	}
	if (node.sessionVariable) {
		icons.push(this.options.icons.database);
	}	
	if (node.resourceId===true) {
		icons.push(this.options.icons.identifier);
	}
	
	var processed = processed || node.pRoot;
	var disabled = disabled || node.disabled
	var header =  !(processed || node.pRoot) && header;
	
	var e = this.area.addElement(node.type, parentNode, node.id, this.formatLabel(node.label), icons, processed, disabled, header);
	e.reusing = node.state==="REUSING";
	e.reused = node.state==="REUSED";
	e.terminalMissing = terminalMissing;
		
	if (node.childElements!=null && node.childElements instanceof Array) {
		for (var i=0; i<node.childElements.length; i++) {
			this.generateTree(area, node.childElements[i], e, isSource, (processed || node.childElements[i].pRoot), (disabled || node.childElements[i].disabled), !(processed || node.childElements[i].pRoot) && header);
		}
	}
}

SchemaEditor.prototype.formatLabel = function(label) {
	if (label!==undefined && label.length > 35) {
		return label.substring(0,35) + "...";
	} else {
		return label;
	}
};

SchemaEditor.prototype.updateGraph = function() {
	this.graph.update();
};

SchemaEditor.prototype.processElementDetails = function(data, callback, container, pathPrefix) { 
	container.append("<h3>" + __translator.translate("~de.unibamberg.minf.dme.model.element.element") + "</h3>");
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.common.model.id"), data.id));
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.dme.model.element.name"), data.label));
	container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.dme.model.element.transient"), data.disabled));

	if (data.type==="Nonterminal" && this.availableNatures!==undefined && this.availableNatures!==null && this.availableNatures.length > 0) {
		var missing = "";
		for (var i=0; i<this.availableNatures.length; i++) {
			if (data.info===undefined || data.info===null || 
					data.info["mappedNatureClasses"]===undefined || 
					data.info["mappedNatureClasses"].indexOf(this.availableNatures[i])<0) {
				
				var label = "~" + this.availableNatures[i] + ".display_label";
		    	__translator.addTranslation(label);
		    	__translator.getTranslations();
				
				missing = missing + __translator.translate(label) + ", ";
			}
		}
		if (missing.length>0) {
			container.append(this.renderContextTabDetail(__translator.translate("~de.unibamberg.minf.dme.form.nature.terminals.missing"), missing.substring(0, missing.length-2), undefined, "color-warning"));
		}
	}
	
	if (callback!==undefined) {
		callback(data, container, pathPrefix);
	}
};