SchemaEditor.prototype.addImportExportTranslations = function() {
	__translator.addTranslations([
	    "~de.unibamberg.minf.common.view.forms.servererror.head",
	    "~de.unibamberg.minf.common.view.forms.servererror.body",

	    "~de.unibamberg.minf.common.model.types.descriptiongrammarimpl",
	    "~de.unibamberg.minf.common.model.types.labelimpl",
	    "~de.unibamberg.minf.common.model.types.nonterminalimpl",
	    "~de.unibamberg.minf.common.model.types.transformationfunctionimpl"
	]);
};

SchemaEditor.prototype.importSchema = function(elementId) {
	var _this = this;
	var form_identifier = "upload-file-" + this.schema.id;
	
	var data = elementId===undefined ? undefined : { elementId : elementId };
	
	var modalFormHandler = new ModalFormHandler({
		formFullUrl: __util.composeRelativeUrl("forms/import/"),
		data: data,
		identifier: form_identifier,
		translations: [{placeholder: "~*servererror.head", key: "~de.unibamberg.minf.common.view.forms.servererror.head"},
		                {placeholder: "~*servererror.body", key: "~de.unibamberg.minf.common.view.forms.servererror.body"}
		                ],
		additionalModalClasses: "modal-lg",
		displayCallback: function(container, form) {
			$(container).find("form").submit(function() {
				$(this).find(".btn")
					.attr("disabled", "disabled")
					.addClass("disabled")
					.removeAttr("type");
				
				$(this).find(".form-btn-submit")
					.prepend("<i class=\"fas fa-spinner fa-lg fa-spin\"></i> ");
			});
		},
		completeCallback: function() { 
			_this.reloadPage();
		}
	});
	
	modalFormHandler.fileUploadElements.push({
		selector: "#schema_source",				// selector for identifying where to put widget
		formSource: "forms/fileupload",			// where is the form
		uploadTarget: "async/upload" + (elementId===undefined ? "": "/" + elementId), 			// where to we upload the file(s) to
		multiFiles: false, 						// one or multiple files
		elementChangeCallback: _this.setupRootSelection
	});
		
	modalFormHandler.show(form_identifier);
};


SchemaEditor.prototype.setupRootSelection = function(data) {
	var rootSelector = $("#schema_root");
	rootSelector.typeahead("destroy");
	
	
	rootSelector.val("");
	
	
	
	// No root elements
	if (data==null || data.pojo==null || data.pojo.elements==null || data.pojo.elements.length==0) {
		rootSelector.prop("disabled", "disabled");
		$("#btn-submit-schema-elements").prop("disabled", "disabled");
		$("#importer-options").addClass("hide");
		return;
	}
	
	$("#importer-type").text(data.pojo.importerMainType);
	$("#importer-subtype").text(data.pojo.importerSubtype);
	
	if (data.pojo.keepIdsAllowed===true) {
		$("#importer-keep-ids").removeClass("hide");
	} else {
		$("#importer-keep-ids").addClass("hide");
	}
	
	$("#importer-options").removeClass("hide");
	$("#schema_root_id").val(data.pojo.elements[0].id);
	
	if (data.pojo.elements[0].namespace===undefined) {
		$("#schema_root_qn").val(data.pojo.elements[0].name);
	} else {
		$("#schema_root_qn").val("{" + data.pojo.elements[0].namespace + "}:" + data.pojo.elements[0].name);
	}
	$("#schema_root_type").val(data.pojo.elements[0].type);
	rootSelector.val(data.pojo.elements[0].name);
	
	if (data.pojo.elements.length==1) {
		return;
	} 
	
	rootSelector.removeAttr("disabled");
	
	var _this = editor;
	var elements = new Bloodhound({
		  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
		  queryTokenizer: Bloodhound.tokenizers.whitespace,
		  identify: function(obj) { return (obj.namespace + ":" + obj.name + " (" + obj.id + ")"); },
		  local: data.pojo.elements
	});
	
	_this.registerTypeahead(rootSelector, "importedelements", elements, "name", 8, 
			function(e) {
				if (e.namespace!==undefined && e.namespace!==null && e.namespace!=="") {
					return '<p><strong>' + e.name + '</strong> (id: ' + e.id + ')<br/ >' + e.namespace + '<p>';
				} else if (e.simpleType!==undefined && e.simpleType!==null && e.simpleType!=="") {
					return '<p><strong>' + e.name + '</strong> (id: ' + e.id + ')' + 
							'<small><em>' + __translator.translate("~de.unibamberg.minf.common.model.types." + e.simpleType.toLowerCase()) + '</em></small>' +
						'<p>';
				} else {
					return '<p><strong>' + e.name + '</strong> (id: ' + e.id + ')</p>';
				}
			},
			function(t, suggestion) {
				if (suggestion.namespace===undefined) {
					$("#schema_root_qn").val(suggestion.name);
				} else {
					$("#schema_root_qn").val("{" + suggestion.namespace + "}:" + suggestion.name);
				}
				$("#schema_root_type").val(suggestion.type);
				$("#schema_root_id").val(suggestion.id!==undefined ? suggestion.id : "")
			},
			function(t, value) {
				for (var i=0; i<data.pojo.elements.length; i++) {
					if (value===data.pojo.elements[i].name) {
						if (data.pojo.elements[i].namespace===undefined) {
							$("#schema_root_qn").val(data.pojo.elements[i].name);
						} else {
							$("#schema_root_qn").val("{" + data.pojo.elements[i].namespace + "}:" + data.pojo.elements[i].name);
						}
						$("#schema_root_type").val(data.pojo.elements[i].type);
						$("#schema_root_id").val(data.pojo.elements[i].id!==undefined ? data.pojo.elements[i].id : "")
						return;
					}
				}
				$("#schema_root_qn").val("");
				$("#schema_root_type").val("");
				$("#schema_root_id").val("")
			}			
	);

	rootSelector.focus();
	$("#btn-submit-schema-elements").removeProp("disabled");
	
};

SchemaEditor.prototype.exportSchema = function() {
	var _this = this;

	$.ajax({
	    url: __util.composeRelativeUrl("async/export"),
	    type: "GET",
	    dataType: "json",
	    success: function(data) {
	    	blob = new Blob([JSON.stringify(data.pojo)], {type: "application/json; charset=utf-8"});
	    	saveAs(blob, "schema_" + _this.schema.id + ".json");
	    },
	    error: __util.processServerError
	});
};

SchemaEditor.prototype.exportSubtree = function(elementId) {
	var _this = this;
	
	$.ajax({
	    url: __util.composeRelativeUrl("async/exportSubtree"),
	    data: { elementId: elementId },
	    type: "GET",
	    dataType: "json",
	    success: function(data) {
	    	blob = new Blob([JSON.stringify(data.pojo)], {type: "application/json; charset=utf-8"});
	    	saveAs(blob, "subtree_" + _this.schema.id + ".json");
	    },
	    error: __util.processServerError
	});
};
