package de.unibamberg.minf.dme.importer.datamodel.json;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.dme.model.base.Identifiable;
import de.unibamberg.minf.dme.model.base.ModelElement;
import de.unibamberg.minf.dme.model.datamodel.NonterminalImpl;
import de.unibamberg.minf.dme.model.datamodel.base.Datamodel;
import de.unibamberg.minf.dme.model.datamodel.base.DatamodelNature;
import de.unibamberg.minf.dme.model.exception.MetamodelConsistencyException;
import de.unibamberg.minf.dme.model.reference.ReferenceHelper;
import de.unibamberg.minf.dme.model.serialization.DatamodelReferenceContainer;
import de.unibamberg.minf.dme.service.interfaces.ReferenceService;

@Component
@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class JsonDatamodelReferenceImporter extends BaseJsonDatamodelImporter {
	
	@Autowired private ReferenceService referenceService;
	
	@Override public String getImporterSubtype() { return "Datamodel v1.1"; }
	
	@Override
	public boolean getIsSupported() {
		if (super.getIsSupported()) {
			try {
				objectMapper.readValue(new File(this.importFilePath), DatamodelReferenceContainer.class);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}

	@Override
	public List<? extends Identifiable> getPossibleRootElements() {
		try {
			List<Class<? extends ModelElement>> allowedRootTypes = new ArrayList<>();
			allowedRootTypes.add(NonterminalImpl.class);
			
			return this.getElementsByTypes(allowedRootTypes);
		} catch (Exception e) {
			logger.error("Failed to retrieve possible root elements for schema", e);
			return new ArrayList<>(0);
		}
	}

	@Override
	public List<? extends ModelElement> getElementsByTypes(List<Class<? extends ModelElement>> allowedSubtreeRoots) {
		try {
			DatamodelReferenceContainer s = objectMapper.readValue(new File(this.importFilePath), DatamodelReferenceContainer.class);
			List<ModelElement> result = new ArrayList<>();
			if (s.getElements()!=null) {
				for (ModelElement me : s.getElements().values()) {
					if (allowedSubtreeRoots.contains(me.getClass())) {
						result.add(me);
					}
				}
			}
			return result;
		} catch (Exception e) {
			logger.error("Failed to extract model elements from serialized datamodel", e);
			return new ArrayList<>(0);
		}
	}

	@Override
	protected void importJson() throws IOException, MetamodelConsistencyException {
		DatamodelReferenceContainer s = objectMapper.readValue(new File(this.importFilePath), DatamodelReferenceContainer.class);
		Datamodel m = s.getModel();
		m.setId(this.getDatamodel().getId());
		
		var importRoot = referenceService.findReferenceById(s.getRoot(), this.getRootElementName());
		
		ModelElement e = (ModelElement)ReferenceHelper.fillElement(importRoot, s.getElements());
		
		Map<String, String> oldToNewIdMap = new HashMap<>();
		this.reworkElementHierarchy(m.getId(), e, oldToNewIdMap, new HashMap<>(), null);
		
		if (!isKeepImportedIds() && m.getNatures()!=null) {
			for (DatamodelNature nature : m.getNatures()) {
				this.regenerateTerminalIds(nature, oldToNewIdMap);
			}
		}
		
		if (this.getRootElementType()==null || this.getRootElementType().isEmpty()) {
			this.setRootElementType(e.getClass().getName());
		}
		
		this.setRootElements(new ArrayList<>());
		this.getRootElements().add(e);
	
		this.setDatamodel(m);
	}
}
