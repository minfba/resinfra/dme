package de.unibamberg.minf.dme.controller.exceptions;

import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import eu.dariah.de.dariahsp.config.SecurityConfig;
import eu.dariah.de.dariahsp.error.RequiredAttributesException;

/**
 * Error controller for the sample application
 * 
 * @author Tobias Gradl
 */
@Controller
@RequestMapping({"${server.error.path:${error.path:/error}}"})
public class ErrorController extends BasicErrorController {
	@Autowired private SecurityConfig securityConfig;
	
	/**
	 * Constructor with autowired ErrorAttributes
	 *
	 * @param errorAttributes
	 */
	@Autowired
	public ErrorController(ErrorAttributes errorAttributes) {
		super(errorAttributes, new ErrorProperties());
	}

	/**
	 * Error handling method dispatching all errors as messages to the default index view. 
	 *  Particular attention lies on the treatment of the {@link RequiredAttributesException}
	 */
    @Override
    @RequestMapping(produces = {"text/html"})
    public ModelAndView errorHtml(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Object> attr = getErrorAttributes(request, getErrorAttributeOptions(request, MediaType.ALL));
        HttpStatus status = getStatus(request);
        this.assembleMap(attr, status);
        
        // This is an error that needs special treatment in DARIAH/CLARIAH
        Object ex = request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
        if (ex!=null && RequiredAttributesException.class.isAssignableFrom(ex.getClass())) {
        	StringBuilder errorStrBldr = new StringBuilder();
        	
        	errorStrBldr.append("Your IdP did not provide all required attributes. ");
        	if (securityConfig.getSaml().getSp().getAttributesIncompleteRedirectUrl()!=null) {
        		errorStrBldr
        			.append("Please visit ")
        			.append("<a href='").append(securityConfig.getSaml().getSp().getAttributesIncompleteRedirectUrl()).append("'>")
        			.append(securityConfig.getSaml().getSp().getAttributesIncompleteRedirectUrl())
        			.append("</a> to validate your profile setup.");
        	}
        	
        	attr.put("hint", errorStrBldr.toString());
        	attr.put("error", HttpStatus.FORBIDDEN.value());
        	attr.put("reason", HttpStatus.FORBIDDEN.getReasonPhrase());
        }                
        
        return new ModelAndView("error", attr);
    }
    
    /* Do not do this in production as users should not see all reasons openly */ 
    @Override
    protected boolean isIncludeMessage(HttpServletRequest request, MediaType produces) {
		return true;
	}
    
	private void assembleMap(Map<String, Object> map, HttpStatus status) {
		map.put("error", status.value());
		map.put("reason", status.getReasonPhrase());
		map.put("localEnabled", securityConfig.getLocal().isEnabled());
	    map.put("samlEnabled", securityConfig.getSaml().isEnabled());
	}
}