package de.unibamberg.minf.dme.profiles;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.dme.service.UserServiceImpl;
import eu.dariah.de.dariahsp.ProfileActionHandler;
import eu.dariah.de.dariahsp.model.ExtendedUserProfile;
import eu.dariah.de.dariahsp.model.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserProfileActionHandler implements ProfileActionHandler {			
	
	@Autowired private UserServiceImpl userService;
	
	
	@Override
	public void handleLogin(ExtendedUserProfile profile) {
		
		
		User u = userService.loadUserByUsername(profile.getIssuerId(), profile.getUsername());
		if (u==null) {
			u = profile.toUser();
		} 
		u.setLastLogin(LocalDateTime.now());
		
		userService.saveUser(u);
		
		profile.setId(u.getId());
		
		log.debug("User has logged in: {}=>{}", profile.getId(), u.getId());
	}

	@Override
	public void handleLogout(ExtendedUserProfile profile) {		
		log.debug("User has logged out: {}", profile.getId());
	}
}