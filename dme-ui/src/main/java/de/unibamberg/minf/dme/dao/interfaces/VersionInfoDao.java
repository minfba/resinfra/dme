package de.unibamberg.minf.dme.dao.interfaces;

import de.unibamberg.minf.dme.dao.base.BaseDao;
import de.unibamberg.minf.dme.model.version.VersionInfo;

public interface VersionInfoDao extends BaseDao<VersionInfo> {
}
