package de.unibamberg.minf.dme.migration;

public interface MigrationAction {
	public boolean migrate();
}
