package de.unibamberg.minf.dme.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import de.unibamberg.minf.dme.dao.interfaces.ElementDao;
import de.unibamberg.minf.dme.dao.interfaces.FunctionDao;
import de.unibamberg.minf.dme.dao.interfaces.GrammarDao;
import de.unibamberg.minf.dme.dao.interfaces.ReferenceDao;
import de.unibamberg.minf.dme.dao.interfaces.RelatedConceptDao;
import de.unibamberg.minf.dme.exception.GenericScheregException;
import de.unibamberg.minf.dme.model.base.Element;
import de.unibamberg.minf.dme.model.base.Function;
import de.unibamberg.minf.dme.model.base.Grammar;
import de.unibamberg.minf.dme.model.base.Identifiable;
import de.unibamberg.minf.dme.model.function.FunctionImpl;
import de.unibamberg.minf.dme.model.grammar.GrammarImpl;
import de.unibamberg.minf.dme.model.mapping.ExportedConceptImpl;
import de.unibamberg.minf.dme.model.mapping.MappedConceptImpl;
import de.unibamberg.minf.dme.model.mapping.ExportedConceptImpl.ExportFormats;
import de.unibamberg.minf.dme.model.mapping.base.MappedConcept;
import de.unibamberg.minf.dme.model.mapping.base.RelatedConcept;
import de.unibamberg.minf.dme.model.reference.Reference;
import de.unibamberg.minf.dme.model.reference.ReferenceHelper;
import de.unibamberg.minf.dme.model.reference.RootReference;
import de.unibamberg.minf.dme.pojo.RelatedConceptPojo.MappingTypes;
import de.unibamberg.minf.dme.service.base.BaseReferenceServiceImpl;
import de.unibamberg.minf.dme.service.interfaces.RelatedConceptService;
import eu.dariah.de.dariahsp.web.model.AuthPojo;

@Service
public class RelatedConceptServiceImpl extends BaseReferenceServiceImpl implements RelatedConceptService {
	@Autowired private RelatedConceptDao mappedConceptDao;
	@Autowired private ElementDao elementDao;
	@Autowired private GrammarDao grammarDao;
	@Autowired private FunctionDao functionDao;
	
	@Autowired private ReferenceDao referenceDao;
	
	// TODO: This method needs quite some refactoring
	@Override
	public void saveRelatedConcept(RelatedConcept relatedConcept, String mappingId, AuthPojo auth) {		
		relatedConcept.setEntityId(mappingId);		
		mappedConceptDao.save(relatedConcept, auth.getUserId(), auth.getSessionId());
		
		RootReference root = this.findReferenceById(mappingId);
		Reference refConcept;
		boolean needReferenceSave = false; 
		
		boolean isNew = ReferenceHelper.findSubreference(root, relatedConcept.getId())==null;
		
		if (isNew) {
			refConcept = BaseReferenceServiceImpl.addChildReference(root, relatedConcept);
			if (MappedConcept.class.isAssignableFrom(relatedConcept.getClass())) {
				Function function = null;
				MappedConcept mappedConcept = MappedConcept.class.cast(relatedConcept);
				if (mappedConcept.getFunctionId()!=null) {
					function = functionDao.findById(mappedConcept.getFunctionId());
				}
				if (function==null) {
					function = new FunctionImpl(mappingId, "fMapping");
					functionDao.save(function, auth.getUserId(), auth.getSessionId());
					mappedConcept.setFunctionId(function.getId());
				}
				addChildReference(refConcept, function); 
			}
			needReferenceSave = true;
		} else {
			refConcept = ReferenceHelper.findSubreference(root, relatedConcept.getId());
		}

		for (String sourceElementId : relatedConcept.getElementGrammarIdsMap().keySet()) {
			
			Grammar grammar;
			
			if (relatedConcept.getElementGrammarIdsMap().get(sourceElementId)==null) {
				Element source = elementDao.findById(sourceElementId);
				
				grammar = new GrammarImpl(mappingId, source.getName());
				grammar.setPassthrough(true);
				grammarDao.save(grammar, auth.getUserId(), auth.getSessionId());
				
				relatedConcept.getElementGrammarIdsMap().put(sourceElementId, grammar.getId());
				
			} else {
				grammar = grammarDao.findById(relatedConcept.getElementGrammarIdsMap().get(sourceElementId));
			}
			
			addChildReference(refConcept, grammar);
			needReferenceSave = true;
		}
		
		if (needReferenceSave) {
			this.saveRootReference(root);
			mappedConceptDao.save(relatedConcept, auth.getUserId(), auth.getSessionId());
		}
	}
	
	
	@Override
	public List<RelatedConcept> findAllByMappingId(String mappingId) {
		return mappedConceptDao.findByEntityId(mappingId);
	}
	
	@Override
	public List<RelatedConcept> findAllByMappingId(String mappingId, boolean eagerLoadHierarchy) {
		Reference reference = this.findReferenceById(mappingId);
		if (reference.getChildReferences()==null || reference.getChildReferences().isEmpty()) {
			return new ArrayList<>();
		}
		List<RelatedConcept> result = new ArrayList<>();
		List<Identifiable> elements = this.getAllElements(mappingId);		
		Map<String, Identifiable> elementMap = new HashMap<>(elements.size()); 
		for (Identifiable e : elements) {
			elementMap.put(e.getId(), e);
		}
		Identifiable iTest;
		for (Reference r : reference.getChildReferences(MappedConceptImpl.class.getName(), ExportedConceptImpl.class.getName())) {
			iTest = ReferenceHelper.fillElement(r, elementMap);
			if (iTest!=null) {
				result.add(RelatedConcept.class.cast(iTest));
			} else {
				// TODO: This null check is only a hotfix. The problem seems to be importer-related however
				logger.warn("Inconsistency warning: Mapping references concept that no longer exists");
			}
		}
		return result;
	}

	@Override
	public RelatedConcept findById(String id) {
		return mappedConceptDao.findById(id);
	}
	
	@Override
	public RelatedConcept findById(String mappingId, String mappedConceptId, boolean eagerLoadHierarchy) {
		Reference reference = this.findReferenceById(mappingId);
		if (reference.getChildReferences()==null || reference.getChildReferences().isEmpty()) {
			return null;
		}
		
		List<String> grammarIds = new ArrayList<>();
		List<String> functionIds = new ArrayList<>();
		Reference r = null;
		
		for (Reference rConcept : reference.getChildReferences(MappedConceptImpl.class.getName(), ExportedConceptImpl.class.getName())) {
			if (rConcept.getReferenceId().equals(mappedConceptId)) {
				r = rConcept;
				if (rConcept.getChildReferences()!=null && rConcept.getChildReferences().containsKey(GrammarImpl.class.getName())) {
					for (Reference rGrammar : rConcept.getChildReferences().get(GrammarImpl.class.getName())) {
						grammarIds.add(rGrammar.getReferenceId());
						if (rGrammar.getChildReferences()!=null && rGrammar.getChildReferences().containsKey(FunctionImpl.class.getName())) {
							for (Reference rFunction : rGrammar.getChildReferences().get(FunctionImpl.class.getName())) {
								functionIds.add(rFunction.getReferenceId());
							}
						}
					}
				}
				break;
			}
		}
		if (r==null) {
			return null;
		}
		
		List<Identifiable> elements = new ArrayList<>();
		elements.add(mappedConceptDao.findById(mappedConceptId));
		elements.addAll(grammarDao.find(Query.query(Criteria.where("_id").in(grammarIds))));
		elements.addAll(functionDao.find(Query.query(Criteria.where("_id").in(functionIds))));
		Map<String, Identifiable> elementMap = new HashMap<>(elements.size()); 
		for (Identifiable e : elements) {
			elementMap.put(e.getId(), e);
		}
		
		return (RelatedConcept)ReferenceHelper.fillElement(r, elementMap);
	}

	@Override
	public void removeRelatedConcept(String mappingId, String conceptId, AuthPojo auth) throws GenericScheregException {
		RelatedConcept c = mappedConceptDao.findById(conceptId);
		if (!c.getEntityId().equals(mappingId)) {
			throw new GenericScheregException("Attempted to delete related concept via wrong mapping");
		}
		try {
			this.removeReference(mappingId, conceptId, auth);
			mappedConceptDao.delete(c, auth.getUserId(), auth.getSessionId());
		} catch (IllegalArgumentException | ClassNotFoundException e) {
			logger.error("Failed to remove related concept", e);
		}
		
	}
	
	@Override
	public void removeSourceElementById(AuthPojo auth, String mappingId, RelatedConcept rc, String sourceId) {
		if (!rc.getElementGrammarIdsMap().containsKey(sourceId)) {
			return;
		}
		rc.getElementGrammarIdsMap().remove(sourceId);
		
		if (rc.getElementGrammarIdsMap().size()==0) {
			try {
				this.removeRelatedConcept(mappingId, rc.getId(), auth);
			} catch (GenericScheregException e) {
				logger.error("Failed to remove empty related concept", e);
			}
		} else {
			this.saveRelatedConcept(rc, mappingId, auth);
			
			RootReference rMapping = this.findReferenceById(mappingId);
			ReferenceHelper.removeSubreference(rMapping, sourceId);
			this.saveRootReference(rMapping);
		}
	}

	@Override
	public void removeSourceElementById(AuthPojo auth, String mappingId, String conceptId, String sourceId) {
		this.removeSourceElementById(auth, mappingId, this.findById(conceptId), sourceId);
	}
	
	private List<Identifiable> getAllElements(String mappingId) {
		List<Identifiable> elements = new ArrayList<>();
		elements.addAll(mappedConceptDao.findByEntityId(mappingId));
		elements.addAll(grammarDao.findByEntityId(mappingId));
		elements.addAll(functionDao.findByEntityId(mappingId));
		return elements;
	}


	@Override
	public void removeElementReferences(String entityId, String elementId) {
		List<String> deleteConcepts = new ArrayList<>();
		List<String> deleteGrammars = new ArrayList<>();
		List<String> deleteFunctions = new ArrayList<>();
		
		Map<String, RootReference> rootReferences = new HashMap<>();
		
		
		List<RelatedConcept> concepts = mappedConceptDao.findBySourceElementId(elementId);
		concepts.addAll(mappedConceptDao.findByTargetElementId(elementId));

		String removeGrammarId;
		RootReference currentRootReference;
		for (RelatedConcept c : concepts) {
			if (rootReferences.containsKey(c.getEntityId())) {
				currentRootReference = rootReferences.get(c.getEntityId());
			} else {
				currentRootReference = referenceDao.findById(c.getEntityId());
				rootReferences.put(c.getEntityId(), currentRootReference);
			}
			
			if (c.getElementGrammarIdsMap().containsKey(elementId)) {
				removeGrammarId = c.getElementGrammarIdsMap().remove(elementId);
				deleteGrammars.add(removeGrammarId);
				referenceDao.removeById(currentRootReference, removeGrammarId);
			} else if (c.getTargetElementIds().contains(elementId)) {
				c.getTargetElementIds().remove(elementId);
			}
			
			// In this case, no individual subreferences are deleted bc the whole concept will be removed
			if (c.getTargetElementIds().isEmpty() || c.getElementGrammarIdsMap().isEmpty()) {
				if (MappedConcept.class.isAssignableFrom(c.getClass())) {
					deleteFunctions.add(((MappedConcept)c).getFunctionId());
				}
				if (c.getElementGrammarIdsMap().size()>0) {
					for (String sourceId : c.getElementGrammarIdsMap().keySet()) {
						deleteGrammars.add(c.getElementGrammarIdsMap().get(sourceId));
					}
				}
				deleteConcepts.add(c.getId());
				referenceDao.removeById(currentRootReference, c.getId());
			} else {
				mappedConceptDao.save(c);
			}
		}
		
		for (RootReference root : rootReferences.values()) {
			referenceDao.save(root);
		}
		functionDao.delete(deleteFunctions);
		grammarDao.delete(deleteGrammars);
		mappedConceptDao.delete(deleteConcepts);
	}


	@Override
	public RelatedConcept switchConceptType(String mappingId, String mappedConceptId, MappingTypes mappingType, AuthPojo auth) {
		try {
			RelatedConcept rcDel = this.findById(mappedConceptId);
			RelatedConcept rcReplace;
			if (mappingType.equals(MappingTypes.EXPORT)) {
				ExportedConceptImpl ecReplace = new ExportedConceptImpl();
				ecReplace.setFormat(ExportFormats.JSON);
				ecReplace.setIncludeTree(true);
				ecReplace.setIncludeSelf(true);
				ecReplace.setEscape(false);
				
				rcReplace = ecReplace;
			} else {
				rcReplace = new MappedConceptImpl();
			}
			rcReplace.setId(mappedConceptId);
			rcReplace.setTargetElementIds(new ArrayList<>(rcDel.getTargetElementIds()));
			rcReplace.setElementGrammarIdsMap(new HashMap<>(rcDel.getElementGrammarIdsMap()
					.entrySet()
					.stream()
					.collect(Collectors.toMap(Entry::getKey, Entry::getValue))
					));
			
			this.removeRelatedConcept(mappingId, mappedConceptId, auth);
			this.saveRelatedConcept(rcReplace, mappingId, auth);
			
			return rcReplace;
		} catch (GenericScheregException e) {
			logger.error("Failed to remove mapped concept", e);
		}
		return null;
	}
}