package de.unibamberg.minf.dme.service.interfaces;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import de.unibamberg.minf.dme.model.base.Grammar;
import de.unibamberg.minf.dme.model.grammar.GrammarContainer;
import de.unibamberg.minf.dme.model.grammar.GrammarImpl;
import de.unibamberg.minf.dme.service.base.BaseReferenceService;
import de.unibamberg.minf.gtf.compilation.GrammarCompilationResult;
import de.unibamberg.minf.gtf.exceptions.GrammarProcessingException;
import eu.dariah.de.dariahsp.web.model.AuthPojo;

public interface GrammarService extends BaseReferenceService {
	public Grammar createAndAppendGrammar(String schemaId, String parentElementId, String label, AuthPojo auth);
	
	public void deleteGrammarsBySchemaId(String schemaId, AuthPojo auth);
	public Grammar deleteGrammarById(String schemaId, String id, AuthPojo auth);

	public Grammar findById(String grammarId);

	public void saveGrammar(GrammarImpl grammar, AuthPojo auth);

	public GrammarCompilationResult saveTemporaryGrammar(Grammar grammar, GrammarContainer grammarContainer) throws IOException;
	public GrammarCompilationResult parseTemporaryGrammar(Grammar grammar);
	public GrammarCompilationResult compileTemporaryGrammar(Grammar grammar) throws IOException;

	public List<String> getParserRules(Grammar grammar) throws GrammarProcessingException;

	public void clearGrammar(Grammar g);

	public List<Grammar> findByEntityId(String entityId, boolean includeSources);

	public List<Grammar> findByIds(List<Object> grammarIds);
	
	public Map<String, GrammarContainer> serializeGrammarSources(String entityId);

	public Map<String, GrammarContainer> serializeGrammarSources(List<Grammar> grammars);

	List<Grammar> getNonPassthroughGrammars(List<Grammar> grammars);

	List<Grammar> getNonPassthroughGrammars(String entityId);
}
