package de.unibamberg.minf.dme.model;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class LogEntry implements Comparable<LogEntry> {
	public static enum LogType { SUCCESS, INFO, WARNING, ERROR }
	
	public static LogEntry createEntry(LogType logType, String code, Object[] args) {
		LogEntry entry = new LogEntry();
		entry.logType = logType;
		entry.code = code;
		entry.args = args;
		entry.setTimestamp(DateTime.now());
		return entry;
	}
	
	private DateTime timestamp;
	private LogType logType;
	private String code;
	private Object[] args;
	
	public DateTime getTimestamp() { return timestamp; }
	public void setTimestamp(DateTime timestamp) { this.timestamp = timestamp; }
	
	public LogType getLogType() { return logType; }
	public void setLogType(LogType logType) { this.logType = logType; }
	
	public Object[] getArgs() { return args; }
	public void setArgs(Object[] args) { this.args = args; }

	public String getCode() { return code; }
	public void setCode(String code) { this.code = code; }
	

	@Override
	public int compareTo(LogEntry o) {
		return this.timestamp.compareTo(o.getTimestamp());
	}
}