package de.unibamberg.minf.dme.confg;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.uniba.minf.core.rest.client.security.SecurityTokenIssuer;
import de.uniba.minf.core.rest.client.security.SecurityTokenIssuerImpl;
import de.uniba.minf.core.rest.client.security.memarc.MemarcSecurityTokenIssuer;
import de.unibamberg.minf.dme.confg.nested.NlpConfigProperties;
import de.unibamberg.minf.dme.confg.nested.RemoteTokensConfigProperties;
import de.unibamberg.minf.dme.exception.GenericScheregException;
import de.unibamberg.minf.gtf.DescriptionEngineImpl;
import de.unibamberg.minf.gtf.GtfElementProcessor;
import de.unibamberg.minf.gtf.GtfMappingProcessor;
import de.unibamberg.minf.gtf.MainEngine;
import de.unibamberg.minf.gtf.MainEngineImpl;
import de.unibamberg.minf.gtf.TransformationEngineImpl;
import de.unibamberg.minf.gtf.commands.CommandDispatcher;
import de.unibamberg.minf.gtf.commands.dispatcher.CoreCommandsDispatcher;
import de.unibamberg.minf.gtf.extensions.dai.commands.ChronontologyCommands;
import de.unibamberg.minf.gtf.extensions.dai.commands.GazetteerCommands;
import de.unibamberg.minf.gtf.extensions.dai.dispatcher.ChronontologyCommandsDispatcher;
import de.unibamberg.minf.gtf.extensions.dai.dispatcher.GazetteerCommandsDispatcher;
import de.unibamberg.minf.gtf.extensions.dai.dispatcher.MainDaiDispatcher;
import de.unibamberg.minf.gtf.extensions.file.commands.OnlineFileCommands;
import de.unibamberg.minf.gtf.extensions.file.dispatcher.FileCommandsDispatcher;
import de.unibamberg.minf.gtf.extensions.geo.commands.GeoCommandsDispatcher;
import de.unibamberg.minf.gtf.extensions.geo.commands.SimpleGeoCommands;
import de.unibamberg.minf.gtf.extensions.nlp.commands.LanguageDispatcher;
import de.unibamberg.minf.gtf.extensions.nlp.commands.NlpMainDispatcher;
import de.unibamberg.minf.gtf.extensions.nlp.stanford.LexicalizedParserWrapper;
import de.unibamberg.minf.gtf.extensions.nlp.stanford.MaxentTaggerWrapper;
import de.unibamberg.minf.gtf.extensions.nlp.stanford.NerClassifierWrapper;
import de.unibamberg.minf.gtf.extensions.nlp.stanford.StanfordProcessor;
import de.unibamberg.minf.gtf.extensions.person.commands.PersonCommandsDispatcher;
import de.unibamberg.minf.gtf.extensions.tika.commands.PdfCommands;
import de.unibamberg.minf.gtf.extensions.vocabulary.commands.SimpleVocabularyCommands;
import de.unibamberg.minf.gtf.extensions.vocabulary.commands.VocabularyCommandsDispatcher;
import de.unibamberg.minf.gtf.extensions.wiki.commands.WikiCommandsDispatcher;
import de.unibamberg.minf.gtf.transformation.processing.GlobalCommandDispatcher;
import de.unibamberg.minf.gtf.vocabulary.VocabularyEngine;
import de.unibamberg.minf.mapping.service.MappingExecutionServiceImpl;
import de.unibamberg.minf.processing.output.json.JsonFileOutputService;
import de.unibamberg.minf.processing.output.json.JsonStringOutputService;
import de.unibamberg.minf.processing.output.xml.XmlFileOutputService;
import de.unibamberg.minf.processing.service.json.JsonProcessingService;
import de.unibamberg.minf.processing.service.json.YamlProcessingService;
import de.unibamberg.minf.processing.service.tabular.CsvProcessingService;
import de.unibamberg.minf.processing.service.tabular.TsvProcessingService;
import de.unibamberg.minf.processing.service.text.TextProcessingService;
import de.unibamberg.minf.processing.service.xml.XmlProcessingService;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "processing")
public class ProcessingConfig  {
	@Autowired private MainConfig mainConfig;
		
	@Autowired private RemoteApiConfig remoteApiConfig;
		
	private NlpConfigProperties[] nlp;
	
	
	@Bean
	public SecurityTokenIssuer securityTokenIssuer(ObjectMapper objectMapper) throws GenericScheregException {
		SecurityTokenIssuerImpl issuer = new SecurityTokenIssuerImpl();
		if (remoteApiConfig.getRemoteTokens()!=null) {
			for (RemoteTokensConfigProperties remoteTokensConfig : remoteApiConfig.getRemoteTokens()) {
				if (remoteTokensConfig.getTokenRetrieval()!=null && remoteTokensConfig.getTokenRetrieval().equals("memarc")) {
					MemarcSecurityTokenIssuer memarcIssuer = new MemarcSecurityTokenIssuer();
					memarcIssuer.setObjectMapper(objectMapper);
					memarcIssuer.setApiServer(remoteTokensConfig.getTokenApiUrl());
					memarcIssuer.setUsername(remoteTokensConfig.getUsername());
					memarcIssuer.setPassword(remoteTokensConfig.getPassword());
					issuer.putTokenIssuer(remoteTokensConfig.getApiUrlRegex(), memarcIssuer);
				} else {
					throw new GenericScheregException("Unknown remoteTokens retrieval type: " + remoteTokensConfig.getTokenRetrieval()==null ? "NULL" : remoteTokensConfig.getTokenRetrieval());
				}
			}
		}
		return issuer;
	}
	
	@Bean
	@Scope("prototype")
	public MainEngineImpl mainEngine(ObjectMapper objectMapper, SecurityTokenIssuer securityTokenIssuer) {
		MainEngineImpl mainEngine = new MainEngineImpl();
		mainEngine.setDescriptionEngine(descriptionEngine());
		mainEngine.setTransformationEngine(transformationEngine(objectMapper, securityTokenIssuer));
		return mainEngine;
	}
		
	@Bean
	public DescriptionEngineImpl descriptionEngine() {
		DescriptionEngineImpl descriptionEngine = new DescriptionEngineImpl();
		descriptionEngine.setGrammarsRoot(new File(mainConfig.getPaths().getGrammars()));
		descriptionEngine.setParseErrorDumpPath(mainConfig.getPaths().getParseErrors());
		return descriptionEngine;
	}
	
	@Bean
	@Scope("prototype")
	public TransformationEngineImpl transformationEngine(ObjectMapper objectMapper, SecurityTokenIssuer securityTokenIssuer) {
		TransformationEngineImpl transformationEngine = new TransformationEngineImpl();
		transformationEngine.setObjMapper(objectMapper);
		transformationEngine.setCommandDispatcher(commandDispatcher(securityTokenIssuer));
		return transformationEngine;
	}
	
	@Bean
	@Scope("prototype")
	public GlobalCommandDispatcher commandDispatcher(SecurityTokenIssuer securityTokenIssuer) {
		GlobalCommandDispatcher commandDispatcher = new GlobalCommandDispatcher();
		Map<String, CommandDispatcher> commandDispatcherMap = new HashMap<>();
		commandDispatcherMap.put("CORE", coreCommandsDispatcher());
		commandDispatcherMap.put("FILE", fileCommandsDispatcher(securityTokenIssuer));
		commandDispatcherMap.put("WIKI", wikiCommandsDispatcher());
		commandDispatcherMap.put("PERSON", personCommandsDispatcher());
		commandDispatcherMap.put("VOCABULARY", vocabularyCommandsDispatcher());
		commandDispatcherMap.put("GEO", geoCommandsDispatcher());
		commandDispatcherMap.put("NLP", nlpDispatcher());
		commandDispatcherMap.put("DAI", mainDaiDispatcher());
		
		commandDispatcher.setCommandDispatchers(commandDispatcherMap);
		return commandDispatcher;
	}
	
	@Bean
	@Scope("prototype")
	public CoreCommandsDispatcher coreCommandsDispatcher() {
		return new CoreCommandsDispatcher();
	}
	
	@Bean
	public FileCommandsDispatcher fileCommandsDispatcher(SecurityTokenIssuer securityTokenIssuer) {
		FileCommandsDispatcher fileCommandsDispatcher = new FileCommandsDispatcher();
		OnlineFileCommands onlineFileCommands = new OnlineFileCommands();
		onlineFileCommands.setBaseDownloadDirectory(mainConfig.getPaths().getDownload());
		onlineFileCommands.setDisabled(false);
		onlineFileCommands.setSecurityTokenIssuer(securityTokenIssuer);
		fileCommandsDispatcher.setCommands(onlineFileCommands);
		fileCommandsDispatcher.setPdfCommands(new PdfCommands());
		return fileCommandsDispatcher;
	}
	
	@Bean
	public WikiCommandsDispatcher wikiCommandsDispatcher() {
		return new WikiCommandsDispatcher();
	}
	
	@Bean
	public PersonCommandsDispatcher personCommandsDispatcher() {
		PersonCommandsDispatcher personCommandsDispatcher = new PersonCommandsDispatcher();
		personCommandsDispatcher.setRestTemplate(restTemplate());
		return personCommandsDispatcher;
	}
	
	@Bean
	@Scope("prototype")
	public VocabularyCommandsDispatcher vocabularyCommandsDispatcher() {
		VocabularyCommandsDispatcher vocabularyCommandsDispatcher = new VocabularyCommandsDispatcher();
		vocabularyCommandsDispatcher.setSimple(new SimpleVocabularyCommands());
		return vocabularyCommandsDispatcher;
	}
	
	@Bean
	public GeoCommandsDispatcher geoCommandsDispatcher() {
		GeoCommandsDispatcher geoCommandsDispatcher = new GeoCommandsDispatcher();
		geoCommandsDispatcher.setSimple(new SimpleGeoCommands());
		return geoCommandsDispatcher;
	}
	
	@Bean
	public NlpMainDispatcher nlpDispatcher() {
		NlpMainDispatcher nlpDispatcher = new NlpMainDispatcher();
		nlpDispatcher.setLanguageDispatchers(new HashMap<>());
		
		LanguageDispatcher langDispatcher;
		if (nlp!=null) {
			for (NlpConfigProperties props : nlp) {
				langDispatcher = new LanguageDispatcher();
				langDispatcher.setProcessors(new HashMap<>());
				langDispatcher.getProcessors().put("Stanford", this.getStanfordProcessor(props));
				
				nlpDispatcher.getLanguageDispatchers().put(props.getLanguage().toUpperCase(), langDispatcher);
			}
		}	
		return nlpDispatcher;
	}
	
	
	private StanfordProcessor getStanfordProcessor(NlpConfigProperties props) {
		StanfordProcessor stanfordProcessor = new StanfordProcessor();
		MaxentTaggerWrapper tagger = getMaxentTaggerWrapper(props);
		NerClassifierWrapper classifier = getNerClassifierWrapper(props, tagger);
		stanfordProcessor.setTagger(tagger);
		stanfordProcessor.setNerClassifier(classifier);
		stanfordProcessor.setLexParser(getLexParserWrapper(props, tagger, classifier));
		return stanfordProcessor;
	}
	
	private MaxentTaggerWrapper getMaxentTaggerWrapper(NlpConfigProperties props) {
		MaxentTaggerWrapper taggerWrapper = new MaxentTaggerWrapper();
		taggerWrapper.setAutoInit(true);
		taggerWrapper.setLexParseModelPath(props.getLexParseModel());
		taggerWrapper.setTaggerModelPath(props.getTaggerModel());
		taggerWrapper.setClassifierModelPath(props.getClassifierModel());
		taggerWrapper.setModelsPath(props.getModels());
		return taggerWrapper;
	}
	
	private NerClassifierWrapper getNerClassifierWrapper(NlpConfigProperties props, MaxentTaggerWrapper tagger) {
		NerClassifierWrapper nerClassifierWrapper = new NerClassifierWrapper();
		nerClassifierWrapper.setAutoInit(true);
		nerClassifierWrapper.setLexParseModelPath(props.getLexParseModel());
		nerClassifierWrapper.setTaggerModelPath(props.getTaggerModel());
		nerClassifierWrapper.setClassifierModelPath(props.getClassifierModel());
		nerClassifierWrapper.setModelsPath(props.getModels());
		nerClassifierWrapper.setTagger(tagger);
		return nerClassifierWrapper;
	}
	
	private LexicalizedParserWrapper getLexParserWrapper(NlpConfigProperties props, MaxentTaggerWrapper tagger, NerClassifierWrapper classifier) {
		LexicalizedParserWrapper parserWrapper = new LexicalizedParserWrapper();
		parserWrapper.setAutoInit(true);
		parserWrapper.setLexParseModelPath(props.getLexParseModel());
		parserWrapper.setTaggerModelPath(props.getTaggerModel());
		parserWrapper.setClassifierModelPath(props.getClassifierModel());
		parserWrapper.setModelsPath(props.getModels());
		parserWrapper.setTagger(tagger);
		parserWrapper.setNerClassifier(classifier);
		return parserWrapper;
	}
	
	
	@Bean
	public MainDaiDispatcher mainDaiDispatcher() {
		MainDaiDispatcher mainDaiDispatcher = new MainDaiDispatcher();
		mainDaiDispatcher.setDaiCommandsDispatcherMap(new HashMap<>());
		mainDaiDispatcher.getDaiCommandsDispatcherMap().put("CHRONONTOLOGY", chronontologyCommandsDispatcher(restTemplate()));
		mainDaiDispatcher.getDaiCommandsDispatcherMap().put("GAZETTEER", gazetteerCommandsDispatcher(restTemplate()));
		
		return mainDaiDispatcher;
	}
	
	@Bean
	public ChronontologyCommandsDispatcher chronontologyCommandsDispatcher(RestTemplate restTemplate) {
		ChronontologyCommandsDispatcher chronontologyCommandsDispatcher = new ChronontologyCommandsDispatcher();
		chronontologyCommandsDispatcher.setCommands(new ChronontologyCommands());
		chronontologyCommandsDispatcher.getCommands().setBaseUrl("http://chronontology.dainst.org/data/period/");
		chronontologyCommandsDispatcher.getCommands().setRestTemplate(restTemplate);
		
		return chronontologyCommandsDispatcher;
	}
	
	@Bean
	public GazetteerCommandsDispatcher gazetteerCommandsDispatcher(RestTemplate restTemplate) {
		GazetteerCommandsDispatcher gazetteerCommandsDispatcher = new GazetteerCommandsDispatcher();
		gazetteerCommandsDispatcher.setCommands(new GazetteerCommands());
		gazetteerCommandsDispatcher.getCommands().setBaseUrl("https://gazetteer.dainst.org/");
		gazetteerCommandsDispatcher.getCommands().setRestTemplate(restTemplate);
		
		return gazetteerCommandsDispatcher;
	}	
	
	@Bean
	@Scope("prototype")
	public GtfElementProcessor gtfElementProcessor(MainEngine mainEngine) {
		GtfElementProcessor gtfElementProcessor = new GtfElementProcessor();
		gtfElementProcessor.setMainEngine(mainEngine);
		return gtfElementProcessor;
	}

	@Bean
	@Scope("prototype")
	public GtfMappingProcessor gtfMappingProcessor(MainEngine mainEngine) {
		GtfMappingProcessor gtfMappingProcessor = new GtfMappingProcessor();
		gtfMappingProcessor.setMainEngine(mainEngine);
		return gtfMappingProcessor;
	}
	
	@Bean
	public VocabularyEngine vocabularyEngine() {
		return new VocabularyEngine();
	} 
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	/*@Bean
	@Scope("prototype")
	public XmlOutputService xmlOutputService() {
		XmlOutputService xmlOutputService = new XmlOutputService();
		xmlOutputService.setOutputBaseDirectory(mainConfig.getPaths().getTemporary());
		xmlOutputService.setExtensionNamespacePattern(Paths.get(mainConfig.getBaseUrl(), "schema/%s").toString());
		return xmlOutputService;
	}*/
	
	@Bean
	public XmlFileOutputService xmlFileOutputService() {
		XmlFileOutputService xmlFileOutputService = new XmlFileOutputService();
		xmlFileOutputService.setOutputBaseDirectory(mainConfig.getPaths().getSampleFiles());
		xmlFileOutputService.setExtensionNamespacePattern(Paths.get(mainConfig.getBaseUrl(), "schema/%s").toString());
		return xmlFileOutputService;
	}
	
	@Bean
	public JsonFileOutputService jsonFileOutputService(ObjectMapper objectMapper) {
		JsonFileOutputService jsonFileOutputService = new JsonFileOutputService();
		jsonFileOutputService.setOutputBaseDirectory(mainConfig.getPaths().getSampleFiles());
		jsonFileOutputService.setObjMapper(objectMapper);
		return jsonFileOutputService;
	}

	@Bean
	@Scope("prototype")
	public JsonStringOutputService jsonOutputService() {
		return new JsonStringOutputService();
	}

	
	@Bean
	@Scope("prototype")
	public XmlProcessingService xmlStringProcessor(GtfElementProcessor gtfElementProcessor) {
		XmlProcessingService xmlStringProcessor = new XmlProcessingService();
		xmlStringProcessor.setElementProcessors(new ArrayList<>());
		xmlStringProcessor.getElementProcessors().add(gtfElementProcessor);
		return xmlStringProcessor;
	}
	
	@Bean
	@Primary // To the overriding YamlProcessingService
	@Scope("prototype")
	public JsonProcessingService jsonProcessingService(GtfElementProcessor gtfElementProcessor) {
		JsonProcessingService jsonProcessingService = new JsonProcessingService();
		jsonProcessingService.setElementProcessors(new ArrayList<>());
		jsonProcessingService.getElementProcessors().add(gtfElementProcessor);
		return jsonProcessingService;
	}
	
	@Bean
	@Scope("prototype")
	public YamlProcessingService yamlProcessingService(GtfElementProcessor gtfElementProcessor) {
		YamlProcessingService yamlProcessingService = new YamlProcessingService();
		yamlProcessingService.setElementProcessors(new ArrayList<>());
		yamlProcessingService.getElementProcessors().add(gtfElementProcessor);
		return yamlProcessingService;
	}
	
	@Bean
	@Scope("prototype")
	public CsvProcessingService csvStringProcessor(GtfElementProcessor gtfElementProcessor) {
		CsvProcessingService csvStringProcessor = new CsvProcessingService();
		csvStringProcessor.setElementProcessors(new ArrayList<>());
		csvStringProcessor.getElementProcessors().add(gtfElementProcessor);
		return csvStringProcessor;
	}
	
	@Bean
	@Scope("prototype")
	public TsvProcessingService tsvStringProcessor(GtfElementProcessor gtfElementProcessor) {
		TsvProcessingService tsvStringProcessor = new TsvProcessingService();
		tsvStringProcessor.setElementProcessors(new ArrayList<>());
		tsvStringProcessor.getElementProcessors().add(gtfElementProcessor);
		return tsvStringProcessor;
	}

	@Bean
	@Scope("prototype")
	public TextProcessingService textStringProcessor(GtfElementProcessor gtfElementProcessor) {
		TextProcessingService textStringProcessor = new TextProcessingService();
		textStringProcessor.setElementProcessors(new ArrayList<>());
		textStringProcessor.getElementProcessors().add(gtfElementProcessor);
		return textStringProcessor;
	}

	@Bean
	@Scope("prototype")
	public MappingExecutionServiceImpl mappingExecutionService(GtfMappingProcessor gtfMappingProcessor) {
		MappingExecutionServiceImpl mappingExecutionService = new MappingExecutionServiceImpl();
		mappingExecutionService.setMappingProcessors(new ArrayList<>());
		mappingExecutionService.getMappingProcessors().add(gtfMappingProcessor);
		return mappingExecutionService;
	}
}
