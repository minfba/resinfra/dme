package de.unibamberg.minf.dme.importer.mapping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;

import de.unibamberg.minf.core.util.Stopwatch;
import de.unibamberg.minf.dme.exception.MappingImportException;
import de.unibamberg.minf.dme.importer.BaseImporter;
import de.unibamberg.minf.dme.model.base.Element;
import de.unibamberg.minf.dme.model.base.Grammar;
import de.unibamberg.minf.dme.model.mapping.ExportedConceptImpl;
import de.unibamberg.minf.dme.model.mapping.MappedConceptImpl;
import de.unibamberg.minf.dme.model.mapping.base.ExportedConcept;
import de.unibamberg.minf.dme.model.mapping.base.MappedConcept;
import de.unibamberg.minf.dme.model.mapping.base.Mapping;
import de.unibamberg.minf.dme.model.mapping.base.RelatedConcept;
import de.unibamberg.minf.dme.model.serialization.MappingContainer;
import de.unibamberg.minf.dme.service.interfaces.ElementService;

public abstract class BaseMappingImporter extends BaseImporter implements MappingImporter {
	
	@Autowired private ElementService elementService;
	
	private Mapping mapping;
	private MappingImportListener importListener;	
	private List<Element> sourceElements;
	private List<Element> targetElements;
	private Element sourceRootElement;
	private Element targetRootElement;
	
	private List<RelatedConcept> importedConcepts;
	private Map<String, String> importedFunctions;
	private Map<String, Grammar> importedGrammars;
	
	public Mapping getMapping() { return mapping; }
	@Override public void setMapping(Mapping mapping) { this.mapping = mapping; }

	public MappingImportListener getImportListener() { return importListener; }
	@Override public void setImportListener(MappingImportListener importListener) { this.importListener = importListener; }
	
	
	@Override
	public void run() {
		Stopwatch sw = new Stopwatch().start();
		
		logger.debug("Started importing mapping {}", this.getMapping().getId());
		try {
			this.setupDatamodels();
			this.importJson();
			if (this.getImportListener()!=null) {
				logger.info("Finished importing mapping {} in {}ms", this.getMapping().getId(), sw.getElapsedTime());
				this.getImportListener().registerImportFinished(this.getMapping(), this.importedConcepts, this.importedFunctions, this.importedGrammars, this.auth);
			}
		} catch (Exception e) {
			logger.error("Error while importing JSON Mapping", e);
			if (this.getImportListener()!=null) {
				this.getImportListener().registerImportFailed(this.getMapping());
			}
		}
	}
	
	protected void importMapping(MappingContainer mappingContainer) {
		importedConcepts = new ArrayList<>();
		importedFunctions = new HashMap<>();
		importedGrammars = new HashMap<>();
		
		RelatedConcept importedRc;
		Map<String, String> targetElementIdMap; 
		Map<String, String> sourceElementIdMap;
		String grammarId;
		String setId;
		
		if (mappingContainer.getMapping().getConcepts()!=null) {
			for (RelatedConcept rc : mappingContainer.getMapping().getConcepts()) {
				if (rc.getTargetElementIds()==null) {
					// No target elements at all
					continue;
				}
				
				targetElementIdMap = new HashMap<>();
				this.matchElementIds(targetElementIdMap, rc.getTargetElementIds(), targetElements);
				this.matchElementPaths(targetElementIdMap, rc.getTargetElementIds(), targetRootElement, mappingContainer.getElementPaths());
				
				sourceElementIdMap = new HashMap<>();
				this.matchElementIds(sourceElementIdMap, rc.getElementGrammarIdsMap().keySet(), sourceElements);
				this.matchElementPaths(sourceElementIdMap, rc.getElementGrammarIdsMap().keySet(), sourceRootElement, mappingContainer.getElementPaths());
				
			
				// TODO: Match ids first, when unsuccessful match element label paths				
				if (targetElementIdMap.isEmpty() || sourceElementIdMap.isEmpty()) {
					// No matching source and/or target elements
					continue;
				}
				
				// If a function is provided, use it, otherwise leave null (assuming value-assignment function)
				if (MappedConcept.class.isAssignableFrom(rc.getClass())) {
					importedRc = new MappedConceptImpl();
					MappedConcept mc = MappedConcept.class.cast(rc);
					if (mc.getFunctionId()!=null && mappingContainer.getFunctions().containsKey(mc.getFunctionId())) {
						setId = this.getOrCreateId(mc.getFunctionId());
						MappedConcept.class.cast(importedRc).setFunctionId(setId);
						if (mappingContainer.getFunctions().get(mc.getFunctionId())!=null) {
							importedFunctions.put(setId, mappingContainer.getFunctions().get(mc.getFunctionId()));
						}
					}
				} else {
					importedRc = new ExportedConceptImpl();
					ExportedConcept.class.cast(importedRc).setFormat(ExportedConcept.class.cast(rc).getFormat());
				}
				
				
				importedRc.setId(this.getOrCreateId(rc.getId()));
				importedRc.setEntityId(this.mapping.getId());
				importedRc.setTargetElementIds(new ArrayList<>(targetElementIdMap.values()));
				
				// Collect imported real grammars, setting a null ID for missing containers (passthrough grammars)
				importedRc.setElementGrammarIdsMap(new HashMap<>());
				for (Entry<String, String> sourceElementEntry : sourceElementIdMap.entrySet()) {
					grammarId = rc.getElementGrammarIdsMap().get(sourceElementEntry.getKey());
					if (grammarId!=null && mappingContainer.getGrammars()!=null && mappingContainer.getGrammars().containsKey(grammarId)) {
						setId = this.getOrCreateId(grammarId); 
						importedRc.getElementGrammarIdsMap().put(sourceElementEntry.getValue(), setId);
						importedGrammars.put(setId, mappingContainer.getGrammars().get(grammarId));
						break;
					} else {
						importedRc.getElementGrammarIdsMap().put(sourceElementEntry.getValue(), null);
					}
				}
				
				
				importedConcepts.add(importedRc);
			}
		}
	}	
	
	private void matchElementIds(Map<String, String> elementIdMap, Collection<String> elementIds, List<Element> matchElements) {
		for (String elementId : elementIds) {
			if (this.findElementById(elementId, matchElements)!=null && !elementIdMap.containsKey(elementId)) {
				elementIdMap.put(elementId, elementId);
			}
		}
	}
	
	
	private void matchElementPaths(Map<String, String> elementIdMap, Collection<String> elementIds, Element rootElement, Map<String, String> elementPaths) {
		Element e;
		for (String elementId : elementIds) {
			if (elementIdMap.containsKey(elementId) || !elementPaths.containsKey(elementId)) {
				continue;
			}
			e = this.findElementByPath(elementPaths.get(elementId), rootElement);
			if (e!=null) {
				elementIdMap.put(elementId, e.getId());
			}
		}
	}
	
	private Element findElementByPath(String path, Element element) {
		// Element name matches last path hierarchy label
		if (!path.contains(".")) {
			return element.getName().equals(path) ? element : null;
		}
		// Element matches current hierarchy label
		String currentName = path.substring(0, path.indexOf('.'));
		if (!element.getName().equals(currentName)) {
			return null;
		}
		// Search in children
		String childPath = path.substring(path.indexOf('.')+1);
		Element matchedChild = null;
		for (Element child : element.getAllChildElements()) {
			matchedChild = this.findElementByPath(childPath, child);
			if (matchedChild!=null) {
				return matchedChild;
			}
		}
		return null;
	}
	
	private Element findElementById(String id, List<Element> elements) {
		if (elements==null) {
			return null;
		}
		for (Element e : elements) {
			if (e.getId().equals(id)) {
				return e;
			}
		}
		return null;
	}
	
	private void setupDatamodels() throws MappingImportException {
		if (this.getMapping()==null) {
			throw new MappingImportException("No import target mapping specified");
		}
		sourceElements = this.elementService.findBySchemaId(this.getMapping().getSourceId());
		targetElements = this.elementService.findBySchemaId(this.getMapping().getTargetId());
		sourceRootElement = this.elementService.findRootBySchemaId(this.getMapping().getSourceId(), true);
		targetRootElement = this.elementService.findRootBySchemaId(this.getMapping().getTargetId(), true);
	}
	
	
	protected abstract void importJson();
}
