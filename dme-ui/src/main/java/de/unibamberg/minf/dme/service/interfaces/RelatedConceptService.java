package de.unibamberg.minf.dme.service.interfaces;

import java.util.List;
import de.unibamberg.minf.dme.exception.GenericScheregException;
import de.unibamberg.minf.dme.model.mapping.base.RelatedConcept;
import de.unibamberg.minf.dme.pojo.RelatedConceptPojo.MappingTypes;
import eu.dariah.de.dariahsp.web.model.AuthPojo;

public interface RelatedConceptService {
	public List<RelatedConcept> findAllByMappingId(String mappingId);
	public List<RelatedConcept> findAllByMappingId(String mappingId, boolean eagerLoadHierarchy);	
	public RelatedConcept findById(String mappingId, String conceptId, boolean eagerLoadHierarchy);
	public RelatedConcept findById(String conceptId);
	
	public void removeRelatedConcept(String mappingId, String conceptId, AuthPojo auth) throws GenericScheregException;
	public void saveRelatedConcept(RelatedConcept concept, String mappingId, AuthPojo auth);
	public void removeSourceElementById(AuthPojo auth, String mappingId, RelatedConcept mc, String sourceId);
	public void removeSourceElementById(AuthPojo auth, String mappingId, String conceptId, String sourceId);
	public void removeElementReferences(String entityId, String elementId);
	public RelatedConcept switchConceptType(String mappingId, String mappedConceptId, MappingTypes mappingType, AuthPojo auth);
}