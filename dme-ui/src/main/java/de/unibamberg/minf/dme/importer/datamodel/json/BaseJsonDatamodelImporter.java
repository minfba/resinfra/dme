package de.unibamberg.minf.dme.importer.datamodel.json;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.unibamberg.minf.core.util.Stopwatch;
import de.unibamberg.minf.dme.importer.datamodel.BaseDatamodelImporter;
import de.unibamberg.minf.dme.importer.datamodel.DatamodelImporter;
import de.unibamberg.minf.dme.model.base.Element;
import de.unibamberg.minf.dme.model.base.Function;
import de.unibamberg.minf.dme.model.base.Grammar;
import de.unibamberg.minf.dme.model.base.Label;
import de.unibamberg.minf.dme.model.base.ModelElement;
import de.unibamberg.minf.dme.model.base.NamedModelElement;
import de.unibamberg.minf.dme.model.base.Nonterminal;
import de.unibamberg.minf.dme.model.base.Terminal;
import de.unibamberg.minf.dme.model.datamodel.NonterminalImpl;
import de.unibamberg.minf.dme.model.datamodel.base.Datamodel;
import de.unibamberg.minf.dme.model.datamodel.base.DatamodelNature;
import de.unibamberg.minf.dme.model.exception.MetamodelConsistencyException;
import de.unibamberg.minf.dme.model.grammar.GrammarContainer;
import de.unibamberg.minf.dme.service.IdentifiableServiceImpl;

/**
 * Importer for JSON based schema definitions
 *  Currently only schemata in the native format (serializations of SerializableSchemaContainer) are supported
 * 
 * @author Tobias Gradl
 *
 */
public abstract class BaseJsonDatamodelImporter extends BaseDatamodelImporter implements DatamodelImporter {
	@Autowired protected ObjectMapper objectMapper;
	
	@Override public boolean isKeepImportedIdsSupported() { return true; } 	
	@Override public String getMainImporterType() { return "JSON"; }
		
	@Override
	public void run() {
		Stopwatch sw = new Stopwatch().start();
		logger.debug("Started importing datamodel {}", this.getDatamodel().getId());
		try {
			this.importJson();
			if (this.getListener()!=null) {
				logger.info("Finished importing datamodel {} in {}ms", this.getDatamodel().getId(), sw.getElapsedTime());
				this.getListener().registerImportFinished(this.getDatamodel(), this.getElementId(), this.getRootElements(), this.getAdditionalRootElements(), this.auth);
			}
		} catch (Exception e) {
			logger.error("Error while importing JSON Datamodel", e);
			if (this.getListener()!=null) {
				this.getListener().registerImportFailed(this.getDatamodel());
			}
		}
	}

	@Override
	public boolean getIsSupported() {
		long size = 0;
		try (final JsonParser parser = objectMapper.getFactory().createParser(new File(this.importFilePath))){
			while (parser.nextToken() != null) {
				size++;
			}			
			return size > 0;
		} catch (Exception e) {
			return false;			
		}
	}

	@Override
	public String[] getNamespaces() {
		return new String[]{""};
	}
		
	protected void importModel(Datamodel m, Element root, Map<String, GrammarContainer> grammars) throws MetamodelConsistencyException {
		Map<String, String> newToOldIdMap = new HashMap<>();
		Map<String, String> oldToNewIdMap = new HashMap<>();
		
		this.reworkElementHierarchy(this.getDatamodel().getId(), root, oldToNewIdMap, newToOldIdMap, grammars);
		
		if (!isKeepImportedIds() && m.getNatures()!=null) {
			for (DatamodelNature nature : m.getNatures()) {
				this.regenerateTerminalIds(nature, oldToNewIdMap);
			}	
		}
		if (this.getRootElementType()==null || this.getRootElementType().isEmpty()) {
			this.setRootElementType(NonterminalImpl.class.getName());
		}
		IdentifiableServiceImpl.extractAllByType(root, this.getRootElementType()).stream()
			.filter(i -> NamedModelElement.class.isAssignableFrom(i.getClass()) && 
						(((NamedModelElement)i).getId().equals(this.getRootElementName()) || 
						 ((NamedModelElement)i).getName().equals(this.getRootElementName())))
			.forEach(i -> this.getRootElements().add(i));
		
		this.setDatamodel(m);
	}
	
	protected abstract void importJson() throws IOException, MetamodelConsistencyException;
	
	
	protected void reworkElementHierarchy(String entityId, ModelElement element, Map<String, String> oldToNewIdMap, Map<String, String> newToOldIdMap, Map<String, GrammarContainer> grammarContainerMap) throws MetamodelConsistencyException {
		if (Nonterminal.class.isAssignableFrom(element.getClass()) || Label.class.isAssignableFrom(element.getClass())) {
			this.reworkHierarchyFromElement(entityId, (Element)element, oldToNewIdMap, newToOldIdMap, grammarContainerMap);
		} else if (Grammar.class.isAssignableFrom(element.getClass())) {
			this.reworkHierarchyFromGrammar(entityId, (Grammar)element, oldToNewIdMap, newToOldIdMap, grammarContainerMap);
		} else if (Function.class.isAssignableFrom(element.getClass())) {
			this.reworkHierarchyFromFunction(entityId, (Function)element, oldToNewIdMap, newToOldIdMap, grammarContainerMap);
		}
	}
	
	protected void reworkHierarchyFromElement(String entityId, Element element, Map<String, String> oldToNewIdMap, Map<String, String> newToOldIdMap, Map<String, GrammarContainer> grammarContainerMap) throws MetamodelConsistencyException {
		if (newToOldIdMap.containsKey(element.getId())) {
			return;
		}
		
		element.setEntityId(entityId);
		
		if (!this.isKeepImportedIds()) {
			String newId = new ObjectId().toString();		
			oldToNewIdMap.put(element.getId(), newId);
			newToOldIdMap.put(newId, element.getId());
			element.setId(newId);
		} else {
			newToOldIdMap.put(element.getId(), element.getId());
		}
		
		List<? extends Element> children = null;
		if (Nonterminal.class.isAssignableFrom(element.getClass())) {
			children = ((Nonterminal)element).getChildNonterminals();
		} else if (Label.class.isAssignableFrom(element.getClass())) {
			children = ((Label)element).getSubLabels();
		}
		
		if (children!=null) {
			for (Element child : children) {
				this.reworkHierarchyFromElement(entityId, child, oldToNewIdMap, newToOldIdMap, grammarContainerMap);
			}
		}
		if (element.getGrammars()!=null) {
			for (Grammar g : element.getGrammars()) {
				this.reworkHierarchyFromGrammar(entityId, g, oldToNewIdMap, newToOldIdMap, grammarContainerMap);
			}
		}
	}
	
	protected void reworkHierarchyFromGrammar(String entityId, Grammar g, Map<String, String> oldToNewIdMap, Map<String, String> newToOldIdMap, Map<String, GrammarContainer> grammarContainerMap) throws MetamodelConsistencyException {
		if (newToOldIdMap.containsKey(g.getId())) {
			return;
		}
		g.setEntityId(entityId);
		
		if (grammarContainerMap!=null && grammarContainerMap.containsKey(g.getId())) {
			g.setGrammarContainer(grammarContainerMap.get(g.getId()));
		}
		
		if (!this.isKeepImportedIds()) {
			String newId = new ObjectId().toString();
			newToOldIdMap.put(newId, g.getId());
			oldToNewIdMap.put(g.getId(), newId);
			g.setId(newId);
		} else {
			newToOldIdMap.put(g.getId(), g.getId());
		}
		
		if (g.getFunctions()!=null) {
			for (Function f : g.getFunctions()) {
				this.reworkHierarchyFromFunction(entityId, f, oldToNewIdMap, newToOldIdMap, grammarContainerMap);
			}
		}
	}
	
	protected void reworkHierarchyFromFunction(String entityId, Function f, Map<String, String> oldToNewIdMap, Map<String, String> newToOldIdMap, Map<String, GrammarContainer> grammarContainerMap) throws MetamodelConsistencyException {
		if (newToOldIdMap.containsKey(f.getId())) {
			return;
		}
		f.setEntityId(entityId);
		if (!this.isKeepImportedIds()) {
			String newId = new ObjectId().toString();
			newToOldIdMap.put(newId, f.getId());
			oldToNewIdMap.put(f.getId(), newId);
			f.setId(newId);
		} else {
			newToOldIdMap.put(f.getId(), f.getId());
		}
		if (f.getOutputElements()!=null) {
			for (Element fOut : f.getOutputElements()) {
				this.reworkHierarchyFromElement(entityId, fOut, oldToNewIdMap, newToOldIdMap, grammarContainerMap);
			}
		}
	}
	
	
	protected void regenerateTerminalIds(DatamodelNature nature, Map<String, String> idMap) throws MetamodelConsistencyException {
		Map<String, String> oldNonterminalTerminalIdMap = new HashMap<>(nature.getNonterminalTerminalIdMap());
		nature.setNonterminalTerminalIdMap(new HashMap<>());
		
		String newId;
		for (Terminal t : nature.getTerminals()) {
			newId = new ObjectId().toString();
			for (Entry<String, String> idMapEntry : oldNonterminalTerminalIdMap.entrySet()) {
				if (idMapEntry.getValue().equals(t.getId())) {
					nature.mapNonterminal(idMap.get(idMapEntry.getKey()), newId);
				}
			}
			t.setId(newId);
		}
	}
}
