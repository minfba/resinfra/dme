package de.unibamberg.minf.dme.pojo.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.unibamberg.minf.dme.model.base.Function;
import de.unibamberg.minf.dme.model.mapping.base.ExportedConcept;
import de.unibamberg.minf.dme.model.mapping.base.MappedConcept;
import de.unibamberg.minf.dme.model.mapping.base.RelatedConcept;
import de.unibamberg.minf.dme.pojo.ExportedConceptPojo;
import de.unibamberg.minf.dme.pojo.MappedConceptPojo;
import de.unibamberg.minf.dme.pojo.RelatedConceptPojo;
import de.unibamberg.minf.dme.pojo.RelatedConceptPojo.MappingTypes;

public class RelatedConceptPojoConverter {

	public static List<RelatedConceptPojo> convert(List<RelatedConcept> concepts, List<Function> functions) {
		List<RelatedConceptPojo> result = new ArrayList<>();
		if (concepts==null) {
			return result;
		}
		for (RelatedConcept c : concepts) {
			result.add(convert(c, convertFunctionsToIdFunctionMap(functions)));
		}
		return result;
	}

	public static RelatedConceptPojo convert(RelatedConcept c, Map<String, String> functionIdFunctionMap) {
		RelatedConceptPojo pojo;
		
		if (ExportedConcept.class.isAssignableFrom(c.getClass())) {
			ExportedConcept ec = ExportedConcept.class.cast(c);
			ExportedConceptPojo ecPojo = new ExportedConceptPojo();
			ecPojo.setFormat(ec.getFormat().name());
			ecPojo.setIncludeTree(ec.isIncludeTree());
			ecPojo.setEscape(ec.isEscape());
			ecPojo.setIncludeSelf(ec.isIncludeSelf());
			ecPojo.setType(MappingTypes.EXPORT);
			ecPojo.setUseTerminalsIfAvailable(ec.isUseTerminalsIfAvailable());
			
			pojo = ecPojo;
		} else {
			MappedConcept mc = MappedConcept.class.cast(c);
			pojo = new MappedConceptPojo();
			if (mc.getFunctionId()==null || !functionIdFunctionMap.containsKey(mc.getFunctionId()) || functionIdFunctionMap.get(mc.getFunctionId())==null || functionIdFunctionMap.get(mc.getFunctionId()).isEmpty()) {
				pojo.setType(MappingTypes.VALUE);
			} else {
				((MappedConceptPojo)pojo).setFunctionId(mc.getFunctionId());
				pojo.setType(MappingTypes.FUNCTION);
			}
		}
		
		pojo.setId(c.getId());
		pojo.setSourceElementIds(new ArrayList<>());
		if (c.getElementGrammarIdsMap()!=null) {
			pojo.getSourceElementIds().addAll(c.getElementGrammarIdsMap().keySet());
		}
		pojo.setTargetElementIds(new ArrayList<>());
		if (c.getTargetElementIds()!=null) {
			pojo.getTargetElementIds().addAll(c.getTargetElementIds());
		}
		return pojo;
	}
	
	public static Map<String, String> convertFunctionsToIdFunctionMap(List<Function> functions) {
		Map<String, String> functionIdNameMap = new HashMap<>();
		if (functions!=null) {
			for (Function f : functions) {
				functionIdNameMap.put(f.getId(), f.getFunction());
			}
		}
		return functionIdNameMap;
	}
}
