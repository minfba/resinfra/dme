package de.unibamberg.minf.dme.confg;

import org.apache.catalina.Context;
import org.apache.catalina.webresources.ExtractingRoot;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import de.unibamberg.minf.core.web.controller.ImageController;
import de.unibamberg.minf.dme.profiles.UserProfileActionHandler;
import eu.dariah.de.dariahsp.CustomizableProfileManager;
import eu.dariah.de.dariahsp.ProfileActionHandler;
import eu.dariah.de.dariahsp.web.controller.CommonLoginController;
import eu.dariah.de.dariahsp.web.controller.SAMLMetadataController;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@ConfigurationProperties
public class ApplicationConfig {
	private String contextPath = "";
	
	public String getContextPath() { return contextPath; }
 	public void setContextPath(String contextPath) { this.contextPath = contextPath; }

	/**
	 * WebServerFactoryCustomizer bean that adapts to a configured context path for the application. 
	 * This adaption helps with setting up the application as it might be available or proxied at their deployments
	 * 
	 * @return WebServerFactoryCustomizer
	 */
	@Bean
	public WebServerFactoryCustomizer<TomcatServletWebServerFactory> servletContainerCustomizer() {	
		log.info("Web server context path set to {}", contextPath.isEmpty() ? "/" : contextPath);
		return container -> container.addContextCustomizers(context -> { 
																context.setReloadable(false); 
																context.setPath(contextPath);
															});
	}
	
	/**
	 * Provides tomcat factory
	 * 
	 * @return TomcatServletWebServerFactory tomcatFactory
	 */
	@Bean
	public TomcatServletWebServerFactory tomcatFactory() {
		return new TomcatServletWebServerFactory() {
			@Override
			protected void postProcessContext(Context context) {
				// extracting packaged war file for better performance:
				// @see https://stackoverflow.com/a/62759292
				context.setResources(new ExtractingRoot());
				// disabling TLD scanning: 
				// @see https://stackoverflow.com/a/52229296
				((StandardJarScanner) context.getJarScanner()).setScanManifest(false);
			}
		};
	}

	/**
	 * Bean that is injected into {@link CustomizableProfileManager} to facilitate observation of login and logout actions
	 *  Implementations can provide custom implementations of the {@link ProfileActionHandler} interface e.g. to log such
	 *  actions into a database
	 *    
	 * @return SampleProfileActionHandler bean
	 */
	@Bean
	public ProfileActionHandler profileActionPostprocessor() {
		return new UserProfileActionHandler();
	}
	
	/**
	 * Controller bean that facilitates access to generated or stored SAML SP metadata
	 * 
	 * @return SAMLMetadataController bean
	 */
	@Bean
	public SAMLMetadataController samlMetadataController() {
		return new SAMLMetadataController();
	}
	
	/**
	 * Controller that binds to common login mappings 
	 * 
	 * @return LoginLogoutController bean
	 */
	@Bean
	public CommonLoginController loginLogoutController() {
		return new CommonLoginController("common/login");
	}
	
	@Bean
	public ImageController imageController() {
		return new ImageController();
	}
}
