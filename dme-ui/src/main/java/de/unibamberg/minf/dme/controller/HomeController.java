package de.unibamberg.minf.dme.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import eu.dariah.de.dariahsp.web.AuthInfoHelper;


@Controller
@RequestMapping(value="")
public class HomeController {
	@Autowired protected AuthInfoHelper authInfoHelper;

	/* For now...redirect; in the future a dash-board is intended */
	@GetMapping(value = {"", "/"})
	public String getHome(HttpServletResponse response) throws IOException  {
		response.sendRedirect("registry/");
		return null;
	}
	
	@RequestMapping("favicon.ico")
    public String forwardFavicon() {
        return "forward:/resources/img/page_icon.png";
    }
	
	@GetMapping(value = "/user")
	public String getUser() throws IOException  {	
		return "common/user";
	}
		
	@GetMapping(value = "/async/isAuthenticated")
	public @ResponseBody boolean getIsLoggedIn(HttpServletRequest request) {
		return authInfoHelper.getAuth().isAuth();
	}
}