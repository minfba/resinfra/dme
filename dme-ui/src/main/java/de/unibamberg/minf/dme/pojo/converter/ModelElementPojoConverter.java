package de.unibamberg.minf.dme.pojo.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.unibamberg.minf.dme.exception.GenericScheregException;
import de.unibamberg.minf.dme.model.base.Element;
import de.unibamberg.minf.dme.model.base.Function;
import de.unibamberg.minf.dme.model.base.Grammar;
import de.unibamberg.minf.dme.model.base.Label;
import de.unibamberg.minf.dme.model.base.ModelElement;
import de.unibamberg.minf.dme.model.base.Nonterminal;
import de.unibamberg.minf.dme.model.datamodel.base.DatamodelNature;
import de.unibamberg.minf.dme.model.datamodel.natures.XmlDatamodelNature;
import de.unibamberg.minf.dme.model.datamodel.natures.xml.XmlNamespace;
import de.unibamberg.minf.dme.model.datamodel.natures.xml.XmlTerminal;
import de.unibamberg.minf.dme.pojo.ModelElementPojo;
import de.unibamberg.minf.dme.pojo.ModelElementPojo.ModelElementState;

public class ModelElementPojoConverter {
	
	private ModelElementPojoConverter() { }
	
	public static List<ModelElementPojo> convertModelElements(List<Element> modelElements, boolean staticElementsOnly) throws GenericScheregException {
		return convertModelElements(modelElements, staticElementsOnly, new HashMap<>(), null);
	}
	
	public static List<ModelElementPojo> convertModelElements(List<Element> modelElements, boolean staticElementsOnly, Map<Element, ModelElementPojo> converted, Map<String, List<String>> nonterminalNatureClassesMap) throws GenericScheregException {
		List<ModelElementPojo> results = new ArrayList<>();
		ModelElementPojo mep;
		if (modelElements!=null) {
			for (Element e : modelElements) {
				mep = convertModelElement(e, staticElementsOnly, converted, nonterminalNatureClassesMap, false);
				if (mep!=null) {
					results.add(mep);
				}
			}
		}
		return results;
	}
	
	public static ModelElementPojo convertModelElement(Element modelElement, Map<String, List<String>> nonterminalNatureClassesMap, boolean staticElementsOnly, boolean skipHierarchy) throws GenericScheregException {
		return convertModelElement(modelElement, staticElementsOnly, new HashMap<>(), nonterminalNatureClassesMap, skipHierarchy);
	}
	
	public static ModelElementPojo convertModelElement(ModelElement modelElement, boolean staticElementsOnly) throws GenericScheregException {
		return convertModelElement(modelElement, staticElementsOnly, new HashMap<>(), null, false);
	}
	
	public static ModelElementPojo convertModelElement(Element modelElement, Map<String, List<String>> nonterminalNatureClassesMap, boolean staticElementsOnly) throws GenericScheregException {
		return convertModelElement(modelElement, staticElementsOnly, new HashMap<>(), nonterminalNatureClassesMap, false);
	}
	
	public static ModelElementPojo convertModelElement(ModelElement modelElement, boolean staticElementsOnly, Map<Element, ModelElementPojo> converted, Map<String, List<String>> nonterminalNatureClassesMap, boolean skipHierarchy) throws GenericScheregException {
		if (modelElement==null) {
			return null;
		} else if (Nonterminal.class.isAssignableFrom(modelElement.getClass())) {
			return convertNonterminal((Nonterminal)modelElement, staticElementsOnly, converted, nonterminalNatureClassesMap, skipHierarchy);
		} else if (Label.class.isAssignableFrom(modelElement.getClass())) {
			return convertLabel((Label)modelElement, staticElementsOnly, converted, nonterminalNatureClassesMap, skipHierarchy);
		} else if (Grammar.class.isAssignableFrom(modelElement.getClass())) {
			if (staticElementsOnly) {
				return null;
			}
			return convertGrammar((Grammar)modelElement, converted, nonterminalNatureClassesMap, skipHierarchy);
		} else if (Function.class.isAssignableFrom(modelElement.getClass())) {
			if (staticElementsOnly) {
				return null;
			}
			return convertFunction((Function)modelElement, converted, nonterminalNatureClassesMap, skipHierarchy);
		}
		throw new GenericScheregException("Failed to convert model element; conversion not supported for " + modelElement.getClass().getName());
	}
	
	public static ModelElementPojo convertModelElementTerminal(Element modelElement, DatamodelNature nature) {
		return convertModelElementTerminal(modelElement, nature, new HashMap<>());
	}
	
	
	public static ModelElementPojo convertModelElementTerminal(ModelElement modelElement, DatamodelNature nature, Map<Nonterminal, ModelElementPojo> converted) {
		if (modelElement!=null && Nonterminal.class.isAssignableFrom(modelElement.getClass())) {
			Nonterminal n = (Nonterminal)modelElement;
			return convertTerminal(n, nature, converted, false);
		}
		return null;
	}
	
	
	
	private static ModelElementPojo convertTerminal(Nonterminal n, DatamodelNature nature, Map<Nonterminal, ModelElementPojo> converted, boolean skipHierarchy) {
		if (converted.containsKey(n)) {
			ModelElementPojo pExist = converted.get(n);
			pExist.setState(ModelElementState.REUSED);
			return createModelElementPojo(pExist.getId(), pExist.getLabel(), pExist.getType(), pExist.isDisabled(), pExist.isSessionVariable(), ModelElementState.REUSING);
		}
		
		var p = createTerminal(nature, n);
		converted.put(n, p);
		if (!skipHierarchy) {
			p.setChildElements(new ArrayList<>());
			if (n.getChildNonterminals()!=null && !n.getChildNonterminals().isEmpty()) {
				for (Nonterminal childN : n.getChildNonterminals()) {
					p.getChildElements().add(convertTerminal(childN, nature, converted, skipHierarchy));
				}
			}
			findDynamicTerminals(p, n, nature, converted);
			if (p.getChildElements().isEmpty()) {
				p.setChildElements(null);
			}
		}
		return p;
	}
	
	private static ModelElementPojo findDynamicTerminals(ModelElementPojo currentP, Element e, DatamodelNature nature, Map<Nonterminal, ModelElementPojo> converted) {
		if (e.getGrammars()!=null && !e.getGrammars().isEmpty()) {
			for (Grammar g : e.getGrammars()) {
				if (g.getFunctions()!=null && !g.getFunctions().isEmpty()) {
					for (Function f : g.getFunctions()) {
						if (f.getOutputElements()!=null && !f.getOutputElements().isEmpty()) {
							for (Element childE : f.getOutputElements()) {
								if (Nonterminal.class.isAssignableFrom(childE.getClass())) {
									currentP.getChildElements().add(convertTerminal((Nonterminal)childE, nature, converted, false));
								} else {
									findDynamicTerminals(currentP, childE, nature, converted);
								}
							}
						}
					}
				}
			}
		}
		if (Label.class.isAssignableFrom(e.getClass())) {
			Label l = (Label)e;
			if (l.getSubLabels()!=null && !l.getSubLabels().isEmpty()) {
				for (Label childL : l.getSubLabels()) {
					findDynamicTerminals(currentP, childL, nature, converted);
				}
			}
		}
		return currentP;
	}
	
	private static ModelElementPojo convertNonterminal(Nonterminal n, boolean staticElementsOnly, Map<Element, ModelElementPojo> converted, Map<String, List<String>> nonterminalNatureClassesMap, boolean skipHierarchy) {
		if (converted.containsKey(n)) {
			converted.get(n).setState(ModelElementState.REUSED);
			return createModelElementPojo(n.getId(), n.getName(), "Nonterminal", n.isDisabled(), n.getSessionVariable()!=null, ModelElementState.REUSING);
		}
		
		var p = createModelElementPojo(n.getId(), n.getName(), "Nonterminal", n.isDisabled(), n.getSessionVariable()!=null);
		if (nonterminalNatureClassesMap!=null && nonterminalNatureClassesMap.containsKey(n.getId())) {
			p.addInfo("mappedNatureClasses", nonterminalNatureClassesMap.get(n.getId()).toArray(new Object[0]));
		}
		converted.put(n, p);
		
		if (!skipHierarchy && n.getChildNonterminals()!=null && !n.getChildNonterminals().isEmpty()) {
			p.setChildElements(new ArrayList<>());
			for (Nonterminal childN : n.getChildNonterminals()) {
				p.getChildElements().add(convertNonterminal(childN, staticElementsOnly, converted, nonterminalNatureClassesMap, skipHierarchy));
			}
		}
		
		return convertElement(p, n, staticElementsOnly, converted, nonterminalNatureClassesMap, skipHierarchy); 
	}
	
	private static ModelElementPojo convertLabel(Label l, boolean staticElementsOnly, Map<Element, ModelElementPojo> converted, Map<String, List<String>> nonterminalNatureClassesMap, boolean skipHierarchy) {
		if (converted.containsKey(l)) {
			converted.get(l).setState(ModelElementState.REUSED);
			return createModelElementPojo(l.getId(), l.getName(), "Label", l.isDisabled(), l.getSessionVariable()!=null, ModelElementState.REUSING);
		}
		
		var p = createModelElementPojo(l.getId(), l.getName(), "Label", l.isDisabled(), l.getSessionVariable()!=null);
		converted.put(l, p);
		
		if (!staticElementsOnly && l.getSubLabels()!=null && !l.getSubLabels().isEmpty()) {
			if (p.getChildElements()==null) {
				p.setChildElements(new ArrayList<>());
			}
			for (Label childL : l.getSubLabels()) {
				p.getChildElements().add(convertLabel(childL, staticElementsOnly, converted, nonterminalNatureClassesMap, skipHierarchy));
			}
		}
		
		return convertElement(p, l, staticElementsOnly, converted, nonterminalNatureClassesMap, skipHierarchy); 
	}
	
	private static ModelElementPojo convertElement(ModelElementPojo p, Element e, boolean staticElementsOnly, Map<Element, ModelElementPojo> converted, Map<String, List<String>> nonterminalNatureClassesMap, boolean skipHierarchy) {
		p.setProcessingRoot(e.isProcessingRoot());
		p.setIdentifierElement(e.isIdentifierElement());
		
		if (skipHierarchy) {
			return p;
		}
		List<Element> producedElements = e.getProducedElements();
		if (staticElementsOnly && producedElements!=null && !producedElements.isEmpty()) {
			if (p.getChildElements()==null) {
				p.setChildElements(new ArrayList<>());
			}
			for (Element childL : producedElements) {
				if (Nonterminal.class.isAssignableFrom(childL.getClass())) {
					p.getChildElements().add(convertNonterminal((Nonterminal)childL, staticElementsOnly, converted, nonterminalNatureClassesMap, skipHierarchy));
				} else {
					p.getChildElements().add(convertLabel((Label)childL, staticElementsOnly, converted, nonterminalNatureClassesMap, skipHierarchy));
				}
			}
		}
		
		if (!staticElementsOnly && e.getGrammars()!=null && !e.getGrammars().isEmpty()) {
			if (p.getChildElements()==null) {
				p.setChildElements(new ArrayList<>());
			}
			for (Grammar g : e.getGrammars()) {
				p.getChildElements().add(convertGrammar(g, converted, nonterminalNatureClassesMap, skipHierarchy));
			}
		}
		return p;
	}
	 	
	private static ModelElementPojo convertGrammar(Grammar g, Map<Element, ModelElementPojo> converted, Map<String, List<String>> nonterminalNatureClassesMap, boolean skipHierarchy) {
		var p = createModelElementPojo(g.getId(), g.getName(), "Grammar", g.isDisabled(), false);
		if (!skipHierarchy && g.getFunctions()!=null && !g.getFunctions().isEmpty()) {
			p.setChildElements(new ArrayList<>());
			for (Function f : g.getFunctions()) {
				p.getChildElements().add(convertFunction(f, converted, nonterminalNatureClassesMap, skipHierarchy));
			}
		}
		return p;
	}

	
	private static ModelElementPojo convertFunction(Function f, Map<Element, ModelElementPojo> converted, Map<String, List<String>> nonterminalNatureClassesMap, boolean skipHierarchy) {
		var p = createModelElementPojo(f.getId(), f.getName(), "Function", f.isDisabled(), false);		
		if (!skipHierarchy && f.getOutputElements()!=null && !f.getOutputElements().isEmpty()) {
			p.setChildElements(new ArrayList<>());
			for (Element l : f.getOutputElements()) {
				if (Nonterminal.class.isAssignableFrom(l.getClass())) {
					p.getChildElements().add(convertNonterminal((Nonterminal)l, false, converted, nonterminalNatureClassesMap, skipHierarchy));
				} else {
					p.getChildElements().add(convertLabel((Label)l, false, converted, nonterminalNatureClassesMap, skipHierarchy));
				}
			}
		}
		
		return p;
	}

	
	private static ModelElementPojo createModelElementPojo(String id, String name, String type, boolean disabled, boolean sessionVariable) {
		return createModelElementPojo(id, name, type, disabled, sessionVariable, ModelElementState.OK);
	}
	 
	private static ModelElementPojo createModelElementPojo(String id, String name, String type, boolean disabled, boolean sessionVariable, ModelElementState state) {
		var p = new ModelElementPojo();
		p.setId(id);
		p.setLabel(name);
		p.setState(state);
		p.setType(type);
		p.setDisabled(disabled);
		p.setSessionVariable(sessionVariable);
		
		return p;
	}
	
	private static ModelElementPojo createTerminal(DatamodelNature nature, Nonterminal n) {
		var p = new ModelElementPojo();
		var t = nature.getTerminalByNonterminalId(n.getId());	
		if (t!=null) {
			p.setState(ModelElementState.OK);
			p.setId(t.getId());
			p.addInfo("natureClass", new Object[] { nature.getClass().getSimpleName(), nature.getClass().getName() });
			
			if (XmlTerminal.class.isAssignableFrom(t.getClass())) {
				Map<String, String> namespacePrefixMap = getNamespacePrefixMap(nature);
				
				XmlTerminal xmlT = (XmlTerminal)t;
				String prefix = null;
				
				if (namespacePrefixMap!=null && xmlT.getNamespace()!=null && namespacePrefixMap.containsKey(xmlT.getNamespace())) {
					prefix = namespacePrefixMap.get(xmlT.getNamespace());
				}
				
				StringBuilder labelBldr = new StringBuilder();
				if (prefix!=null && !prefix.isBlank()) {
					labelBldr.append(prefix).append(":");
				}
				labelBldr.append(t.getName());
				if (xmlT.getPredicate()!=null && !xmlT.getPredicate().isBlank()) {
					labelBldr.append(xmlT.getPredicate());
				}
				p.setLabel(labelBldr.toString());		
				
				
			} else {
				p.setLabel(t.getName());
			}
			p.setType("Terminal");
		} else {
			p.setState(ModelElementState.ERROR);
			p.setType("Terminal/Missing");
			p.setLabel(n.getName());
			p.setId(n.getId());
		}
		p.setProcessingRoot(n.isProcessingRoot());
		p.setDisabled(n.isDisabled());
		return p;
	}
	
	private static Map<String, String> getNamespacePrefixMap(DatamodelNature nature) {
		Map<String, String> namespacePrefixMap = null;
		if (XmlDatamodelNature.class.isAssignableFrom(nature.getClass()) && ((XmlDatamodelNature)nature).getNamespaces()!=null) {
			namespacePrefixMap = new HashMap<>();
			for (XmlNamespace xmlNs : ((XmlDatamodelNature)nature).getNamespaces()) {
				namespacePrefixMap.put(xmlNs.getUrl(), xmlNs.getPrefix());
			}
		}
		return namespacePrefixMap;
	}
}
