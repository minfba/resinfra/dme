package de.unibamberg.minf.dme.pojo;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TreeElementPojo {
	private String id;
	private String label;
	private Object value;
	private List<TreeElementPojo> children;	
}
