package de.unibamberg.minf.dme.confg;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import eu.dariah.de.dariahsp.config.web.SecurityConfigurerAdapter;
import eu.dariah.de.dariahsp.config.web.DefaultFiltersConfigurerAdapter;

/**
 * Web security configuration addressing protected areas and authorization patterns for this sample application
 * 
 * @author Tobias Gradl
 */
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	/**
	 * Adapt this as required in a target application
	 * 
	 * @author Tobias Gradl
	 */
	@Configuration
    @Order(1)
	public static class WebSecurityConfigAdapter extends SecurityConfigurerAdapter {
		@Override
		protected void configure(final HttpSecurity http) throws Exception {
			 http
	        	.requestMatchers()
	        		.antMatchers("/protected/**", "/blocked/**")
	        	.and()
	        	.authorizeRequests()
	        		.expressionHandler(this.hierarchicalExpressionHandler())
	        		.antMatchers("/protected/authenticated").authenticated()    
	        		.antMatchers("/protected/contributor").hasRole("CONTRIBUTOR")
	        		.antMatchers("/protected/admin").hasRole("ADMINISTRATOR")
	        		.antMatchers("/blocked/noaccess").denyAll();
			 
			 super.configure(http);
		}
	}
	
	/**
	 * Make sure to include this for logout, login and callback filters 
	 * 
	 * @author Tobias Gradl
	 */
	@Configuration
    @Order(2)
    public static class CallbackLoginLogoutConfigurationAdapter extends DefaultFiltersConfigurerAdapter {}
}
