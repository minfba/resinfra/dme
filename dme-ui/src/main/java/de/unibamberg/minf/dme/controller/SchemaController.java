package de.unibamberg.minf.dme.controller;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import de.unibamberg.minf.dme.service.interfaces.MappingService;
import de.unibamberg.minf.dme.service.interfaces.SchemaService;

@Controller
@RequestMapping(value="/schema/{entityId}/")
public class SchemaController { 
	
	@Autowired protected SchemaService schemaService;
	@Autowired protected MappingService mappingService;
	
		
	@GetMapping(value="")
	public String getSchemaRedirect(@PathVariable String entityId, HttpServletResponse response) throws IOException {
		if (entityId==null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		} else if (schemaService.findSchemaById(entityId)!=null) {
			response.sendRedirect("../../model/editor/" + entityId + "/");
		} else if (mappingService.findMappingById(entityId)!=null) {
			response.sendRedirect("../../mapping/editor/" + entityId + "/");
		} else {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		return null;
	}
}
