package de.unibamberg.minf.dme.controller.editors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import de.unibamberg.minf.dme.controller.base.BaseScheregController;
import de.unibamberg.minf.dme.exception.GenericScheregException;
import de.unibamberg.minf.dme.model.base.Element;
import de.unibamberg.minf.dme.model.base.Label;
import de.unibamberg.minf.dme.model.base.Nonterminal;
import de.unibamberg.minf.dme.model.datamodel.LabelImpl;
import de.unibamberg.minf.dme.model.datamodel.NonterminalImpl;
import de.unibamberg.minf.dme.model.datamodel.base.Datamodel;
import de.unibamberg.minf.dme.model.datamodel.base.DatamodelNature;
import de.unibamberg.minf.dme.model.grammar.GrammarImpl;
import de.unibamberg.minf.dme.model.reference.Reference;
import de.unibamberg.minf.dme.pojo.ModelElementPojo;
import de.unibamberg.minf.dme.pojo.converter.ModelElementPojoConverter;
import de.unibamberg.minf.dme.service.ElementServiceImpl;
import de.unibamberg.minf.dme.service.interfaces.ElementService;
import de.unibamberg.minf.dme.service.interfaces.GrammarService;
import de.unibamberg.minf.dme.service.interfaces.RelatedConceptService;
import eu.dariah.de.dariahsp.web.model.AuthPojo;
import de.unibamberg.minf.core.web.pojo.ModelActionPojo;

@Controller
@RequestMapping(value={"/model/editor/{schemaId}/element/{elementId}", "/mapping/editor/{schemaId}/element/{elementId}"})
public class ElementEditorController extends BaseScheregController {
	@Autowired private ElementService elementService;
	@Autowired private GrammarService grammarService;
	
	@Autowired private RelatedConceptService relatedConceptService;
	
	public ElementEditorController() {
		super("schemaEditor");
	}
		
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/assignChild")
	public @ResponseBody ModelActionPojo assignChild(@PathVariable String schemaId, @PathVariable String elementId, @RequestParam(value="element-id") String childId, Model model, Locale locale, HttpServletRequest request, HttpServletResponse response) {		
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		
		Reference parentReference = elementService.assignChildTreeToParent(schemaId, elementId, childId);		
		if (parentReference!=null) {
			return new ModelActionPojo(true);
		} else {
			return new ModelActionPojo(false);
		}
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.GET, value = "/form/assignChild")
	public String getAssignChildForm(@PathVariable String schemaId, @PathVariable String elementId, Model model, Locale locale, HttpServletRequest request, HttpServletResponse response) {	
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}		
		model.addAttribute("actionPath", "/model/editor/" + schemaId + "/element/" + elementId + "/assignChild");
		return "elementEditor/form/assign_child";
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/linkElement")
	public @ResponseBody ModelActionPojo linkElement(@PathVariable String schemaId, @PathVariable String elementId, @RequestParam(value="element-id") String linkId, Model model, Locale locale, HttpServletRequest request, HttpServletResponse response) {		
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		
		Reference linkReference = elementService.replaceWithLink(schemaId, elementId, linkId);
		if (linkReference!=null) {
			return new ModelActionPojo(true);
		} else {
			return new ModelActionPojo(false);
		}
	}
	
	@PreAuthorize("isAuthenticated()")
	@GetMapping("/form/linkElement")
	public String getLinkElementForm(@PathVariable String schemaId, @PathVariable String elementId, Model model, Locale locale, HttpServletRequest request, HttpServletResponse response) {	
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}		
		model.addAttribute("actionPath", "/model/editor/" + schemaId + "/element/" + elementId + "/linkElement");
		return "elementEditor/form/link_element";
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/form/element")
	public String getEditElementForm(@PathVariable String schemaId, @PathVariable String elementId, Model model, Locale locale, HttpServletRequest request) {		
		Element elem = elementService.findById(elementId);
		model.addAttribute("element", elem);
		
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			model.addAttribute("readonly", true);
		} else {
			model.addAttribute("readonly", false);
		}
		if (elem instanceof Nonterminal) {
			model.addAttribute("availableTerminals", schemaService.getAvailableTerminals(schemaId));
			model.addAttribute("actionPath", "/model/editor/" + schemaId + "/element/" + elementId + "/async/saveNonterminal");
			return "elementEditor/form/edit_nonterminal";
		} else {
			model.addAttribute("actionPath", "/model/editor/" + schemaId + "/element/" + elementId + "/async/saveLabel");
			return "elementEditor/form/edit_label";
		}
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.GET, value = "/form/new_nonterminal")
	public String getNewNonterminalForm(@PathVariable String schemaId, @PathVariable String elementId, HttpServletRequest request, HttpServletResponse response, Model model, Locale locale) {
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		model.addAttribute("element", new NonterminalImpl(schemaId, null));
		model.addAttribute("availableTerminals", schemaService.getAvailableTerminals(schemaId));
		model.addAttribute("actionPath", "/model/editor/" + schemaId + "/element/" + elementId + "/async/saveNewNonterminal");
		return "elementEditor/form/edit_nonterminal";
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.GET, value = "/form/new_label")
	public String getNewLabelForm(@PathVariable String schemaId, @PathVariable String elementId, Model model, HttpServletRequest request, HttpServletResponse response, Locale locale) {
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		model.addAttribute("element", new LabelImpl(schemaId, null));
		model.addAttribute("actionPath", "/model/editor/" + schemaId + "/element/" + elementId + "/async/saveNewLabel");
		return "elementEditor/form/edit_label";
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.GET, value = "/form/new_grammar")
	public String getNewGrammarForm(@PathVariable String schemaId, @PathVariable String elementId, Model model, HttpServletRequest request, HttpServletResponse response, Locale locale) {
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		model.addAttribute("grammar", new GrammarImpl(schemaId, null));
		model.addAttribute("actionPath", "/model/editor/" + schemaId + "/element/" + elementId + "/async/saveNewGrammar");
		return "grammarEditor/form/new";
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/async/saveLabel")
	public @ResponseBody ModelActionPojo saveLabel(@PathVariable String schemaId, @Valid LabelImpl element, BindingResult bindingResult, Locale locale, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		ModelActionPojo result = this.getActionResult(bindingResult, locale);
		if (result.isSuccess()) {
			this.mergeAndSaveElement(elementService.findById(element.getId()), element, schemaId, auth);
		}		
		return result;
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/async/saveNonterminal")
	public @ResponseBody ModelActionPojo saveNonterminal(@PathVariable String schemaId, @Valid NonterminalImpl element, BindingResult bindingResult, Locale locale, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if (!schemaService.getUserCanWriteEntity(schemaId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		ModelActionPojo result = this.getActionResult(bindingResult, locale);
		if (result.isSuccess()) {
			Nonterminal n = (Nonterminal)elementService.findById(element.getId());
			n.setAssignComplexContent(element.isAssignComplexContent());
			this.mergeAndSaveElement(n, element, schemaId, auth);
		}		
		return result;
	}
	
	private void mergeAndSaveElement(Element saveElement, Element mergeElement, String entityId, AuthPojo auth) {
		//Element saveElement = (Element)elementService.findById(mergeElement.getId());
		// Not changeable here
		//n.setTerminalId(element.getTerminalId());
		saveElement.setTransient(mergeElement.isTransient());
		saveElement.setIdentifierElement(mergeElement.isIdentifierElement());
		saveElement.setName(ElementServiceImpl.getNormalizedName(mergeElement.getName()));
		saveElement.setEntityId(entityId);
		saveElement.setIncludeHeader(mergeElement.isIncludeHeader());
		saveElement.setHierarchicalRoot(mergeElement.isHierarchicalRoot());
		saveElement.setSessionVariable(mergeElement.getSessionVariable()==null || mergeElement.getSessionVariable().isBlank() ? null : mergeElement.getSessionVariable());
		
		if (saveElement.isProcessingRoot()!=mergeElement.isProcessingRoot()) {	
			elementService.unsetSchemaProcessingRoot(entityId);
			saveElement.setProcessingRoot(mergeElement.isProcessingRoot());
			if (!saveElement.isProcessingRoot()) {
				// Set root -> processingRoot
				Element e = elementService.findRootBySchemaId(entityId);
				if (Nonterminal.class.isAssignableFrom(e.getClass())) {
					Nonterminal root = (Nonterminal)e;
					root.setProcessingRoot(true);
					elementService.saveElement(root, auth);
				}
			}
		}
		elementService.saveElement(saveElement, auth);
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/async/saveNewLabel")
	public @ResponseBody ModelActionPojo saveNewLabel(@PathVariable String schemaId, @PathVariable String elementId, @Valid LabelImpl element, BindingResult bindingResult, Locale locale, HttpServletRequest request, HttpServletResponse response) throws GenericScheregException {
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		ModelActionPojo result = this.getActionResult(bindingResult, locale);
		if (result.isSuccess()) {
			elementService.createAndAppendElement(schemaId, elementId, element.getName(), authInfoHelper.getAuth(), Label.class);
		}
		return result;
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/async/saveNewNonterminal")
	public @ResponseBody ModelActionPojo saveNewNonterminal(@PathVariable String schemaId, @PathVariable String elementId, @Valid NonterminalImpl element, BindingResult bindingResult, Locale locale, HttpServletRequest request, HttpServletResponse response) throws GenericScheregException {
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		ModelActionPojo result = this.getActionResult(bindingResult, locale);
		if (result.isSuccess()) {
			element.setEntityId(schemaId);
			elementService.createAndAppendElement(schemaId, elementId, element.getName(), authInfoHelper.getAuth(), Nonterminal.class);
		}
		return result;
	}

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/async/saveNewGrammar")
	public @ResponseBody ModelActionPojo saveNewGrammar(@PathVariable String schemaId, @PathVariable String elementId, @Valid GrammarImpl grammar, BindingResult bindingResult, Locale locale, HttpServletRequest request, HttpServletResponse response) {
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		ModelActionPojo result = this.getActionResult(bindingResult, locale);
		if (result.isSuccess()) {
			grammarService.createAndAppendGrammar(schemaId, elementId, grammar.getName(), authInfoHelper.getAuth());
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/get")
	public @ResponseBody ModelElementPojo getElement(@PathVariable String schemaId, @PathVariable String elementId, HttpServletRequest request, HttpServletResponse response) throws IOException, GenericScheregException {
		AuthPojo auth = authInfoHelper.getAuth();
		Element result = elementService.findById(elementId);
		if (result==null) {
			response.getWriter().print("null");
			response.setContentType("application/json");
		}
		
		Map<String, List<String>> nonterminalNatureClassesMap = new HashMap<String, List<String>>();
		Datamodel m = schemaService.findByIdAndAuth(schemaId, auth).getElement();
		
		if (m.getNatures()!=null) {
			for (DatamodelNature n : m.getNatures()) {
				if (n.getNonterminalTerminalIdMap()!=null) {
					for (String nId : n.getNonterminalTerminalIdMap().keySet()) {
						List<String> natureClasses = nonterminalNatureClassesMap.get(nId);
						if (natureClasses==null) {
							natureClasses = new ArrayList<String>();
						}
						natureClasses.add(n.getClass().getName());
						nonterminalNatureClassesMap.put(nId, natureClasses);
					}
				}
			}
		}
		
		return ModelElementPojoConverter.convertModelElement(result, nonterminalNatureClassesMap, false, true);
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.GET, value = "/async/remove")
	public @ResponseBody ModelActionPojo removeElement(@PathVariable String schemaId, @PathVariable String elementId, HttpServletRequest request, HttpServletResponse response) {
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		relatedConceptService.removeElementReferences(schemaId, elementId);
		elementService.removeElement(schemaId, elementId, authInfoHelper.getAuth());
		
		return new ModelActionPojo(true);
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.GET, value = "/async/disable")
	public @ResponseBody ModelActionPojo disableElement(@PathVariable String schemaId, @PathVariable String elementId, @RequestParam boolean disabled, HttpServletRequest request, HttpServletResponse response) {
		if (!schemaService.getUserCanWriteEntity(schemaId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		
		Element e = elementService.findById(elementId);
		e.setDisabled(disabled);
		
		elementService.saveElement(e, authInfoHelper.getAuth());
		return new ModelActionPojo(true);
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/async/clone", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public @ResponseBody ModelActionPojo cloneElement(@PathVariable String schemaId, @PathVariable String elementId, @RequestParam(value = "path[]") String[] path, HttpServletRequest request, HttpServletResponse response) throws GenericScheregException {
		AuthPojo auth = authInfoHelper.getAuth();
		elementService.cloneElement(elementId, path, auth);
		return new ModelActionPojo(true);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/move")
	public @ResponseBody ModelActionPojo parseSampleInput(@PathVariable String schemaId, @PathVariable String elementId, @RequestParam int delta, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!schemaService.getUserCanWriteEntity(schemaId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		
		Class<?> elementType;
		if (NonterminalImpl.class.isAssignableFrom(elementService.findById(elementId).getClass())) {
			elementType = NonterminalImpl.class; 
		} else {
			elementType = LabelImpl.class;
		}
		
		elementService.moveReference(schemaId, elementId, elementType, delta, authInfoHelper.getAuth());
		
		return new ModelActionPojo(true);
	}
	
}