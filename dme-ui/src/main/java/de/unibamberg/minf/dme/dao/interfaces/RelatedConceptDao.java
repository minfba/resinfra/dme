package de.unibamberg.minf.dme.dao.interfaces;

import java.util.List;

import de.unibamberg.minf.dme.dao.base.ModelElementDao;
import de.unibamberg.minf.dme.model.mapping.base.RelatedConcept;

public interface RelatedConceptDao extends ModelElementDao<RelatedConcept> {
	public List<RelatedConcept> findByEntityId(String entityId);

	public List<RelatedConcept> findBySourceElementId(String elementId);
	public List<RelatedConcept> findByTargetElementId(String elementId);
}
