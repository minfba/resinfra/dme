package de.unibamberg.minf.dme.confg.nested;

import lombok.Data;

@Data
public class RemoteTokensConfigProperties {
	private String tokenRetrieval;
	private String apiUrlRegex;
	private String tokenApiUrl;
	private String username;
	private String password;
}
