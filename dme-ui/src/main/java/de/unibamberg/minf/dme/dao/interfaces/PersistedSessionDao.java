package de.unibamberg.minf.dme.dao.interfaces;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import de.unibamberg.minf.dme.dao.base.BaseDao;
import de.unibamberg.minf.dme.model.PersistedSession;
import de.unibamberg.minf.dme.model.SessionSampleFile;
import de.unibamberg.minf.dme.model.SessionSampleFile.FileTypes;
import de.unibamberg.minf.processing.model.SerializableResource;
import de.unibamberg.minf.processing.model.SerializableRootResource;
import de.unibamberg.minf.processing.model.base.Resource;

public interface PersistedSessionDao extends BaseDao<PersistedSession> {
	public SessionSampleFile saveAsSessionInputFile(File file, FileTypes type, String sessionId);
	public SessionSampleFile saveAsSessionInputFile(String sample, FileTypes type, String sessionId);
	public PersistedSession updateSessionFileType(String sessionId, FileTypes fileType);
	
	public List<SerializableRootResource> loadSampleOutput(String sessionId);
	public List<SerializableRootResource> loadSampleMapped(String sessionId);
	public Map<String, String> loadSelectedValueMap(String sessionId);
	public void saveSampleOutput(String sessionId, List<Resource> sampleOutput);
	public void saveSampleMapped(String sessionId, List<Resource> sampleMapped);
	public void saveSelectedValueMap(String sessionId, Map<String, String> selectedValueMap);
	
	public String loadSessionData(String sessionId);
	public void saveSessionData(String sessionId, JsonNode json);
	public void removeSessionData(String sessionId);
	public void saveSelectedResourceMap(String sessionId, List<Resource> selectedResourceMap);
	public List<SerializableResource> loadSelectedResourceMap(String sessionId);
}
