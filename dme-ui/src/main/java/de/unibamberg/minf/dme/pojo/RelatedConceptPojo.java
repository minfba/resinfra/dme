package de.unibamberg.minf.dme.pojo;

import java.util.List;

import de.unibamberg.minf.dme.model.base.Identifiable;
import lombok.Data;

@Data
public abstract class RelatedConceptPojo implements Identifiable {
	private static final long serialVersionUID = -5462724852373673591L;
	
	public enum MappingTypes { EXPORT, FUNCTION, VALUE }
	
	private String id;
	private MappingTypes type;
	private List<String> sourceElementIds;
	private List<String> targetElementIds;
}