package de.unibamberg.minf.dme.controller.editors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.unibamberg.minf.dme.confg.DebugConfig;
import de.unibamberg.minf.dme.controller.base.BaseScheregController;
import de.unibamberg.minf.dme.exception.GenericScheregException;
import de.unibamberg.minf.dme.model.PersistedSession;
import de.unibamberg.minf.dme.model.SessionSampleFile;
import de.unibamberg.minf.dme.model.base.Element;
import de.unibamberg.minf.dme.model.datamodel.NonterminalImpl;
import de.unibamberg.minf.dme.model.datamodel.base.Datamodel;
import de.unibamberg.minf.dme.model.datamodel.base.DatamodelNature;
import de.unibamberg.minf.dme.model.grammar.GrammarImpl;
import de.unibamberg.minf.dme.model.mapping.ExportedConceptImpl;
import de.unibamberg.minf.dme.model.mapping.MappedConceptImpl;
import de.unibamberg.minf.dme.model.mapping.base.ExportedConcept;
import de.unibamberg.minf.dme.model.mapping.base.MappedConcept;
import de.unibamberg.minf.dme.model.mapping.base.RelatedConcept;
import de.unibamberg.minf.dme.pojo.ModelElementPojo;
import de.unibamberg.minf.dme.pojo.RelatedConceptPojo;
import de.unibamberg.minf.dme.pojo.TreeElementPojo;
import de.unibamberg.minf.dme.pojo.converter.ModelElementPojoConverter;
import de.unibamberg.minf.dme.pojo.converter.RelatedConceptPojoConverter;
import de.unibamberg.minf.dme.pojo.converter.ResourcePojoConverter;
import de.unibamberg.minf.dme.service.interfaces.ElementService;
import de.unibamberg.minf.dme.service.interfaces.FunctionService;
import de.unibamberg.minf.dme.service.interfaces.GrammarService;
import de.unibamberg.minf.dme.service.interfaces.PersistedSessionService;
import de.unibamberg.minf.dme.service.interfaces.RelatedConceptService;
import de.unibamberg.minf.gtf.transformation.processing.params.OutputParam;
import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.model.SerializableResource;
import de.unibamberg.minf.processing.model.base.Resource;
import de.unibamberg.minf.processing.output.json.JsonStringOutputService;
import eu.dariah.de.dariahsp.web.model.AuthPojo;
import lombok.extern.slf4j.Slf4j;
import de.unibamberg.minf.core.web.pojo.ModelActionPojo;

@Slf4j
@Controller
@RequestMapping(value="/mapping/editor/{mappingId}/mappedconcept/{mappedConceptId}")
public class MappedConceptEditorController extends BaseScheregController {
	@Autowired private RelatedConceptService relatedConceptService;
	@Autowired private ElementService elementService;
	@Autowired private GrammarService grammarService;
	@Autowired private PersistedSessionService sessionService;
	@Autowired private FunctionService functionService;
	
	@Autowired private DebugConfig debugConfig;
	
	public MappedConceptEditorController() {
		super("mappingEditor");
	}
	
	@RequestMapping(value="/async/get", method=RequestMethod.GET)
	public @ResponseBody RelatedConceptPojo getMappedConcept(@PathVariable String mappingId, @PathVariable String mappedConceptId, Model model, HttpServletRequest request) {
		return RelatedConceptPojoConverter.convert(relatedConceptService.findById(mappingId, mappedConceptId, true), RelatedConceptPojoConverter.convertFunctionsToIdFunctionMap(functionService.findByEntityId(mappingId)));
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/async/switchConceptType")
	public @ResponseBody ModelActionPojo switchConceptType(@PathVariable String mappingId, @PathVariable String mappedConceptId, @RequestParam String type, HttpServletRequest request, HttpServletResponse response) throws GenericScheregException {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!mappingService.getUserCanWriteEntity(mappingId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		
		RelatedConcept switchResult = relatedConceptService.switchConceptType(mappingId, mappedConceptId, RelatedConceptPojo.MappingTypes.valueOf(type), authInfoHelper.getAuth());
		ModelActionPojo result = new ModelActionPojo(true);
		result.setPojo(RelatedConceptPojoConverter.convert(switchResult, RelatedConceptPojoConverter.convertFunctionsToIdFunctionMap(functionService.findByEntityId(mappingId))));
		return result;
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/async/remove")
	public @ResponseBody ModelActionPojo removeConcept(@PathVariable String mappingId, @PathVariable String mappedConceptId, HttpServletRequest request, HttpServletResponse response) throws GenericScheregException {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!mappingService.getUserCanWriteEntity(mappingId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		relatedConceptService.removeRelatedConcept(mappingId, mappedConceptId, authInfoHelper.getAuth());
		return new ModelActionPojo(true);
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/async/save")
	public @ResponseBody ModelActionPojo saveConcept(@PathVariable String mappingId, @PathVariable String mappedConceptId, @RequestParam(value="sourceElementId[]") List<String> sourceElementId, @RequestParam(value="targetElementId[]") List<String> targetElementIds,  HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!mappingService.getUserCanWriteEntity(mappingId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		
		RelatedConcept c = null;
		if (mappedConceptId!=null && !mappedConceptId.equals("") && !mappedConceptId.equals("undefined")) {
			c = relatedConceptService.findById(mappedConceptId);
		}
		if (c==null) {
			c = new MappedConceptImpl();
			c.setElementGrammarIdsMap(new HashMap<>());
		}
		c.setEntityId(mappingId);
		
		for (String sourceId : sourceElementId) {
			if (!c.getElementGrammarIdsMap().keySet().contains(sourceId)) {
				c.getElementGrammarIdsMap().put(sourceId, null);
			}
		}

		for (String targetElementId : targetElementIds) {
			if (c.getTargetElementIds()==null) {
				c.setTargetElementIds(new ArrayList<String>());
			}			
			if (!c.getTargetElementIds().contains(targetElementId)) {
				c.getTargetElementIds().add(targetElementId);
			}
		}
		
		relatedConceptService.saveRelatedConcept(c, mappingId, auth);
		
		ModelActionPojo result = new ModelActionPojo(true);
		result.setPojo(c);
		
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/function")
	public @ResponseBody String getConceptFunction(@PathVariable String mappingId, @PathVariable String mappedConceptId, Model model, Locale locale, HttpServletResponse response) throws IOException {
		RelatedConcept rc = relatedConceptService.findById(mappingId, mappedConceptId, false);
		if (MappedConcept.class.isAssignableFrom(rc.getClass())) {
			return MappedConcept.class.cast(rc).getFunctionId();
		}
		return null;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/source")
	public @ResponseBody List<ModelElementPojo> getRenderedSource(@PathVariable String mappingId, @PathVariable String mappedConceptId, Model model, Locale locale, HttpServletResponse response) throws IOException, GenericScheregException {
		RelatedConcept mc = relatedConceptService.findById(mappingId, mappedConceptId, true);
		if (mc==null) {
			response.getWriter().print("null");
			return null;
		}
		
		List<String> loadIds = new ArrayList<>();
		loadIds.addAll(mc.getElementGrammarIdsMap().keySet());
		
		List<Element> sourceElements = elementService.findByIds(loadIds);

		for (String sourceId : mc.getElementGrammarIdsMap().keySet()) {
			for (Element sourceElement : sourceElements) {
				if (sourceId.equals(sourceElement.getId())) {
					sourceElement.setGrammars(new ArrayList<>());
					sourceElement.getGrammars().add((GrammarImpl) grammarService.findById(mc.getElementGrammarIdsMap().get(sourceId)));
					
					break;
				}
			}
		}
		return ModelElementPojoConverter.convertModelElements(sourceElements, false);
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/source/{sourceId}/remove")
	public @ResponseBody ModelActionPojo removeSource(@PathVariable String mappingId, @PathVariable String mappedConceptId, @PathVariable String sourceId, HttpServletRequest request) throws GenericScheregException {
		relatedConceptService.removeSourceElementById(authInfoHelper.getAuth(), mappingId, mappedConceptId, sourceId);
		return new ModelActionPojo(true);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/target")
	public @ResponseBody List<ModelElementPojo> getRenderedTargets(@PathVariable String mappingId, @PathVariable String mappedConceptId, Model model, Locale locale, HttpServletResponse response) throws IOException, GenericScheregException {
		RelatedConcept mc = relatedConceptService.findById(mappingId, mappedConceptId, true);
		if (mc==null) {
			response.getWriter().print("null");
			return null;
		}
		List<String> targetElementIds = new ArrayList<>();
		targetElementIds.addAll(mc.getTargetElementIds());
				
		return ModelElementPojoConverter.convertModelElements(elementService.findByIds(targetElementIds), false);
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/target/{targetId}/remove")
	public @ResponseBody ModelActionPojo removeTarget(@PathVariable String mappingId, @PathVariable String mappedConceptId, @PathVariable String targetId, HttpServletRequest request, HttpServletResponse response) throws GenericScheregException {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!mappingService.getUserCanWriteEntity(mappingId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		RelatedConcept mc = relatedConceptService.findById(mappingId, mappedConceptId, true);
		
		if (mc.getTargetElementIds().contains(targetId)) {
			mc.getTargetElementIds().remove(targetId);
		}
		
		// Delete mapping if there are no remaining targets
		if (mc.getTargetElementIds().isEmpty()) {
			relatedConceptService.removeRelatedConcept(mappingId, mc.getId(), auth);
		} else {		
			relatedConceptService.saveRelatedConcept(mc, mappingId, auth);
		}
		return new ModelActionPojo(true);
	}
	
	@GetMapping("/form/editExport")
	public String getEditExportForm(@PathVariable String mappingId, @PathVariable String mappedConceptId, HttpServletRequest request, HttpServletResponse response, Model model, Locale locale) {
		PersistedSession s = sessionService.get(mappingId, request.getSession().getId(), authInfoHelper.getUserId());
		if (s==null) {
			response.setStatus(HttpServletResponse.SC_RESET_CONTENT);
			return null;
		}
		RelatedConcept rc = this.setupEditModel(mappingId, mappedConceptId, s, response, model, locale);
		
		String schemaId = mappingService.findMappingById(mappingId).getSourceId();
		Datamodel dm = schemaService.findSchemaById(schemaId);
				
		
		this.addExportSampleToModel(s, rc, model);

		model.addAttribute("formats", ExportedConceptImpl.ExportFormats.values());
		model.addAttribute("natures", dm.getNatures()!=null ? dm.getNatures().stream().map(n -> n.getClass().getSimpleName()).collect(Collectors.toList()): new ArrayList<String>());
		model.addAttribute("actionPath", "/mapping/editor/" + mappingId + "/mappedconcept/" + mappedConceptId + "/async/saveExport");
		return "conceptEditor/form/edit_export_concept";
	}

	@PreAuthorize("isAuthenticated()")
	@PostMapping(value="/async/saveExport", produces = "application/json; charset=utf-8")
	public @ResponseBody ModelActionPojo saveExport(@PathVariable String mappingId, @PathVariable String mappedConceptId, @Valid ExportedConceptImpl concept, BindingResult bindingResult, Locale locale, HttpServletRequest request, HttpServletResponse response) throws IOException {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!mappingService.getUserCanWriteEntity(mappingId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}			
		ModelActionPojo result = this.getActionResult(bindingResult, locale);
		if (!result.isSuccess()) {
			return result;
		}
		ExportedConcept ec = (ExportedConcept)relatedConceptService.findById(mappedConceptId);
		if (!ec.getEntityId().equals(mappingId)) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}	
		
		ec.setFormat(concept.getFormat());
		ec.setIncludeTree(concept.isIncludeTree());
		ec.setEscape(concept.isEscape());
		ec.setIncludeSelf(concept.isIncludeSelf());
		ec.setUseTerminalsIfAvailable(concept.isUseTerminalsIfAvailable());
		
		relatedConceptService.saveRelatedConcept(ec, mappingId, auth);
		
		return new ModelActionPojo(true);
	}
	
	@GetMapping("/form/editFunction")
	public String getEditFunctionForm(@PathVariable String mappingId, @PathVariable String mappedConceptId, HttpServletRequest request, HttpServletResponse response, Model model, Locale locale) {
		PersistedSession s = sessionService.get(mappingId, request.getSession().getId(), authInfoHelper.getUserId());
		if (s==null) {
			response.setStatus(HttpServletResponse.SC_RESET_CONTENT);
			return null;
		}
		this.setupEditModel(mappingId, mappedConceptId, s, response, model, locale);
		return "conceptEditor/form/edit_function_concept";
	}
	
	
	@PostMapping("/async/exportSample")
	public @ResponseBody ModelActionPojo exportSampleInput(@PathVariable String mappingId, @PathVariable String mappedConceptId, @RequestBody JsonNode jsonNode, HttpServletRequest request, HttpServletResponse response, Locale locale) {
		PersistedSession s = sessionService.get(mappingId, request.getSession().getId(), authInfoHelper.getUserId());
		ExportedConcept ec = (ExportedConcept)relatedConceptService.findById(mappedConceptId);
		if (!ec.getEntityId().equals(mappingId)) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		
		// Parse options
		boolean includeTree = jsonNode.path("includeTree").asBoolean();
		boolean includeSelf = jsonNode.path("includeSelf").asBoolean();
		boolean escape = jsonNode.path("escape").asBoolean();
		boolean useTerminalsIfAvailable = jsonNode.path("useTerminalsIfAvailable").asBoolean();
		
		// Load and possibly prune resources
		List<? extends Resource> res = this.getSampleExportResources( s, ec, includeTree, includeSelf);
		if (res==null) {
			return new ModelActionPojo(false);
		}
		// Get output service
		// TODO: Make more generic and include XML
		JsonStringOutputService jos = this.setupOutputService(mappingId, escape, useTerminalsIfAvailable);
		if (jos==null) {
			return new ModelActionPojo(false);
		}
		
		// Convert and return sample export result
		try {
			String conv = jos.writeToString(res.size()==1 ? new Resource[] { res.get(0) } : res.toArray(new Resource[0]));
			return this.renderExportResult(ec, conv);
		} catch (ProcessingConfigException e) {
			log.error("Exception when trying to convert resources to JSON", e);
			return new ModelActionPojo(false);
		}
	}
	
	private RelatedConcept setupEditModel(String mappingId, String mappedConceptId, PersistedSession s, HttpServletResponse response, Model model, Locale locale) {				
		AuthPojo auth = authInfoHelper.getAuth();
		// Checks both entity types
		model.addAttribute("readonly", !schemaService.getUserCanWriteEntity(mappingId, auth.getUserId()));
		
		RelatedConcept mc = relatedConceptService.findById(mappingId, mappedConceptId, true);
		Map<Element, String> sampleInputs = new LinkedHashMap<>();
		
		List<String> inputElementIds = new ArrayList<>();
		inputElementIds.addAll(mc.getElementGrammarIdsMap().keySet());
		
		for (Element e : elementService.findByIds(inputElementIds) ){
			sampleInputs.put(e, sessionService.getSampleInputValue(s, e.getId()));
		}

		model.addAttribute("sampleInputMap", sampleInputs);		
		model.addAttribute("concept", mc);
		
		return mc;
	}
	
	private void addExportSampleToModel(PersistedSession s, RelatedConcept rc, Model model) {
		List<? extends Resource> res = null;
		for (String inputElementId : rc.getElementGrammarIdsMap().keySet()) {
			res = sessionService.getSelectedResources(s, inputElementId);
		}
		if (res!=null && !res.isEmpty()) {
			JsonNode pojo = ResourcePojoConverter.convertResource(res.get(0));
			model.addAttribute("sampleAvailable", true);
			if (pojo.toString().getBytes().length > this.debugConfig.getSamplesMaxTravelSize()) {
				model.addAttribute("sampleOversize", true);
			} else {
				model.addAttribute("sampleOversize", false);
				model.addAttribute("sample", pojo.toString());
			}
		} else {
			model.addAttribute("sampleAvailable", false);
		}
	}
	

	private List<? extends Resource> getSampleExportResources(PersistedSession s, ExportedConcept ec, boolean includeTree, boolean includeSelf) {
		List<? extends Resource> res = null;
		for (String inputElementId : ec.getElementGrammarIdsMap().keySet()) {
			res = sessionService.getSelectedResources(s, inputElementId);
			
			// Navigate to children immediately
			if (!includeSelf) {
				//res = res.stream().map(Resource::getChildResources).flatMap(Collection::stream).collect(Collectors.toList());
				res.stream().forEach(r->r.setExploded(true));
			}
			
			// Prune tree if configured
			if (!includeTree) {
				res.stream().forEach(r->r.setChildResources(null));
			}
		}
		return res;
	}
	
	private JsonStringOutputService setupOutputService(String mappingId, boolean escape, boolean useTerminals) {
		JsonStringOutputService jos = appContext.getBean(JsonStringOutputService.class);
		
		String schemaId = mappingService.findMappingById(mappingId).getSourceId();
		jos.setSchema(schemaService.findSchemaById(schemaId));
		jos.setRoot(elementService.findRootBySchemaId(schemaId, true));
		
		jos.setEscape(escape);
		jos.setUseTerminals(useTerminals);
		
		try {
			jos.init();
		} catch (ProcessingConfigException e) {
			log.error("Failed to initialize OutputService", e);
			return null;
		}
		
		return jos;
	}
	
	private ModelActionPojo renderExportResult(ExportedConcept ec, String convertedResourcesAsString) {
		List<Element> targetElements = elementService.findByIds(ec.getTargetElementIds());
		ModelActionPojo result = new ModelActionPojo(true);
		List<TreeElementPojo> resultPojos = new ArrayList<>();
			
		String label;
		for (String targetId : ec.getTargetElementIds()) {
			label = targetElements.stream().filter(e -> e.getId().equals(targetId)).findFirst().orElse(new NonterminalImpl(targetId, "?")).getName();
			resultPojos.add(new TreeElementPojo(targetId, label, convertedResourcesAsString, null));
		}
		result.setPojo(resultPojos);		
		return result;
	}
}
