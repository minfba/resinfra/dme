package de.unibamberg.minf.dme.dao;

import org.springframework.stereotype.Repository;

import de.unibamberg.minf.dme.dao.base.BaseDaoImpl;
import de.unibamberg.minf.dme.dao.interfaces.VersionInfoDao;
import de.unibamberg.minf.dme.model.version.VersionInfo;

@Repository
public class VersionInfoDaoImpl extends BaseDaoImpl<VersionInfo> implements VersionInfoDao {
	public VersionInfoDaoImpl() {
		super(VersionInfo.class);
	}
}
