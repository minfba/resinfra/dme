package de.unibamberg.minf.dme.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.MissingNode;

import de.unibamberg.minf.dme.dao.interfaces.PersistedSessionDao;
import de.unibamberg.minf.dme.exception.GenericScheregException;
import de.unibamberg.minf.dme.model.LogEntry;
import de.unibamberg.minf.dme.model.PersistedSession;
import de.unibamberg.minf.dme.model.SessionSampleFile;
import de.unibamberg.minf.dme.model.LogEntry.LogType;
import de.unibamberg.minf.dme.model.SessionSampleFile.FileTypes;
import de.unibamberg.minf.dme.service.interfaces.PersistedSessionService;
import de.unibamberg.minf.processing.model.SerializableResource;
import de.unibamberg.minf.processing.model.SerializableRootResource;
import de.unibamberg.minf.processing.model.base.Resource;

@Service
public class PersistedSessionServiceImpl implements PersistedSessionService {
	protected final static Logger logger = LoggerFactory.getLogger(PersistedSessionServiceImpl.class);
		
	@Autowired private PersistedSessionDao sessionDao;
	@Autowired private ObjectMapper objMapper;

	@Override
	public List<PersistedSession> findAllByUser(String entityId, String userId) {
		if (userId==null) {
			return null;
		}		
		return sessionDao.find(Query.query(Criteria.where("userId").is(userId).and("entityId").is(entityId)));
	}
	
	@Override
	public SessionSampleFile saveAsSessionInputFile(File file, FileTypes type, String sessionId) {
		return sessionDao.saveAsSessionInputFile(file, type, sessionId);
	}
	
	@Override
	public SessionSampleFile saveAsSessionInputFile(String sample, FileTypes type, String sessionId) {
		return sessionDao.saveAsSessionInputFile(sample, type, sessionId);
	}
	
	@Override
	public void saveSessionData(String sessionData, String sessionId) throws JsonMappingException, JsonProcessingException {
		if (sessionData.isBlank()) {
			sessionDao.removeSessionData(sessionId);
		} else {
			JsonNode parsedJson = objMapper.readTree(sessionData);
			sessionDao.saveSessionData(sessionId, parsedJson);
		}
	}
	
	@Override
	public JsonNode loadSessionData(String sessionId) {
		String sessionData = sessionDao.loadSessionData(sessionId);
		if (sessionData==null) {
			return MissingNode.getInstance();
		}
		try {
			return objMapper.readTree(sessionData);
		} catch (JsonProcessingException e) {
			logger.error("Failed to parse session data file");
			return MissingNode.getInstance();
		}
	}
	
	
	@Override
	public PersistedSession setSampleFileType(PersistedSession session, FileTypes fileType) {
		if (session.getSampleFile()==null || session.getSampleFile().getType().equals(fileType)) {
			return session;
		}
		return sessionDao.updateSessionFileType(session.getId(), fileType);
	}
	
	public static InputStream getSampleInputStream(PersistedSession session) {
		if (session.getSampleFile()==null || session.getSampleFile().getPath()==null ||
				!(new File(session.getSampleFile().getPath()).exists())) {
			return null;
		}
		try {
			return new FileInputStream(session.getSampleFile().getPath());
		} catch (Exception e) {
			logger.error("Failed to open session sample file");
			return null;
		}
	}
	
	@Override
	public List<PersistedSession> findExpiredSessions(DateTime cutoffTimestamp) {
		return sessionDao.find(Query.query(Criteria.where("notExpiring").is(false).and("lastAccessed").lte(cutoffTimestamp)));
	}
	
	@Override
	public PersistedSession accessOrCreate(String entityId, String httpSessionId, String userId) throws GenericScheregException {
		PersistedSession s = this.findSession(entityId, httpSessionId, userId);
		if (s==null) {
			if (userId!=null) {
				// User has just logged in: current session assigned to null user
				s = this.findSession(entityId, httpSessionId, null);
				if (s!=null) {
					if (s.hasData()) {
						s.setUserId(userId);
						return this.saveSession(s);
					} else {
						this.deleteSession(s);
						s = null;
					}
				}
				// No current session -> find and load latest
				s = this.findLatest(entityId, userId);
				if (s!=null) {
					return reassignPersistedSession(httpSessionId, userId, s.getId());
				}
			}
			return createAndSaveSession(entityId, httpSessionId, userId);
		}
		return this.saveSession(s);
	}
	
	@Override
	public PersistedSession get(String entityId, String httpSessionId, String userId) {
		PersistedSession s = this.findSession(entityId, httpSessionId, userId);
		if (s==null) {
			return null;
		}
		return s;
	}
	
	@Override
	public String getSampleInputValue(PersistedSession s, String elementId) {
		Map<String, String> selectedValueMap = this.loadSelectedValueMap(s.getId());
		
		if (selectedValueMap!=null) {
			if (selectedValueMap.containsKey(elementId)) {
				return selectedValueMap.get(elementId);
			}
		}
		return "";
	}
	
	@Override
	public List<SerializableResource> getSelectedResources(PersistedSession s, String elementId) {
		List<SerializableResource> selectedValueMap = this.loadSelectedResourceMap(s.getId());
		
		if (selectedValueMap!=null) {
			return selectedValueMap.stream().filter(r->r.getElementId().equals(elementId)).collect(Collectors.toList());
		}
		return new ArrayList<>();
	}
	
	@Override
	public List<SerializableRootResource> loadSampleOutput(String sessionId) { 
		return sessionDao.loadSampleOutput(sessionId);
	}
	
	@Override
	public List<SerializableRootResource> loadSampleMapped(String sessionId) { 
		return sessionDao.loadSampleMapped(sessionId); 
	}
	
	@Override
	public Map<String, String> loadSelectedValueMap(String sessionId) { 
		return sessionDao.loadSelectedValueMap(sessionId);
	}
	

	@Override
	public List<SerializableResource> loadSelectedResourceMap(String sessionId) { 
		return sessionDao.loadSelectedResourceMap(sessionId);
	}
	
	@Override
	public void saveSampleOutput(String sessionId, List<Resource> sampleOutput) {
		sessionDao.saveSampleOutput(sessionId, sampleOutput); 
	}
	
	@Override
	public void saveSampleMapped(String sessionId, List<Resource> sampleMapped) { 
		sessionDao.saveSampleMapped(sessionId, sampleMapped);  
	}
	
	@Override
	public void saveSelectedValueMap(String sessionId, Map<String, String> selectedValueMap) { 
		sessionDao.saveSelectedValueMap(sessionId, selectedValueMap); 
	}
	
	@Override
	public void saveSelectedResourceMap(String sessionId, List<Resource> selectedResourceMap) {
		sessionDao.saveSelectedResourceMap(sessionId, selectedResourceMap); 
	}
	
	@Override
	public PersistedSession createAndSaveSession(String entityId, String httpSessionId, String userId) throws GenericScheregException {
		if (httpSessionId==null) {
			throw new GenericScheregException("PersistedSession can only be created on a valid http session -> none provided");
		}
		PersistedSession session = new PersistedSession();
		session.setId(new ObjectId().toString());
		session.setHttpSessionId(httpSessionId);
		session.setUserId(userId);
		session.setEntityId(entityId);
		session.addLogEntry(LogEntry.createEntry(LogType.INFO, "~de.unibamberg.minf.dme.editor.sample.log.session_started", new String[]{session.getId()}));
		session.setCreated(DateTime.now());
		return this.saveSession(session);
	}

	@Override
	public PersistedSession reassignPersistedSession(String httpSessionId, String userId, String persistedSessionId) {
		PersistedSession s = sessionDao.findById(persistedSessionId);
		if (s!=null) {
			PersistedSession sCurrent = this.findSession(s.getEntityId(), httpSessionId, userId);
			if (sCurrent!=null) {
				// Loading 'current' session -> nothing to do really
				if (sCurrent.getId().equals(persistedSessionId)) {
					return sCurrent;
				}
				// Switching HTTP Session ID to remain consistent
				sCurrent.setHttpSessionId(s.getHttpSessionId());
				sessionDao.save(sCurrent);
			}
			s.setUserId(userId);
			s.setHttpSessionId(httpSessionId);
			return sessionDao.save(s);
		}
		return null;
	}

	@Override
	public PersistedSession saveSession(PersistedSession session) {
		session.setLastAccessed(DateTime.now());
		return sessionDao.save(session);
	}
	
	@Override
	public void deleteSession(String entityId, String httpSessionId, String userId) {
		this.deleteSession(this.findSession(entityId, httpSessionId, userId));
	}
	
	@Override
	public void deleteSession(PersistedSession session) {
		if (session!=null) {
			sessionDao.delete(session);
		}
	}
	
	@Override
	public void deleteSessions(List<PersistedSession> sessions) {
		sessionDao.delete(sessions);
	}
	
	private PersistedSession findLatest(String entityId, String userId) {
		// Only authenticated users can have old sessions
		if (userId==null) {
			return null;
		}		
		PersistedSession s = sessionDao.findOne(Query.query(Criteria.where("userId").is(userId).and("entityId").is(entityId)), Sort.by(Sort.Direction.DESC, "lastAccessed"));
		if (s==null) {
			return null;
		}
		return s;
	}
		
	private PersistedSession findSession(String entityId, String httpSessionId, String userId) {
		if (userId==null) {
			return sessionDao.findOne(Query.query(Criteria.where("httpSessionId").is(httpSessionId).and("entityId").is(entityId).and("userId").exists(false)));
		} else {
			return sessionDao.findOne(Query.query(Criteria.where("httpSessionId").is(httpSessionId).and("entityId").is(entityId).and("userId").is(userId)));
		}
	}

}