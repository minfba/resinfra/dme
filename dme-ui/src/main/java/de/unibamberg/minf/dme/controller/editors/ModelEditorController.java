package de.unibamberg.minf.dme.controller.editors;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import de.unibamberg.minf.dme.controller.base.BaseMainEditorController;
import de.unibamberg.minf.dme.exception.GenericScheregException;
import de.unibamberg.minf.dme.exporter.DatamodelExporter;
import de.unibamberg.minf.dme.importer.DatamodelImportWorker;
import de.unibamberg.minf.dme.importer.datamodel.DatamodelImporter;
import de.unibamberg.minf.dme.model.RightsContainer;
import de.unibamberg.minf.dme.model.base.Element;
import de.unibamberg.minf.dme.model.base.Identifiable;
import de.unibamberg.minf.dme.model.base.Label;
import de.unibamberg.minf.dme.model.base.ModelElement;
import de.unibamberg.minf.dme.model.base.Nonterminal;
import de.unibamberg.minf.dme.model.base.Terminal;
import de.unibamberg.minf.dme.model.datamodel.NonterminalImpl;
import de.unibamberg.minf.dme.model.datamodel.base.Datamodel;
import de.unibamberg.minf.dme.model.datamodel.base.DatamodelNature;
import de.unibamberg.minf.dme.model.datamodel.natures.XmlDatamodelNature;
import de.unibamberg.minf.dme.model.function.FunctionImpl;
import de.unibamberg.minf.dme.model.grammar.GrammarImpl;
import de.unibamberg.minf.dme.model.mapping.base.Mapping;
import de.unibamberg.minf.dme.model.serialization.DatamodelReferenceContainer;
import de.unibamberg.minf.dme.pojo.converter.AuthWrappedPojoConverter;
import de.unibamberg.minf.dme.pojo.converter.ModelElementPojoConverter;
import de.unibamberg.minf.dme.service.ElementServiceImpl;
import de.unibamberg.minf.dme.service.base.BaseEntityService;
import de.unibamberg.minf.dme.service.interfaces.IdentifiableService;
import eu.dariah.de.dariahsp.web.model.AuthPojo;
import de.unibamberg.minf.core.web.pojo.ModelActionPojo;
import de.unibamberg.minf.core.web.pojo.MessagePojo;

@Controller
@RequestMapping(value="/model/editor/{entityId}/")
public class ModelEditorController extends BaseMainEditorController implements InitializingBean {	
	@Autowired private DatamodelImportWorker importWorker;
	@Autowired private AuthWrappedPojoConverter authPojoConverter;
	
	@Autowired private IdentifiableService identifiableService;
	
	@Autowired private DatamodelExporter datamodelExporter;
	
	
	@Override protected String getPrefix() { return "/model/editor/"; }
	@Override protected DatamodelImportWorker getImportWorker() { return this.importWorker; }
	@Override protected BaseEntityService getMainEntityService() { return this.schemaService; }
	
	
	public ModelEditorController() {
		super("schemaEditor");
	}
			
	@GetMapping("/query/{query}")
	public @ResponseBody List<Identifiable> queryElements(@PathVariable String entityId, @PathVariable String query, @RequestParam(required=false) List<String> types, @RequestParam(required=false) List<String> excludeIds) {
		List<Class<?>> requestedClasses = new ArrayList<>();// = new Class<?>[] { Nonterminal.class, GrammarImpl.class }
		if (types==null) {
			requestedClasses.add(Nonterminal.class);
			requestedClasses.add(GrammarImpl.class);
		} else {
			if (types.contains("Nonterminal")) {
				requestedClasses.add(Nonterminal.class);
			}
			if (types.contains("Label")) {
				requestedClasses.add(Label.class);
			}
			if (types.contains("GrammarImpl")) {
				requestedClasses.add(GrammarImpl.class);
			}
			if (types.contains("FunctionImpl")) {
				requestedClasses.add(FunctionImpl.class);
			}
		}
		return identifiableService.findByNameAndSchemaId(query, entityId, requestedClasses.toArray(new Class<?>[0]), excludeIds);
	}
	
	@RequestMapping(method=GET, value="")
	public String getEditor(@PathVariable String entityId, Model model, @ModelAttribute String sample, Locale locale, HttpServletRequest request, HttpServletResponse response) throws IOException {
		AuthPojo auth = authInfoHelper.getAuth();
		RightsContainer<Datamodel> schema = schemaService.findByIdAndAuth(entityId, auth);
		if (schema==null) {
			return "redirect:/registry/";
		}		
		model.addAttribute("schema", authPojoConverter.convert(schema, auth.getUserId()));
		
		List<RightsContainer<Mapping>> mappings = mappingService.getMappings(entityId);
		model.addAttribute("mapped", mappings!=null && !mappings.isEmpty());
		
		return "schemaEditor";
	}
	
	@RequestMapping(method=GET, value={"/incl/editor"})
	public String getEditorIncl(@PathVariable String entityId, Model model, HttpServletRequest request) {
		AuthPojo auth = authInfoHelper.getAuth();
		RightsContainer<Datamodel> schema = schemaService.findByIdAndAuth(entityId, auth);
		
		model.addAttribute("datamodel", authPojoConverter.convert(schema, auth.getUserId()));
		
		return "schemaEditor/incl/editor";
	}
	
	@RequestMapping(method=GET, value={"/incl/properties"})
	public String getEntityProperties(@PathVariable String entityId, Model model, HttpServletRequest request) {
		AuthPojo auth = authInfoHelper.getAuth();
		RightsContainer<Datamodel> schema = schemaService.findByIdAndAuth(entityId, auth);
		
		model.addAttribute("datamodel", authPojoConverter.convert(schema, auth.getUserId()));
		
		return "schemaEditor/incl/properties";
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method=GET, value={"/forms/edit"})
	public String getEditForm(@PathVariable String entityId, Model model, Locale locale, HttpServletRequest request, HttpServletResponse response) {
		if (!schemaService.getUserCanWriteEntity(entityId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		RightsContainer<Datamodel> schema = schemaService.findByIdAndAuth(entityId, authInfoHelper.getAuth());
		model.addAttribute("actionPath", "/model/async/save");
		model.addAttribute("datamodelImpl", schema.getElement());
		model.addAttribute("readOnly", schema.isReadOnly());
		return "model/form/edit";
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method=GET, value={"/async/delete"}, produces = "application/json; charset=utf-8")
	public @ResponseBody ModelActionPojo deleteSchema(@PathVariable String entityId, HttpServletRequest request, HttpServletResponse response) {
		if (!schemaService.getUserCanWriteEntity(entityId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		ModelActionPojo result;
		if (entityId!=null && !entityId.isEmpty()) {
			schemaService.deleteSchemaById(entityId, authInfoHelper.getAuth());
			result = new ModelActionPojo(true);
		} else {
			result = new ModelActionPojo(false);
		}		
		return result;
	}
	
	
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method=GET, value={"/forms/import"})
	public String getImportForm(@PathVariable String entityId, @RequestParam(required=false) String elementId, Model model, Locale locale, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!schemaService.getUserCanWriteEntity(entityId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		model.addAttribute("actionPath", "/model/editor/" + entityId + "/async/import");
		model.addAttribute("schema", schemaService.findSchemaById(entityId));
		if (elementId!=null){
			model.addAttribute("elementId", elementId);
		}
		return "schemaEditor/form/import";
	}
	
	
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.GET, value = "/form/createRoot")
	public String getNewNonterminalForm(@PathVariable String entityId, Model model, Locale locale, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!schemaService.getUserCanWriteEntity(entityId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		model.addAttribute("element", new NonterminalImpl());
		model.addAttribute("availableTerminals", schemaService.getAvailableTerminals(entityId));
		model.addAttribute("actionPath", "/model/editor/" + entityId + "/async/saveNewRoot");
		return "elementEditor/form/edit_nonterminal";
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/async/saveNewRoot")
	public @ResponseBody ModelActionPojo saveNonterminal(@PathVariable String entityId, @Valid NonterminalImpl element, BindingResult bindingResult, Locale locale, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!schemaService.getUserCanWriteEntity(entityId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		ModelActionPojo result = this.getActionResult(bindingResult, locale);
		if (result.isSuccess()) {
			element.setEntityId(entityId);
			elementService.saveOrReplaceRoot(entityId, element, authInfoHelper.getAuth());
		}		
		return result;
	}
	
	
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method=POST, value={"/async/import"}, produces = "application/json; charset=utf-8")
	public @ResponseBody ModelActionPojo importSchemaElements(@PathVariable String entityId, @RequestParam(value="file.id") String fileId, @RequestParam(required=false, value="elementId") String elementId, 
			@RequestParam(value="schema_root_qn") String schemaRoot, @RequestParam(required=false, value="schema_root_type") String schemaRootType, @RequestParam(required=false, value="schema_root_id") String schemaRootId, 
			@RequestParam(defaultValue="false", value="keep-imported-ids") boolean keepImportedIds,			
			Locale locale, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!schemaService.getUserCanWriteEntity(entityId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		if (schemaRootId!=null && schemaRootId.isEmpty()) {
			schemaRootId = null;
		}
		
		ModelActionPojo result = new ModelActionPojo();
		try {
			if (temporaryFilesMap.containsKey(fileId)) {
				if (schemaRoot.isEmpty()) {
					result.setSuccess(false);
					result.addFieldError("schema_root", messageSource.getMessage("~de.unibamberg.minf.dme.notification.import.root_missing", null, locale));
					
					return result;
				}
				
				if (elementId!=null) {
					importWorker.importSubtree(temporaryFilesMap.remove(fileId), entityId, elementId, (schemaRootId!=null ? schemaRootId : schemaRoot), schemaRootType, keepImportedIds, authInfoHelper.getAuth());
				} else {
					Datamodel m = schemaService.findByIdAndAuth(entityId, auth).getElement();
					m.setNatures(null);
					
					elementService.clearElementTree(entityId, auth);
					
					schemaService.saveSchema(m, auth);
					
					importWorker.importSchema(temporaryFilesMap.remove(fileId), entityId, (schemaRootId!=null ? schemaRootId : schemaRoot), keepImportedIds, authInfoHelper.getAuth());
				}
				result.setSuccess(true);
				return result;
			}
		} catch (Exception e) {
			MessagePojo msg = new MessagePojo("danger", 
					messageSource.getMessage("~de.unibamberg.minf.common.view.forms.file.generalerror.head", null, locale), 
					messageSource.getMessage("~de.unibamberg.minf.common.view.forms.file.generalerror.body", new Object[] {e.getLocalizedMessage()}, locale));
			result.setMessage(msg);
		}
		result.setSuccess(false);
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/export")
	public @ResponseBody ModelActionPojo exportSchema(@PathVariable String entityId, Model model, HttpServletRequest request, Locale locale) {
		DatamodelReferenceContainer container = datamodelExporter.exportDatamodel(entityId, authInfoHelper.getAuth());
		if (container==null) {
			return new ModelActionPojo(false);
		}
		ModelActionPojo result = new ModelActionPojo(true);
		result.setPojo(container);
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/exportSubtree")
	public @ResponseBody ModelActionPojo exportSubtree(@PathVariable String entityId, @RequestParam String elementId, Model model, HttpServletRequest request, Locale locale) {
		AuthPojo auth = authInfoHelper.getAuth();
		ModelActionPojo result = new ModelActionPojo(true);
		result.setPojo(datamodelExporter.exportDatamodelSubtree(entityId, elementId, auth));
		return result;
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/getHierarchy")
	public @ResponseBody ModelActionPojo getHierarchy(@PathVariable String entityId, @RequestParam(defaultValue="false") boolean staticElementsOnly, @RequestParam(defaultValue="false") boolean collectNatureClasses,
			@RequestParam(defaultValue="logical_model", name="model") String modelClass, Model model, Locale locale, HttpServletRequest request, HttpServletResponse response) throws IOException, GenericScheregException {
		ModelActionPojo rPojo = new ModelActionPojo(true);
		AuthPojo auth = authInfoHelper.getAuth();
		Element result = elementService.findRootBySchemaId(entityId, true);
		if (result==null) {
			response.getWriter().print("null");
			response.setContentType("application/json");
			return null;
		}
		
		Map<String, List<String>> nonterminalNatureClassesMap = new HashMap<String, List<String>>();
		Datamodel m = schemaService.findByIdAndAuth(entityId, auth).getElement();
		if (modelClass.equals("logical_model")) {
			if (m.getNatures()!=null) {
				for (DatamodelNature n : m.getNatures()) {
					if (n.getNonterminalTerminalIdMap()!=null) {
						for (String nId : n.getNonterminalTerminalIdMap().keySet()) {
							List<String> natureClasses = nonterminalNatureClassesMap.get(nId);
							if (natureClasses==null) {
								natureClasses = new ArrayList<>();
							}
							natureClasses.add(n.getClass().getName());
							nonterminalNatureClassesMap.put(nId, natureClasses);
						}
					}
				}
			}
			rPojo.setPojo(ModelElementPojoConverter.convertModelElement(result, nonterminalNatureClassesMap, staticElementsOnly));
		} else {
			try {
				@SuppressWarnings("unchecked")
				Class<? extends DatamodelNature> modelClazz = (Class<? extends DatamodelNature>)Class.forName(modelClass);
				DatamodelNature n = schemaService.findByIdAndAuth(entityId, auth).getElement().getNature(modelClazz);
				
				rPojo.setPojo(ModelElementPojoConverter.convertModelElementTerminal(result, n));
			} catch (Exception e) {
				logger.error(String.format("Failed to retrieve model of class %s for datamodel %s", modelClass, entityId), e);
			}
		}
		
		ObjectNode statusNode = objectMapper.createObjectNode();
		boolean headers = false;
		Element processingRoot = ElementServiceImpl.findProcessingRoot(result);
		if (processingRoot==null || processingRoot.isIncludeHeader()) {
			headers = true;
		}
		statusNode.put("includeHeaders", headers);
		rPojo.setStatusInfo(statusNode);
		
		return rPojo;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/getTerminals")
	public @ResponseBody List<? extends Terminal> getTerminals(@PathVariable String entityId) {
		Datamodel s = schemaService.findSchemaById(entityId);
		
		if (s.getNature(XmlDatamodelNature.class)!=null) {
			return s.getNature(XmlDatamodelNature.class).getTerminals();
		}
		return null;
	}

	@Override
	protected ModelActionPojo validateImportedFile(String entityId, String fileId, String elementId, Locale locale) {
		JsonNode rootNodes;
		ModelActionPojo result = new ModelActionPojo();
		DatamodelImporter importer = importWorker.getSupportingImporter(temporaryFilesMap.get(fileId));
		
		if (importer==null) {
			MessagePojo msg = new MessagePojo("danger", 
					messageSource.getMessage("~de.unibamberg.minf.dme.notification.no_importer_matched.head", null, locale), 
					messageSource.getMessage("~de.unibamberg.minf.dme.notification.no_importer_matched.body", null, locale));
			result.setMessage(msg);
			return result;
		}
		
		if (elementId==null) {
			rootNodes = objectMapper.valueToTree(importer.getPossibleRootElements());
		} else {
			List<Class<? extends ModelElement>> allowedSubtreeRoots = identifiableService.getAllowedSubelementTypes(elementId);
			rootNodes = objectMapper.valueToTree(importer.getElementsByTypes(allowedSubtreeRoots));
		}
		
		if (!rootNodes.isEmpty()) {
			result.setSuccess(true);
			MessagePojo msg = new MessagePojo("success", 
					messageSource.getMessage("~de.unibamberg.minf.common.view.forms.file.validationsucceeded.head", null, locale), 
					messageSource.getMessage("~de.unibamberg.minf.common.view.forms.file.validationsucceeded.body", null, locale));
			result.setMessage(msg);
			
			ObjectNode pojoNode = objectMapper.createObjectNode();
			pojoNode.set("elements", rootNodes);
			pojoNode.set("keepIdsAllowed", BooleanNode.valueOf(importer.isKeepImportedIdsSupported()));
			pojoNode.set("importerMainType", TextNode.valueOf(importer.getMainImporterType()));
			pojoNode.set("importerSubtype", TextNode.valueOf(importer.getImporterSubtype()));
			
			result.setPojo(pojoNode);
		}
		return result;
	}
}