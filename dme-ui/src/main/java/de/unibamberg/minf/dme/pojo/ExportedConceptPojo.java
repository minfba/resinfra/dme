package de.unibamberg.minf.dme.pojo;

import de.unibamberg.minf.dme.model.base.Identifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ExportedConceptPojo extends RelatedConceptPojo implements Identifiable {
	private static final long serialVersionUID = 6500197642333612196L;
	
	private String format;
	private boolean includeTree;
	private boolean includeSelf;
	private boolean escape;
	private boolean useTerminalsIfAvailable;
}