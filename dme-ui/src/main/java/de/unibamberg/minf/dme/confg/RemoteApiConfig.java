package de.unibamberg.minf.dme.confg;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import de.unibamberg.minf.dme.confg.nested.RemoteTokensConfigProperties;
import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix="security")
public class RemoteApiConfig {
	private RemoteTokensConfigProperties[] remoteTokens;
}
