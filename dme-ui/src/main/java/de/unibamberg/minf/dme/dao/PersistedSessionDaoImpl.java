package de.unibamberg.minf.dme.dao;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.Charsets;
import org.apache.commons.io.FileUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.unibamberg.minf.core.util.InitializingObjectMapper;
import de.unibamberg.minf.dme.confg.MainConfig;
import de.unibamberg.minf.dme.dao.base.BaseDaoImpl;
import de.unibamberg.minf.dme.dao.interfaces.PersistedSessionDao;
import de.unibamberg.minf.dme.model.PersistedSession;
import de.unibamberg.minf.dme.model.SessionSampleFile;
import de.unibamberg.minf.dme.model.SessionSampleFile.FileTypes;
import de.unibamberg.minf.processing.model.SerializableResource;
import de.unibamberg.minf.processing.model.SerializableRootResource;
import de.unibamberg.minf.processing.model.base.Resource;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class PersistedSessionDaoImpl extends BaseDaoImpl<PersistedSession> implements PersistedSessionDao {
	@Autowired private ObjectMapper objectMapper;
	@Autowired private MainConfig mainConfig;
	
	// Strips IDs of temporary files and matches original filename components
	private static final Pattern FILENAME_PATTERN = Pattern.compile("^(?:(?:[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}_)|(?:([a-fA-F0-9]{24})))?(?:(.+)\\.(.+))|(.+)$");
	
	private static final String SAMPLE_OUTPUT = "output.json";;
	private static final String SAMPLE_MAPPED = "mapped.json";
	private static final String SAMPLE_SELECTED_VALUE_MAP = "valueMap.json";
	private static final String SAMPLE_SELECTED_RESOURCE_MAP = "resourceMap.json"; 
	private static final String SAMPLE_SESSION_DATA = "sessionData.json"; 
	
	private static final TypeReference<HashMap<String, String>> mapRef = new TypeReference<HashMap<String, String>>() {};
	private static final TypeReference<ArrayList<SerializableRootResource>> resourceListRef = new TypeReference<ArrayList<SerializableRootResource>>() {};
	private static final TypeReference<ArrayList<SerializableResource>> resListRef = new TypeReference<ArrayList<SerializableResource>>() {};
	
	/**
	 * Helper class for JSON serializing resources
	 */
	private class ResourceList extends ArrayList<Resource> {
		private static final long serialVersionUID = 4998781729252376207L;
		public ResourceList(List<Resource> sampleOutput) {
			super(sampleOutput);
		}
	}

	public PersistedSessionDaoImpl() {
		super(PersistedSession.class, "persistedSession");
	}
	
	@Override
	public PersistedSession findById(String id) {
		return super.findById(id);
	}

	@Override
	public PersistedSession findOne(Query q) {
		return super.findOne(q);
	}

	@Override
	public PersistedSession findOne(Query q, Sort sort) {
		return super.findOne(q, sort);
	}
		
	@Override
	public <S extends PersistedSession> S save(S entity) {
		if (entity.getId()==null) {
			entity.setId(new ObjectId().toString());
		}
		//this.saveData(entity);
		
		/*PersistedSession saveSession = new PersistedSession();
		saveSession.setCreated(entity.getCreated());
		saveSession.setEntityId(entity.getEntityId());
		saveSession.setHttpSessionId(entity.getHttpSessionId());
		saveSession.setId(entity.getId());
		saveSession.setLabel(entity.getLabel());
		saveSession.setLastAccessed(entity.getLastAccessed());
		saveSession.setNotExpiring(entity.isNotExpiring());
		saveSession.setSelectedOutputIndex(entity.getSelectedOutputIndex());
		saveSession.setUserId(entity.getUserId());
		saveSession.setSampleFile(entity.getSampleFile());
*/
		super.save(entity);
		
		//entity.setId(saveSession.getId());
		return entity;
	}

	@Override
	public void delete(String id) {
		this.deleteData(id);
		super.delete(id);
	}

	@Override
	public void delete(PersistedSession entity) {
		if (entity.getId()!=null) {
			this.deleteData(entity.getId());
		}
		super.delete(entity);
	}

	@Override
	public int delete(Iterable<? extends PersistedSession> entities) {
		for (PersistedSession entity : entities) {
			if (entity.getId()!=null) {
				this.deleteData(entity.getId());
			}
		}
		return super.delete(entities);
	}

	@Override
	public long delete(Collection<String> ids) {
		for (String id : ids) {
			this.deleteData(id);
		}
		return super.delete(ids);
	}
	
	@Override
	public ArrayList<SerializableRootResource> loadSampleOutput(String sessionId) { 
		return this.readDataFile(sessionId, SAMPLE_OUTPUT, resourceListRef);
	}
	
	@Override
	public ArrayList<SerializableRootResource> loadSampleMapped(String sessionId) { 
		return this.readDataFile(sessionId, SAMPLE_MAPPED, resourceListRef); 
	}
	
	@Override
	public Map<String, String> loadSelectedValueMap(String sessionId) { 
		return this.readDataFile(sessionId, SAMPLE_SELECTED_VALUE_MAP, mapRef);
	}
	
	@Override
	public List<SerializableResource> loadSelectedResourceMap(String sessionId) { 
		return this.readDataFile(sessionId, SAMPLE_SELECTED_RESOURCE_MAP, resListRef);
	}
	
	@Override
	public String loadSessionData(String sessionId) {
		return this.readDataFile(sessionId, SAMPLE_SESSION_DATA);
	}
	
	@Override
	public void saveSampleOutput(String sessionId, List<Resource> sampleOutput) {
		this.saveDataFile(sessionId, sampleOutput, SAMPLE_OUTPUT); 
	}
	
	@Override
	public void saveSampleMapped(String sessionId, List<Resource> sampleMapped) { 
		this.saveDataFile(sessionId, sampleMapped, SAMPLE_MAPPED);  
	}
	
	@Override
	public void saveSelectedValueMap(String sessionId, Map<String, String> selectedValueMap) { 
		this.saveDataFile(sessionId, selectedValueMap, SAMPLE_SELECTED_VALUE_MAP); 
	}
	
	@Override
	public void saveSelectedResourceMap(String sessionId, List<Resource> selectedResourceMap) { 
		this.saveDataFile(sessionId, selectedResourceMap, SAMPLE_SELECTED_RESOURCE_MAP); 
	}
	
	@Override
	public void saveSessionData(String sessionId, JsonNode json) {
		this.saveDataFile(sessionId, json, SAMPLE_SESSION_DATA, true);
	}
	
	@Override
	public void removeSessionData(String sessionId) {
		this.removeDataFile(sessionId, SAMPLE_SESSION_DATA);
	}
	
	private File getSessionFile(String sessionId, String file) {
		StringBuilder sessionDataPathBuilder = new StringBuilder();
		sessionDataPathBuilder.append(this.mainConfig.getPaths().getSessionData()).append(File.separator).append(sessionId);
		if (file!=null) {
			sessionDataPathBuilder.append(File.separator).append(file);
		}
		return new File(sessionDataPathBuilder.toString());
	}
	
	private void removeDataFile(String sessionId, String file) {
		File f = this.getSessionFile(sessionId, file);
		if (f.exists()) {
			FileUtils.deleteQuietly(f);
		}		
	}
	
	private void saveDataFile(String sessionId, Object o, String file) {
	this.saveDataFile(sessionId, o, file, false);	
	}
	
	private void saveDataFile(String sessionId, Object o, String file, boolean pretty) {
		File f = this.getSessionFile(sessionId, file);
		if (o==null) {
			if (f.exists()) {
				FileUtils.deleteQuietly(f);
			}
			return;
		}
		
		try {
			if (!Files.exists(f.getParentFile().toPath())) {
				Files.createDirectories(f.getParentFile().toPath());
			}
			String outJson;
			if (pretty) {
				outJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);
			} else {
				outJson = objectMapper.writeValueAsString(o);
			}
			FileUtils.writeStringToFile(f, outJson, StandardCharsets.UTF_8, false);
		} catch (IOException e) {
			log.error(String.format("Failed to save data file [%s]", f.getAbsolutePath()), e);
		}
	}
	
	private <T> T readDataFile(String sessionId, String file, TypeReference<T> typeRef) {
		try {
			String inJson = this.readDataFile(sessionId, file);
			if (inJson!=null) {
				return objectMapper.readValue(inJson, typeRef);
			}
		} catch (Exception e) {
			log.error("Failed to parse JSON data", e);
		}
		return null;
	}
	
	private String readDataFile(String sessionId, String file) {
		File inFile = this.getSessionFile(sessionId, file);
		
		try {
			if (inFile.exists()) {
				return FileUtils.readFileToString(inFile, StandardCharsets.UTF_8);
			}
		} catch (IOException e) {
			log.error(String.format("Failed to load data file [%s]", inFile.getAbsolutePath()), e);
		}
		return null;
	}
	
	@Override
	public SessionSampleFile saveAsSessionInputFile(File file, FileTypes type, String sessionId) {
		return this.innerSaveSessionInputFile(null, file, type, sessionId);
	}
	
	@Override
	public SessionSampleFile saveAsSessionInputFile(String string, FileTypes type, String sessionId) {
		return this.innerSaveSessionInputFile(string, null, type, sessionId);
	}
	
	@Override
	public PersistedSession updateSessionFileType(String sessionId, FileTypes fileType)  {
		PersistedSession session = this.findById(sessionId);
		SessionSampleFile file = session.getSampleFile();
		if (file!=null) {		
			try {
				String newPath = file.getPath().substring(0, file.getPath().lastIndexOf('.')) + "." + fileType.toString().toLowerCase();
				
				File targetFile = new File(newPath);
				if (targetFile.exists()) {
					FileUtils.forceDelete(targetFile);
				}			
				FileUtils.moveFile(new File(file.getPath()), targetFile);
				
				file.setType(fileType);
				file.setPath(newPath);
				
				this.save(session);
			} catch (Exception e) {
				log.error("Failed to update session file type", e);
			}
		}
		return session;
	}
	
	private SessionSampleFile innerSaveSessionInputFile(String input, File inputFile, FileTypes type, String sessionId) {
		try {
			Assert.isTrue(input!=null ^ inputFile!=null);
			
			Path sessionDirectoryPath = this.getSessionFile(sessionId, null).toPath();
			if (!Files.exists(sessionDirectoryPath)) {
				FileUtils.forceMkdir(sessionDirectoryPath.toFile());
			}
			
			File targetFile;
			if (input!=null) {
				targetFile = new File(sessionDirectoryPath + File.separator + "uploaded_sample." + type.toString().toLowerCase());
				FileUtils.writeStringToFile(targetFile, input, Charsets.UTF_8, false);
			} else {
				String fileName = inputFile.getName();
				Matcher filenameMatcher = FILENAME_PATTERN.matcher(inputFile.getName());
				if (filenameMatcher.matches()) {
					if (filenameMatcher.group(2)!=null) {
						fileName = filenameMatcher.group(2) + "." + filenameMatcher.group(3);
					} else if (filenameMatcher.group(4)!=null) {
						fileName = filenameMatcher.group(4);
					}
				}
				targetFile = new File(sessionDirectoryPath + File.separator + fileName);
				FileUtils.copyFile(inputFile, targetFile);
			}
			
			SessionSampleFile result = new SessionSampleFile();
			result.setFileCount(1);
			result.setPath(targetFile.getAbsolutePath());
			result.setType(type);
			
			return result;
		} catch (Exception e) {
			log.error("Failed to save session input file", e);
		}
		return null;
	}
	
	private void deleteData(String sessionId) {
		try {
			Path sessionDirectoryPath = this.getSessionFile(sessionId, null).toPath();
			if (Files.exists(sessionDirectoryPath)) {
				FileUtils.deleteDirectory(sessionDirectoryPath.toFile());
			}
		} catch (Exception e) {
			log.error("Failed to delete session data", e);
		}
	}
}
