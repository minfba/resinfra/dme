package de.unibamberg.minf.dme.confg.nested;

import lombok.Data;

@Data
public class PathsConfigProperties {
	private String main;
	private String config;
	private String grammars;
	private String models;	
	private String temporary;
	private String sessionData;
	private String tmpUpload;
	private String sampleFiles;
	private String parseErrors;
	private String download;
	private String backup;
}