package de.unibamberg.minf.dme.model;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.util.Assert;

import com.fasterxml.jackson.databind.JsonNode;

import de.unibamberg.minf.gtf.context.ExecutionContext;

public class SessionExecutionContext implements ExecutionContext {
	public static final String SESSION_EXECUTION_DATA_FOLDERNAME = "session_execution_data";
	
	private final String pathPrefix;
	private final String sessionId;
	private final String workingDir;
	private final JsonNode sessionData;
	
	public String getPathPrefix() { return pathPrefix; }
	public String getSessionId() { return sessionId; }
	@Override public String getWorkingDir() { return this.workingDir; }
	@Override public JsonNode getSessionData() { return this.sessionData; }
	
	public SessionExecutionContext(String pathPrefix, String sessionId, JsonNode sessionData) throws IOException {
		Assert.notNull(pathPrefix);
		Assert.notNull(sessionId);
		this.pathPrefix = pathPrefix;
		this.sessionId = sessionId;
		this.workingDir = pathPrefix + File.separator + this.sessionId + File.separator + SESSION_EXECUTION_DATA_FOLDERNAME + File.separator;
		this.sessionData = sessionData;
		
		File workingDir = new File(this.workingDir); 
		if (!workingDir.exists()) {
			FileUtils.forceMkdir(new File(this.workingDir));
		}
	}
}