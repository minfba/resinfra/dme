package de.unibamberg.minf.dme.confg.nested;

import lombok.Data;

@Data
public class ThumbnailsConfigProperties {
	private int width = 150;
	private int height = 150;
}