package de.unibamberg.minf.dme.confg;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "debug")
public class DebugConfig {
	private boolean processing;
	private int samplesMaxTravelSize = 10000;	// Maximum size in bytes of uploaded sample data
}
