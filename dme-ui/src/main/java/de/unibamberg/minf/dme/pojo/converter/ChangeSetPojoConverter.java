package de.unibamberg.minf.dme.pojo.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.dme.dao.interfaces.UserDao;
import de.unibamberg.minf.dme.model.tracking.Change;
import de.unibamberg.minf.dme.model.tracking.ChangeSet;
import de.unibamberg.minf.dme.model.tracking.ChangeType;
import de.unibamberg.minf.dme.pojo.ChangeSetPojo;
import eu.dariah.de.dariahsp.model.User;

@Component
public class ChangeSetPojoConverter {	
	@Autowired private UserDao userDetailsDao;
	
	public Collection<ChangeSetPojo> convert(List<ChangeSet> changeSets, Locale locale) {
		Map<String, ChangeSetPojo> bySessionMap = new HashMap<>();
		ChangeSetPojo cPojo;
		if (changeSets!=null) {
			for (ChangeSet c : changeSets) {
				if (bySessionMap.containsKey(c.getSessionId())) {
					cPojo = bySessionMap.get(c.getSessionId());
					if (c.getTimestamp()!=null && (cPojo.getTimestamp()==null || c.getTimestamp().isAfter(cPojo.getTimestamp()))) {
						cPojo.setTimestamp(c.getTimestamp());
					}
				} else {
					cPojo = new ChangeSetPojo();
					if (c.getUserId()!=null) {
						User ud = userDetailsDao.findById(c.getUserId());
						if (ud!=null) {
							cPojo.setUser(ud.getUsername());
						}
					}
					if (cPojo.getUser()==null) {
						cPojo.setUser(c.getUserId());
					}
					cPojo.setTimestamp(c.getTimestamp());
				}
				if (cPojo.getChanges()==null) {
					cPojo.setChanges(new HashMap<>());
				}
				
				if (cPojo.getTimestamp()!=null) {
					cPojo.setTimestampString(cPojo.getTimestamp().toString(DateTimeFormat.patternForStyle("M-", locale)));
				}
				
				List<Change> elementChanges = new ArrayList<>();
				if (cPojo.getChanges().containsKey(c.getElementId())) {
					elementChanges.addAll(cPojo.getChanges().get(c.getElementId()));
				}
				
				for (Change newC : c.getChanges()) {
					elementChanges.add(newC);
					if (newC.getChangeType()==ChangeType.EDIT_VALUE) {
						cPojo.setEdits(cPojo.getEdits()+1);
					} else if (newC.getChangeType()==ChangeType.NEW_OBJECT) {
						cPojo.setNews(cPojo.getNews()+1);
					} else if (newC.getChangeType()==ChangeType.DELETE_OBJECT) {
						cPojo.setDeletes(cPojo.getDeletes()+1);
					}
				}
				cPojo.getChanges().put(c.getElementId(), c.getChanges());
				
				bySessionMap.put(c.getSessionId(), cPojo);
			}
		}
		return bySessionMap.values();
	}
}
