package de.unibamberg.minf.dme.confg;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import ch.qos.logback.core.status.InfoStatus;
import ch.qos.logback.core.status.StatusManager;
import ch.qos.logback.core.util.FileSize;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@Configuration
@ConfigurationProperties(prefix="log")
public class LogConfig {
	@Autowired private MainConfig mainConfig;
	
	private String logFile = "dme.log";
	private String oldlogSuffix = "-%d{yyyy-MM-dd-HH-mm}-%i.log.gz";
	private String dir;
	private String pattern = "%d [%thread] %-5level %logger{36}[%line] - %msg%n";
	private int maxHistory = 90;
	private String totalSizeCap = "1GB";
	private String maxFileSize = "10MB";
	
	private boolean logQueries;
	
	
	@PostConstruct
    public void reconfigureLogging() throws IOException {
		log.debug("Reconfiguring logging...");

		LoggerContext lc = (LoggerContext) org.slf4j.LoggerFactory.getILoggerFactory();
		StatusManager statusManager = lc.getStatusManager();
        if (statusManager != null) {
            statusManager.add(new InfoStatus("Configuring logger", lc));
        }
     
        Logger rootLogger = lc.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        // RollingFileAppender
        if (this.getDir()!=null) {
        	rootLogger.addAppender(this.configureRollingFileAppender(lc));
        }       
        this.logAppenderConfiguration(rootLogger);        
	}
	
	private void logAppenderConfiguration(Logger rootLogger) {
		log.info("Logging reconfigured");
		Iterator<Appender<ILoggingEvent>> appendersIterator = rootLogger.iteratorForAppenders();
        Appender<ILoggingEvent> configuredAppender;
        while(appendersIterator.hasNext()) {
        	configuredAppender = appendersIterator.next();
        	log.info("Configured appender: {}", configuredAppender.getClass().getName());
        }
	}
	
	private Appender<ILoggingEvent> configureRollingFileAppender(Context context) throws IOException {
		RollingFileAppender<ILoggingEvent> appender = new RollingFileAppender<>();
		appender.setName("FILE");
		appender.setContext(context);
		appender.setFile(this.setupLogFile(this.getDir(), this.getLogFile()));

		TimeBasedRollingPolicy<ILoggingEvent> policy = new TimeBasedRollingPolicy<>();
		policy.setContext(context);
		policy.setMaxHistory(this.getMaxHistory());
		policy.setFileNamePattern(this.setupLogFile(this.getDir(), this.getLogFile() + this.getOldlogSuffix()));
		policy.setParent(appender);
		policy.start();

		SizeAndTimeBasedFNATP<ILoggingEvent> innerpolicy = new SizeAndTimeBasedFNATP<>();
		innerpolicy.setContext(context);
		innerpolicy.setMaxFileSize(FileSize.valueOf(this.getMaxFileSize()));
		innerpolicy.setTimeBasedRollingPolicy(policy);
		innerpolicy.start();

		policy.setTimeBasedFileNamingAndTriggeringPolicy(innerpolicy);
		policy.start();

		appender.setRollingPolicy(policy);

		PatternLayoutEncoder pl = new PatternLayoutEncoder();
		pl.setContext(context);
		pl.setPattern(this.getPattern());
		pl.start();
		appender.setEncoder(pl);

		log.info("RollingFileAppender logging to file: {}", appender.getFile());
		
		appender.start();
		
		return appender;
	}
	
	private String setupLogFile(String first, String... more) throws IOException {
		Path p = Paths.get(first, more);
		if (!Files.exists(p.getParent())) {
			Files.createDirectories(p.getParent());
		}
		return p.toString();
	}
}
