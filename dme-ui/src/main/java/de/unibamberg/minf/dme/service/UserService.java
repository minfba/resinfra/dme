package de.unibamberg.minf.dme.service;

import java.util.List;

import eu.dariah.de.dariahsp.model.User;

public interface UserService {
	public User findById(String id);
	public List<String> getUsernames(String query);
}
