package de.unibamberg.minf.dme.confg.nested;

import lombok.Data;

@Data
public class NlpConfigProperties {
	private String language;
	private String models;
	private String taggerModel;
	private String lexParseModel;
	private String classifierModel;
}