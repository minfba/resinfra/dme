package de.unibamberg.minf.dme.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.unibamberg.minf.dme.model.base.BaseIdentifiable;
import de.unibamberg.minf.processing.model.base.Resource;

/**
 * Persisted session
 *  
 * 
 * @author tobias
 */
public class PersistedSession extends BaseIdentifiable implements Comparable<PersistedSession> {
	private static final long serialVersionUID = 6949914982830008951L;
	
	private String httpSessionId;
	private String entityId;
	private String userId;
	private String label;
	private DateTime lastAccessed;
	private DateTime created;
	private boolean notExpiring;
	private SessionSampleFile sampleFile;
	private int selectedOutputIndex;
	
	private int sampleMappedCount;
	
	/*private List<Resource> sampleOutput;
	private List<Resource> sampleMapped;
	private Map<String, String> selectedValueMap;*/
	private List<LogEntry> sessionLog;
	
	
	public String getHttpSessionId() { return httpSessionId; }
	public void setHttpSessionId(String httpSessionId) { this.httpSessionId = httpSessionId; }

	public String getEntityId() { return entityId; }
	public void setEntityId(String entityId) { this.entityId = entityId; }
	
	public String getUserId() { return userId; }
	public void setUserId(String userId) { this.userId = userId; }
	
	public String getLabel() { return label; }
	public void setLabel(String label) { this.label = label; }
	
	public DateTime getLastAccessed() { return lastAccessed; }
	public void setLastAccessed(DateTime lastAccessed) { this.lastAccessed = lastAccessed; }
	
	public DateTime getCreated() { return created; }
	public void setCreated(DateTime created) { this.created = created; }
	
	public boolean isNotExpiring() { return notExpiring; }
	public void setNotExpiring(boolean notExpiring) { this.notExpiring = notExpiring; }
		
	public int getSelectedOutputIndex() { return selectedOutputIndex; }
	public void setSelectedOutputIndex(int selectedOutputIndex) { this.selectedOutputIndex = selectedOutputIndex; }

	public List<LogEntry> getSessionLog() { return sessionLog; }
	public void setSessionLog(List<LogEntry> sessionLog) { this.sessionLog = sessionLog; }
	
	public SessionSampleFile getSampleFile() { return sampleFile; }
	public void setSampleFile(SessionSampleFile sampleFile) { this.sampleFile = sampleFile; }
	
	public int getSampleMappedCount() { return sampleMappedCount; }
	public void setSampleMappedCount(int sampleMappedCount) { this.sampleMappedCount = sampleMappedCount; }
	
	
	@Transient
	@JsonIgnore
	public boolean hasData() {
		return (sampleFile!=null && sampleFile.getPath()!=null && new File(sampleFile.getPath()).exists());
	}
	
	/*public List<LogEntry> getSortedSessionLog() {
		List<LogEntry> result = this.getSessionLog();
		if (result!=null) {
			Collections.sort(result);
			Collections.reverse(result);
		}
		return result;
	}*/
	
	public void addLogEntry(LogEntry entry) {
		if (this.getSessionLog()==null) {
			this.setSessionLog(new ArrayList<LogEntry>());
		}
		this.getSessionLog().add(0, entry);
	}
	
	@Override
	public int compareTo(PersistedSession arg0) {
		return this.lastAccessed.compareTo(arg0.getLastAccessed());
	} 
	
	
}