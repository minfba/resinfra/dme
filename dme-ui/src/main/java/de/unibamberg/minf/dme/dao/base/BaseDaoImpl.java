package de.unibamberg.minf.dme.dao.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import de.unibamberg.minf.dme.model.base.Identifiable;
import lombok.Getter;

public abstract class BaseDaoImpl<T extends Identifiable> implements BaseDao<T> {	
	protected static final String ID_FIELD = "_id";
	public static final String ENTITY_ID_FIELD = "entityId";
	
	@Getter protected final String collectionName;
	@Getter protected final Class<T> clazz;
	
	@Autowired public MongoTemplate mongoTemplate;
	
	protected BaseDaoImpl(Class<T> clazz) {
		this.clazz = clazz;
		this.collectionName = clazz.getSimpleName().toLowerCase();
	}
	
	protected BaseDaoImpl(Class<T> clazz, String collectionName) {
		this.clazz = clazz;
		this.collectionName = collectionName;
	}

	@Override
	public long count(Query q) {
		return this.mongoTemplate.count(q, this.clazz, collectionName);
	}
	
	@Override
	public List<T> find(Query q) {
		return mongoTemplate.find(q, this.clazz, this.getCollectionName());
	}
	
	@Override
	public List<T> findAll() {
		return mongoTemplate.findAll(clazz, collectionName);
	}

	@Override
	public List<T> findByPropertyValue(String property, Object value) {
		Query q = new Query();
		q.addCriteria(Criteria.where(property).is(value));
		return mongoTemplate.find(q, this.clazz, collectionName);
	}

	public T findOne(Query q) {
		return mongoTemplate.findOne(q, this.clazz, collectionName);
	}
	
	@Override
	public T findOne(Query q, Sort sort) {
		q.with(sort);
		return mongoTemplate.findOne(q, clazz, this.getCollectionName());		
	}

	
	@Override
	public List<T> findByQuery(Query q) {
		return mongoTemplate.find(q, this.clazz, collectionName);
	}
	
	@Override
	public T findById(String id) {
		return mongoTemplate.findById(id, clazz, collectionName);
	}

	@Override
	public T findByPropertyValueDistinct(String property, Object value) {
		Query q = new Query();
		q.addCriteria(Criteria.where(property).is(value));
		return mongoTemplate.findOne(q, this.clazz, collectionName);
	}

	@Override
	public <S extends T> S save(S entity) {
		if (entity.getId()!=null && entity.getId().isEmpty()) {
			entity.setId(null);
		}
		mongoTemplate.save(entity, this.collectionName);
		return entity;
	}

	
	@Override
	public int delete(Iterable<? extends T> entities) {
		int count = 0;
		for (T e : entities) {
			count += mongoTemplate.remove(e).wasAcknowledged() ? 1 : 0;
		}
		return count;
	}

	@Override
	public int delete(List<String> ids) {
		return mongoTemplate.findAllAndRemove(new Query(Criteria.where(ID_FIELD).in(ids)), clazz, collectionName).size();
	}

	@Override
	public void delete(T entity) {
		mongoTemplate.remove(entity, collectionName);
	}

	@Override
	public void delete(String id) {
		mongoTemplate.findAllAndRemove(new Query(Criteria.where(ID_FIELD).is(id)), clazz, collectionName);
	}
	
	@Override
	public long delete(Collection<String> ids) {
		return mongoTemplate.findAllAndRemove(new Query(Criteria.where("_id").in(ids)), clazz, this.getCollectionName()).size();
	}
	
	public static boolean isNewId(String id) {
		return id==null || id.equals("") || id.equals("undefined");
	}
	
	public static boolean isValidObjectId(String id) {
		return (id!=null && ObjectId.isValid(id)); 
	}
		
	public static String createNewObjectId() {
		return new ObjectId().toString();
	}
	
	@Override
	public List<T> combineQueryResults(Criteria[] criteria, int limit) {
		Query q;
		List<T> result = new ArrayList<T>();
		List<T> innerResult;

		for (Criteria cr : criteria) {
			q = new Query();
			q.addCriteria(cr);
			
			q.limit(result.size() + limit); // Could overlap
			innerResult = this.findByQuery(q);
			if (innerResult!=null && innerResult.size()>0) {
				for (T t : innerResult) {
					boolean contains = false;
					for (T tX : result) {
						if (t.getId().equals(tX.getId())) {
							contains = true;
							break;
						}
					}
					if (!contains) {
						result.add(t);
					}
				}
				if (result.size()>=limit) {
					innerResult = result;
					result = new ArrayList<T>(limit);
					for (int i=0; i<limit; i++) {
						result.add(innerResult.get(i));
					}
					return result;
				}
			}
			
		}
		return result;
	}
}
