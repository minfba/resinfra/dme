package de.unibamberg.minf.dme.dao.base;

import java.util.Collection;
import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.domain.Sort;

import de.unibamberg.minf.dme.model.base.Identifiable;

public interface BaseDao<T extends Identifiable> {
	
	public String getCollectionName();
	public Class<T> getClazz();
	
	public List<T> findAll();
	public List<T> findByPropertyValue(String property, Object value);
	
	public T findById(String id);
	public T findByPropertyValueDistinct(String property, Object value);
	
	
	public <S extends T> S save(S entity);

	int delete(Iterable<? extends T> entities);
	int delete(List<String> ids);
	
	void delete(T entity);
	void delete(String id);
	//String getNewObjectId();
	
	public List<T> findByQuery(Query q);
	public List<T> find(Query q);
	
	public long count(Query q);
	
	public long delete(Collection<String> ids);
	
	/* Old stuff
	public List<T> findAll();
	public List<T> findAll(Sort sort);
	public T findById(String id);*/
	///public List<T> find(Query q);
	/*public T findOne(Query q);*/
	public T findOne(Query q);
	public T findOne(Query q, Sort sort);
	

	/*
	public <S extends T> S save(S entity);

	
	public void delete(String id);
	public void delete(T entity);
	public void delete(Iterable<? extends T> entities);
	public long delete(Collection<String> id);
*/
	public List<T> combineQueryResults(Criteria[] criteria, int limit);
}