package de.unibamberg.minf.dme.service.base;

import eu.dariah.de.dariahsp.web.model.AuthPojo;

public interface BaseReferenceService extends BaseService {
	void moveReference(String entityId, String elementId, Class<?> elementType, int delta, AuthPojo auth);
}
