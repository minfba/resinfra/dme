package de.unibamberg.minf.dme.service.interfaces;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;

import de.unibamberg.minf.dme.exception.GenericScheregException;
import de.unibamberg.minf.dme.model.PersistedSession;
import de.unibamberg.minf.dme.model.SessionSampleFile;
import de.unibamberg.minf.dme.model.SessionSampleFile.FileTypes;
import de.unibamberg.minf.processing.model.SerializableResource;
import de.unibamberg.minf.processing.model.SerializableRootResource;
import de.unibamberg.minf.processing.model.base.Resource;

public interface PersistedSessionService {
	public List<PersistedSession> findAllByUser(String entityId, String userId);
	public List<PersistedSession> findExpiredSessions(DateTime cutoffTimestamp);
	
	public PersistedSession get(String entityId, String httpSessionId, String userId);
	public PersistedSession accessOrCreate(String entityId, String httpSessionId, String userId) throws GenericScheregException;
	
	public PersistedSession reassignPersistedSession(String httpSessionId, String userId, String persistedSessionId);
	public PersistedSession saveSession(PersistedSession session);
	
	public void deleteSession(String entityId, String httpSessionId, String userId);
	public void deleteSession(PersistedSession session);
	public void deleteSessions(List<PersistedSession> sessions);
	public PersistedSession createAndSaveSession(String entityId, String httpSessionId, String userId) throws GenericScheregException;	

	public String getSampleInputValue(PersistedSession s, String elementId);
	public SessionSampleFile saveAsSessionInputFile(File file, FileTypes type, String sessionId);
	public SessionSampleFile saveAsSessionInputFile(String sample, FileTypes type, String sessionId);
	
	public void saveSessionData(String sessionData, String sessionId) throws JsonMappingException, JsonProcessingException;
	public JsonNode loadSessionData(String sessionId);
	
	public PersistedSession setSampleFileType(PersistedSession session, FileTypes fileType);
	
	public List<SerializableRootResource> loadSampleOutput(String sessionId);
	public List<SerializableRootResource> loadSampleMapped(String sessionId);
	public Map<String, String> loadSelectedValueMap(String sessionId);
	public void saveSampleOutput(String sessionId, List<Resource> sampleOutput);
	public void saveSampleMapped(String sessionId, List<Resource> sampleMapped);
	public void saveSelectedValueMap(String sessionId, Map<String, String> selectedValueMap);
	
	public void saveSelectedResourceMap(String id, List<Resource> resourceMap);
	public List<SerializableResource> loadSelectedResourceMap(String sessionId);
	public List<SerializableResource> getSelectedResources(PersistedSession s, String elementId);
}