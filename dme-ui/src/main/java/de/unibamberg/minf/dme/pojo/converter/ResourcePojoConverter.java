package de.unibamberg.minf.dme.pojo.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.unibamberg.minf.processing.model.SerializableResource;
import de.unibamberg.minf.processing.model.SerializableRootResource;
import de.unibamberg.minf.processing.model.base.Resource;

public class ResourcePojoConverter {
	private static ObjectMapper objectMapper = new ObjectMapper();
	
	public static JsonNode convertResource(Resource r) {
		if (r==null) {
			return NullNode.getInstance();
		}
		ObjectNode rJson = objectMapper.createObjectNode();
		if (SerializableRootResource.class.isAssignableFrom(r.getClass())) {
			SerializableRootResource rRoot = (SerializableRootResource)r;
			if (rRoot.getMetaChildResources()!=null && !rRoot.getMetaChildResources().isEmpty()) {
				rJson.set("_metadata", convertChildResource(r, rRoot.getMetaChildResources()));
			}
		}
		rJson.set(r.getKey(), convertChildResource(r, r.getChildResources()));
		return rJson;
	}
	
	public static JsonNode convertChildResource(Resource r, List<Resource> children) {
		if (r==null) {
			return null;
		}
		ObjectNode rJson = objectMapper.createObjectNode(); 
		if (r.getValue()!=null) {
			if (r.getValue() instanceof Collection<?> || r.getValue().getClass().isArray()) {
				ArrayNode a = objectMapper.createArrayNode();
				for (Object o : ((Collection)r.getValue())) {
					a.add(o.toString());					
				}
				rJson.set(SerializableResource.SELF_KEY, a);
			} else {
				rJson.put(SerializableResource.SELF_KEY, escapeValue(r.getValue()));
			}
			
		}		
		if (children!=null && !children.isEmpty()) {
			Map<String, JsonNode> childMap = new LinkedHashMap<>();	
			JsonNode childNode;
			ArrayNode childArrayNode;
			
			// Collect and group child nodes
			for (Resource rChild : children) {
				if (childMap.containsKey(rChild.getKey())) {
					childNode = childMap.get(rChild.getKey());
					if (ArrayNode.class.isAssignableFrom(childNode.getClass())) {
						childArrayNode = (ArrayNode)childNode; 
					} else {
						childArrayNode = objectMapper.createArrayNode();
						childArrayNode.add(childNode);
						childMap.put(rChild.getKey(), childArrayNode);
					}
					childArrayNode.add(convertChildResource(rChild, rChild.getChildResources()));
				} else {
					childMap.put(rChild.getKey(), convertChildResource(rChild, rChild.getChildResources()));
				}
			}
			
			// Append child nodes to json node
			for (String childKey : childMap.keySet()) {
				rJson.set(childKey, childMap.get(childKey));
			}
		}
		return rJson;
	}
	
	private static String escapeValue(Object value) {
		return (value==null ? null : StringEscapeUtils.escapeHtml4(
				value.toString()
					.replace("\n", "\\n")
					.replace("\t", "\\t")
					.replace("\r", "\\r")
				));
	}
}
