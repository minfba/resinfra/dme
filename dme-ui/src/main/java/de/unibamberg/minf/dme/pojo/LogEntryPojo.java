package de.unibamberg.minf.dme.pojo;

import de.unibamberg.minf.dme.model.LogEntry.LogType;

public class LogEntryPojo {
	private String timestamp;
	private LogType logType;
	private String message;
	
	
	public String getTimestamp() { return timestamp; }
	public void setTimestamp(String timestamp) { this.timestamp = timestamp; }
	
	public LogType getLogType() { return logType; }
	public void setLogType(LogType logType) { this.logType = logType; }
	
	public String getMessage() { return message; }
	public void setMessage(String message) { this.message = message; }
}