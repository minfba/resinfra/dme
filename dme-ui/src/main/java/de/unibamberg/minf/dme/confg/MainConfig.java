package de.unibamberg.minf.dme.confg;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import de.unibamberg.minf.core.util.InitializingObjectMapper;
import de.unibamberg.minf.core.util.json.JsonNodeHelper;
import de.unibamberg.minf.core.web.service.ImageServiceImpl;
import de.unibamberg.minf.core.web.service.ImageServiceImpl.SizeBoundsType;
import de.unibamberg.minf.dme.confg.nested.ImagesConfigProperties;
import de.unibamberg.minf.dme.confg.nested.PathsConfigProperties;
import de.unibamberg.minf.dme.confg.nested.ThumbnailsConfigProperties;
import de.unibamberg.minf.dme.sessions.SessionCleanerService;
import lombok.Data;

@Data
@Configuration
@ConfigurationProperties
public class MainConfig {
	private String contextPath = "/";
	private String baseUrl = "http://localhost:8080/";
	
	@Autowired ResourceLoader resourceLoader;
		
	protected PathsConfigProperties paths;
	protected ImagesConfigProperties images;
		
	@PostConstruct
    public void completeConfiguration() throws IOException {
		if (paths.getConfig()==null) {
			paths.setConfig(this.setupPath(paths.getMain(), "config"));
		}
		if (paths.getGrammars()==null) {
			paths.setGrammars(this.setupPath(paths.getMain(), "grammars"));
		}
		if (paths.getModels()==null) {
			paths.setModels(this.setupPath(paths.getMain(), "models"));
		}
		if (paths.getTemporary()==null) {
			paths.setTemporary(this.setupPath(paths.getMain(), "temp"));
		}
		if (paths.getSessionData()==null) {
			paths.setSessionData(this.setupPath(paths.getMain(), "sessionData"));
		}
		if (paths.getTmpUpload()==null) {
			paths.setTmpUpload(this.setupPath(paths.getMain(), "tmpUpload"));
		}
		if (paths.getSampleFiles()==null) {
			paths.setSampleFiles(this.setupPath(paths.getMain(), "sampleFiles"));
		}
		if (paths.getParseErrors()==null) {
			paths.setParseErrors(this.setupPath(paths.getMain(), "parseErrors"));
		}
		if (paths.getDownload()==null) {
			paths.setDownload(this.setupPath(paths.getMain(), "download"));
		}
		if (paths.getBackup()==null) {
			paths.setBackup(this.setupPath(paths.getMain(), "backup"));
		}
		
		
		if (images==null) {
			images = new ImagesConfigProperties();
		} 
		if (images.getThumbnails()==null) {
			images.setThumbnails(new ThumbnailsConfigProperties());
		}
    }
	
	protected String setupPath(String first, String... more) throws IOException {
		Path p = Paths.get(first, more);
		if (!Files.exists(p)) {
			Files.createDirectories(p);
		}
		return p.toString();
	}
	
	@Autowired
	public void setMapKeyDotReplacement(MappingMongoConverter mongoConverter) {
		mongoConverter.setMapKeyDotReplacement("~");
	}
	
	@Bean
	@Primary // alternative bean, exportObjectMapper in ProcessingConfig
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JodaModule());
		
		return objectMapper;
	}
	
	@Bean
	public JsonNodeHelper jsonNodeHelper(ObjectMapper objectMapper) {
		JsonNodeHelper jsonNodeHelper = new JsonNodeHelper();
		jsonNodeHelper.setObjMapper(objectMapper);
		return jsonNodeHelper;
	}	

	@Bean
	public SessionCleanerService sessionCleanerService() {
		SessionCleanerService sessionCleanerService = new SessionCleanerService();
		sessionCleanerService.setDefaultExpirationMins(525600);	// 365 days
		sessionCleanerService.setIntervalMins(2880); 			// every two days
		
		return sessionCleanerService;
	}
	
	@Bean
	public ImageServiceImpl imageService() {
		ImageServiceImpl imageServiceImpl = new ImageServiceImpl();
		imageServiceImpl.setImagePath(paths.getDownload());
		imageServiceImpl.setImagesSizeType(SizeBoundsType.MAX_SHORTEST_SIDE);
		imageServiceImpl.setImagesWidth(images.getWidth());
		imageServiceImpl.setImagesHeight(images.getHeight());
		imageServiceImpl.setThumbnailsSizeType(SizeBoundsType.MAX_SHORTEST_SIDE);
		imageServiceImpl.setThumbnailsWidth(images.getThumbnails().getWidth());
		imageServiceImpl.setThumbnailsHeight(images.getThumbnails().getHeight());
		
		return imageServiceImpl;
	}
}
