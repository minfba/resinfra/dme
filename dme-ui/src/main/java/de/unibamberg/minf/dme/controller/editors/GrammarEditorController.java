package de.unibamberg.minf.dme.controller.editors;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.antlr.v4.tool.ANTLRMessage;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.unibamberg.minf.dme.controller.base.BaseFunctionController;
import de.unibamberg.minf.dme.model.PersistedSession;
import de.unibamberg.minf.dme.model.base.Grammar;
import de.unibamberg.minf.dme.model.base.Identifiable;
import de.unibamberg.minf.dme.model.function.FunctionImpl;
import de.unibamberg.minf.dme.model.grammar.AuxiliaryFile;
import de.unibamberg.minf.dme.model.grammar.GrammarContainer;
import de.unibamberg.minf.dme.model.grammar.GrammarImpl;
import de.unibamberg.minf.dme.service.interfaces.FunctionService;
import de.unibamberg.minf.dme.service.interfaces.GrammarService;
import de.unibamberg.minf.gtf.MainEngine;
import de.unibamberg.minf.gtf.compilation.GrammarCompilationResult;
import eu.dariah.de.dariahsp.web.model.AuthPojo;
import de.unibamberg.minf.core.web.pojo.ModelActionPojo;
import de.unibamberg.minf.core.web.service.ImageServiceImpl.ImageTypes;

@Controller
@RequestMapping(value={"/model/editor/{entityId}/grammar/{grammarId}", "/mapping/editor/{entityId}/grammar/{grammarId}"})
public class GrammarEditorController extends BaseFunctionController {
	
	@Autowired private GrammarService grammarService;
	@Autowired private FunctionService functionService;
	
	@Autowired private MainEngine mainEngine;
	
		
	public GrammarEditorController() {
		super("schemaEditor");
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.GET, value = "/async/disable")
	public @ResponseBody ModelActionPojo disableElement(@PathVariable String entityId, @PathVariable String grammarId, @RequestParam boolean disabled, HttpServletRequest request, HttpServletResponse response) {
		if (!schemaService.getUserCanWriteEntity(entityId, authInfoHelper.getAuth().getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		
		GrammarImpl g = (GrammarImpl)grammarService.findById(grammarId);
		g.setDisabled(disabled);
		
		grammarService.saveGrammar(g, authInfoHelper.getAuth());
		return new ModelActionPojo(true);
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.GET, value = "/async/remove")
	public @ResponseBody Grammar removeElement(@PathVariable String entityId, @PathVariable String grammarId, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!schemaService.getUserCanWriteEntity(entityId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		return grammarService.deleteGrammarById(entityId, grammarId, authInfoHelper.getAuth());
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.GET, value = "/form/new_function")
	public String getNewGrammarForm(@PathVariable String entityId, @PathVariable String grammarId, Model model, Locale locale, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!schemaService.getUserCanWriteEntity(entityId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		model.addAttribute("function", new FunctionImpl(entityId, null));
		model.addAttribute("actionPath", "/model/editor/" + entityId + "/grammar/" + grammarId + "/async/saveNewFunction");
		return "functionEditor/form/new";
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/async/saveNewFunction")
	public @ResponseBody ModelActionPojo saveNewGrammar(@PathVariable String entityId, @PathVariable String grammarId, @Valid FunctionImpl function, BindingResult bindingResult, Locale locale, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!schemaService.getUserCanWriteEntity(entityId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		ModelActionPojo result = this.getActionResult(bindingResult, locale);
		if (result.isSuccess()) {
			functionService.createAndAppendFunction(entityId, grammarId, function.getName(), authInfoHelper.getAuth());
		}
		return result;
	}
	
	@PreAuthorize("isAuthenticated()")
	@GetMapping("/includes/inclAuxFileTab")
	public String getInclAuxFileTab(Model model, Locale locale, @RequestParam Integer index) {
		AuxiliaryFile f = new AuxiliaryFile();
		f.setFileName(messageSource.getMessage("~de.unibamberg.minf.dme.editor.grammar.aux_file.new_file", null, locale));
		
		model.addAttribute("currIndex", index);
		model.addAttribute("currFile", f);
		return "grammarEditor/incl/auxfile_tab";
	}
	
	@PreAuthorize("isAuthenticated()")
	@GetMapping("/includes/inclAuxFilePane")
	public String getInclAuxFilePane(Model model, @RequestParam Integer index) {
		GrammarContainer gc = new GrammarContainer();
		AuxiliaryFile f = new AuxiliaryFile();
		model.addAttribute("currIndex", index);
		model.addAttribute("currFile", f);
		model.addAttribute("grammarContainer", gc);
		model.addAttribute("grammarContainer.auxiliaryFiles[" + index + "]", f);
		model.addAttribute("auxiliaryFileTypes", AuxiliaryFile.FileTypes.values());
		return "grammarEditor/incl/auxfile_pane";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/get")
	public @ResponseBody Grammar getElement(@PathVariable String entityId, @PathVariable String grammarId) {
		return grammarService.findById(grammarId);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/form/editWdata")
	public String getEditFormWithData(@PathVariable String entityId, @PathVariable String grammarId, @RequestParam String sample, HttpServletRequest request, HttpServletResponse response, Model model, Locale locale) {
		AuthPojo auth = authInfoHelper.getAuth();
		Identifiable entity = this.getEntity(entityId);
		
		PersistedSession s = sessionService.get(entityId, auth.getSessionId(), auth.getUserId());
		if (s==null) {
			response.setStatus(HttpServletResponse.SC_RESET_CONTENT);
			return null;
		}
				
		GrammarImpl g;
		if (grammarId.equals("undefined")) {
			g = new GrammarImpl(entityId, "");
		} else {
			g = (GrammarImpl)grammarService.findById(grammarId);
		}
		if (g.getGrammarContainer()==null) {
			g.setGrammarContainer(new GrammarContainer());
		}

		if (sample==null) {
			model.addAttribute("elementSample", this.getSampleInputValue(entity, grammarId, request.getSession().getId(), auth.getUserId(), response));
		} else {
			model.addAttribute("elementSample", sample);
		}

		model.addAttribute("grammar", g);	
		model.addAttribute("auxiliaryFileTypes", AuxiliaryFile.FileTypes.values());
		model.addAttribute("readonly", this.getIsReadOnly(entity, auth.getUserId()));
		model.addAttribute("actionPath", "/model/editor/" + entityId + "/grammar/" + grammarId + "/async/save");
		return "grammarEditor/form/edit";
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/form/edit")
	public String getEditForm(@PathVariable String entityId, @PathVariable String grammarId, HttpServletRequest request, HttpServletResponse response, Model model, Locale locale) {
		return this.getEditFormWithData(entityId, grammarId, null, request, response, model, locale);
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.POST, value = "/async/save")
	public @ResponseBody ModelActionPojo saveGrammar(@PathVariable String entityId, @Valid GrammarImpl grammar, BindingResult bindingResult, 
			@RequestParam(value="lexer-parser-options", defaultValue="combined") String lexerParserOption, Locale locale, HttpServletRequest request, HttpServletResponse response) {
		
		AuthPojo auth = authInfoHelper.getAuth();
		if(!schemaService.getUserCanWriteEntity(entityId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		ModelActionPojo result = this.getActionResult(bindingResult, locale);
		if (!result.isSuccess()) {
			return result;
		}
		if (grammar.getId().isEmpty()) {
			grammar.setId(null);
		}
		
		GrammarImpl gSave = null;
		if (grammar.getId()!=null) {
			gSave = (GrammarImpl)grammarService.findById(grammar.getId());
			if (gSave!=null) {
				gSave.setBaseMethod(grammar.getBaseMethod());
				gSave.setError(grammar.isError());
				gSave.setName(grammar.getName());
				gSave.setPassthrough(grammar.isPassthrough());
				gSave.setEntityId(grammar.getEntityId());
				gSave.setTemporary(grammar.isTemporary());
				gSave.setGrammarContainer(grammar.getGrammarContainer());
			}
		}
		if (gSave==null) {
			gSave = grammar;
		}

		Grammar gTmp = this.getTemporaryGrammar(gSave.getId(), auth.getUserId());
		grammarService.clearGrammar(gTmp);
		
		grammarService.clearGrammar(gSave);
		grammarService.saveGrammar(gSave, auth);
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/processGrammarDialog")
	public String getProcessGrammarDialog(@PathVariable String grammarId, Model model, Locale locale) {
		GrammarImpl g = (GrammarImpl)grammarService.findById(grammarId);
		if (g.getGrammarContainer()==null) {
			g.setGrammarContainer(new GrammarContainer());
		}
		model.addAttribute("grammar", g);		
		return "grammarEditor/form/process";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/help/editGrammar")
	public String showHelpEditGrammar(Model model, Locale locale) {	
		return "schemaEditor/help/grammar/editGrammar";
	}
		
	@RequestMapping(method = RequestMethod.POST, value = "/async/upload")
	public @ResponseBody ModelActionPojo uploadGrammar(@PathVariable String grammarId, @RequestParam boolean combined, @RequestBody GrammarContainer gc, HttpServletRequest request, Locale locale) {
		ModelActionPojo result = new ModelActionPojo(false);
		if (gc.getParserGrammar()==null || gc.getParserGrammar().trim().isEmpty()) {			
			result.addFieldError("grammarContainer_parserGrammar", messageSource.getMessage("~de.unibamberg.minf.dme.model.grammar.validation.parser_grammar_empty", null, locale));
		}
		if (!combined && (gc.getLexerGrammar()==null || gc.getLexerGrammar().trim().isEmpty())) {
			result.addFieldError("grammarContainer_lexerGrammar", messageSource.getMessage("~de.unibamberg.minf.dme.model.grammar.validation.lexer_grammar_empty", null, locale));
		}
		
		if (result.getErrorCount()==0) {
			try {
				Grammar g = getTemporaryGrammar(grammarId, authInfoHelper.getUserId());
				grammarService.clearGrammar(g);
				
				GrammarCompilationResult parseResult = grammarService.saveTemporaryGrammar(g, gc);
				result.setPojo(parseResult.getResultingFiles());
				result.setSuccess(true);
			} catch (IOException e) {
				result.addObjectError("Failed to upload grammar: " + e.getClass().getName());
			}
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/parse")
	public @ResponseBody ModelActionPojo parseGrammar(@PathVariable String grammarId, HttpServletRequest request) {
		ModelActionPojo result = new ModelActionPojo(false);
		Grammar g = null;
		GrammarCompilationResult parseResult;
		try {
			g = getTemporaryGrammar(grammarId, authInfoHelper.getUserId());
			parseResult = grammarService.parseTemporaryGrammar(g);
			result.setPojo(parseResult.getResultingFiles());
			result.setSuccess(true);
			
			for (String m : parseResult.getErrors()) {
				result.setSuccess(false);
				result.addFieldError("error", m);
			}
			for (String m : parseResult.getWarnings()) {
				result.addFieldError("warning", m);
			}
			for (String m : parseResult.getInfos()) {
				result.addFieldError("info", m);
			}
			
		} catch (Exception e) {
			result.addObjectError("Unspecified error while parsing grammar: " + e.getClass().getName());
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/compile")
	public @ResponseBody ModelActionPojo validateGrammar(@PathVariable String grammarId, HttpServletRequest request) {
		ModelActionPojo result = new ModelActionPojo(false);
		Grammar g = null;
		GrammarCompilationResult parseResult;
		try {
			g = getTemporaryGrammar(grammarId, authInfoHelper.getUserId());
			parseResult = grammarService.compileTemporaryGrammar(g);
			result.setPojo(parseResult.getResultingFiles());
			result.setSuccess(true);
			
			for (String m : parseResult.getErrors()) {
				result.setSuccess(false);
				result.addFieldError("error", m);
			}
			for (String m : parseResult.getWarnings()) {
				result.addFieldError("warning", m);
			}
			for (String m : parseResult.getInfos()) {
				result.addFieldError("info", m);
			}
		} catch (Exception e) {
			result.addObjectError("Unspecified error while compiling grammar: " + e.getClass().getName());
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/sandbox")
	public @ResponseBody ModelActionPojo sandboxGrammar(@PathVariable String grammarId, @RequestParam String baseMethod, HttpServletRequest request, Locale locale) {
		ModelActionPojo result = new ModelActionPojo(false);
		try {
			if (baseMethod==null || baseMethod.trim().isEmpty()) {
				result.setSuccess(true);				
			} else {
				Grammar g = getTemporaryGrammar(grammarId, authInfoHelper.getUserId());
				List<String> parserRules = grammarService.getParserRules(g);
				if (parserRules.contains(baseMethod.trim())) {
					result.setSuccess(true);
				} else {
					result.addFieldError("base_method", messageSource.getMessage("~de.unibamberg.minf.dme.model.grammar.validation.base_rule_not_found", null, locale));
				}
			}
			
			
		} catch (Exception e) {
			result.addObjectError("Unspecified error while compiling grammar: " + e.getClass().getName());
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/async/parseSample")
	public @ResponseBody ModelActionPojo parseSampleInput(@PathVariable String grammarId, @RequestParam String initRule, @RequestParam String sample, @RequestParam(defaultValue="true") Boolean temporary, HttpServletRequest request, Locale locale) {
		ModelActionPojo result = new ModelActionPojo(false);
		try {
			Grammar g;
			
			if (Boolean.TRUE.equals(temporary)) {
				g = getTemporaryGrammar(grammarId, authInfoHelper.getUserId());
			} else {
				g = grammarService.findById(grammarId);
			}
			List<String> parserRules = grammarService.getParserRules(g);
			
			if (initRule==null || initRule.trim().isEmpty()) {
				g.setBaseMethod(parserRules.get(0));
			} else {
				g.setBaseMethod(initRule);
				if (!parserRules.contains(initRule.trim())) {
					result.addObjectError(messageSource.getMessage("~de.unibamberg.minf.dme.model.grammar.validation.base_rule_not_found", null, locale));
					return result;
				}
			}
			if (mainEngine.getDescriptionEngine().checkAndLoadGrammar(g)!=null) {
				result.setSuccess(true);
				result.setPojo(mainEngine.getDescriptionEngine().processDescriptionGrammarToSVG(sample, g, new HashMap<>()));
			} else {
				// Grammar not on server yet (new or error)
				result.addObjectWarning(messageSource.getMessage("~de.unibamberg.minf.dme.model.grammar.validation.no_grammar_found", null, locale));
			}
			
		} catch (Exception e) {
			logger.error("Transformation error", e);
		}
		return result;
	}
		
	@RequestMapping(method = RequestMethod.GET, value = "/move")
	public @ResponseBody ModelActionPojo parseSampleInput(@PathVariable String entityId, @PathVariable String grammarId, @RequestParam int delta, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!schemaService.getUserCanWriteEntity(entityId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		grammarService.moveReference(entityId, grammarId, GrammarImpl.class, delta, authInfoHelper.getAuth());
		
		return new ModelActionPojo(true);
	}
	
	private Grammar getTemporaryGrammar(String id, String userId) {
		Grammar g = new GrammarImpl();
		g.setTemporary(true);
		g.setId(id);
		g.setUserId(userId==null ? null : DigestUtils.md5Hex(userId));
		g.setName(g.getIdentifier());
		return g;
	}
}
