package de.unibamberg.minf.dme.pojo;

import de.unibamberg.minf.dme.model.base.Identifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class MappedConceptPojo extends RelatedConceptPojo implements Identifiable {
	private static final long serialVersionUID = -5462724852373673591L;
	
	private String functionId;
}