package de.unibamberg.minf.dme.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import de.unibamberg.minf.dme.exception.GenericScheregException;
import de.unibamberg.minf.dme.model.LogEntry;
import de.unibamberg.minf.dme.model.LogEntry.LogType;
import de.unibamberg.minf.dme.pojo.LogEntryPojo;
import de.unibamberg.minf.dme.pojo.converter.LogEntryPojoConverter;
import de.unibamberg.minf.dme.model.PersistedSession;
import de.unibamberg.minf.dme.service.interfaces.PersistedSessionService;
import eu.dariah.de.dariahsp.web.AuthInfoHelper;
import de.unibamberg.minf.core.web.pojo.ModelActionPojo;

@Controller
@RequestMapping(value="/sessions")
public class SessionsController {
	
	@Autowired protected AuthInfoHelper authInfoHelper;
	@Autowired private PersistedSessionService sessionService;
	@Autowired protected MessageSource messageSource;
	
	@Autowired private LogEntryPojoConverter logEntryConverter;
	
	@RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
	public ModelActionPojo getHome(HttpServletResponse response)  {
		return null;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/deleteSession")
	public @ResponseBody ModelActionPojo deleteSession(@RequestParam String entityId, HttpServletRequest request, HttpServletResponse response, Locale locale) {
		PersistedSession sCurrent = sessionService.get(entityId, request.getSession().getId(), authInfoHelper.getUserId());
		if (sCurrent==null) {
			response.setStatus(HttpServletResponse.SC_RESET_CONTENT);
			return null;
		}
		sessionService.deleteSession(entityId, request.getSession().getId(), authInfoHelper.getUserId());
		return new ModelActionPojo(true);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/newSession")
	public @ResponseBody ModelActionPojo newSession(@RequestParam String entityId, HttpServletRequest request, HttpServletResponse response, Locale locale) throws GenericScheregException {
		String userId = authInfoHelper.getUserId();
		PersistedSession sCurrent = sessionService.get(entityId, request.getSession().getId(), userId);
		if (sCurrent==null) {
			response.setStatus(HttpServletResponse.SC_RESET_CONTENT);
			return null;
		}
		// Unassign current session
		sCurrent.setHttpSessionId(null);
		sessionService.saveSession(sCurrent);
		
		// New session
		sessionService.createAndSaveSession(entityId, request.getSession().getId(), userId);
		return new ModelActionPojo(true);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/form/loadSession")
	public String getFormLoadSession(@RequestParam String entityId, Model model, HttpServletRequest request, HttpServletResponse response, Locale locale) {
		PersistedSession sCurrent = sessionService.get(entityId, request.getSession().getId(), authInfoHelper.getUserId());
		if (sCurrent==null) {
			response.setStatus(HttpServletResponse.SC_RESET_CONTENT);
			return null;
		}
		
		List<PersistedSession> userSessions = sessionService.findAllByUser(entityId, authInfoHelper.getUserId());
		List<PersistedSession> savedSessions = new ArrayList<PersistedSession>();
		List<PersistedSession> transientSessions = new ArrayList<PersistedSession>();
		
		for (PersistedSession s : userSessions) {
			if (s.isNotExpiring()) {
				savedSessions.add(s);
			} else {
				transientSessions.add(s);
			}
		}
		Collections.sort(savedSessions);
		Collections.reverse(savedSessions);
		
		Collections.sort(transientSessions);
		Collections.reverse(transientSessions);
		
		model.addAttribute("locale", locale);
		model.addAttribute("savedSessions", savedSessions);
		model.addAttribute("transientSessions", transientSessions);
		model.addAttribute("currentSessionId", sCurrent.getId());
		model.addAttribute("actionPath", "/sessions/async/loadSession");
		return "session/forms/load";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/form/saveSession")
	public String getFormSaveSession(@RequestParam String entityId, Model model, HttpServletRequest request, Locale locale, HttpServletResponse response) {
		PersistedSession s = sessionService.get(entityId, request.getSession().getId(), authInfoHelper.getUserId());
		if (s==null) {
			response.setStatus(HttpServletResponse.SC_RESET_CONTENT);
			return null;
		}
		
		model.addAttribute("session", s);
		model.addAttribute("actionPath", "/sessions/async/saveSession");
		return "session/forms/save";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/async/saveSession")
	public @ResponseBody ModelActionPojo saveSession(@Valid PersistedSession session, HttpServletRequest request, Locale locale, HttpServletResponse response) {
		PersistedSession saveSession = sessionService.get(session.getEntityId(), request.getSession().getId(), authInfoHelper.getUserId());
		if (saveSession!=null) {
			saveSession.setLabel(session.getLabel());
			saveSession.setNotExpiring(true);
			saveSession.addLogEntry(LogEntry.createEntry(LogType.INFO, "~de.unibamberg.minf.dme.editor.sample.log.session_persisted", new String[] { session.getLabel() }));
			
			sessionService.saveSession(saveSession);
			return new ModelActionPojo(true);
		}
		response.setStatus(HttpServletResponse.SC_RESET_CONTENT);
		return new ModelActionPojo(false);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/loadSession")
	public @ResponseBody ModelActionPojo loadSession(@RequestParam String sessionId, HttpServletRequest request, Locale locale, HttpServletResponse response) {
		PersistedSession s = sessionService.reassignPersistedSession(request.getSession().getId(), authInfoHelper.getUserId(), sessionId);
		if (s==null) {
			response.setStatus(HttpServletResponse.SC_RESET_CONTENT);
			return new ModelActionPojo(false);
		}
		return new ModelActionPojo(true);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/getLog")
	public @ResponseBody Collection<LogEntryPojo> getLog(@RequestParam String entityId, @RequestParam(defaultValue="10") Integer maxEntries, @RequestParam(required=false) Long tsMin, HttpServletRequest request, HttpServletResponse response, Locale locale) {
		PersistedSession session = sessionService.get(entityId, request.getSession().getId(), authInfoHelper.getUserId());
		if (session==null) {
			response.setStatus(HttpServletResponse.SC_RESET_CONTENT);
			return null;
		}
		List<LogEntry> log = session.getSessionLog();
		if (tsMin!=null && log.size()>0 && log.get(0).getTimestamp().getMillis()<=tsMin) {
			return new ArrayList<LogEntryPojo>();
		}
		
		if (log!=null && log.size()>0) {
			if (log.size() > maxEntries) {
				return logEntryConverter.convert(log.subList(0, maxEntries), locale);
			}
			return logEntryConverter.convert(log, locale);
		} else {
			return new ArrayList<LogEntryPojo>();
		}
	}
}
