package de.unibamberg.minf.dme.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import de.unibamberg.minf.dme.dao.interfaces.UserDao;
import eu.dariah.de.dariahsp.model.User;

@Service
public class UserServiceImpl implements UserService {
	@Autowired private UserDao userDetailsDao;
	
	public User loadUserByUsername(String domain, String username) throws UsernameNotFoundException {
		return userDetailsDao.findByUsername(domain, username);
	}

	public void saveUser(User persistedUser) {
		userDetailsDao.save(persistedUser);
	}

	@Override
	public User findById(String id) {
		return userDetailsDao.findById(id);
	}

	@Override
	public List<String> getUsernames(String query) {
		List<String> usernames = new ArrayList<String>();
		int limit = 10;
		
		this.addIfListSizeSmaller(usernames, userDetailsDao.findByPropertyValue("username", query), limit);
		
		// Startswith/case-insensitive
		Query q = new Query();
		q.addCriteria(Criteria.where("username").regex("/^" + query + "/i"));
		q.limit(limit);
		this.addIfListSizeSmaller(usernames, userDetailsDao.findByQuery(q), limit);
		
		if (usernames.size()>=limit) {
			return usernames;
		}
		
		// Like/case-insensitive
		q = new Query();
		q.addCriteria(Criteria.where("username").regex("/" + query + "/i"));
		q.limit(limit);
		this.addIfListSizeSmaller(usernames, userDetailsDao.findByQuery(q), limit);
		
		return usernames;
	}
	
	private void addIfListSizeSmaller(List<String> usernames, List<User> users, int limit) {
		for (User u : users) {
			if (usernames.size()<limit && !usernames.contains(u.getUsername())) {
				usernames.add(u.getUsername());
			}
			if (usernames.size()>=limit) {
				return;
			}
		}
	}
}
