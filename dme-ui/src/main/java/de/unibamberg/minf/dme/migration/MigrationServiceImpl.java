package de.unibamberg.minf.dme.migration;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.bson.Document;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.CollectionCallback;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import de.unibamberg.minf.dme.confg.MainConfig;
import de.unibamberg.minf.dme.dao.interfaces.VersionInfoDao;
import de.unibamberg.minf.dme.model.mapping.base.MappedConcept;
import de.unibamberg.minf.dme.model.version.VersionInfo;
import de.unibamberg.minf.dme.model.version.VersionInfoImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MigrationServiceImpl implements MigrationService {
	private static final String VERSION_HASH_PREFIX = "DME";
	
	@Autowired private MainConfig appConfig;
	@Autowired private MongoTemplate mongoTemplate;
	@Autowired private ObjectMapper objectMapper;
	
	
	
	@Autowired private VersionInfoDao versionDao;
	
	private final MessageDigest md;
	
	public MigrationServiceImpl() throws NoSuchAlgorithmException {
		md = MessageDigest.getInstance("MD5");
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		List<String> versions = new ArrayList<>();
		List<VersionInfo> versionInfos = versionDao.findAll();
		for (VersionInfo vi : versionInfos) {
			if (!vi.getVersionHash().equals(new String(md.digest(new String(VERSION_HASH_PREFIX + vi.getVersion()).getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8))) {
				log.error("Cancelling migration checks: failed to compare version hashes. Is the correct database configured?");
				return;
			}
			versions.add(vi.getVersion());
		}
		this.performMigrations(versions);
	}
	
	private void performMigrations(List<String> existingVersions) throws Exception {
		boolean backedUp = false;
		
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "4.4", this::migrateToReferenceId);
		backedUp = backedUp | this.migrate(existingVersions, !backedUp, "4.5", this::migrateToRelatedConcepts);
		
		log.info("Backup performed: {}", backedUp);
	}
		
	private boolean migrate(List<String> existingVersions, boolean backup, String version, MigrationAction migration) throws Exception {
		if ((version.equals("1.0")&&existingVersions.isEmpty()) || (!version.equals("1.0") && !existingVersions.contains(version))) {
			log.info("Migrating to version [{}]", version);	
			if (backup) {
				this.backupDb();
			}
			boolean success = migration.migrate();
			this.saveVersionInfo(version, success);
			log.info("Migration to version [{}] performed {}", version, success ? "sucessfully" : "WITH ERRORS");		
			return true;
		}
		return false;
	}
	
	private boolean migrateToReferenceId() {
		ObjectNode objectNode;
		for (String rawReference : this.getObjectsAsString("reference")) {
			try {
 				objectNode = (ObjectNode)objectMapper.readTree(rawReference);
 				this.migrateReferenceNode(objectNode, true);
 				mongoTemplate.save(objectNode.toString(), "reference");
			} catch (Exception e) {
				log.error("Failed to update database", e);
				return false;
			}
		}
		return true;  
	}
		
	private void migrateReferenceNode(ObjectNode referenceNode, boolean preserveOid) {
		if (referenceNode.get("_id")!=null && !referenceNode.get("_id").isMissingNode() && 
				referenceNode.get("_id").get("$oid")!=null && !referenceNode.get("_id").get("$oid").isMissingNode()) {
			String referenceId = referenceNode.get("_id").get("$oid").asText();
			referenceNode.set("referenceId", TextNode.valueOf(referenceId));
			if (!preserveOid) {
				referenceNode.remove("_id");
			}
		}
		
		Iterator<Entry<String, JsonNode>> fieldIterator;
		Entry<String, JsonNode> field;
		if (referenceNode.get("childReferences")!=null && !referenceNode.get("childReferences").isMissingNode()) {
			fieldIterator = referenceNode.get("childReferences").fields();
			while (fieldIterator.hasNext()) {
				field = fieldIterator.next();
				if (field.getValue().isArray()) {
					for (JsonNode childReferenceNode : field.getValue()) {
						if (childReferenceNode.isObject()) {
							this.migrateReferenceNode(ObjectNode.class.cast(childReferenceNode), false);
						}
					}
				}
				
			}
			
			
		}
	}
	
	private boolean migrateToRelatedConcepts() {
		final String sourceCollection = "mappedConcept";
		final String targetCollection = "relatedConcept";
		
		if (!mongoTemplate.collectionExists(sourceCollection)) {
			return true;
		}
		try {
			List<MappedConcept> mappedConcepts = mongoTemplate.findAll(MappedConcept.class, sourceCollection);
			for (MappedConcept mappedConcept : mappedConcepts) {
				mongoTemplate.save(mappedConcept, targetCollection);
			}
			mongoTemplate.dropCollection(sourceCollection);
			
			return true;
			//return migrateChildReferenceKeys();
		} catch (Exception e) {
			log.error("Failed to migrate mapped concepts to new relatedConcepts collection", e);
			return false;
		}
	}
	
	private boolean migrateChildReferenceKeys() {
		ObjectNode objectNode;
		for (String rawReference : this.getObjectsAsString("reference")) {
			try {
 				objectNode = (ObjectNode)objectMapper.readTree(rawReference);
 				this.migrateChildReferenceKeys(objectNode,
 						"de~unibamberg~minf~dme~model~mapping~MappedConceptImpl",
 						"de~unibamberg~minf~dme~model~mapping~base~BaseRelatedConcept");
 				mongoTemplate.save(objectNode.toString(), "reference");
			} catch (Exception e) {
				log.error("Failed to update database", e);
				return false;
			}
		}
		return true;  
	}
		
	private void migrateChildReferenceKeys(ObjectNode referenceNode, String formerKey, String replaceKey) {

		Iterator<Entry<String, JsonNode>> fieldIterator;
		Entry<String, JsonNode> field;
		ObjectNode childReferencesNode;
		
		JsonNode childReference;
		
		if (referenceNode.get("childReferences")!=null && !referenceNode.get("childReferences").isMissingNode()) {
			childReferencesNode = (ObjectNode)referenceNode.get("childReferences"); 
			childReference = null;
			
			fieldIterator = referenceNode.get("childReferences").fields();
			while (fieldIterator.hasNext()) {
				field = fieldIterator.next();				
				if (field.getValue().isArray()) {
					for (JsonNode childReferenceNode : field.getValue()) {
						if (childReferenceNode.isObject()) {
							this.migrateChildReferenceKeys(ObjectNode.class.cast(childReferenceNode), formerKey, replaceKey);
						}
					}
				}
				if (field.getKey().equals(formerKey)) {
					childReference = field.getValue();
				}
			}
			if (childReference != null) {
				childReferencesNode.set(replaceKey, childReference);
				childReferencesNode.remove(formerKey);
			}
		}
	}
	
	private void backupDb() throws Exception {
		String backupPath = appConfig.getPaths().getBackup() + File.separator + DateTime.now().toString(DateTimeFormat.forPattern("yyyyMMdd_HHmmss"));
		Files.createDirectories(Paths.get(new File(backupPath).toURI()), new FileAttribute<?>[0]);
		
		try {
			Runtime.getRuntime().exec(String.format("mongodump --out %s --db %s", backupPath, mongoTemplate.getDb().getName()));
			log.info("Backed up database {} to [{}]", mongoTemplate.getDb().getName(), backupPath);
		} catch (Exception e) {
			log.error("Failed to create mongodb backup", e);
			//throw e;
		}
	}
	
	private void saveVersionInfo(String version, boolean success) {
		VersionInfo vi = new VersionInfoImpl();
		vi.setUpdateWithErrors(!success);
		vi.setVersion(version);		
		vi.setVersionHash(new String(md.digest(new String(VERSION_HASH_PREFIX + vi.getVersion()).getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8));
		
		versionDao.save(vi);
	}
	
	private List<String> getObjectsAsString(String queryObject) {
		return mongoTemplate.execute(queryObject, new CollectionCallback<List<String>>() {
			public List<String> doInCollection(MongoCollection<Document> collection) {
				MongoCursor<Document> cursor = collection.find().cursor();
				List<String> result = new ArrayList<>();
				while (cursor.hasNext()) {
					result.add(cursor.next().toJson());
				}
				return result;
			}
		});
	}
}
