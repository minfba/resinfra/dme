package de.unibamberg.minf.dme.confg;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import eu.dariah.de.dariahsp.config.web.AuthInfoConfigurer;

@Configuration
@ConfigurationProperties(prefix = "auth")
@Import({AuthInfoConfigurer.class})
public class SecurityConfig extends eu.dariah.de.dariahsp.config.SecurityConfig {
	
}
