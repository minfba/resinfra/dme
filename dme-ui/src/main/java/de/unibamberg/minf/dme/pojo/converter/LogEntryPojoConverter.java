package de.unibamberg.minf.dme.pojo.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.core.web.localization.MessageSource;
import de.unibamberg.minf.dme.model.LogEntry;
import de.unibamberg.minf.dme.pojo.LogEntryPojo;

@Component
public class LogEntryPojoConverter {
	@Autowired private MessageSource messageSource;
	
	public List<LogEntryPojo> convert(List<LogEntry> entries, Locale locale) {
		if (entries==null) {
			return null;
		}
		List<LogEntryPojo> result = new ArrayList<LogEntryPojo>();
		for (LogEntry entry : entries) {
			result.add(this.convert(entry, locale));
		}
		return result;
	}
	
	public LogEntryPojo convert(LogEntry entry, Locale locale) {
		if (entry==null) {
			return null;
		}
		LogEntryPojo p = new LogEntryPojo();
		p.setLogType(entry.getLogType());
		p.setMessage(messageSource.getMessage(entry.getCode(), entry.getArgs(), locale));
		p.setTimestamp(entry.getTimestamp().toString("yyyy-MM-dd HH:mm:ss.SSS"));
		return p;
	}
	
}
