package de.unibamberg.minf.dme.controller.editors;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import de.unibamberg.minf.core.util.Stopwatch;
import de.unibamberg.minf.dme.controller.base.BaseMainEditorController;
import de.unibamberg.minf.dme.importer.MappingImportWorker;
import de.unibamberg.minf.dme.importer.mapping.MappingImporter;
import de.unibamberg.minf.dme.model.LogEntry;
import de.unibamberg.minf.dme.model.PersistedSession;
import de.unibamberg.minf.dme.model.RightsContainer;
import de.unibamberg.minf.dme.model.SessionExecutionContext;
import de.unibamberg.minf.dme.model.LogEntry.LogType;
import de.unibamberg.minf.dme.model.base.Element;
import de.unibamberg.minf.dme.model.base.Function;
import de.unibamberg.minf.dme.model.base.Grammar;
import de.unibamberg.minf.dme.model.datamodel.base.Datamodel;
import de.unibamberg.minf.dme.model.mapping.MappingImpl;
import de.unibamberg.minf.dme.model.mapping.base.MappedConcept;
import de.unibamberg.minf.dme.model.mapping.base.Mapping;
import de.unibamberg.minf.dme.model.mapping.base.RelatedConcept;
import de.unibamberg.minf.dme.model.reference.ReferenceHelper;
import de.unibamberg.minf.dme.model.serialization.MappingContainer;
import de.unibamberg.minf.dme.model.tracking.ChangeSet;
import de.unibamberg.minf.dme.pojo.AuthWrappedPojo;
import de.unibamberg.minf.dme.pojo.MappedConceptPojo;
import de.unibamberg.minf.dme.pojo.RelatedConceptPojo;
import de.unibamberg.minf.dme.pojo.converter.AuthWrappedPojoConverter;
import de.unibamberg.minf.dme.pojo.converter.RelatedConceptPojoConverter;
import de.unibamberg.minf.dme.service.base.BaseEntityService;
import de.unibamberg.minf.dme.service.interfaces.FunctionService;
import de.unibamberg.minf.dme.service.interfaces.GrammarService;
import de.unibamberg.minf.dme.service.interfaces.PersistedSessionService;
import de.unibamberg.minf.dme.service.interfaces.RelatedConceptService;
import de.unibamberg.minf.dme.service.interfaces.SchemaService;
import de.unibamberg.minf.mapping.model.MappingExecGroup;
import de.unibamberg.minf.mapping.service.MappingExecutionService;
import de.unibamberg.minf.processing.consumption.CollectingResourceConsumptionServiceImpl;
import de.unibamberg.minf.processing.exception.ProcessingConfigException;
import de.unibamberg.minf.processing.model.SerializableResource;
import de.unibamberg.minf.processing.model.SerializableRootResource;
import de.unibamberg.minf.processing.model.base.Resource;
import eu.dariah.de.dariahsp.web.model.AuthPojo;
import de.unibamberg.minf.core.web.pojo.MessagePojo;
import de.unibamberg.minf.core.web.pojo.ModelActionPojo;

@Controller
@RequestMapping(value="/mapping/editor/{entityId}/")
public class MappingEditorController extends BaseMainEditorController {
	@Autowired private MappingImportWorker importWorker;
	
	@Autowired private GrammarService grammarService;
	@Autowired private FunctionService functionService;
	@Autowired private RelatedConceptService relatedConceptService;
	@Autowired private AuthWrappedPojoConverter authPojoConverter;
	@Autowired private PersistedSessionService sessionService;
	

	@Override protected String getPrefix() { return "/mapping/editor/"; }
	@Override protected MappingImportWorker getImportWorker() { return this.importWorker; }
	@Override protected BaseEntityService getMainEntityService() { return this.mappingService; }
	
	
	public MappingEditorController() {
		super("mappingEditor");
	}
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public String getEditor(@PathVariable String entityId, Model model, HttpServletRequest request, Locale locale) {
		AuthPojo auth = authInfoHelper.getAuth();
		
		RightsContainer<Mapping> mapping = mappingService.findByIdAndAuth(entityId, auth);
		
		AuthWrappedPojo<Mapping> mappingPojo = authPojoConverter.convert(mapping, auth.getUserId()); 
		if (mapping==null) {
			return "redirect:/registry/";
		}
		
		model.addAttribute("mapping", mappingPojo);
		model.addAttribute("source", authPojoConverter.convert(schemaService.findByIdAndAuth(mappingPojo.getPojo().getSourceId(), auth), auth.getUserId()));
		model.addAttribute("target", authPojoConverter.convert(schemaService.findByIdAndAuth(mappingPojo.getPojo().getTargetId(), auth), auth.getUserId()));
		
		boolean oversized = false;
		try {
			PersistedSession s = sessionService.accessOrCreate(entityId, request.getSession().getId(), auth.getUserId());
			model.addAttribute("session", s);
			
			/*if (s.getSampleInput()!=null) {
				if (s.getSampleInput().getBytes().length>this.maxTravelSize) {*/
					oversized = true;
			/*	} else {
					model.addAttribute("sampleInput", s.getSampleInput());
				}
			}*/
			
		} catch (Exception e) {
			logger.error("Failed to load/initialize persisted session", e);
		}
		
		model.addAttribute("sampleInputOversize", oversized);
		
		return "mappingEditor";
	}

	@RequestMapping(method=GET, value={"/incl/editor"})
	public String getEditorIncl(@PathVariable String entityId, Model model, HttpServletRequest request) {
		AuthPojo auth = authInfoHelper.getAuth();
		RightsContainer<Mapping> mapping = mappingService.findByIdAndAuth(entityId, auth);
		AuthWrappedPojo<Mapping> mappingPojo = authPojoConverter.convert(mapping, auth.getUserId());
		
		model.addAttribute("mapping", mappingPojo);
		model.addAttribute("source", authPojoConverter.convert(schemaService.findByIdAndAuth(mappingPojo.getPojo().getSourceId(), auth), auth.getUserId()));
		model.addAttribute("target", authPojoConverter.convert(schemaService.findByIdAndAuth(mappingPojo.getPojo().getTargetId(), auth), auth.getUserId()));
		
		return "mappingEditor/incl/editor";
	}
	
	
	@RequestMapping(method=GET, value={"/incl/properties"})
	public String getEntityProperties(@PathVariable String entityId, Model model, HttpServletRequest request) {
		AuthPojo auth = authInfoHelper.getAuth();
		RightsContainer<Mapping> mapping = mappingService.findByIdAndAuth(entityId, auth);
		AuthWrappedPojo<Mapping> mappingPojo = authPojoConverter.convert(mapping, auth.getUserId());
		
		model.addAttribute("mapping", mappingPojo);
		model.addAttribute("source", authPojoConverter.convert(schemaService.findByIdAndAuth(mappingPojo.getPojo().getSourceId(), auth), auth.getUserId()));
		model.addAttribute("target", authPojoConverter.convert(schemaService.findByIdAndAuth(mappingPojo.getPojo().getTargetId(), auth), auth.getUserId()));
		
		return "mappingEditor/incl/properties";
	}
	
	@RequestMapping(value="/async/getConcept", method=RequestMethod.GET)
	public @ResponseBody RelatedConceptPojo getMappedConcepts(@PathVariable String entityId, @RequestParam String id, Model model) {		
		return RelatedConceptPojoConverter.convert(relatedConceptService.findById(id), RelatedConceptPojoConverter.convertFunctionsToIdFunctionMap(functionService.findByEntityId(entityId)));
	}
	
	@RequestMapping(value="/async/getConcepts", method=RequestMethod.GET)
	public @ResponseBody List<RelatedConceptPojo> getMappedConcepts(@PathVariable String entityId, Model model) {
		return RelatedConceptPojoConverter.convert(relatedConceptService.findAllByMappingId(entityId), functionService.findByEntityId(entityId));
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method=GET, value={"/forms/import"})
	public String getImportForm(@PathVariable String entityId, Model model, Locale locale, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!schemaService.getUserCanWriteEntity(entityId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return null;
		}
		model.addAttribute("actionPath", "/mapping/editor/" + entityId + "/async/import");
		model.addAttribute("mapping", mappingService.findMappingById(entityId));

		return "mapping/form/import";
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method=POST, value={"/async/import"}, produces = "application/json; charset=utf-8")
	public @ResponseBody ModelActionPojo importSchemaElements(@PathVariable String entityId, @RequestParam(value="file.id") String fileId, @RequestParam(defaultValue="false", value="keep-imported-ids") boolean keepImportedIds,			
			Locale locale, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!schemaService.getUserCanWriteEntity(entityId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		ModelActionPojo result = new ModelActionPojo();
		try {
			if (temporaryFilesMap.containsKey(fileId)) {
				
				importWorker.importMapping(temporaryFilesMap.remove(fileId), entityId, keepImportedIds, authInfoHelper.getAuth());
				result.setSuccess(true);
				return result;
			}
		} catch (Exception e) {
			MessagePojo msg = new MessagePojo("danger", 
					messageSource.getMessage("~de.unibamberg.minf.common.view.forms.file.generalerror.head", null, locale), 
					messageSource.getMessage("~de.unibamberg.minf.common.view.forms.file.generalerror.body", new Object[] {e.getLocalizedMessage()}, locale));
			result.setMessage(msg);
			logger.error("Failed to import mapping", e);
		}
		result.setSuccess(false);
		return result;
	}
	
	@Override
	protected ModelActionPojo validateImportedFile(String entityId, String fileId, String elementId, Locale locale) {
		ModelActionPojo result = new ModelActionPojo();
		MappingImporter importer = importWorker.getSupportingImporter(temporaryFilesMap.get(fileId));
				
		if (importer!=null) {
			result.setSuccess(true);
			MessagePojo msg = new MessagePojo("success", 
					messageSource.getMessage("~de.unibamberg.minf.common.view.forms.file.validationsucceeded.head", null, locale), 
					messageSource.getMessage("~de.unibamberg.minf.common.view.forms.file.validationsucceeded.body", null, locale));
			result.setMessage(msg);
			
			ObjectNode pojoNode = objectMapper.createObjectNode();
			pojoNode.set("keepIdsAllowed", BooleanNode.valueOf(importer.isKeepImportedIdsSupported()));
			pojoNode.set("importerMainType", TextNode.valueOf(importer.getMainImporterType()));
			pojoNode.set("importerSubtype", TextNode.valueOf(importer.getImporterSubtype()));
			
			result.setPojo(pojoNode);
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/executeSampleMapping")
	public @ResponseBody ModelActionPojo executeSampleMapping(@PathVariable String entityId, HttpServletRequest request, HttpServletResponse response, Locale locale) throws ProcessingConfigException {
		Stopwatch sw = new Stopwatch().start();
		Stopwatch swTotal = new Stopwatch().start();
		ModelActionPojo result = new ModelActionPojo(true);
		result.setPojo(0);
		
		PersistedSession session = sessionService.get(entityId, request.getSession().getId(), authInfoHelper.getUserId());
		if (session==null) {
			response.setStatus(HttpServletResponse.SC_RESET_CONTENT);
			return null;
		}

		if (this.debugConfig.isProcessing()) {
			logger.debug("Session for transformation loaded [{}] took {}ms", entityId, sw.getElapsedTime());
			sw.reset();
		}
		
		Mapping m = mappingService.findMappingById(entityId);
		List<SerializableRootResource> inputResources = sessionService.loadSampleOutput(session.getId());
		List<Resource> mappedResources = null;
		if (inputResources!=null && !inputResources.isEmpty()) {
			Element r = elementService.findRootBySchemaId(m.getTargetId(), true);
			List<RelatedConcept> concepts = relatedConceptService.findAllByMappingId(m.getId(), true);
					
			MappingExecutionService mappingExecService = appContext.getBean(MappingExecutionService.class);
			CollectingResourceConsumptionServiceImpl consumptionService = new CollectingResourceConsumptionServiceImpl();
			
			// TODO Cache mapping execution group -> in ScheReg
			SessionExecutionContext ctx = null;
			try {
				ctx = new SessionExecutionContext(this.mainConfig.getPaths().getSessionData(), session.getId(), sessionService.loadSessionData(session.getId()));
			} catch (Exception e) {
				logger.error("Failed to create session execution context", e);
			}
			
			Datamodel sourceModel = schemaService.findSchemaById(m.getSourceId());
			Element sourceRootElement = elementService.findRootBySchemaId(m.getSourceId(), true);
			
			MappingExecGroup mapExecGroup = new MappingExecGroup(m, ctx, r, entityId, sourceModel, sourceRootElement);
			
			if (this.debugConfig.isProcessing()) {
				logger.debug("Mapping loaded [{}] took {}ms", entityId, sw.getElapsedTime());
				sw.reset();
			}
			
			// TODO: Sources really needed?
			for (Grammar g : grammarService.findByEntityId(m.getId(), true)) {
				mapExecGroup.addGrammar(g);
			}
			
			for (RelatedConcept c : concepts) {
				if (MappedConcept.class.isAssignableFrom(c.getClass())) {
					MappedConcept mc = MappedConcept.class.cast(c);
					mapExecGroup.addRelatedConcept(mc, functionService.findById(mc.getFunctionId()));
				} else {
					mapExecGroup.addRelatedConcept(c, null);
				}
			}
	
			mappingExecService.init(mapExecGroup, inputResources);
			mappingExecService.addConsumptionService(consumptionService);
			
			if (this.debugConfig.isProcessing()) {
				logger.debug("Mapping initialized [{}] took {}ms", entityId, sw.getElapsedTime());
				sw.reset();
			}
			
			mappingExecService.run();
			
			if (this.debugConfig.isProcessing()) {
				logger.debug("Transformation to target datamodel [{}] took {}ms", entityId, sw.getElapsedTime());
			}
			
			mappedResources = consumptionService.getResources();
		} 
		
		sessionService.saveSampleMapped(session.getId(), mappedResources);

		int count = 0;		
		if (mappedResources!=null && !mappedResources.isEmpty()) {
			for (Resource r : mappedResources) {
				if (r!=null) {
					count++;
				}
			}
			result.setPojo(count);
			
			if (count==1) {
				session.addLogEntry(LogEntry.createEntry(LogType.SUCCESS, "~de.unibamberg.minf.dme.editor.sample.log.translated_1_result", new Object[]{sw.getElapsedTime(), swTotal.getElapsedTime()}));
			} else {
				session.addLogEntry(LogEntry.createEntry(LogType.SUCCESS, "~de.unibamberg.minf.dme.editor.sample.log.translated_n_results", new Object[]{sw.getElapsedTime(), swTotal.getElapsedTime(), count}));	
			}
		} else if (inputResources==null || inputResources.isEmpty()) {
			session.addLogEntry(LogEntry.createEntry(LogType.WARNING, "~de.unibamberg.minf.dme.editor.sample.log.translated_no_input", null));
		} else {
			session.addLogEntry(LogEntry.createEntry(LogType.WARNING, "~de.unibamberg.minf.dme.editor.sample.log.translated_no_results", null));
		}
		
		session.setSampleMappedCount(count);
		sessionService.saveSession(session);
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/async/export")
	public @ResponseBody ModelActionPojo exportMapping(@PathVariable String entityId, Model model, Locale locale, HttpServletRequest request, HttpServletResponse response) {
		AuthPojo auth = authInfoHelper.getAuth();
		if(!mappingService.getUserCanWriteEntity(entityId, auth.getUserId())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return new ModelActionPojo(false);
		}
		
		MappingImpl m = (MappingImpl)mappingService.findMappingById(entityId);
		m.setConcepts(relatedConceptService.findAllByMappingId(entityId, true));
		m.flush();
		
		MappingContainer mc = new MappingContainer();
		mc.setMapping(m);
		
		ChangeSet ch = schemaService.getLatestChangeSetForEntity(m.getId());
		if (ch!=null) {
			m.setVersionId(ch.getId());
		}

		List<Grammar> grammars = grammarService.getNonPassthroughGrammars(entityId);
		if (grammars!=null && !grammars.isEmpty()) {
			mc.setGrammars(new HashMap<>());
			for (Grammar g : grammars) {
				mc.getGrammars().put(g.getId(), g);
			}
		}
		
		Map<String, String> serializedFunctions = new HashMap<>();
		List<Function> functions = functionService.findByEntityId(entityId);
		for (Function f : functions) {
			serializedFunctions.put(f.getId(), f.getFunction());
		}
		mc.setFunctions(serializedFunctions);
		
		Map<String, String> elementIdPathMap = new HashMap<>();
		List<String> elementIds = m.getConcepts().stream()
			.flatMap(c -> Stream.concat(c.getElementGrammarIdsMap().keySet().stream(), c.getTargetElementIds().stream()))
			.collect(Collectors.toList());
		
		this.fillElementIdPathMap(elementIdPathMap, elementIds, new ArrayList<>(), elementService.findRootBySchemaId(m.getSourceId(), true), "");
		this.fillElementIdPathMap(elementIdPathMap, elementIds, new ArrayList<>(), elementService.findRootBySchemaId(m.getTargetId(), true), "");
		mc.setElementPaths(elementIdPathMap);
		
		
		ModelActionPojo result = new ModelActionPojo(true);
		result.setPojo(mc);
		return result;
	}
	
	private void fillElementIdPathMap(Map<String, String> elementIdPathMap, List<String> elementIds, List<String> scannedIds, Element e, String path) {
		// Prevent infinite recursion in graph models
		if (scannedIds.contains(e.getId())) {
			return;
		}
		scannedIds.add(e.getId());
		final String subpath = path.isEmpty() ? e.getName() : (path + "." + e.getName());
		if (elementIds.contains(e.getId()) && !elementIdPathMap.containsKey(e.getId())) {
			elementIdPathMap.put(e.getId(), subpath);
		}
		e.getAllChildElements().stream().forEach(c -> this.fillElementIdPathMap(elementIdPathMap, elementIds, scannedIds, c, subpath));
	}
	
}