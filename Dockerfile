FROM eclipse-temurin:17-jdk
ADD https://fastdl.mongodb.org/tools/db/mongodb-database-tools-debian10-x86_64-100.9.4.deb /tmp
RUN dpkg -i /tmp/mongodb-database-tools-debian10-x86_64-100.9.4.deb
ARG VERSION
ARG WAR_FILE=dme-ui/build/libs/dme-ui-${VERSION}.war
COPY ${WAR_FILE} app.war
EXPOSE 8080
ENTRYPOINT ["/app.war","-Djava.security.egd=file:/dev/./urandom","-Dspring.config.additional-location=optional:file:/config/application.yml","-XX:+HeapDumpOnOutOfMemoryError"]
