.. DARIAH-DE Data Modeling Environment (DME) documentation master file

DARIAH-DE Data Modeling Environment (DME)
=========================================

Das :term:`DARIAH-DE Data Modeling Environment (DME)<DME>` ist Nachfolger der Schema und Crosswalk Registry und löst diese als Komponente der :term:`DARIAH-DE Datenföderationsarchitektur (DFA)<DFA>` [1]_ ab. Die :term:`DME` ist ein umfangreiches Werkzeug zur Modellierung und Integration von Forschungsdaten. Ursprünglich als einfache Registrierungs- und Verwaltungskomponente geplant, zeichnete sich bereits im Verlauf der ersten Förderphase von DARIAH-DE hat ab, dass ein derartig einfaches Konzept für eine forschungsnahe Unterstützung der Beschreibung von Datenstrukturen und deren Zusammenhänge nicht ausreicht. Gemeinsam mit der :term:`DARIAH-DE Collection Registry (CR)<CR>` [2]_ bildet das :term:`DME` eine wesentliche Komponente für die Verarbeitung und Darstellung von Forschungsdaten in der :term:`Generische Suchen (GS)<GS>` [3]_. 

Anders als die weiteren Komponenten der :term:`DFA` ist das :term:`DME` als Expertenwerkzeug konzipiert und unterstützt die tiefgreifende Beschreibung der Erstellungs- und Verwendungskontexte von Forschungsdaten. Basierend auf dem Konzept domänenspezifischer Sprachen wurden neuartige Methoden zur Spezifikation von Daten und deren Transformation entwickelt, die zwischenzeitlich erprobt und publiziert werden konnten (z. B. [Gr16b]_, [Gr16c]_). Eine primäre Zielsetzung bei der Weiterentwicklung des :term:`DME` besteht in dessen Verbesserung hinsichtlich der Bedienbarkeit und Zugänglichkeit durch Forschende und Sammlungsinhaber. 

Wichtige Links
--------------

 * Produktivsystem: https://dme.de.dariah.eu
 * Testinstanz: https://dfatest.de.dariah.eu/dme
 * Dokumentation: https://dfa.de.dariah.eu/doc/dme
 * Source Code: https://gitlab.rz.uni-bamberg.de/dariah/dme

.. toctree::
   :maxdepth: 1
   :caption: Inhalte

   userguide
   reference
   installation
   background
   glossary
   bibliography

.. homescreen:
.. figure:: ./pics/dme-homescreen-mapping.png


.. [1] Übersicht zur :term:`DFA`: https://dfa.de.dariah.eu/doc/dfa/
.. [2] Dokumentation der :term:`CR`: https://dfa.de.dariah.eu/doc/colreg/
.. [3] Dokumentation der :term:`GS`: https://dfa.de.dariah.eu/doc/search/
