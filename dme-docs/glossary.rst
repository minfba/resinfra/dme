Begriffe
========

.. glossary::

    CR
      DARIAH-DE Collection Registry

    DC
      Dublin Core

    DFA
      DARIAH-DE Forschungsdaten-Föderationsarchitektur, oder kürzer Datenföderationsarchitektur

    DME
      DARIAH-DE Data Modeling Environment (DME)

    GS
      DARIAH-DE Generische Suche

    MWW
      Forschungsverbund Marbach Weimar Wolfenbüttel

    OAI-PMH
      Open Archives Initiative Protocol for Metadata Harvesting