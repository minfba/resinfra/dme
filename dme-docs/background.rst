===========
Hintergrund
===========
Abbildung :numref:`dfa_architektur` skizziert wesentliche Bausteine und deren Beziehungen innerhalb der DFA. Die dargestellte Übersicht ist eine Adaption des ursprünglichen DFA Architekturmodells (Gradl et al., 2015) und führt die Komponente des DME als Weiterentwicklung der DARIAH-DE Schema Registry (SR) ein. 

.. dfa_architektur:
.. figure:: ./pics/dfa_architektur.png

Die Komponenten der DFA können anhand ihrer wesentlichen Funktionalität der Datenschicht, Föderationsschicht oder Diensteschicht zugeordnet werden.
In der Datenschicht werden im Kontext der DFA solche Komponenten zusammengefasst, die einen maschinellen Zugriff auf Daten über Schnittstellen erlauben und dadurch für Dienste übergeordneter Schichten zugreifbar sind. Angebotene Schnittstellen sind zumeist web- und XML-basiert. Das im Bereich digitaler Sammlungen häufig verwendete OAI-PMH [#oaipmh]_ oder über das Internet zugängliche XML-Dateien sind typische Zugriffsschnittstellen. DARIAH-DE bietet daneben eine eigene Repository-Lösung für die Publikation von Forschungsdaten an.

Die Föderationsschicht besteht aus den Komponenten der CR und des DME. Beide Dienste verwalten jeweils unterschiedliche Objektarten, ermöglichen jedoch in der Kombination der jeweils beinhalteten Informationen die Erstellung integrativer Sichten über heterogene Daten. Sammlungsbeschreibungen der CR umfassen neben unterschiedlichen weiteren Aspekten [#sammlungen]_ insbesondere auch Verweise auf die für eine Kollektion jeweils gültigen Datenmodelle im DME. Durch diese Verbindung werden Daten der beschriebenen Kollektion im Rahmen der DFA interpretierbar. Das DME verzeichnet neben einzelnen Datenmodellen auch – soweit modelliert – deren Zusammenhänge (Abbildungen, Mappings) und bildet so die Basis für eine Zusammenführung (Föderation) modellkonformer Daten.

Komponenten der Diensteschicht nutzen die Informationen untergeordneter Schichten und zielen im Wesentlichen darauf ab, einen Nutzen3 für fachwissenschaftliche Anwenderinnen und Anwender zu generieren. In Bezug auf die Idee der DFA bedeutet dies typischerweise die Bereitstellung heterogener Daten in Form harmonisierter Sichten. Die in der Abbildung hervorgehobene generische Suche nutzt die in Sammlungsbeschreibungen verzeichneten Schnittstellen für den Zugriff auf Kollektionen. Mit Hilfe der in der DME hinterlegten Datenmodelle können die bezogenen Daten verarbeitet, interpretiert und schließlich indexiert werden. Zum Anfragezeitpunkt erlauben Abbildungen zwischen den Datenmodellen eine Definition von Suchparametern anhand einzelner Datenmodelle, welche in äquivalente Konzepte anderer Datenmodelle übersetzt werden.



Technischer Hintergrund
-----------------------

Als weitere Perspektive auf die DFA stellt Abbildung :numref:`uml_architektur` die bislang implementierten und verwendeten Komponenten und Bibliotheken aus dem technischen Blickwinkel der Softwareentwicklung zusammen. Für eine verbesserte Abgrenzbarkeit sind die Komponenten unterschiedlich eingefärbt: Eigenständig lauffähige, von DARIAH-DE entwickelte Dienste sind blau, die in DARIAH-DE entwickelten Bibliotheken grün, sowie die verwendeten Fremdbibliotheken grau eingefärbt. Um das Diagramm nicht zu überladen werden nur die für das Verständnis der Funktionsweise relevanten Bibliotheken dargestellt. Alle Abhängigkeiten können in den jeweiligen Project Object Model (POM)[#pom]_ Dateien der Komponenten eingesehen werden. Für die CR [#gitlab_cr]_ und das DME [#gitlab_dme]_ sind diese direkt in GitHub verfügbar, alle weiteren Komponenten sind (auch im Quellcode) über ein dediziertes Artifactory Softwarerepository [#artifactory]_ öffentlich zugänglich.

.. uml_architektur:
.. figure:: ./pics/uml_architektur.png


* Sämtliche Dienste basieren auf Grundfunktionalität der core Bibliotheken: Das Metamodell für Datenmodelle, Mappings etc. findet sich in core-metamodel, core-util implementiert Hilfsklassen und core-web bietet einen gemeinsamen technischen Rahmen für die Web-Anwendungen auf Basis von Spring MVC [#spring_mvc]_, der Servlet- [#servlet_api]_ und JSP-API [#jsp_api]_.
* Das Grammatical Transformation Framework (gtf) implementiert die Methoden der Datendefinition und Datentransformation und damit die grundlegende Idee der forschungsorientierten Föderation (vgl. 4.2.1). gtf-base und gtf-core kapseln basale Modellklassen und Funktionalität. Weiterführende Transformationslogik wird im Rahmen von Erweiterungsbibliotheken implementiert. Die derzeitigen Erweiterungen und jeweils verwendete externe Bibliotheken sind in der Abbildung dargestellt.
* Das GTF ist generisch implementiert und auch für andere Szenarien einsetzbar. Die Bibliothek processing bildet einen Wrapper für die Funktionalität des GTF für die DARIAH-DE DFA.
* Das DME ist als Konfigurationsoberfläche für gtf und processing zu verstehen, nutzt beide Komponenten darüber hinaus auch selbst für deren Anwendung auf Beispieldaten (vgl. 4.3).
* Grundlegende Suchfunktionalität der Generischen Suche ist in search-commons zusammengefasst. Letzteres kapselt u. a. den Zugriff auf processing und elasticsearch.  Das Cosmotool basiert in Bezug auf die DFA auf der Generischen Suche und teilt deren diesbezügliche Eigenschaften.
* Sämtliche Dienste nutzen dariahsp [#dariahsp]_ als SAML Implementierung zur Anbindung an die DARIAH-DE AAI bzw. DFN AAI.

.. [#oaipmh] siehe DARIAH Collection Description Data Model (DCDDM), https://github.com/DARIAH-DE/DCDDM
.. [#sammlungen] Die Modellierung von Daten mit Hilfe des DME erzielt durch die intensive Auseinandersetzung mit den Kollektionen auch einen direkten Nutzen. Sie dient dennoch typischerweise nicht diesem Selbstzweck, sondern der Vorbereitung der Föderation durch weiterverarbeitende Dienst
.. [#pom] Konzept von Apache Maven: https://maven.apache.org/
.. [#gitlab_cr] https://gitlab.com/DARIAH-DE/colreg
.. [#gitlab_dme] https://gitlab.rz.uni-bamberg.de/dariah/dme 
.. [#artifactory] Einstieg über https://minfba.de.dariah.eu/artifactory, für z. B. core-metamodel: https://minfba.de.dariah.eu/artifactory/webapp/#/artifacts/browse/tree/General/dariah-minfba-snapshots/de/unibamberg/minf/core/core-metamodel 
.. [#spring_mvc] https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html 
.. [#servlet_api] https://github.com/javaee/javaee-jsp-api 
.. [#jsp_api] https://github.com/javaee/servlet-spec 
.. [#dariahsp] https://gitlab.rz.uni-bamberg.de/dariah/dariahsp

