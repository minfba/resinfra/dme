=============
Bibliographie
=============
 .. _eigene_veröffentlichungen:
 
Eigene Veröffentlichungen
-------------------------
.. [Gr16a] Gradl, Tobias; Henrich, Andreas (2016): Die DARIAH-DE-Föderationsarchitektur – Datenintegration im Spannungsfeld forschungsspezifischer und domänenübergreifender Anforderungen. In: Bibliothek Forschung und Praxis 40 (2). DOI: 10.1515/bfp-2016-0027.

.. [Gr16b] Gradl, Tobias; Henrich, Andreas (2016): Data Integration for the Arts and Humanities: A Language Theoretical Concept. In: Fuhr, Norbert et al. (Hg.): Research and Advanced Technology for Digital Libraries: 20th International Conference on Theory and Practice of Digital Libraries, TPDL 2016, Hannover, Germany, September 5‐9, 2016, Proceedings. Cham: Springer International Publishing, S. 281–293

.. [Gr16c] Gradl, Tobias; Henrich, Andreas (2016): Extending Data Models by Declaratively Specifying Contextual Knowledge. In: Robert Sablatnig und Tamir Hassan (Hg.): DocEng '16 Proceedings of the 2016 ACM Symposium on Document Engineering. DocEng'16. Vienna, Austria, September 13-16, 2016. New York, NY, USA: ACM, S. 123–126.
