Installation
============

Aktuelle Prototypen aller DFA Komponenten und damit auch des Data Modeling Environments sind im Rahmen der DARIAH-DE Infrastruktur auf zwei primären Maschinen eingerichtet.

Produktivinstanz
----------------

Jeweils stabile Versionen werden durch eine virtuelle Maschine (https://dfa.de.dariah.eu) bereitgestellt.
* Die CR ist unter der URL https://colreg.de.dariah.eu verfügbar.
* Das DME steht unter https://dme.de.dariah.eu zur Verfügung. 

Testinstanz
-----------

Neben der stabilen Installation werden jeweilige Vorabversionen der DFA Komponenten auf einer weiteren, eigens eingerichteten virtuellen Maschine installiert. Diese ist unter der URL https://dfatest.de.dariah.eu erreichbar. 

.. _screenshot_dfatest:
.. figure:: ./pics/dfatest_screen.png

Die Produktiv- und Testinstallationen basieren auf einem Debian Repository [#debrep]_, welches eine besonders einfache Installation von Paketen unter Debian/Ubuntu ermöglicht. Die produktiven Dienste (DFA) werden durch Release-Versionen aktualisiert. Die Vorabversionen (DFAtest) basieren auf Snapshot-Versionen, dienen dem Test neuer Funktionalität und können auch Instabilitäten aufweisen. Sämtliche Daten der Testinstallationen sind losgelöst vom Produktivsystem und können jederzeit auch gelöscht werden. Weiterführende Hinweise finden sich auch in den Installationsanweisungen der Komponenten in den jeweiligen GitHub Repositories.

.. [#debrep] https://minfba.de.dariah.eu/rep

Eigene Installation
-------------------

Eine Anleitung für die Installation einer selbst gehosteten Instanz steht hier: https://gitlab.rz.uni-bamberg.de/dariah/dariahsp/blob/master/README.md zur Verfügung.
