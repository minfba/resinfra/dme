Befehlsreferenz
============================

An dieser Stelle entsteht eine Übersicht über die im Rahmen der Transformationssprache des GTF, und damit des DME, verfügbaren Kommandos. 
Aufgeteilt in jederzeit erweiterbare Teilbibliotheken werden Kommandos stets als namensraum.kommando(parameter1, ..., parameterN) aufgerufen. 

Derzeit stehen die Bibliotheken *core*, *dai*, *file*, *geo*, *nlp*, *vokabulary* und *wiki*. 

Die Entwicklung neuer Bibliotheken und Kommandos unterliegt keiner grundsätzlichen Systematik, sondern basiert primär auf Anforderungen aus
Anwendungsszenarien.

core
----

* length
* detectoffset
* padleft
* padright
* contains
* equals
* trim
* replace
* substring
* ifthenelse
* unicode
* unescape
* escape
* alignsubtrees
* exists
* nullorempty

dai
---

chronontology
+++++++++++++

* query
* get

gazetteer
+++++++++

* query
* get
* topcoord