# Data Modeling Environment (DME)

The Data Modeling Environment (DME) is a component of the *DARIAH-DE Data Federation Architecture (DFA)* software stack. The main intention of the DME is to enable domain experts in the arts and humanities to specify and correlate data structures in order to make data more accessible. The DME is a web-application and the frontend to the *Grammatical Transformation Framework (GTF)*, which will be published to GitHub in the near future. A particular focus lies on the explication of contextual knowledge in the form of rules to (1) define data in terms of domain specific languages and (2) provide transformative functions that operate on parsed instances of the data.

Further information on the concepts behind the DME are accessible at https://de.dariah.eu/dme.

## Installation

### Prerequisites

The Generic Search is a Java application currently has the following requirements:
* Java 11 (JDK for compiling grammars in the [GTF](https://gitlab.rz.uni-bamberg.de/gtf/gtf-framework/gtf))
* MongoDB

### APT package installation

#### 1. Add the repository key
````
wget -O - https://minfba.de.dariah.eu/minfba_public.asc | sudo apt-key add -
````

#### 2. Add the main and/or testing repository source and update

Main production packages:
````
echo "deb [arch=all] https://minfba.de.dariah.eu/nexus/repository/minfba-apt-releases/ any main" \
    | sudo tee /etc/apt/sources.list.d/minfba_repository.list
````

Testing packages:
````
echo "deb [arch=all] https://minfba.de.dariah.eu/nexus/repository/minfba-apt-testing/ any main" \
    | sudo tee /etc/apt/sources.list.d/minfba_repository.list
````

#### 3. Install the service

Update respository sources
````
sudo apt-get update
````

Install the package
````
sudo apt-get install dariah-dme-ui
````

Install as service and enable
````
sudo systemctl start dariah-dme-ui
sudo systemctl enable dariah-dme-ui
````

For rare cases that require the installation of two parallel instances of the service, a alternative version is built and deployed as `dariah-dme-ui-alt`, which can be installed without conflicting with the primary `dariah-dme-ui` installation

### Java package installation

Release and snapshot packages can be downloaded from the Maven package repositories.

* For **release** packages see: https://minfba.de.dariah.eu/nexus/#browse/browse:minfba-releases:de%2Funibamberg%2Fminf%2Fdme-ui
* For **snapshot** packages see: https://minfba.de.dariah.eu/nexus/#browse/browse:minfba-snapshots:de%2Funibamberg%2Fminf%2Fdme-ui
